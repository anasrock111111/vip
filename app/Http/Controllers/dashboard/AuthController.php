<?php

namespace App\Http\Controllers\dashboard;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    public function show(Request $request)
    {
        return view('dashboard.auth.login');
    }

    public function do_login(Request $request)
    {


        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {


            $user = User::find(Auth::user()->id);
            $type = $user->type;

            if ($type == "admin" || $type == "project_manager" || $type == "technical_supervisor") {


                return redirect('admin/home');
            }

            if ($type == "employee") {


                return redirect('employee/home');
            }

            if ($type == "user_collect") {


                return redirect('user_collect/home');
            }


        } else {
            return view('dashboard.auth.login', array('error_login' => ConstantController::$wrong_email_or_password[$_SESSION['lang']]));
        }
    }


    public function do_logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
