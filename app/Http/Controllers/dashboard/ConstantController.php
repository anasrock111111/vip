<?php

namespace App\Http\Controllers\dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConstantController extends Controller
{
    public static $constant = [
        'ar' => "الثوابت",
        'en' => "Constants",
    ];
    public static $marketing = [
        'ar' => "المسوقين",
        'en' => "Marketing",
    ];
    public static $TasksEditType = [
        'ar' => "انواع التعديل للمهام",
        'en' => "Tasks Edit types",
    ];
    public static $TasksType = [
        'ar' => "انواع المهام",
        'en' => "Tasks types",
    ];
    public static $tasks = [
        'ar' => "المهام",
        'en' => "Tasks",
    ];
    public static $files = [
        'ar' => "الملفات",
        'en' => "files",
    ];
    public static $orders = [
        'ar' => "الطلبات",
        'en' => "Orders",
    ];
    public static $spaces = [
        'ar' => "الفراغات",
        'en' => "Spaces ",
    ];
    public static $space_location = [
        'ar' => "مواقع الفراغ",
        'en' => "Spaces location",
    ];
    public static $space_type = [
        'ar' => "أنواع الفراغ",
        'en' => "Space type",
    ];
    public static $nature_project = [
        'ar' => "طبيعة المشاريع",
        'en' => "طبيعة المشاريع",
    ];
    public static $customers = [
        'ar' => "الزبائن",
        'en' => "Customers",
    ];
    public static $projects = [
        'ar' => "المشاريع",
        'en' => "Projects",
    ];
    public static $clice = [
        'ar' => "الكليشات",
        'en' => "Clices",
    ];
    public static $department = [
        'ar' => "اختصاصات الموظفين",
        'en' => "Departments employee",
    ];
    public static $employees = [
        'ar' => "الموظفين",
        'en' => "Employee",
    ];
    public static $deleted_successfully = [
        'ar' => "تم الحذف بنجاح",
        'en' => "Successfully deleted",
    ];
    public static $edit_successfully = [
        'ar' => "تم التعديل بنجاح",
        'en' => "Successfully edited",
    ];

    public static function importance($role)
    {
        $lang = $_SESSION['lang'];
        if ($role == "normal") {

            $val = [
                'ar' => "عادي",
                'en' => "Normal",
            ];

            return $val[$lang];

        } elseif ($role == "important") {
            $val = [
                'ar' => "عالي",
                'en' => "Important",
            ];

            return $val[$lang];
        }

    }

    public static function clice_type($role)
    {
        $lang = $_SESSION['lang'];
        if ($role == "design") {

            $val = [
                'ar' => "كليشة تصميم 3D",
                'en' => "3D design clice",
            ];

            return $val[$lang];

        } elseif ($role == "important") {
            $val = [
                'ar' => "كليشة المخططات التنفيذية",
                'en' => "Plan  clice",
            ];

            return $val[$lang];
        }

    }

    public static function currency($role)
    {
        $lang = $_SESSION['lang'];
        if ($role == "doller") {

            $val = [
                'ar' => "دولار",
                'en' => "Doller",
            ];

            return $val[$lang];

        } elseif ($role == "euro") {
            $val = [
                'ar' => "يورو",
                'en' => "Euro",
            ];

            return $val[$lang];
        } elseif ($role == "ksa") {
            $val = [
                'ar' => "ريال سعودي",
                'en' => "Real KSA",
            ];

            return $val[$lang];
        } elseif ($role == "uae") {
            $val = [
                'ar' => "درهم اماراتي",
                'en' => "derham UAE",
            ];

            return $val[$lang];
        } elseif ($role == "sp") {
            $val = [
                'ar' => "ليرة سورية",
                'en' => "Syrian pound",
            ];

            return $val[$lang];
        }

    }

    public static function role($role)
    {
        $lang = $_SESSION['lang'];
        if ($role == "admin") {

            $val = [
                'ar' => "أدمن",
                'en' => "Admin",
            ];

            return $val[$lang];

        } elseif ($role == "technical_supervisor") {
            $val = [
                'ar' => "المشرف الفني",
                'en' => "Technical supervisor",
            ];

            return $val[$lang];
        } elseif ($role == "project_manager") {
            $val = [
                'ar' => "مدير المشاريع",
                'en' => "Project manager",
            ];

            return $val[$lang];
        } elseif ($role == "employee") {
            $val = [
                'ar' => "موظف",
                'en' => "Employee",
            ];

            return $val[$lang];
        } elseif ($role == "customer") {
            $val = [
                'ar' => "زبون",
                'en' => "Customer",
            ];

            return $val[$lang];
        } elseif ($role == "user_collect") {
            $val = [
                'ar' => "مسؤول التجميع",
                'en' => "User Collect",
            ];

            return $val[$lang];
        }

    }

    public static function project_type($role)
    {
        $lang = $_SESSION['lang'];
        if ($role == "geometric") {

            $val = [
                'ar' => "هندسي",
                'en' => "Geometric",
            ];

            return $val[$lang];

        } elseif ($role == "advertising") {
            $val = [
                'ar' => "إعلان",
                'en' => "Advertising",
            ];

            return $val[$lang];
        } elseif ($role == "marketing") {
            $val = [
                'ar' => "تسويق",
                'en' => "Marketing",
            ];

            return $val[$lang];
        }

    }


    public static $error = [
        'ar' => "حدث خطأ يرجى المحاولة مجدداً",
        'en' => "Error, please try again",
    ];
    public static $saved = [
        'ar' => "تمت الحفط بنجاح",
        'en' => "Successfully added",
    ];
    public static $actions = [
        'ar' => "أحداث",
        'en' => "Events",
    ];
    public static $deleted_success = [
        'ar' => "تم الحذف بنجاح",
        'en' => "Successfully deleted ",
    ];
    public static $off = [
        'ar' => "لا",
        'en' => "No",
    ];
    public static $deleted = [
        'ar' => "حذف",
        'en' => "Delete",
    ];
    public static $on = [
        'ar' => "نعم",
        'en' => "Yes",
    ];
    public static $are_you_sure = [
        'ar' => "هل انت متأكد؟",
        'en' => "Are you sure?",
    ];
    public static $close = [
        'ar' => "إغلاق",
        'en' => "Close",
    ];
    public static $add = [
        'ar' => "إضافة",
        'en' => "Add",
    ];
    public static $users = [
        'ar' => "المستخدمون",
        'en' => "Users",
    ];
    public static $content = [
        'ar' => "جميع",
        'en' => "All",
    ];
    public static $wrong_email_or_password = [
        'ar' => "خطأ في معلومات الدخول ",
        'en' => "Wrong In Data Login ",
    ];
    public static $login_to_your_account = [
        'ar' => "سجل الدخول الآن",
        'en' => "Login to your account",
    ];
    public static $password = [
        'ar' => "كلمة المرور",
        'en' => "Password",
    ];
    public static $email = [
        'ar' => "البريد الالكتروني",
        'en' => "Email",
    ];
    public static $login = [
        'ar' => "تسجيل الدخول",
        'en' => "Login",
    ];
    public static $home = [
        'ar' => "الرئيسية",
        'en' => "Home",
    ];
    public static $profile = [
        'ar' => "الصفحة الشخصية",
        'en' => "Profile",
    ];
    public static $logout = [
        'ar' => "تسجيل الخروج",
        'en' => "logout",
    ];
    public static $main = [
        'ar' => "القائمة",
        'en' => "Main",
    ];
}
