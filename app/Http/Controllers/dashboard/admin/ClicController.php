<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\dashboard\ConstantController;
use App\Models\CliceModel;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ClicController extends Controller
{

    public static $path_view = "dashboard.main_views.admin.";
    public static $base = "admin";

    public function index(Request $request)
    {

        $data = array(
            'view_name' => ConstantController::$clice[$_SESSION['lang']],
            'view_parent' => null,
            'a_id' => "a_clice",
            'view_url' => "/admin/clices",
            "clices" => CliceModel::where('deleted', 0)->get(),

        );
        return view($this::$path_view . 'clices', $data);

    }


    public function save(Request $request)
    {

        $pro_find = CliceModel::where('name', $request->input('name'))->where('type', $request->input('type'))->get();
        if (sizeof($pro_find) > 0) {


            $request->session()->flash('error', "الاسم مستخدم من قبل ");
            return redirect(EmployeeController::$base . '/clices');
        }

        $item = new CliceModel();

        $item->name = $request->input('name');
        $item->type = $request->input('type');
        $item->added_by = Auth::user()->id;

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$saved[$_SESSION['lang']]);
            return redirect(DepartmentController::$base . '/clices');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(DepartmentController::$base . '/clices');
        }

    }


    public function edit(Request $request)
    {
        $item = CliceModel::where('id', $request->input('item_id'))->first();
        $item->name = $request->input('name');
        $item->type = $request->input('type');

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$edit_successfully[$_SESSION['lang']]);
            return redirect(DepartmentController::$base . '/clices');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(DepartmentController::$base . '/clices');
        }
    }

    public function defautl_clices(Request $request, $item_id)
    {
        $item = CliceModel::where('id', $item_id)->first();
        $type = $request->input('type');
        $q = DB::update("update clice set  clice.default=0 where type ='$type'");

        $item->default = 1;

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$edit_successfully[$_SESSION['lang']]);
            return redirect(DepartmentController::$base . '/clices');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(DepartmentController::$base . '/clices');
        }
    }

    public function remove(Request $request, $item_id)
    {

        $item = CliceModel::where('id', $item_id)->first();

        $col="";
        if($item->type=="design")
        {
            $col="clice_design";
        }
        else
        {

            $col="clice_plan";
        }


        if (sizeof(User::where($col, $item_id)->get()) > 0) {
            $request->session()->flash('error', "الرجاء حذف جميع الزبائن المرتبطين أولاً");
            return redirect(UserController::$base . '/users_get_works');
        } else {


            if ($item->delete()) {
                $request->session()->flash('success', ConstantController::$deleted_successfully[$_SESSION['lang']]);
                return redirect(DepartmentController::$base . '/clices');
            } else {

                $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
                return redirect(DepartmentController::$base . '/clices');
            }

        }


    }
}
