<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Models\ConstantModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConstantController extends Controller
{
    public function index()
    {
        $constants = array();
        foreach (ConstantModel::all() as $item) {
            $constants[$item->item] = $item->value;
        }
        $data = array(
            'constants' => $constants,
            'view_name' => "Constants",
            'view_parent' => null,
            'view_url' => "/constants",
            'a_id' => "constant_id"

        );
        return view('dashboard.main_views.admin.constant', $data);
    }


    public function save(Request $request)
    {


        foreach (ConstantModel::all() as $item) {
            if ($request->input($item->item)) {
                $item_ = ConstantModel::where('item', $item->item)->first();
                $item_->value = $request->input($item->item);
                $item_->save();
            }
        }

        $request->session()->flash('success', 'Success Edit');
        return redirect('/admin/constant');

    }

}
