<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\dashboard\ConstantController;
use App\Models\ProjectModel;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Project;
use Validator;

class CustomerController extends Controller
{
    public static $path_view = "dashboard.main_views.admin.";
    public static $base = "admin";

    public function index(Request $request)
    {

        $data = array(
            'view_name' => ConstantController::$customers[$_SESSION['lang']],
            'view_parent' => null,
            'a_id' => "a_customer",
            'view_url' => "/admin/customers",
            "customers" => User::where('type', '=', 'customer')->where('deleted', 0)->get(),

        );
        return view($this::$path_view . 'customers', $data);

    }


    public function save(Request $request)
    {


        $rules = [
            'code' => 'unique:user',
            'name' => 'unique:user',
            'mobile' => 'unique:user',
        ];

        $messages = [
            'code.unique' => "كود الزبون مستخدم من قبل",
            'name.unique' => "اسم الزبون مستخدم من قبل",
            'mobile.unique' => "رقم الجوال موجود مسبقاً",
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $arr1 = $validator->errors()->toArray();
            $array_validation_error = array();
            foreach ($arr1 as $key => $ele1) {
                foreach ($ele1 as $ele2) {
                    array_push($array_validation_error, array('key' => $key, 'message' => $ele2));
                }
            }

            if (sizeof($array_validation_error) > 0) {

                $request->session()->flash('error', $array_validation_error[0]['message']);
            }

            return redirect(CustomerController::$base . '/customers');
        }


        $item = new User();

        $item->name = $request->input('name');
        $item->code = $request->input('code');
        $item->work = $request->input('work');
        $item->customer_ownership = $request->input('customer_ownership');
        $item->country = $request->input('country');
        $item->city = $request->input('city');
        $item->responsible_person = $request->input('responsible_person');
        $item->address = $request->input('address');
        $item->phone = $request->input('phone');
        $item->mobile = $request->input('mobile');
        $item->email = $request->input('email');
        $item->other_contact_name = $request->input('other_contact_name');
        $item->other_contact_phone = $request->input('other_contact_phone');
        $item->website = $request->input('website');
        $item->facebook = $request->input('facebook');
        $item->instagram = $request->input('instagram');
        $item->user_connect = $request->input('user_connect');
        $item->note = $request->input('note');
        $item->type = "customer";
        $item->clice_design = $request->input('clice_design');
        $item->clice_plan = $request->input('clice_plan');
        $item->added_by = Auth::user()->id;

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$saved[$_SESSION['lang']]);
            return redirect(CustomerController::$base . '/customers');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(CustomerController::$base . '/customers');
        }

    }


    public function save_ajax(Request $request)
    {


        $rules = [
            'name' => 'unique:user',
            'code' => 'unique:user',
            'responsible_person' => 'unique:user',
            'phone' => 'unique:user',
            'email' => 'unique:user',
            'website' => '',
            'facebook' => '',
            'instagram' => '',
            'twitter' => '',
            'customer_ownership' => 'required|min:1',
        ];

        if (strlen($request->input('website')) > 0) {
            $rules['website'] = 'unique:user';
        }

        if (strlen($request->input('facebook')) > 0) {
            $rules['facebook'] = 'unique:user';
        }
        if (strlen($request->input('instagram')) > 0) {
            $rules['instagram'] = 'unique:user';
        }
        if (strlen($request->input('twitter')) > 0) {
            $rules['twitter'] = 'unique:user';
        }

        $messages = [
            'code.unique' => "كود الزبون مستخدم من قبل",
            'name.unique' => "اسم الزبون مستخدم من قبل",
            'responsible_person.unique' => "الشخص المسوول مستخدم من قبل",
            'mobile.unique' => "رقم الجوال موجود مسبقاً",
            'phone.unique' => "رقم الهاتف موجود مسبقاً",
            'email.unique' => "البريد الالكتروني مستخدم",
            'website.unique' => "الموقع الرسمي مستخدم",
            'facebook.unique' => "الفيسبوك مستخدم",
            'instagram.unique' => "الانستغرام مستخدم",
            'twitter.unique' => "التويتر مستخدم",
            'customer_ownership.min' => "ملكية الزبون مطلوبة",
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $arr1 = $validator->errors()->toArray();
            $array_validation_error = array();
            foreach ($arr1 as $key => $ele1) {
                foreach ($ele1 as $ele2) {
                    array_push($array_validation_error, array('key' => $key, 'message' => $ele2));
                }
            }

            if (sizeof($array_validation_error) > 0) {

                return response()->json(['status' => 403, 'message' => $array_validation_error]);
            }

        }


        $item = new User();

        $item->name = $request->input('name');
        $item->code = $request->input('code');
        $item->work = $request->input('work');
        $item->customer_ownership = $request->input('customer_ownership');
        $item->country = $request->input('country');
        $item->city = $request->input('city');
        $item->responsible_person = $request->input('responsible_person');
        $item->address = $request->input('address');
        $item->phone = $request->input('phone');
        $item->mobile = $request->input('mobile');
        $item->email = $request->input('email');
        $item->other_contact_name = $request->input('other_contact_name');
        $item->other_contact_phone = $request->input('other_contact_phone');
        $item->website = $request->input('website');
        $item->facebook = $request->input('facebook');
        $item->instagram = $request->input('instagram');
        $item->twitter = $request->input('twitter');
        $item->user_connect = $request->input('user_connect');
        $item->note = $request->input('note');
        $item->type = "customer";
        $item->clice_design = $request->input('clice_design');
        $item->clice_plan = $request->input('clice_plan');
        $item->added_by = Auth::user()->id;

        if ($item->save()) {

            return response()->json(['status' => 200, 'message' => [['message' => "" . ConstantController::$saved[$_SESSION['lang']]]]]);

        } else {


            return response()->json(['status' => 400, 'message' => [['message' => "" . ConstantController::$error[$_SESSION['lang']]]]]);


        }

    }


    public function edit(Request $request)
    {



        $rules = [
            'name' => 'unique:user,name,'.$request->input('item_id'),
            'code' => 'unique:user,code,'.$request->input('item_id'),
            'responsible_person' => 'unique:user,responsible_person,'.$request->input('item_id'),
            'phone' => 'unique:user,phone,'.$request->input('item_id'),
            'email' => 'unique:user,email,'.$request->input('item_id'),
            'website' => '',
            'facebook' => '',
            'instagram' => '',
            'twitter' => '',
            'customer_ownership' => 'required|min:1',
        ];

        if (strlen($request->input('website')) > 0) {
            $rules['website'] = 'unique:user,website,'.$request->input('item_id');
        }

        if (strlen($request->input('facebook')) > 0) {
            $rules['facebook'] = 'unique:user,facebook,'.$request->input('item_id');
        }
        if (strlen($request->input('instagram')) > 0) {
            $rules['instagram'] = 'unique:user,instagram,'.$request->input('item_id');
        }
        if (strlen($request->input('twitter')) > 0) {
            $rules['twitter'] = 'unique:user,twitter,'.$request->input('item_id');
        }

        $messages = [
            'code.unique' => "كود الزبون مستخدم من قبل",
            'name.unique' => "اسم الزبون مستخدم من قبل",
            'responsible_person.unique' => "الشخص المسوول مستخدم من قبل",
            'mobile.unique' => "رقم الجوال موجود مسبقاً",
            'phone.unique' => "رقم الهاتف موجود مسبقاً",
            'email.unique' => "البريد الالكتروني مستخدم",
            'website.unique' => "الموقع الرسمي مستخدم",
            'facebook.unique' => "الفيسبوك مستخدم",
            'instagram.unique' => "الانستغرام مستخدم",
            'twitter.unique' => "التويتر مستخدم",
            'customer_ownership.min' => "ملكية الزبون مطلوبة",
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $arr1 = $validator->errors()->toArray();
            $array_validation_error = array();
            foreach ($arr1 as $key => $ele1) {
                foreach ($ele1 as $ele2) {
                    array_push($array_validation_error, array('key' => $key, 'message' => $ele2));
                }
            }

            if (sizeof($array_validation_error) > 0) {

                $request->session()->flash('error', $array_validation_error[0]['message']);
                return redirect(UserController::$base . '/customers');
            }

        }


        $item = User::where('id', $request->input('item_id'))->first();
        $item->name = $request->input('name');
        $item->code = $request->input('code');
        $item->work = $request->input('work');
        $item->customer_ownership = $request->input('customer_ownership');
        $item->country = $request->input('country');
        $item->city = $request->input('city');
        $item->responsible_person = $request->input('responsible_person');
        $item->address = $request->input('address');
        $item->phone = $request->input('phone');
        $item->mobile = $request->input('mobile');
        $item->email = $request->input('email');
        $item->other_contact_name = $request->input('other_contact_name');
        $item->other_contact_phone = $request->input('other_contact_phone');
        $item->website = $request->input('website');
        $item->facebook = $request->input('facebook');
        $item->twitter = $request->input('twitter');
        $item->instagram = $request->input('instagram');
        $item->user_connect = $request->input('user_connect');
        $item->note = $request->input('note');
        $item->clice_design = $request->input('clice_design');
        $item->clice_plan = $request->input('clice_plan');
        $item->added_by = Auth::user()->id;

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$edit_successfully[$_SESSION['lang']]);
            return redirect(CustomerController::$base . '/customers');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(CustomerController::$base . '/customers');
        }
    }

    public function remove(Request $request, $item_id)
    {

        if (sizeof(ProjectModel::where('user', $item_id)->where('deleted', 0)->get()) > 0) {
            $request->session()->flash('error', "الرجاء حذف جميع المشاريع الخاصة بالزبون أولاً");
            return redirect(CustomerController::$base . '/customers');
        } else {
            $item = User::where('id', $item_id)->first();


            if ($item->delete()) {
                $request->session()->flash('success', ConstantController::$deleted_successfully[$_SESSION['lang']]);
                return redirect(CustomerController::$base . '/customers');
            } else {

                $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
                return redirect(CustomerController::$base . '/customers');
            }
        }


    }

    public function check_validation(Request $request)
    {


        $rules = [
            'name' => 'unique:user',
            'code' => 'unique:user',
            'phone' => 'unique:user',
            'mobile' => 'unique:user',
            'email' => 'unique:user',
        ];

        $messages = [
            'code.unique' => "كود الزبون مستخدم من قبل",
            'name.unique' => "اسم الزبون مستخدم من قبل",
            'mobile.unique' => "رقم الجوال موجود مسبقاً",
            'phone.unique' => "رقم الهاتف موجود مسبقاً",
            'email.unique' => "البريد الالكتروني موجود مسبقاً",
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $arr1 = $validator->errors()->toArray();
            $array_validation_error = array();
            foreach ($arr1 as $key => $ele1) {
                foreach ($ele1 as $ele2) {
                    array_push($array_validation_error, array('key' => $key, 'message' => $ele2));
                }
            }

            if (sizeof($array_validation_error) > 0) {

                return response()->json(['status' => 403, 'message' => $array_validation_error[0]['message']]);
            }

        }

    }
}
