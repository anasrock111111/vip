<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\dashboard\ConstantController;
use App\Models\DepartmentModel;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DepartmentController extends Controller
{
    public static $path_view = "dashboard.main_views.admin.";
    public static $base = "admin";

    public function index(Request $request)
    {

        $data = array(
            'view_name' => ConstantController::$department[$_SESSION['lang']],
            'view_parent' => null,
            'a_id' => "a_department",
            'view_url' => "/admin/departments",
            "departments" => DepartmentModel::where('deleted', 0)->get(),

        );
        return view($this::$path_view . 'departments', $data);

    }


    public function save(Request $request)
    {
        $item = new DepartmentModel();

        $item->name = $request->input('name');
        $item->added_by = Auth::user()->id;

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$saved[$_SESSION['lang']]);
            return redirect(DepartmentController::$base . '/departments');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(DepartmentController::$base . '/departments');
        }

    }


    public function edit(Request $request)
    {
        $item = DepartmentModel::where('id', $request->input('item_id'))->first();
        $item->name = $request->input('name');

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$edit_successfully[$_SESSION['lang']]);
            return redirect(DepartmentController::$base . '/departments');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(DepartmentController::$base . '/departments');
        }
    }

    public function remove(Request $request, $item_id)
    {

        if (sizeof(User::where("department", $item_id)->get()) > 0) {
            $request->session()->flash('error', "الرجاء حذف جميع الموظفين المرتبطين أولاً");
            return redirect(DepartmentController::$base . '/departments');
        } else {
            $item = DepartmentModel::where('id', $item_id)->first();


            if ($item->delete()) {
                $request->session()->flash('success', ConstantController::$deleted_successfully[$_SESSION['lang']]);
                return redirect(DepartmentController::$base . '/departments');
            } else {

                $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
                return redirect(DepartmentController::$base . '/departments');
            }

        }


    }
}
