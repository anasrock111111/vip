<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\dashboard\ConstantController;
use App\Models\SalaryLogModel;
use App\Models\TaskModel;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;

class EmployeeController extends Controller
{
    public static $path_view = "dashboard.main_views.admin.";
    public static $base = "admin";

    public function index(Request $request)
    {

        $data = array(
            'view_name' => ConstantController::$employees[$_SESSION['lang']],
            'view_parent' => null,
            'a_id' => "a_employee",
            'view_url' => "/admin/employees",
            "employees" => User::where('type', '=', 'employee')->where('deleted', 0)->get(),

        );
        return view($this::$path_view . 'employees', $data);

    }


    public function save(Request $request)
    {

        $rules = [
            'code' => 'unique:user',
            'name' => 'unique:user',
            'mobile' => 'unique:user',
            'phone' => 'unique:user',
            'email' => 'unique:user',
        ];

        $messages = [
            'code.unique' => "كود الزبون مستخدم من قبل",
            'name.unique' => "الاسم مستخدم من قبل",
            'mobile.unique' => "رقم الجوال موجود مسبقاً",
            'phone.unique' => "الهاتف موجود مسبقاً",
            'email.unique' => "البريد الالكتروني موجود مسبقاً",
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $arr1 = $validator->errors()->toArray();
            $array_validation_error = array();
            foreach ($arr1 as $key => $ele1) {
                foreach ($ele1 as $ele2) {
                    array_push($array_validation_error, array('key' => $key, 'message' => $ele2));
                }
            }

            if (sizeof($array_validation_error) > 0) {

                $request->session()->flash('error', $array_validation_error[0]['message']);
            }

            return redirect(EmployeeController::$base . '/employees');
        }


        $item = new User();

        $user_find = User::where('name', $request->input('name'))->get();
        if (sizeof($user_find) > 0) {


            $request->session()->flash('error', "الاسم مستخدم من قبل ");
            return redirect(EmployeeController::$base . '/employees');
        }

        $item->name = $request->input('name');
        $item->email = $request->input('email');
        $item->password = bcrypt($request->input('password'));
        $item->type = "employee";
        $item->department = $request->input('department');
        $item->job_title = $request->input('job_title');
        $item->address = $request->input('address');
        $item->phone = $request->input('phone');
        $item->mobile = $request->input('mobile');
        $item->study_type = $request->input('study_type');
        $item->study_note = $request->input('study_note');
        $item->employee_start = $request->input('employee_start');
        $item->employee_active = $request->input('employee_active');
        $item->salary = $request->input('salary');
        $item->currency = $request->input('currency');
        $item->added_by = Auth::user()->id;

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$saved[$_SESSION['lang']]);
            return redirect(EmployeeController::$base . '/employees');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(EmployeeController::$base . '/employees');
        }

    }

    public function save_ajax(Request $request)
    {

        if(strlen($request->input('phone'))>0)
        {
            $rules = [
                'name' => 'unique:user',
                'mobile' => 'unique:user',
                'email' => 'unique:user',
                'phone' => 'unique:user',
            ];
        }
        else
        {
            $rules = [
                'name' => 'unique:user',
                'mobile' => 'unique:user',
                'email' => 'unique:user',
            ];
        }


        $messages = [
            'name.unique' => "الاسم مستخدم من قبل",
            'mobile.unique' => "رقم الجوال موجود مسبقاً",
            'phone.unique' => "الهاتف موجود مسبقاً",
            'email.unique' => "البريد الالكتروني موجود مسبقاً",
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $arr1 = $validator->errors()->toArray();
            $array_validation_error = array();
            foreach ($arr1 as $key => $ele1) {
                foreach ($ele1 as $ele2) {
                    array_push($array_validation_error, array('key' => $key, 'message' => $ele2));
                }
            }

            if (sizeof($array_validation_error) > 0) {
                return response()->json(['status' => 403, 'message' => $array_validation_error]);
            }

        }


        $item = new User();

//        $user_find = User::where('name', $request->input('name'))->get();
//        if (sizeof($user_find) > 0) {
//
//
//            $request->session()->flash('error', "الاسم مستخدم من قبل ");
//            return redirect(EmployeeController::$base . '/employees');
//        }

        $item->name = $request->input('name');
        $item->email = $request->input('email');
        $item->password = bcrypt($request->input('password'));
        $item->type = "employee";
        $item->department = $request->input('department');
        $item->job_title = $request->input('job_title');
        $item->address = $request->input('address');
        $item->phone = $request->input('phone');
        $item->mobile = $request->input('mobile');
        $item->study_type = $request->input('study_type');
        $item->study_note = $request->input('study_note');
        $item->employee_start = $request->input('employee_start');
        $item->employee_active = $request->input('employee_active');
        $item->salary = $request->input('salary');
        $item->currency = $request->input('currency');
        $item->added_by = Auth::user()->id;


        if ($item->save()) {
            if ($request->input('salary')) {
                $log = new SalaryLogModel();
                $log->added_by = Auth::user()->id;
                $log->employee =$item->id;
                $log->currency =$request->input('currency');
                $log->old = 0;
                $log->new = $request->input('salary');
                $log->save();
            }

            return response()->json(['status' => 200, 'message' => [['message' => "" . ConstantController::$saved[$_SESSION['lang']]]]]);

        } else {


            return response()->json(['status' => 400, 'message' => [['message' => "" . ConstantController::$error[$_SESSION['lang']]]]]);


        }


    }


    public function edit(Request $request)
    {


        if(strlen($request->input('phone'))>0)
        {
            $rules = [
                'name' => 'unique:user,name,'. $request->input('item_id'),
                'mobile' => 'unique:user,mobile,'. $request->input('item_id'),
                'phone' => 'unique:user,phone,'. $request->input('item_id'),
                'email' => 'unique:user,email,'. $request->input('item_id'),
            ];

        }
        else
        {
            $rules = [
                'name' => 'unique:user,name,'. $request->input('item_id'),
                'mobile' => 'unique:user,mobile,'. $request->input('item_id'),

                'email' => 'unique:user,email,'. $request->input('item_id'),
            ];

        }



        $messages = [
            'name.unique' => "الاسم مستخدم من قبل",
            'mobile.unique' => "رقم الجوال موجود مسبقاً",
            'phone.unique' => "الهاتف موجود مسبقاً",
            'email.unique' => "البريد الالكتروني موجود مسبقاً",
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {

            $arr1 = $validator->errors()->toArray();
            $array_validation_error = array();
            foreach ($arr1 as $key => $ele1) {
                foreach ($ele1 as $ele2) {
                    array_push($array_validation_error, array('key' => $key, 'message' => $ele2));
                }
            }

            if (sizeof($array_validation_error) > 0) {

                $request->session()->flash('error', $array_validation_error[0]['message']);
                return redirect(UserController::$base . '/employees');
//                return response()->json(['status' => 403, 'message' => $array_validation_error]);
            }

        }

        $item = User::where('id', $request->input('item_id'))->first();

        $user_find = User::where('name', $request->input('name'))->where('id', '<>', $request->input('item_id'))->get();
        if (sizeof($user_find) > 0) {


            $request->session()->flash('error', "الاسم مستخدم من قبل ");
            return redirect(EmployeeController::$base . '/employees');
        }

        $user_find = User::where('email', $request->input('email'))->where('id', '<>', $request->input('item_id'))->get();
        if (sizeof($user_find) > 0) {


            $request->session()->flash('error', "البريد الالكتروني مستخدم من قبل ");
            return redirect(EmployeeController::$base . '/employees');
        }

        $old_salary = $item->salary;
        $new_salary = $request->input('salary');

        $item->name = $request->input('name');
        $item->email = $request->input('email');
        if ($request->input('password')) {

            $item->password = bcrypt($request->input('password'));
        }

        $item->department = $request->input('department');
        $item->job_title = $request->input('job_title');
        $item->address = $request->input('address');
        $item->phone = $request->input('phone');
        $item->mobile = $request->input('mobile');
        $item->study_type = $request->input('study_type');
        $item->study_note = $request->input('study_note');
        $item->employee_start = $request->input('employee_start');
        $item->employee_active = $request->input('employee_active');
        $item->salary = $request->input('salary');
        $item->currency = $request->input('currency');
        $item->added_by = Auth::user()->id;


        if ($old_salary != $new_salary) {
            $log = new SalaryLogModel();
            $log->added_by = Auth::user()->id;
            $log->employee = $request->input('item_id');
            $log->old = $old_salary;
            $log->currency =$request->input('currency');
            $log->new = $new_salary;
            $log->save();
        }

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$edit_successfully[$_SESSION['lang']]);
            return redirect(EmployeeController::$base . '/employees');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(EmployeeController::$base . '/employees');
        }
    }

    public function remove(Request $request, $item_id)
    {


        if (sizeof(TaskModel::where('employee', $item_id)->where('deleted', 0)->get()) > 0) {
            $request->session()->flash('error', "الرجاء حذف جميع المهام الخاصة بالموظف أولاً");
            return redirect(CustomerController::$base . '/employees');
        } else {
            $item = User::where('id', $item_id)->first();


            if ($item->delete()) {
                $request->session()->flash('success', ConstantController::$deleted_successfully[$_SESSION['lang']]);
                return redirect(EmployeeController::$base . '/employees');
            } else {

                $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
                return redirect(EmployeeController::$base . '/employees');
            }
        }


    }


    public function employee_salary(Request $request)
    {

        $id = $request->input('elem_id');
        $salary = SalaryLogModel::where('employee', $id)->get();

        foreach ($salary as $item) {

            $item->currency= ConstantController::currency($item->currency);
        }

        return response()->json(['status' => 200, 'salary' => $salary]);
    }

}
