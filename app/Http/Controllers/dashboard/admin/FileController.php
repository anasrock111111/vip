<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\dashboard\ConstantController;
use App\Models\FileModel;
use Faker\Provider\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class FileController extends Controller
{
    public static $path_view = "dashboard.main_views.admin.";
    public static $base = "admin";

    public function index(Request $request, $item_id)
    {

        $type = $request->input('type');

        $data = array(
            'view_name' => ConstantController::$files[$_SESSION['lang']],
            'view_parent' => null,
            'a_id' => "a_" . $type,
            'view_url' => "/admin/files/" . $item_id . "?type=" . $type,
            'item_id' => $item_id,
            'type' => $type,

            "files" => FileModel::where('type', $type)->where('deleted', 0)->where('item_id', $item_id)->orderBy('created_at', 'desc')->get(),

        );
        return view($this::$path_view . 'files', $data);

    }

    public function save(Request $request)
    {


        if ($request->hasFile('filesType')) {


            foreach ($request->file('filesType') as $key => $file) {

                $item = new FileModel();
                $item->note = $request->input('note');
                $item_id = $request->input('item_id');
                $item->item_id = $item_id;
                $type = $request->input('type');
                $item->type = $type;
                $item->added_by = Auth::user()->id;
                $getimageName = time() . $key . '.' . $file->getClientOriginalExtension();
                $file->move('uploads/files/' . $type, $getimageName);
                $item->file = $getimageName;
                $item->save();
            }

        }
        $request->session()->flash('success', ConstantController::$saved[$_SESSION['lang']]);
        return redirect(FileController::$base . '/files/' . $item_id . "?type=" . $type);


    }

    public function remove(Request $request, $item_id)
    {

        $item = FileModel::where('id', $item_id)->first();

        $type_item = $request->input('type');
        $type_item_id = $request->input('type_item_id');



        if ($item->delete()) {
            $request->session()->flash('success', ConstantController::$deleted_successfully[$_SESSION['lang']]);
            return redirect(FileController::$base . '/files/' . $type_item_id . "?type=" . $type_item);
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(FileController::$base . '/files/' . $type_item_id . "?type=" . $type_item);
        }

    }
}
