<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\dashboard\ConstantController;
use App\Imports\ClientImport;
use App\Imports\ProjectImport;
use App\Imports\UserImport;
use App\Models\OrderModel;
use App\Models\ProjectModel;
use App\Models\TaskModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Maatwebsite\Excel\Facades\Excel;

class HomeController extends Controller
{
    public static $path_view = "dashboard.main_views.admin.";

    public function importUser(Request $request)
    {


          Excel::import(new UserImport,'user.xlsx');

            Excel::import(new ClientImport,'client.xlsx');

            Excel::import(new ProjectImport,'project.xlsx');



    }

    public function index(Request $request)
    {


        if (Auth::user()->type == "technical_supervisor") {

            $orders = OrderModel::where('deleted', 0)->where('seen_by_technical', 0)->get();

            $order = array();
            foreach ($orders as $item_) {

                $tasks = TaskModel::where('order', $item_->id)->where('deleted', 0)->get();

                $all_tasks = sizeof($tasks);
                $tasks_ended = 0;
                $task_received_from_technical = 0;
                foreach ($tasks as $task) {

                    if ($task->finished == 1) {
                        $tasks_ended++;
                    }
                    if ($task->technical_supervisor_receive == 1) {
                        $task_received_from_technical++;
                    }

                    $item_->tasks_ended = $tasks_ended;
                    $item_->all_tasks = $all_tasks;
                    $item_->task_received_from_technical = $task_received_from_technical;
                }
                $item_->tasks_not_inserted = sizeof(TaskModel::where('order', $item_->id)->where('deleted', 0)->where('employee', '=', null)->get());

                array_push($order, $item_);

            }

            $date_today = date('Y-m-d');
            $tasks = DB::select("
            select * from `task`
            where  (start_date='$date_today' ) or (start_date<'$date_today'   )
            and deleted=0 and collected=0
            and employee<>null 
            order by created_at desc
            ");

            $tasks_f = array();

            foreach ($tasks as $task) {
                if (OrderModel::find($task->order)->delivered == 0) {
                    array_push($tasks_f, $task);
                }
            }


            $tasks_should_recived = TaskModel::where('deleted', 0)->where('start_date', '<>', date('Y-m-d'))->where('employee', '<>', null)->get();
            $tasks_should_recived_f = array();

            foreach ($tasks_should_recived as $task) {
                if (OrderModel::find($task->order)->delivered == 0) {
                    array_push($tasks_should_recived_f, $task);
                }
            }


            $task_ready_to_recive = TaskModel::where('technical_supervisor_receive', 0)->where('finished', 1)->where('deleted', 0)->get();
            $task_ready_to_recive_final = array();
            foreach ($task_ready_to_recive as $task) {
                if (OrderModel::find($task->order)->delivered == 0) {
                    array_push($task_ready_to_recive_final, $task);
                }
            }

            $data = array(
                'view_name' => ConstantController::$home[$_SESSION['lang']],
                'view_parent' => null,
                'view_url' => "/admin/home",
                'a_id' => "a_home",
                'orders' => $order,
                'tasks' => $tasks_f,
                'task_ready_to_recive' => $task_ready_to_recive_final,
                'tasks_should_recived' => $tasks_should_recived_f,

            );
            return view($this::$path_view . 'home_technical', $data);

        } else if (Auth::user()->type == "project_manager") {


            $project = ProjectModel::where('project_ended', 0)->where('deleted', 0)->get();
            $order = array();
            foreach ($project as $item) {


                //   $orders = OrderModel::where('project', $item->id)->where('deleted', 0)->get();


                $date_today = date('Y-m-d');
                $orders = DB::select("
            
            select * from `order`
            where  (project=$item->id  and  (delivery_date<'$date_today' and delivered=0  ))
            or project=$item->id 
            
            
            ");
                foreach ($orders as $item_) {

                    $tasks = TaskModel::where('order', $item_->id)->where('deleted', 0)->get();

                    $all_tasks = sizeof($tasks);
                    $tasks_ended = 0;
                    $task_received_from_technical = 0;
                    foreach ($tasks as $task) {

                        if ($task->finished == 1) {
                            $tasks_ended++;
                        }
                        if ($task->technical_supervisor_receive == 1) {
                            $task_received_from_technical++;
                        }

                        $item_->tasks_ended = $tasks_ended;
                        $item_->all_tasks = $all_tasks;
                        $item_->task_received_from_technical = $task_received_from_technical;
                    }
                    $item_->tasks_not_inserted = sizeof(TaskModel::where('order', $item_->id)->where('deleted', 0)->where('employee', '=', null)->get());

                    array_push($order, $item_);

                }

            }

            $date_today = date('Y-m-d');
            $next_date = date('Y-m-d', strtotime($date_today . " + 1 day"));
            $next_date_ = "";

            if (date('D', strtotime($date_today)) === 'Thu') {
//            if (date('D', strtotime($date_today)) === 'Wed') {
                $next_date_ = date('Y-m-d', strtotime($date_today . " + 2 day"));
            }

            $orders_next_day = OrderModel::where('deleted', 0)->whereIn('delivery_date', [$next_date, $next_date_])->get();


            $orders_next_day_arr = array();
            foreach ($orders_next_day as $item) {
                $tasks = TaskModel::where('order', $item->id)->where('deleted', 0)->get();
                $all_tasks = sizeof($tasks);
                $tasks_ended = 0;
                $task_received_from_technical = 0;
                foreach ($tasks as $task) {

                    if ($task->finished == 1) {
                        $tasks_ended++;
                    }
                    if ($task->technical_supervisor_receive == 1) {
                        $task_received_from_technical++;
                    }

                    $item->tasks_ended = $tasks_ended;
                    $item->all_tasks = $all_tasks;
                    $item->task_received_from_technical = $task_received_from_technical;
                }

                $item->tasks_not_inserted = sizeof(TaskModel::where('order', $item->id)->where('deleted', 0)->where('employee', '=', null)->get());

                array_push($orders_next_day_arr, $item);

            }
//            $order_delivery_today_ = DB::select("
//
//            select * from `order`
//            where  (delivery_date='$date_today' ) or (delivery_date<'$date_today' and delivered=0  )
//
//            ");
            $order_delivery_today_ = DB::select("
            
            select * from `order`
            where  (delivery_date='$date_today' ) 
            
            ");
            $order_delivery_today_aray = array();
            foreach ($order_delivery_today_ as $item) {
                $tasks = TaskModel::where('order', $item->id)->where('deleted', 0)->get();
                $all_tasks = sizeof($tasks);
                $tasks_ended = 0;
                $task_received_from_technical = 0;
                foreach ($tasks as $task) {

                    if ($task->finished == 1) {
                        $tasks_ended++;
                    }
                    if ($task->technical_supervisor_receive == 1) {
                        $task_received_from_technical++;
                    }

                    $item->tasks_ended = $tasks_ended;
                    $item->all_tasks = $all_tasks;
                    $item->task_received_from_technical = $task_received_from_technical;
                }
                $item->tasks_not_inserted = sizeof(TaskModel::where('order', $item->id)->where('deleted', 0)->where('employee', '=', null)->get());

                array_push($order_delivery_today_aray, $item);

            }

            $data = array(
                'view_name' => ConstantController::$home[$_SESSION['lang']],
                'view_parent' => null,
                'view_url' => "/admin/home",
                'a_id' => "a_home",
                'orders' => $order,
                'orders_next_day' => $orders_next_day_arr,
                'order_delivery_today' => $order_delivery_today_aray,
                'tasks' => TaskModel::where('deleted', 0)->where('start_date', '' . date('Y-m-d'))->where('employee', '<>', null)->get(),
            );

            return view($this::$path_view . 'home_project_manager', $data);

        } else if (Auth::user()->type == "admin") {


            $project = ProjectModel::where('project_ended', 0)->where('deleted', 0)->get();
            $order = array();
            foreach ($project as $item) {


                //   $orders = OrderModel::where('project', $item->id)->where('deleted', 0)->get();


                $date_today = date('Y-m-d');
                $orders = DB::select("
            
            select * from `order`
            where  (project=$item->id  and  (delivery_date<'$date_today' and delivered=0  ))
            or project=$item->id 
            
            
            ");
                foreach ($orders as $item_) {

                    $tasks = TaskModel::where('order', $item_->id)->where('deleted', 0)->get();

                    $all_tasks = sizeof($tasks);
                    $tasks_ended = 0;
                    $task_received_from_technical = 0;
                    foreach ($tasks as $task) {

                        if ($task->finished == 1) {
                            $tasks_ended++;
                        }
                        if ($task->technical_supervisor_receive == 1) {
                            $task_received_from_technical++;
                        }

                        $item_->tasks_ended = $tasks_ended;
                        $item_->all_tasks = $all_tasks;
                        $item_->task_received_from_technical = $task_received_from_technical;
                    }
                    $item_->tasks_not_inserted = sizeof(TaskModel::where('order', $item_->id)->where('deleted', 0)->where('employee', '=', null)->get());

                    array_push($order, $item_);

                }

            }

            $date_today = date('Y-m-d');
            $next_date = date('Y-m-d', strtotime($date_today . " + 1 day"));

            $next_date_ = "";
            if (date('D', strtotime($date_today)) === 'Thu') {
//            if (date('D', strtotime($date_today)) === 'Wed') {
                $next_date_ = date('Y-m-d', strtotime($date_today . " + 2 day"));
            }

            $orders_next_day = OrderModel::where('deleted', 0)->whereIn('delivery_date', [$next_date, $next_date_])->get();


            $orders_next_day_arr = array();
            foreach ($orders_next_day as $item) {
                $tasks = TaskModel::where('order', $item->id)->where('deleted', 0)->get();
                $all_tasks = sizeof($tasks);
                $tasks_ended = 0;
                $task_received_from_technical = 0;
                foreach ($tasks as $task) {

                    if ($task->finished == 1) {
                        $tasks_ended++;
                    }
                    if ($task->technical_supervisor_receive == 1) {
                        $task_received_from_technical++;
                    }

                    $item->tasks_ended = $tasks_ended;
                    $item->all_tasks = $all_tasks;
                    $item->task_received_from_technical = $task_received_from_technical;
                }

                $item->tasks_not_inserted = sizeof(TaskModel::where('order', $item->id)->where('deleted', 0)->where('employee', '=', null)->get());

                array_push($orders_next_day_arr, $item);

            }
//            $order_delivery_today_ = DB::select("
//
//            select * from `order`
//            where  (delivery_date='$date_today' ) or (delivery_date<'$date_today' and delivered=0  )
//
//            ");
            $order_delivery_today_ = DB::select("
            
            select * from `order`
            where  (delivery_date='$date_today' ) 
            
            ");
            $order_delivery_today_aray = array();
            foreach ($order_delivery_today_ as $item) {
                $tasks = TaskModel::where('order', $item->id)->where('deleted', 0)->get();
                $all_tasks = sizeof($tasks);
                $tasks_ended = 0;
                $task_received_from_technical = 0;
                foreach ($tasks as $task) {

                    if ($task->finished == 1) {
                        $tasks_ended++;
                    }
                    if ($task->technical_supervisor_receive == 1) {
                        $task_received_from_technical++;
                    }

                    $item->tasks_ended = $tasks_ended;
                    $item->all_tasks = $all_tasks;
                    $item->task_received_from_technical = $task_received_from_technical;
                }
                $item->tasks_not_inserted = sizeof(TaskModel::where('order', $item->id)->where('deleted', 0)->where('employee', '=', null)->get());

                array_push($order_delivery_today_aray, $item);

            }

            $data = array(
                'view_name' => ConstantController::$home[$_SESSION['lang']],
                'view_parent' => null,
                'view_url' => "/admin/home",
                'a_id' => "a_home",
                'orders' => $order,
                'orders_next_day' => $orders_next_day_arr,
                'order_delivery_today' => $order_delivery_today_aray,
                'tasks' => TaskModel::where('deleted', 0)->where('start_date', '' . date('Y-m-d'))->where('employee', '<>', null)->get(),
            );

            return view($this::$path_view . 'home', $data);

        }
    }


}
