<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\dashboard\ConstantController;
use App\Models\NatureProjectModel;
use App\Models\ProjectModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NatureProjectController extends Controller
{
    public static $path_view = "dashboard.main_views.admin.";
    public static $base = "admin";

    public function index(Request $request)
    {

        $data = array(
            'view_name' => ConstantController::$nature_project[$_SESSION['lang']],
            'view_parent' => null,
            'a_id' => "a_nature_project",
            'view_url' => "/admin/nature_projects",
            "nature_projects" => NatureProjectModel::where('deleted', 0)->get(),

        );
        return view($this::$path_view . 'nature_projects', $data);

    }


    public function save(Request $request)
    {
        $item = new NatureProjectModel();

        $item->name = $request->input('name');
        $item->added_by = Auth::user()->id;

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$saved[$_SESSION['lang']]);
            return redirect(NatureProjectController::$base . '/nature_projects');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(NatureProjectController::$base . '/nature_projects');
        }

    }


    public function edit(Request $request)
    {
        $item = NatureProjectModel::where('id', $request->input('item_id'))->first();
        $item->name = $request->input('name');

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$edit_successfully[$_SESSION['lang']]);
            return redirect(NatureProjectController::$base . '/nature_projects');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(NatureProjectController::$base . '/nature_projects');
        }
    }

    public function remove(Request $request, $item_id)
    {

        if (sizeof(ProjectModel::where("nature", $item_id)->get()) > 0) {
            $request->session()->flash('error', "الرجاء حذف جميع المشاريع المرتبطةأولاً");
            return redirect(NatureProjectController::$base . '/nature_projects');
        } else {
            $item = NatureProjectModel::where('id', $item_id)->first();


            if ($item->delete()) {
                $request->session()->flash('success', ConstantController::$deleted_successfully[$_SESSION['lang']]);
                return redirect(NatureProjectController::$base . '/nature_projects');
            } else {

                $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
                return redirect(NatureProjectController::$base . '/nature_projects');
            }


        }


    }
}
