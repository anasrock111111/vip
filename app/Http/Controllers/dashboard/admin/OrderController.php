<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\dashboard\ConstantController;
use App\Models\CliceModel;
use App\Models\OrderModel;
use App\Models\ProjectModel;
use App\Models\TaskModel;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Project;
use Validator;

class OrderController extends Controller
{

    public static $path_view = "dashboard.main_views.admin.";
    public static $base = "admin";

    public function index(Request $request)
    {

        $ordrs = OrderModel::where('deleted', 0)->orderBy('created_at', 'desc')->get();

//        foreach ($ordrs as  $item)
//        {
//
//            $tasks=TaskModel::where('order',$item->id)->where('deleted',0)->get();
//            foreach ($tasks as $task)
//            {
//                if ($task->collected==0)
//                {
//                    $item->collected=0;
//                }
//            }
//        }

        $ordrs_array = array();
        foreach ($ordrs as $item) {
            $tasks = TaskModel::where('order', $item->id)->where('deleted', 0)->get();
            $all_tasks = sizeof($tasks);
            $tasks_ended = 0;
            $task_received_from_technical = 0;
            foreach ($tasks as $task) {

                if ($task->finished == 1) {
                    $tasks_ended++;
                }
                if ($task->technical_supervisor_receive == 1) {
                    $task_received_from_technical++;
                }

                $item->tasks_ended = $tasks_ended;
                $item->all_tasks = $all_tasks;
                $item->task_received_from_technical = $task_received_from_technical;
            }

            $item->tasks_not_inserted = sizeof(TaskModel::where('order', $item->id)->where('deleted', 0)->where('employee', '=', null)->get());

            array_push($ordrs_array, $item);

        }


        if (Auth::user()->type == "technical_supervisor") {


            $project = ProjectModel::where('project_ended', 0)->where('deleted', 0)->get();
            $order = array();
            foreach ($project as $item) {


                $orders = OrderModel::where('project', $item->id)->where('deleted', 0)->get();

                foreach ($orders as $item_) {

                    $tasks = TaskModel::where('order', $item_->id)->where('deleted', 0)->where('employee', '<>', null)->get();
                    $all_tasks = sizeof($tasks);
                    $tasks_ended = 0;
                    $task_received_from_technical = 0;
                    foreach ($tasks as $task) {

                        if ($task->finished == 1) {
                            $tasks_ended++;
                        }
                        if ($task->technical_supervisor_receive == 1) {
                            $task_received_from_technical++;
                        }

                        $item_->tasks_ended = $tasks_ended;
                        $item_->all_tasks = $all_tasks;
                        $item_->task_received_from_technical = $task_received_from_technical;
                    }
                    $item_->tasks_not_inserted = sizeof(TaskModel::where('order', $item_->id)->where('deleted', 0)->where('employee', '=', null)->get());

                    array_push($order, $item_);

                }

            }

            $date_today = date('Y-m-d');
            $next_date = date('Y-m-d', strtotime($date_today . " + 1 day"));

            $next_date_ = "";
            if (date('D', strtotime($date_today)) === 'Thu') {

                $next_date_ = date('Y-m-d', strtotime($date_today . " + 2 day"));
            }


            $orders_next_day = OrderModel::where('deleted', 0)->whereIn('delivery_date', [$next_date, $next_date_])->get();


            $orders_next_day_arr = array();
            foreach ($orders_next_day as $item) {
                $tasks = TaskModel::where('order', $item->id)->where('deleted', 0)->where('employee', '<>', null)->get();
                $all_tasks = sizeof($tasks);
                $tasks_ended = 0;
                $task_received_from_technical = 0;
                foreach ($tasks as $task) {

                    if ($task->finished == 1) {
                        $tasks_ended++;
                    }
                    if ($task->technical_supervisor_receive == 1) {
                        $task_received_from_technical++;
                    }

                    $item->tasks_ended = $tasks_ended;
                    $item->all_tasks = $all_tasks;
                    $item->task_received_from_technical = $task_received_from_technical;
                }
                $item->tasks_not_inserted = sizeof(TaskModel::where('order', $item->id)->where('deleted', 0)->where('employee', '=', null)->get());

                array_push($orders_next_day_arr, $item);

            }
            $order_delivery_today_ = DB::select("
            
            select * from `order`
            where  (delivery_date='$date_today' ) or (delivery_date<'$date_today' and delivered=0  )
            
            ");
            $order_delivery_today_aray = array();
            foreach ($order_delivery_today_ as $item) {
                $tasks = TaskModel::where('order', $item->id)->where('deleted', 0)->where('employee', '<>', null)->get();
                $all_tasks = sizeof($tasks);
                $tasks_ended = 0;
                $task_received_from_technical = 0;
                foreach ($tasks as $task) {

                    if ($task->finished == 1) {
                        $tasks_ended++;
                    }
                    if ($task->technical_supervisor_receive == 1) {
                        $task_received_from_technical++;
                    }

                    $item->tasks_ended = $tasks_ended;
                    $item->all_tasks = $all_tasks;
                    $item->task_received_from_technical = $task_received_from_technical;
                }
                $item->tasks_not_inserted = sizeof(TaskModel::where('order', $item->id)->where('deleted', 0)->where('employee', '=', null)->get());

                array_push($order_delivery_today_aray, $item);

            }

            $data = array(
                'view_name' => ConstantController::$orders[$_SESSION['lang']],
                'view_parent' => null,
                'a_id' => "a_order",
                'view_url' => "/admin/orders",
                "orders" => $ordrs_array,
                'orders_working_on' => $order,
                'orders_next_day' => $orders_next_day_arr,
                'order_delivery_today' => $order_delivery_today_aray,

            );
        } else {
            $data = array(
                'view_name' => ConstantController::$orders[$_SESSION['lang']],
                'view_parent' => null,
                'a_id' => "a_order",
                'view_url' => "/admin/orders",
                "orders" => $ordrs_array,

            );
        }


        return view($this::$path_view . 'orders', $data);

    }

    public function save_view(Request $request)
    {
        $data = array(
            'view_name' => ConstantController::$orders[$_SESSION['lang']],
            'view_parent' => null,
            'a_id' => "a_order",
            'view_url' => "/admin/orders",
            "project" => $request->input('project'),

        );
        return view($this::$path_view . 'add_order', $data);
    }

    public function order_task(Request $request, $id)
    {
        $data = array(
            'view_name' => ConstantController::$orders[$_SESSION['lang']],
            'view_parent' => null,
            'a_id' => "a_order",
            'view_url' => "/admin/orders",
            'tasks' => TaskModel::where('order', $id)->where('deleted', 0)->where('employee', '<>', null)->get(),

        );
        return view($this::$path_view . 'order_task', $data);
    }

    public function edit_view(Request $request, $id)
    {
        $data = array(
            'view_name' => ConstantController::$orders[$_SESSION['lang']],
            'view_parent' => null,
            'a_id' => "a_order",
            'view_url' => "/admin/orders",
            "order" => OrderModel::find($id),
        );
        return view($this::$path_view . 'edit_order', $data);
    }

    public function save(Request $request)
    {


        $item = new OrderModel();

        $item->project = $request->input('project');
        $item->order_number = $request->input('order_number');
        $item->date_received = $request->input('date_received');
        $item->order_direction = ProjectModel::find($request->input('project'))->edit_direction;
        $item->delivery_date = $request->input('delivery_date');
        $item->Importance = $request->input('importance');
        $item->note = $request->input('note');
        $item->added_by = Auth::user()->id;

        if ($item->save()) {
            $request->session()->flash('success', ConstantController::$saved[$_SESSION['lang']]);
            return redirect(OrderController::$base . '/orders');

        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(OrderController::$base . '/orders');

        }

    }

    public function save_ajax(Request $request)
    {


        $projec_id = $request->input('project');

        $array_validation_error = [];
        if (sizeof(OrderModel::where('order_number', $request->input('order_number'))->where('project', $projec_id)->get()) > 0) {

            array_push($array_validation_error, array('key' => 'order_number', 'message' => "رقم الطلب موجود سابقاً لهذا المشروع"));

            return response()->json(['status' => 403, 'message' => $array_validation_error]);

        }

        $item = new OrderModel();

        $item->project = $request->input('project');
        $item->order_number = $request->input('order_number');
        $item->date_received = $request->input('date_received');
        $item->order_direction = ProjectModel::find($request->input('project'))->edit_direction;
        $item->delivery_date = $request->input('delivery_date');
        $item->Importance = $request->input('importance');
        $item->note = $request->input('note');
        $item->completion = $request->input('completion');
        $item->edit_other = $request->input('edit_other');
        $item->added_by = Auth::user()->id;

        if ($item->save()) {
            return response()->json(['status' => 200, 'message' => [['message' => "" . ConstantController::$saved[$_SESSION['lang']]]]]);

        } else {


            return response()->json(['status' => 400, 'message' => [['message' => "" . ConstantController::$error[$_SESSION['lang']]]]]);


        }


    }


    public function edit(Request $request)
    {
        $item = OrderModel::where('id', $request->input('item_id'))->first();

        $item->project = $request->input('project');
        $item->order_number = $request->input('order_number');
        $item->date_received = $request->input('date_received');
        $item->order_direction = ProjectModel::find($request->input('project'))->edit_direction;
        $item->delivery_date = $request->input('delivery_date');
        $item->Importance = $request->input('importance');
        $item->note = $request->input('note');


        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$edit_successfully[$_SESSION['lang']]);
//            return redirect(OrderController::$base . '/edit_order/' . $request->input('item_id'));
            return redirect(OrderController::$base . '/orders');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
//            return redirect(OrderController::$base . '/edit_order/' . $request->input('item_id'));
            return redirect(OrderController::$base . '/orders');
        }
    }

    public function remove(Request $request, $item_id)
    {

        if (sizeof(TaskModel::where('order', $item_id)->where('deleted', 0)->get()) > 0) {
            $request->session()->flash('error', "الرجاء حذف جميع المهام الخاصة بالطلب أولاً");
            return redirect(OrderController::$base . '/orders');
        } else {
            $item = OrderModel::where('id', $item_id)->first();


            if ($item->delete()) {
                $request->session()->flash('success', ConstantController::$deleted_successfully[$_SESSION['lang']]);
                return redirect(OrderController::$base . '/orders');

            } else {

                $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
                return redirect(OrderController::$base . '/orders');
            }
        }

    }

    public function seen_by_technical(Request $request, $id)
    {
        $item = OrderModel::where('id', $id)->first();

        $item->seen_by_technical = 1;

        if ($item->save()) {
            $request->session()->flash('success', ConstantController::$edit_successfully[$_SESSION['lang']]);
            return redirect(OrderController::$base . '/home');

        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(OrderController::$base . '/home');
        }
    }

    public function delivered_order(Request $request)
    {
        $item = OrderModel::where('id', $request->input('order'))->first();

        $item->delivered = 1;
        $item->delivered_by = Auth::user()->id;
        $item->delivered_date = date('Y-m-d H:i:s');


        $tasks_not_collected = TaskModel::where('order', $item->id)->where('collected', 0)->get();
        if (sizeof($tasks_not_collected) > 0) {
            $request->session()->flash('error', "هنالك مهام غير مجمعة بعد");
            if ($request->input('home') == "home") {

                return redirect(OrderController::$base . '/home');
            } else {

                return redirect(OrderController::$base . '/orders');
            }
        } else {

            if ($item->save()) {
                $request->session()->flash('success', ConstantController::$edit_successfully[$_SESSION['lang']]);
                if ($request->input('home') == "home") {

                    return redirect(OrderController::$base . '/home');
                } else {

                    return redirect(OrderController::$base . '/orders');
                }

            } else {

                $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
                if ($request->input('home') == "home") {

                    return redirect(OrderController::$base . '/home');
                } else {

                    return redirect(OrderController::$base . '/orders');
                }
            }
        }


    }

    public function get_next_order_code(Request $request)
    {

        $item = $request->input('item_id');
        $pr = OrderModel::where('project', $item)->orderBy('created_at', 'desc')->where('deleted', 0)->get();
        if (sizeof($pr) > 0) {
            $code = intval($pr[0]->order_number);
            $new_code = $code + 1;
            return response()->json(['status' => 200, 'code' => $new_code]);
        } else {
            return response()->json(['status' => 403, 'code' => 1]);

        }
    }


    public function check_validation_order(Request $request)
    {


        $rules = [
            'order_number' => 'unique:order',
        ];

        $messages = [
            'order_number.unique' => "رقم الطلب موجود مسبقاً",
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $arr1 = $validator->errors()->toArray();
            $array_validation_error = array();
            foreach ($arr1 as $key => $ele1) {
                foreach ($ele1 as $ele2) {
                    array_push($array_validation_error, array('key' => $key, 'message' => $ele2));
                }
            }

            if (sizeof($array_validation_error) > 0) {

                return response()->json(['status' => 403, 'message' => $array_validation_error[0]['message']]);
            }

        }

    }

    public function assign_to_collector(Request $request)
    {

        $order = $request->input('order');

        $order_one = OrderModel::find($order);

        if ($order_one->assign_to_collector == 1) {
            $order_one->assign_to_collector = 0;
        } else {
            $order_one->assign_to_collector = 1;

        }
        $order_one->save();
        return redirect(OrderController::$base . '/orders');
    }

    public function project_note(Request $request)
    {

        $order_id = $request->input('elem_id');

        $order = OrderModel::find($order_id);
        $project = ProjectModel::find($order->project);
        $customer = User::find($project->user);


        $data = [
            'project_user_note' => $project->user_note,
            'project_technical_supervisor_note' => $project->technical_supervisor_note,
            'order_note' => $order->note,
            'customer_code' => $customer->code,
            'customer_note' => $customer->note,
            'clice_design' => CliceModel::find($customer->clice_design)->name,
            'clice_plan' => CliceModel::find($customer->clice_plan)->name,
        ];
        return response()->json(['status' => 200, 'data' => $data]);

    }

    public function update_task_date(Request $request)
    {

        $item_id = $request->input('order_id');
        $delivery_date = $request->input('delivery_date');

        $tasks = TaskModel::where('order', $item_id)->get();
        foreach ($tasks as $task) {
            $task->start_date = $delivery_date;
            $task->save();

        }
        return redirect('admin/home');


    }
}
