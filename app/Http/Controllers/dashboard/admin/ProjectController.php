<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\dashboard\ConstantController;
use App\Models\ConstantModel;
use App\Models\OrderModel;
use App\Models\ProjectModel;
use App\Models\SpaceLocationModel;
use App\Models\SpaceModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Project;
use Validator;

class ProjectController extends Controller
{

    public static $path_view = "dashboard.main_views.admin.";
    public static $base = "admin";

    public function index(Request $request)
    {

        $data = array(
            'view_name' => ConstantController::$projects[$_SESSION['lang']],
            'view_parent' => null,
            'a_id' => "a_project",
            'view_url' => "/admin/projects",
            "projects" => ProjectModel::where('deleted', 0)->orderBy('created_at', 'desc')->get(),

        );
        return view($this::$path_view . 'projects', $data);

    }

    public function save_view(Request $request)
    {
        $data = array(
            'view_name' => ConstantController::$projects[$_SESSION['lang']],
            'view_parent' => null,
            'a_id' => "a_project",
            'view_url' => "/admin/projects",

        );
        return view($this::$path_view . 'add_project', $data);
    }

    public function edit_view(Request $request, $id)
    {
        $data = array(
            'view_name' => ConstantController::$projects[$_SESSION['lang']],
            'view_parent' => null,
            'a_id' => "a_project",
            'view_url' => "/admin/projects",
            "project" => ProjectModel::find($id),

        );
        return view($this::$path_view . 'edit_project', $data);
    }

    public function save(Request $request)
    {
        $item = new ProjectModel();

        $pro_find = ProjectModel::where('name_ar', $request->input('name_ar'))->get();
        if (sizeof($pro_find) > 0) {


            $request->session()->flash('error', "الاسم مستخدم من قبل ");
            return redirect(EmployeeController::$base . '/projects');
        }
        $pro_find = ProjectModel::where('name_en', $request->input('name_en'))->get();
        if (sizeof($pro_find) > 0) {


            $request->session()->flash('error', "الاسم مستخدم من قبل ");
            return redirect(EmployeeController::$base . '/projects');
        }

        $item->name_ar = $request->input('name_ar');
        $item->name_en = $request->input('name_en');
        $item->user = $request->input('user');
        $item->type = $request->input('type');
        $item->nature = $request->input('nature');
        $item->receive_date = $request->input('receive_date');
        $item->internal_space = $request->input('internal_space');
        $item->price = $request->input('price');
        $item->currency = $request->input('currency');
        $item->edit_direction = "\\" . "\\server\\Projects\\" . str_replace(" ", "_", $request->input('name_en'));
        $item->project_direction = "\\" . "\\pc-15\\Render_out\\" . str_replace(" ", "_", $request->input('name_en'));
        $item->ceiling_height = $request->input('ceiling_height');
        $item->type_condition = $request->input('type_condition');
        $item->space_condition = $request->input('space_condition');
        $item->user_note = $request->input('user_note');
        $item->technical_supervisor_note = $request->input('technical_supervisor_note');
        $item->planer_condition_file = $request->input('planer_condition_file');
        $item->added_by = Auth::user()->id;

//        if ($request->hasFile('planer_condition_file')) {
//
//
//            $image = $request->file('planer_condition_file');
//            $getimageName = time() . '.' . $image->getClientOriginalExtension();
//            $image->move('uploads/planer_condition_file', $getimageName);
//            $item->planer_condition_file = $getimageName;
//
//        }


        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$saved[$_SESSION['lang']]);
            return redirect(ProjectController::$base . '/projects');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(ProjectController::$base . '/projects');
        }

    }

    public function save_ajax(Request $request)
    {


        $rules = [
            'name_ar' => 'unique:project',
            'name_en' => 'unique:project',
        ];

        $messages = [
            'name_ar.unique' => "الاسم العربي مستخدم من قبل",
            'name_en.unique' => "الاسم الانكليزي مستخدم من قبل",

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $arr1 = $validator->errors()->toArray();
            $array_validation_error = array();
            foreach ($arr1 as $key => $ele1) {
                foreach ($ele1 as $ele2) {
                    array_push($array_validation_error, array('key' => $key, 'message' => $ele2));
                }
            }

            if (sizeof($array_validation_error) > 0) {
                return response()->json(['status' => 403, 'message' => $array_validation_error]);
            }

        }


        $item = new ProjectModel();
//
//        $pro_find = ProjectModel::where('name_ar', $request->input('name_ar'))->get();
//        if (sizeof($pro_find) > 0) {
//
//
//            $request->session()->flash('error', "الاسم مستخدم من قبل ");
//            return redirect(EmployeeController::$base . '/projects');
//        }
//        $pro_find = ProjectModel::where('name_en', $request->input('name_en'))->get();
//        if (sizeof($pro_find) > 0) {
//
//
//            $request->session()->flash('error', "الاسم مستخدم من قبل ");
//            return redirect(EmployeeController::$base . '/projects');
//        }

        $item->name_ar = $request->input('name_ar');
        $item->name_en = $request->input('name_en');
        $item->user = $request->input('user');
        $item->type = $request->input('type');
        $item->nature = $request->input('nature');
        $item->receive_date = $request->input('receive_date');
        $item->internal_space = $request->input('internal_space');
        $item->price = $request->input('price');
        $item->currency = $request->input('currency');
        $item->base_edit_dir =   ConstantModel::get_const("edit_dir");
        $item->base_project_dir =   ConstantModel::get_const("project_dir");
//        $item->edit_direction = "\\" . "\\server\\Projects\\" . str_replace(" ", "_", $request->input('name_en'));
//        $item->project_direction = "\\" . "\\pc-15\\Render_out\\" . str_replace(" ", "_", $request->input('name_en'));
//
        $item->edit_direction = ConstantModel::get_const("edit_dir") . str_replace(" ", "_", $request->input('name_en'));
        $item->project_direction = ConstantModel::get_const("project_dir") . str_replace(" ", "_", $request->input('name_en'));

        $item->ceiling_height = $request->input('ceiling_height');
        $item->type_condition = $request->input('type_condition');
        $item->space_condition = $request->input('space_condition');
        $item->user_note = $request->input('user_note');
        $item->technical_supervisor_note = $request->input('technical_supervisor_note');
        $item->planer_condition_file = $request->input('planer_condition_file');
        $item->added_by = Auth::user()->id;

//        if ($request->hasFile('planer_condition_file')) {
//
//
//            $image = $request->file('planer_condition_file');
//            $getimageName = time() . '.' . $image->getClientOriginalExtension();
//            $image->move('uploads/planer_condition_file', $getimageName);
//            $item->planer_condition_file = $getimageName;
//
//        }


        if ($item->save()) {

            return response()->json(['status' => 200, 'message' => [['message' => "" . ConstantController::$saved[$_SESSION['lang']]]]]);

        } else {


            return response()->json(['status' => 400, 'message' => [['message' => "" . ConstantController::$error[$_SESSION['lang']]]]]);


        }

    }


    public function edit(Request $request)
    {

        $rules = [
            'name_ar' => 'unique:project,name_ar,' . $request->input('item_id'),
            'name_en' => 'unique:project,name_en,' . $request->input('item_id'),
        ];

        $messages = [
            'name_ar.unique' => "الاسم العربي مستخدم من قبل",
            'name_en.unique' => "الاسم الانكليزي مستخدم من قبل",

        ];


        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {

            $arr1 = $validator->errors()->toArray();
            $array_validation_error = array();
            foreach ($arr1 as $key => $ele1) {
                foreach ($ele1 as $ele2) {
                    array_push($array_validation_error, array('key' => $key, 'message' => $ele2));
                }
            }

            if (sizeof($array_validation_error) > 0) {

                $request->session()->flash('error', $array_validation_error[0]['message']);
                return redirect(UserController::$base . '/edit_project/' . $request->input('item_id'));
//                return response()->json(['status' => 403, 'message' => $array_validation_error]);
            }

        }


        $id = $request->input('item_id');
        $item = ProjectModel::where('id', $request->input('item_id'))->first();


        $pro_find = ProjectModel::where('name_ar', $request->input('name_ar'))->where('id', '<>', $request->input('item_id'))->get();
        if (sizeof($pro_find) > 0) {


            $request->session()->flash('error', "الاسم مستخدم من قبل ");
            return redirect(ProjectController::$base . '/edit_project/' . $request->input('item_id'));
        }
        $pro_find = ProjectModel::where('name_en', $request->input('name_en'))->where('id', '<>', $request->input('item_id'))->get();
        if (sizeof($pro_find) > 0) {


            $request->session()->flash('error', "الاسم مستخدم من قبل ");
            return redirect(ProjectController::$base . '/edit_project/' . $request->input('item_id'));
        }

        $item->name_en = $request->input('name_en');

        if ($request->input('name_ar')) {
            $item->name_ar = $request->input('name_ar');

        }
        if ($request->input('user')) {
            $item->user = $request->input('user');

        }
        if ($request->input('type')) {
            $item->type = $request->input('type');

        }
        if ($request->input('nature')) {
            $item->nature = $request->input('nature');

        }
        if ($request->input('receive_date')) {
            $item->receive_date = $request->input('receive_date');

        }
        if ($request->input('technical_supervisor_note')) {
            $item->technical_supervisor_note = $request->input('technical_supervisor_note');

        }

        $item->internal_space = $request->input('internal_space');
        $item->price = $request->input('price');
        $item->currency = $request->input('currency');

        $item->edit_direction = $item->base_edit_dir. str_replace(" ", "_", $request->input('name_en'));
        $item->project_direction =$item->base_project_dir.str_replace(" ", "_", $request->input('name_en'));

//        $item->edit_direction = $request->input('edit_direction');
//        $item->project_direction = $request->input('project_direction');

        $item->ceiling_height = $request->input('ceiling_height');
        $item->type_condition = $request->input('type_condition');
        $item->space_condition = $request->input('space_condition');
        $item->user_note = $request->input('user_note');
        $item->planer_condition_file = $request->input('planer_condition_file');

        //  DB::select("update `ord3er` set order_direction='$item->edit_direction' where project=$id ");
        foreach (OrderModel::where('project', $id)->get() as $items) {

            $items->order_direction = $item->edit_direction;
            $items->save();
        }
//
//        if ($request->hasFile('planer_condition_file')) {
//
//            if (strlen($item->planer_condition_file) > 0) {
//
//                \File::delete("uploads/planer_condition_file/" . $item->planer_condition_file);
//
//            }
//
//
//            $image = $request->file('planer_condition_file');
//            $getimageName = time() . '.' . $image->getClientOriginalExtension();
//            $image->move('uploads/planer_condition_file', $getimageName);
//            $item->planer_condition_file = $getimageName;
//
//        }


        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$edit_successfully[$_SESSION['lang']]);
//            return redirect(ProjectController::$base . '/edit_project/'.$request->input('item_id'));

            return redirect(EmployeeController::$base . '/projects');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
//            return redirect(ProjectController::$base .  '/edit_project/'.$request->input('item_id'));

            return redirect(EmployeeController::$base . '/projects');
        }
    }

    public function remove(Request $request, $item_id)
    {

        if (sizeof(SpaceModel::where('project', $item_id)->where('deleted', 0)->get()) > 0) {
            $request->session()->flash('error', "الرجاء حذف جميع الفراغات الخاصة بالمشروع أولاً");
            return redirect(CustomerController::$base . '/projects');
        } else {
            $item = ProjectModel::where('id', $item_id)->first();


            if ($item->delete()) {
                $request->session()->flash('success', ConstantController::$deleted_successfully[$_SESSION['lang']]);
                return redirect(ProjectController::$base . '/projects');
            } else {

                $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
                return redirect(ProjectController::$base . '/projects');
            }
        }


    }

    public function check_validation_project(Request $request)
    {


        $rules = [
            'name_ar' => 'unique:project',];

        $messages = [
            'name_ar.unique' => "اسم المشروع مستخدم من قبل"
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $arr1 = $validator->errors()->toArray();
            $array_validation_error = array();
            foreach ($arr1 as $key => $ele1) {
                foreach ($ele1 as $ele2) {
                    array_push($array_validation_error, array('key' => $key, 'message' => $ele2));
                }
            }

            if (sizeof($array_validation_error) > 0) {

                return response()->json(['status' => 403, 'message' => $array_validation_error[0]['message']]);
            }

        }
    }

    public function get_spaces_project(Request $request)
    {
        $project = $request->input('elem_id');
        $spaces = SpaceModel::where('project', $project)->where('deleted', 0)->orderBy('location', 'asc')->get();
        foreach ($spaces as $pace) {
            $pace->location = SpaceLocationModel::find($pace->location)->name;
        }

        return response()->json(['status' => 200, 'data' => $spaces]);
    }
}
