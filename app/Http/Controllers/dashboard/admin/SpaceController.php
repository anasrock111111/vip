<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\dashboard\ConstantController;
use App\Models\SpaceModel;
use App\Models\TaskModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
class SpaceController extends Controller
{

    public static $path_view = "dashboard.main_views.admin.";
    public static $base = "admin";

    public function index(Request $request)
    {

        $data = array(

            'view_name' => ConstantController::$spaces[$_SESSION['lang']],
            'view_parent' => null,
            'a_id' => "a_space",
            'view_url' => "/admin/spaces",
            "spaces" => SpaceModel::where('deleted', 0)->orderBy('created_at', 'desc')->get(),

        );
        return view($this::$path_view . 'spaces', $data);

    }

    public function save_view(Request $request)
    {
        $data = array(
            'view_name' => ConstantController::$spaces[$_SESSION['lang']],
            'view_parent' => null,
            'a_id' => "a_space",
            'view_url' => "/admin/spaces",
            "project" => $request->input('project'),

        );
        return view($this::$path_view . 'add_space', $data);
    }

    public function edit_view(Request $request, $id)
    {
        $data = array(
            'view_name' => ConstantController::$spaces[$_SESSION['lang']],
            'view_parent' => null,
            'a_id' => "a_space",
            'view_url' => "/admin/spaces",
            "space" => SpaceModel::find($id),

        );
        return view($this::$path_view . 'edit_space', $data);
    }

    public function save(Request $request)
    {
        $item = new SpaceModel();

        $item->name = $request->input('name');
        $item->project = $request->input('project');
        $item->type = $request->input('type');
        $item->location = $request->input('location');
        $item->count = $request->input('count');
        $item->added_by = Auth::user()->id;


        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$saved[$_SESSION['lang']]);
            return redirect(SpaceController::$base . '/spaces');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(SpaceController::$base . '/spaces');
        }

    }

    public function save_ajax(Request $request)
    {



        $rules = [
            'project' => 'required',
            'type' => 'required',
            'location' => 'required',
            'name' => 'required',
        ];

        $messages = [
            'project.required' => "المشروع مطلوب",
            'type.required' => "نوع الفراغ  مطلوب",
            'location.required' => "موقع الفراغ مطلوب",
            'name.required' => "اسم الفراغ  مطلوب",
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {

            $arr1 = $validator->errors()->toArray();
            $array_validation_error = array();
            foreach ($arr1 as $key => $ele1) {
                foreach ($ele1 as $ele2) {
                    array_push($array_validation_error, array('key' => $key, 'message' => $ele2));
                }
            }

            if (sizeof($array_validation_error) > 0) {

                return response()->json(['status' => 403, 'message' => $array_validation_error]);
            }

        }

        $item = new SpaceModel();

        $item->name = $request->input('name');
        $item->project = $request->input('project');
        $item->type = $request->input('type');
        $item->location = $request->input('location');
        $item->count = $request->input('count');
        $item->added_by = Auth::user()->id;


        if ($item->save()) {

            return response()->json(['status' => 200, 'message' => [['message' => "" . ConstantController::$saved[$_SESSION['lang']]]]]);

        } else {


            return response()->json(['status' => 400, 'message' => [['message' => "" . ConstantController::$error[$_SESSION['lang']]]]]);
        }

    }


    public function edit(Request $request)
    {
        $item = SpaceModel::where('id', $request->input('item_id'))->first();


        $item->name = $request->input('name');
        $item->project = $request->input('project');
        $item->type = $request->input('type');
        $item->location = $request->input('location');
        $item->count = $request->input('count');


        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$edit_successfully[$_SESSION['lang']]);
//            return redirect(SpaceController::$base . '/edit_space/' . $request->input('item_id'));
            return redirect(SpaceController::$base . '/spaces');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
//            return redirect(SpaceController::$base . '/edit_space/' . $request->input('item_id'));
            return redirect(SpaceController::$base . '/spaces');
        }

    }

    public function remove(Request $request, $item_id)
    {


        if (sizeof(TaskModel::where('space', $item_id)->where('deleted', 0)->get()) > 0) {
            $request->session()->flash('error', "الرجاء حذف جميع المهام الخاصة بالفراغ أولاً");
            return redirect(CustomerController::$base . '/spaces');
        } else {
            $item = SpaceModel::where('id', $item_id)->first();


            if ($item->delete()) {
                $request->session()->flash('success', ConstantController::$deleted_successfully[$_SESSION['lang']]);
                return redirect(SpaceController::$base . '/spaces');
            } else {

                $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
                return redirect(SpaceController::$base . '/spaces');
            }

        }


    }
}
