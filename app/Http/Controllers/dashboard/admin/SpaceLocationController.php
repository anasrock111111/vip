<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\dashboard\ConstantController;
use App\Models\SpaceLocationModel;
use App\Models\SpaceModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SpaceLocationController extends Controller
{
    public static $path_view = "dashboard.main_views.admin.";
    public static $base = "admin";

    public function index(Request $request)
    {

        $data = array(
            'view_name' => ConstantController::$space_location[$_SESSION['lang']],
            'view_parent' => null,
            'a_id' => "a_space_location",
            'view_url' => "/admin/space_locations",
            "space_locations" => SpaceLocationModel::where('deleted', 0)->get(),

        );
        return view($this::$path_view . 'space_locations', $data);

    }


    public function save(Request $request)
    {
        $item = new SpaceLocationModel();

        $item->name = $request->input('name');
        $item->added_by = Auth::user()->id;

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$saved[$_SESSION['lang']]);
            return redirect(SpaceLocationController::$base . '/space_locations');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(SpaceLocationController::$base . '/space_locations');
        }

    }


    public function edit(Request $request)
    {
        $item = SpaceLocationModel::where('id', $request->input('item_id'))->first();
        $item->name = $request->input('name');

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$edit_successfully[$_SESSION['lang']]);
            return redirect(SpaceLocationController::$base . '/space_locations');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(SpaceLocationController::$base . '/space_locations');
        }
    }

    public function remove(Request $request, $item_id)
    {

        if (sizeof(SpaceModel::where("location", $item_id)->get()) > 0) {
            $request->session()->flash('error', "الرجاء حذف جميع الفراغات المرتبطةأولاً");
            return redirect(SpaceLocationController::$base . '/space_locations');
        } else {
            $item = SpaceLocationModel::where('id', $item_id)->first();


            if ($item->delete()) {
                $request->session()->flash('success', ConstantController::$deleted_successfully[$_SESSION['lang']]);
                return redirect(SpaceLocationController::$base . '/space_locations');
            } else {

                $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
                return redirect(SpaceLocationController::$base . '/space_locations');
            }

        }


    }
}
