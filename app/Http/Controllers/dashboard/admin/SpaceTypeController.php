<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\dashboard\ConstantController;
use App\Models\SpaceModel;
use App\Models\SpaceTypeModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SpaceTypeController extends Controller
{

    public static $path_view = "dashboard.main_views.admin.";
    public static $base = "admin";

    public function index(Request $request)
    {

        $data = array(
            'view_name' => ConstantController::$space_type[$_SESSION['lang']],
            'view_parent' => null,
            'a_id' => "a_space_type",
            'view_url' => "/admin/space_types",
            "space_types" => SpaceTypeModel::where('deleted', 0)->get(),

        );
        return view($this::$path_view . 'space_types', $data);

    }


    public function save(Request $request)
    {
        $item = new SpaceTypeModel();

        $item->name = $request->input('name');
        $item->added_by = Auth::user()->id;

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$saved[$_SESSION['lang']]);
            return redirect(SpaceTypeController::$base . '/space_types');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(SpaceTypeController::$base . '/space_types');
        }

    }


    public function edit(Request $request)
    {
        $item = SpaceTypeModel::where('id', $request->input('item_id'))->first();
        $item->name = $request->input('name');

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$edit_successfully[$_SESSION['lang']]);
            return redirect(SpaceTypeController::$base . '/space_types');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(SpaceTypeController::$base . '/space_types');
        }
    }

    public function remove(Request $request, $item_id)
    {

        if (sizeof(SpaceModel::where("type", $item_id)->get()) > 0) {
            $request->session()->flash('error', "الرجاء حذف جميع الفراغات المرتبطةأولاً");
            return redirect(SpaceTypeController::$base . '/space_types');
        } else {
            $item = SpaceTypeModel::where('id', $item_id)->first();


            if ($item->delete()) {
                $request->session()->flash('success', ConstantController::$deleted_successfully[$_SESSION['lang']]);
                return redirect(SpaceTypeController::$base . '/space_types');
            } else {

                $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
                return redirect(SpaceTypeController::$base . '/space_types');
            }

        }


    }
}
