<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\dashboard\ConstantController;
use App\Models\StudyTypeModel;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class StudyTypeConroller extends Controller
{

    public static $path_view = "dashboard.main_views.admin.";
    public static $base = "admin";

    public function index(Request $request)
    {

        $data = array(
            'view_name' => "أنواع الدراسة",
            'view_parent' => null,
            'a_id' => "a_study_type",
            'view_url' => "/admin/study_types",
            "study_types" => StudyTypeModel::where('deleted', 0)->get(),

        );
        return view($this::$path_view . 'study_types', $data);

    }


    public function save(Request $request)
    {
        $item = new StudyTypeModel();

        $item->name = $request->input('name');
        $item->added_by = Auth::user()->id;

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$saved[$_SESSION['lang']]);
            return redirect(StudyTypeConroller::$base . '/study_types');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(StudyTypeConroller::$base . '/study_types');
        }

    }


    public function edit(Request $request)
    {
        $item = StudyTypeModel::where('id', $request->input('item_id'))->first();
        $item->name = $request->input('name');

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$edit_successfully[$_SESSION['lang']]);
            return redirect(StudyTypeConroller::$base . '/study_types');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(StudyTypeConroller::$base . '/study_types');
        }
    }

    public function remove(Request $request, $item_id)
    {

        if (sizeof(User::where("study_type", $item_id)->get()) > 0) {
            $request->session()->flash('error', "الرجاء حذف جميع الموظفين المرتبطين أولاً");
            return redirect(StudyTypeConroller::$base . '/study_types');
        } else {
            $item = StudyTypeModel::where('id', $item_id)->first();


            if ($item->delete()) {
                $request->session()->flash('success', ConstantController::$deleted_successfully[$_SESSION['lang']]);
                return redirect(StudyTypeConroller::$base . '/study_types');
            } else {

                $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
                return redirect(StudyTypeConroller::$base . '/study_types');
            }
        }


    }
}
