<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\dashboard\ConstantController;
use App\Models\OrderModel;
use App\Models\ProjectModel;
use App\Models\SpaceLocationModel;
use App\Models\SpaceModel;
use App\Models\TaskCommentModel;
use App\Models\TaskEditTypeModel;
use App\Models\TaskModel;
use App\Models\TaskTypeModel;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;

class TaskController extends Controller
{

    public static $path_view = "dashboard.main_views.admin.";
    public static $base = "admin";

    public function index(Request $request)
    {


        $order_id_get = $request->input('order_id');

        if (Auth::user()->type == "technical_supervisor") {

            $date_today = date('Y-m-d');
            $tasks_today_final = array();

            if ($order_id_get > 0) {
                $tasks_today = $order_delivery_today_ = DB::select("
            select * from `task`
            where  ((start_date='$date_today' ) or (start_date<'$date_today' and finished=0  ))
            and deleted=0 and collected=0
            and task.order= $order_id_get
            and employee <> null 
            order by created_at desc
            ");

            } else {
                $tasks_today = $order_delivery_today_ = DB::select("
            select * from `task`
            where  ((start_date='$date_today' ) or (start_date<'$date_today' and finished=0  ))
            and deleted=0 and collected=0
            and employee <> null 
            order by created_at desc
            ");
            }

            foreach ($tasks_today as $task) {
                if (OrderModel::find($task->order)->delivered == 0) {
                    array_push($tasks_today_final, $task);
                }
            }

            $date_today = date('Y-m-d');
            $next_date = date('Y-m-d', strtotime($date_today . " + 1 day"));

            $next_date_ = "";
            if (date('D', strtotime($date_today)) === 'Thu') {
                $next_date_ = date('Y-m-d', strtotime($date_today . " + 2 day"));
            }

            if ($order_id_get > 0) {
                $orders_next_day = TaskModel::where('deleted', 0)->whereIn('start_date', [$next_date, $next_date_])->where('order', $order_id_get)->where('deleted', 0)->where('collected', 0)->where('employee', '<>', null)->orderBy('created_at', 'desc')->get();

            } else {
                $orders_next_day = TaskModel::where('deleted', 0)->whereIn('start_date', [$next_date, $next_date_])->where('deleted', 0)->where('collected', 0)->where('employee', '<>', null)->orderBy('created_at', 'desc')->get();

            }
            $orders_next_day_final = array();
            foreach ($orders_next_day as $task) {
                if (OrderModel::find($task->order)->delivered == 0) {
                    array_push($orders_next_day_final, $task);
                }
            }
            $arr_of_ids = array();
            foreach ($tasks_today_final as $item) {
                array_push($arr_of_ids, $item->id);
            }
            foreach ($orders_next_day_final as $item) {
                array_push($arr_of_ids, $item->id);
            }


            if ($order_id_get > 0) {

                $all_tasks = TaskModel::where('deleted', 0)->where('order', $order_id_get)->orderBy('created_at', 'desc')->get();
            } else {

                $all_tasks = TaskModel::where('deleted', 0)->orderBy('created_at', 'desc')->get();
            }

            $final_all_tasks = array();
            foreach ($all_tasks as $task) {
                if (OrderModel::find($task->order)->delivered == 0) {
                    array_push($final_all_tasks, $task);
                }
            }


            $task_ready_to_recive = TaskModel::where('technical_supervisor_receive', 0)->where('finished', 1)->where('deleted', 0)->get();
            $task_ready_to_recive_final = array();
            foreach ($task_ready_to_recive as $task) {
                if (OrderModel::find($task->order)->delivered == 0) {
                    array_push($task_ready_to_recive_final, $task);
                }
            }


            $data = array(
                'view_name' => ConstantController::$tasks[$_SESSION['lang']],
                'view_parent' => null,
                'a_id' => "a_task",
                'view_url' => "/admin/tasks",
                'tasks_today' => $tasks_today_final,
                'task_next_day' => $orders_next_day_final,
                "tasks" => $final_all_tasks,
                "task_ready_to_recive" => $task_ready_to_recive_final,
                "tasks_with_out_employee" => TaskModel::where('deleted', 0)->where('employee', '=', null)->orderBy('created_at', 'desc')->get(),

            );

        } else {

            $data = array(
                'view_name' => ConstantController::$tasks[$_SESSION['lang']],
                'view_parent' => null,
                'a_id' => "a_task",
                'view_url' => "/admin/tasks",
                "tasks" => TaskModel::where('deleted', 0)->orderBy('created_at', 'desc')->get(),
                "tasks_with_out_employee" => TaskModel::where('deleted', 0)->where('employee', '=', null)->orderBy('created_at', 'desc')->get(),

            );
        }

        return view($this::$path_view . 'tasks', $data);

    }

    public function save_view(Request $request)
    {

        $data = array(
            'view_name' => ConstantController::$tasks[$_SESSION['lang']],
            'view_parent' => null,
            'a_id' => "a_task",
            'view_url' => "/admin/add_task",
            "project" => $request->input('project'),
            "order" => $request->input('order'),

        );
        return view($this::$path_view . 'add_task', $data);
    }

    public function edit_view(Request $request, $id)
    {


        $data = array(
            'view_name' => ConstantController::$tasks[$_SESSION['lang']],
            'view_parent' => null,
            'a_id' => "a_task",
            'view_url' => "/admin/tasks",
            "task" => TaskModel::find($id),
        );
        return view($this::$path_view . 'edit_task', $data);
    }

    public function save(Request $request)
    {


        $item = new TaskModel();

        $item->project = $request->input('project');
        $item->order = $request->input('order');
        $item->space = $request->input('space');
        $item->type = $request->input('type');
        if ($request->input('edit_type') > 0) {

            $item->edit_type = $request->input('edit_type');

        }
        $item->start_date = $request->input('start_date');
        $item->employee = $request->input('employee');
        $item->task_direction = $request->input('task_direction');
        $item->technical_supervisor = $request->input('technical_supervisor');
        $item->added_by = Auth::user()->id;

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$saved[$_SESSION['lang']]);
            return redirect(TaskController::$base . '/tasks');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(TaskController::$base . '/tasks');
        }

    }


    public function edit(Request $request)
    {
        $item = TaskModel::where('id', $request->input('item_id'))->first();

        $item->project = $request->input('project');
        $item->order = $request->input('order');
        $item->space = $request->input('space');
        $item->type = $request->input('type');
        if ($request->input('edit_type') > 0) {
            $item->edit_type = $request->input('edit_type');

        }
        $item->start_date = $request->input('start_date');
        $item->employee = $request->input('employee');
        $item->task_direction = $request->input('task_direction');
        $item->technical_supervisor = $request->input('technical_supervisor');

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$edit_successfully[$_SESSION['lang']]);
//            return redirect(ProjectController::$base . '/edit_task/' . $request->input('item_id'));

            return redirect(OrderController::$base . '/tasks');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
//            return redirect(ProjectController::$base . '/edit_task/' . $request->input('item_id'));

            return redirect(OrderController::$base . '/tasks');
        }
    }

    public function remove(Request $request, $item_id)
    {


        $item = TaskModel::where('id', $item_id)->first();


        if ($item->delete()) {
            $request->session()->flash('success', ConstantController::$deleted_successfully[$_SESSION['lang']]);
            return redirect(TaskController::$base . '/tasks');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(TaskController::$base . '/tasks');
        }

    }

    public function get_orders_spaces(Request $request)
    {

        $project = $request->input('project');

        $order = OrderModel::where('project', $project)->where('deleted', 0)->orderBy('created_at', 'desc')->where('delivered', 0)->get();
        foreach ($order as $item_) {
            if ($item_->delivery_date != null) {

                $item_->date_received = date('Y-m-d', strtotime(date('Y-m-d', strtotime($item_->delivery_date . " -1 days"))));
            } else {

                $item_->date_received = null;
            }

            $item_->order_direction = date('Y-m-d', strtotime($item_->created_at));
        }
        $spaces = SpaceModel::where('project', $project)->where('deleted', 0)->get();
        foreach ($spaces as $item) {
            $item->location = SpaceLocationModel::find($item->location)->name;
        }

        return response()->json(['status' => 200, 'orders' => $order, 'space' => $spaces]);

    }

    public function technical_supervisor_receive_task(Request $request, $id)
    {
        $item = TaskModel::where('id', $id)->first();

        $item->technical_supervisor_receive = 1;
        $item->technical_supervisor_receive_date = date('Y-m-d H:i:s');

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$edit_successfully[$_SESSION['lang']]);
            return redirect(OrderController::$base . '/home');

        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(OrderController::$base . '/home');
        }
    }

    public function technical_supervisor_receive_task_post(Request $request)
    {
        $item = TaskModel::where('id', $request->input('task_id'))->first();

        $item->technical_supervisor_receive = 1;
        $item->rate = $request->input('rate');
        $item->technical_supervisor_receive_date = date('Y-m-d H:i:s');

        if ($item->save()) {
            $request->session()->flash('success', ConstantController::$edit_successfully[$_SESSION['lang']]);
            return redirect(OrderController::$base . '/home');

        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(OrderController::$base . '/home');
        }
    }

    public function task_comments(Request $request)
    {

        $id = $request->input('elem_id');
        $coments = TaskCommentModel::where('task', $id)->where('deleted', 0)->get();

        foreach ($coments as $item) {

            if ($item->added_type == "employee") {
                $comm = new TaskCommentModel();

                $comm = $item;
                $comm->seen = 1;
                $comm->save();
            }

            $item->user = User::find($item->user)->name;
        }

        return response()->json(['status' => 200, 'comment' => $coments]);
    }

    public function tasks_order(Request $request)
    {

        $id = $request->input('elem_id');
        $items = TaskModel::where('order', $id)->where('deleted', 0)->get();

        foreach ($items as $item) {

            $item->added_by = User::find($item->added_by)->name;
            $item->project = ProjectModel::find($item->project)->name_ar;

            if($item->space>0)
            {
                $sp = SpaceModel::find($item->space);
                $item->space = SpaceLocationModel::find($sp->location)->name . " - " . $sp->name;
            }
            else
            {

                $item->space =  $item->note_plan ;
            }
            $item->type = TaskTypeModel::find($item->type)->name;
            if ($item->employee > 0) {

                $item->employee = User::find($item->employee)->name;
            } else {

                $item->employee = "";
            }
            $item->end_date = $item->end_date . "";
            $item->start_date = $item->start_date . "";
            $item->rate = $item->rate . "";
            $item->edit_type = $item->edit_type . "";
            $item->collected_date = $item->collected_date . "";
            $item->technical_supervisor_receive_date = $item->technical_supervisor_receive_date . "";
            $item->technical_supervisor = User::find($item->technical_supervisor)->name;

            if ($item->edit_type != null) {
                $item->edit_type = TaskEditTypeModel::find($item->edit_type)->name;
            }

        }


        return response()->json(['status' => 200, 'items' => $items]);
    }


    public function tasks_history(Request $request)
    {

        $id = $request->input('elem_id');
        $task = TaskModel::find($id);
        $items = TaskModel::where('space', $task->space)->where('deleted', 0)->get();

        foreach ($items as $item) {

            $item->order = OrderModel::find($item->order)->order_number ." | ". OrderModel::find($item->order)->delivery_date;
            $item->type = TaskTypeModel::find($item->type)->name;
            if ($item->employee > 0) {

                $item->employee = User::find($item->employee)->name;
            } else {

                $item->employee = "";
            }
            $item->edit_type = $item->edit_type . "";
            if ($item->edit_type != null) {
                $item->edit_type = TaskEditTypeModel::find($item->edit_type)->name;
            }

        }


        return response()->json(['status' => 200, 'items' => $items]);
    }

    public function get_tasks_order(Request $request)
    {

        $id = $request->input('elem_id');
        $items = TaskModel::where('order', $id)->where('deleted', 0)->get();

        foreach ($items as $item) {

            $item->added_by = User::find($item->added_by)->name;
            $item->project = ProjectModel::find($item->project)->name_ar;
            $item->space = SpaceModel::find($item->space)->name;
            $item->type = TaskTypeModel::find($item->type)->name;
            if ($item->employee > 0) {

                $item->employee = User::find($item->employee)->name;
            } else {

                $item->employee = "";
            }
            $item->order = OrderModel::find($item->order)->order_number;
            $item->technical_supervisor = User::find($item->technical_supervisor)->name;
            if ($item->edit_type != null) {
                $item->edit_type = TaskEditTypeModel::find($item->edit_type)->name;
            }

        }


        return response()->json(['status' => 200, 'items' => $items]);
    }


    public function add_comment(Request $request)
    {

        $task_id = $request->input('task_id');

        $comment = $request->input('comment');
        $employee = Auth::user()->id;

        $add_comm = new TaskCommentModel();
        $add_comm->task = $task_id;
        $add_comm->user = $employee;
        $add_comm->comment = $comment;
        $add_comm->added_type = Auth::user()->type;

        if ($add_comm->save()) {

            $task = TaskModel::find($task_id);
            $task->have_inquiry = $request->input('have_inquiry');

            $task->save();
            $request->session()->flash('success', ConstantController::$saved[$_SESSION['lang']]);
            return redirect(TaskController::$base . '/tasks/');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(TaskController::$base . '/tasks/');
        }

    }

    public function add_task_ajax(Request $request)
    {


        if ($request->input('type') == 2) {

            $rules = [
                'project' => 'required',
                'order' => 'required',
//                'space' => 'required',
                'type' => 'required',
//                'employee' => 'required',
                'edit_type' => 'required',
            ];

        } else {
            $rules = [
                'project' => 'required',
                'order' => 'required',
//                'space' => 'required',
                'type' => 'required',
//                'employee' => 'required',
            ];


        }


        $messages = [
            'project.required' => "المشروع مطلوب",
            'order.required' => "الطلب مطلوب",
            'space.required' => "الفراغ مطلوب",
            'type.required' => "نوع المهمة مطلوب",
            'edit_type.required' => "نوع التعديل المهمة مطلوب",
            'employee.required' => "الموظف مطلوب",

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {

            $arr1 = $validator->errors()->toArray();
            $array_validation_error = array();
            foreach ($arr1 as $key => $ele1) {
                foreach ($ele1 as $ele2) {
                    array_push($array_validation_error, array('key' => $key, 'message' => $ele2));
                }
            }

            if (sizeof($array_validation_error) > 0) {

                return response()->json(['status' => 403, 'message' => $array_validation_error]);
            }

        }


        if ($request->input('if_more_than') == 0) {

            if (sizeof(TaskModel::where('space', $request->input('space'))->where('order', $request->input('order'))->where('deleted', 0)->get()) > 0) {

                return response()->json(['status' => 200, 'added' => 0, 'more_than' => 1]);

            } else {


                $item = new TaskModel();

                $item->project = $request->input('project');
                $item->order = $request->input('order');
                $item->space = $request->input('space');
                $item->type = $request->input('type');
                if ($request->input('type') == 2) {

                    if ($request->input('edit_type') > 0) {


                        $item->edit_type = $request->input('edit_type');

                    } else {
                        return response()->json(['status' => 403, 'more_than' => 1, 'message' => [['message' => "اختر نوع التعديل اولاً"]]]);
                    }

                }


                $item->start_date = $request->input('start_date');
                $item->employee = $request->input('employee');
                $item->task_direction = $request->input('task_direction');
                $item->technical_supervisor = $request->input('technical_supervisor');
                $item->added_by = Auth::user()->id;
                $item->note_plan = $request->input('note_plan');


                if ($item->save()) {

                    if (sizeof(TaskModel::where('space', $request->input('space'))->where('deleted', 0)->get()) > 0) {

                        return response()->json(['status' => 200, 'added' => 1, 'more_than' => 1]);
                    } else {
                        return response()->json(['status' => 200, 'added' => 1, 'more_than' => 0]);
                    }


                } else {

                    return response()->json(['status' => 410, 'added' => 0, 'more_than' => 0]);
                }

                //    return response()->json(['status' => 200, 'added' => 0, 'more_than' => 0]);
            }

        } else {


            $item = new TaskModel();

            $item->project = $request->input('project');
            $item->order = $request->input('order');
            $item->space = $request->input('space');
            $item->type = $request->input('type');
            if ($request->input('type') == 2) {

                if ($request->input('edit_type') > 0) {


                    $item->edit_type = $request->input('edit_type');

                } else {
                    return response()->json(['status' => 403, 'more_than' => 1, 'message' => [['message' => "اختر نوع التعديل اولاً"]]]);
                }

            }


            $item->start_date = $request->input('start_date');
            $item->employee = $request->input('employee');
            $item->task_direction = $request->input('task_direction');
            $item->technical_supervisor = $request->input('technical_supervisor');
            $item->added_by = Auth::user()->id;
            $item->note_plan = $request->input('note_plan');


            if ($item->save()) {

                if (sizeof(TaskModel::where('space', $request->input('space'))->where('deleted', 0)->get()) > 0) {

                    return response()->json(['status' => 200, 'added' => 1, 'more_than' => 1]);
                } else {
                    return response()->json(['status' => 200, 'added' => 1, 'more_than' => 0]);
                }


            } else {


                return response()->json(['status' => 410, 'added' => 0, 'more_than' => 0]);
            }


        }


    }


    //vasyco VA12345678

    public function add_have_inquiry(Request $request, $id)
    {
        $item_id = $id;

        $item = TaskModel::where('id', $item_id)->first();
        if ($item->have_inquiry == 0) {

            $item->have_inquiry = 1;
        } else {

            $item->have_inquiry = 0;
        }


        if ($item->save()) {


            $request->session()->flash('success', ConstantController::$saved[$_SESSION['lang']]);
            return redirect(TaskController::$base . '/tasks/');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(TaskController::$base . '/tasks/');
        }
    }


    public function check_order_have_tasks(Request $request)
    {

        if (sizeof(TaskModel::where('space', $request->input('space'))->where('deleted', 0)->get())) {

            return response()->json(['status' => 200, 'more_than' => 1]);

        } else {

            return response()->json(['status' => 200, 'more_than' => 0]);
        }


    }

}
