<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\dashboard\ConstantController;
use App\Models\TaskEditTypeModel;
use App\Models\TaskModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TaskEditTypeController extends Controller
{
    public static $path_view = "dashboard.main_views.admin.";
    public static $base = "admin";

    public function index(Request $request)
    {

        $data = array(
            'view_name' => ConstantController::$TasksEditType[$_SESSION['lang']],
            'view_parent' => null,
            'a_id' => "a_task_edit_type",
            'view_url' => "/admin/task_edit_types",
            "task_types" => TaskEditTypeModel::where('deleted', 0)->get(),

        );
        return view($this::$path_view . 'task_edit_types', $data);

    }


    public function save(Request $request)
    {
        $item = new TaskEditTypeModel();

        $item->name = $request->input('name');
        $item->added_by = Auth::user()->id;

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$saved[$_SESSION['lang']]);
            return redirect(TaskEditTypeController::$base . '/task_edit_types');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(TaskEditTypeController::$base . '/task_edit_types');
        }

    }


    public function edit(Request $request)
    {
        $item = TaskEditTypeModel::where('id', $request->input('item_id'))->first();
        $item->name = $request->input('name');

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$edit_successfully[$_SESSION['lang']]);
            return redirect(TaskEditTypeController::$base . '/task_edit_types');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(TaskEditTypeController::$base . '/task_edit_types');
        }
    }

    public function remove(Request $request, $item_id)
    {

        if (sizeof(TaskModel::where("edit_type", $item_id)->get()) > 0) {
            $request->session()->flash('error', "الرجاء حذف جميع المهام المرتبطةأولاً");
            return redirect(TaskEditTypeController::$base . '/task_edit_types');
        } else {
            $item = TaskEditTypeModel::where('id', $item_id)->first();


            if ($item->delete()) {
                $request->session()->flash('success', ConstantController::$deleted_successfully[$_SESSION['lang']]);
                return redirect(TaskEditTypeController::$base . '/task_edit_types');
            } else {

                $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
                return redirect(TaskEditTypeController::$base . '/task_edit_types');
            }

        }



    }
}
