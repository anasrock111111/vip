<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\dashboard\ConstantController;
use App\Models\TaskModel;
use App\Models\TaskTypeModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TaskTypeController extends Controller
{
    public static $path_view = "dashboard.main_views.admin.";
    public static $base = "admin";

    public function index(Request $request)
    {

        $data = array(
            'view_name' => ConstantController::$TasksType[$_SESSION['lang']],
            'view_parent' => null,
            'a_id' => "a_task_type",
            'view_url' => "/admin/task_types",
            "task_types" => TaskTypeModel::where('deleted', 0)->get(),

        );
        return view($this::$path_view . 'task_types', $data);

    }


    public function save(Request $request)
    {
        $item = new TaskTypeModel();

        $item->name = $request->input('name');
        $item->added_by = Auth::user()->id;

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$saved[$_SESSION['lang']]);
            return redirect(TaskTypeController::$base . '/task_types');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(TaskTypeController::$base . '/task_types');
        }

    }


    public function edit(Request $request)
    {
        $item = TaskTypeModel::where('id', $request->input('item_id'))->first();
        $item->name = $request->input('name');

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$edit_successfully[$_SESSION['lang']]);
            return redirect(TaskTypeController::$base . '/task_types');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(TaskTypeController::$base . '/task_types');
        }
    }

    public function remove(Request $request, $item_id)
    {

        if (sizeof(TaskModel::where("type", $item_id)->get()) > 0) {
            $request->session()->flash('error', "الرجاء حذف جميع المهام المرتبطةأولاً");
            return redirect(TaskTypeController::$base . '/task_types');
        } else {

            $item = TaskTypeModel::where('id', $item_id)->first();


            if ($item->delete()) {
                $request->session()->flash('success', ConstantController::$deleted_successfully[$_SESSION['lang']]);
                return redirect(TaskTypeController::$base . '/task_types');
            } else {

                $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
                return redirect(TaskTypeController::$base . '/task_types');
            }

        }



    }
}
