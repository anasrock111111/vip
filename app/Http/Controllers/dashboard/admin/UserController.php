<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\dashboard\ConstantController;
use App\Models\CliceModel;
use App\Models\DepartmentModel;
use App\Models\NatureProjectModel;
use App\Models\OrderModel;
use App\Models\ProjectModel;
use App\Models\SpaceLocationModel;
use App\Models\SpaceModel;
use App\Models\SpaceTypeModel;
use App\Models\StudyTypeModel;
use App\Models\TaskEditTypeModel;
use App\Models\TaskModel;
use App\Models\TaskTypeModel;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserController extends Controller
{
    public static $path_view = "dashboard.main_views.admin.";
    public static $base = "admin";

    public function index(Request $request)
    {

        $data = array(
            'view_name' => ConstantController::$users[$_SESSION['lang']],
            'view_parent' => null,
            'a_id' => "a_user",
            'view_url' => "/admin/users",
            "users" => User::where('id', '<>', Auth::user()->id)->where('type', '<>', 'employee')->where('type', '<>', 'customer')->where('type', '<>', 'user_get_work')->where('deleted', 0)->get(),
            //  "users" => User::where('type', '<>', 'employee')->where('type', '<>', 'customer')->where('type', '<>', 'user_get_work')->where('deleted', 0)->get(),

        );
        return view($this::$path_view . 'users', $data);

    }


    public function save(Request $request)
    {
        $item = new User();

        $item->name = $request->input('name');
        $item->email = $request->input('email');
        $item->password = bcrypt($request->input('password'));
        $item->type = $request->input('type');
        $item->added_by = Auth::user()->id;

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$saved[$_SESSION['lang']]);
            return redirect(UserController::$base . '/users');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(UserController::$base . '/users');
        }

    }

    public function save_ajax(Request $request)
    {


        $rules = [
            'name' => 'required|unique:user',
            'email' => 'required|email|unique:user',
        ];

        $messages = [
            'name.required' => "الاسم مطلوب",
            'name.unique' => "الاسم موجود",
            'email.required' => "الاسم مطلوب",
            'email.email' => "البريد الالكتروني غير صالح",
            'email.unique' => "البريد الالكتروني موجود",
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {

            $arr1 = $validator->errors()->toArray();
            $array_validation_error = array();
            foreach ($arr1 as $key => $ele1) {
                foreach ($ele1 as $ele2) {
                    array_push($array_validation_error, array('key' => $key, 'message' => $ele2));
                }
            }

            if (sizeof($array_validation_error) > 0) {

                return response()->json(['status' => 403, 'message' => $array_validation_error]);
            }

        }

        $item = new User();

        $item->name = $request->input('name');
        $item->email = $request->input('email');
        $item->password = bcrypt($request->input('password'));
        $item->type = $request->input('type');
        $item->added_by = Auth::user()->id;

        if ($item->save()) {

            return response()->json(['status' => 200, 'message' => [['message' => "" . ConstantController::$saved[$_SESSION['lang']]]]]);

        } else {

            return response()->json(['status' => 400, 'message' => [['message' => "" . ConstantController::$error[$_SESSION['lang']]]]]);


        }

    }


    public function edit(Request $request)
    {


        $rules = [
            'name' => 'required|unique:user,name,'. $request->input('item_id'),
            'email' => 'required|email|unique:user,email,'. $request->input('item_id'),
        ];

        $messages = [
            'name.required' => "الاسم مطلوب",
            'name.unique' => "الاسم موجود",
            'email.required' => "الاسم مطلوب",
            'email.email' => "البريد الالكتروني غير صالح",
            'email.unique' => "البريد الالكتروني موجود",
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {

            $arr1 = $validator->errors()->toArray();
            $array_validation_error = array();
            foreach ($arr1 as $key => $ele1) {
                foreach ($ele1 as $ele2) {
                    array_push($array_validation_error, array('key' => $key, 'message' => $ele2));
                }
            }

            if (sizeof($array_validation_error) > 0) {

                $request->session()->flash('error', $array_validation_error[0]['message']);
                return redirect(UserController::$base . '/users');
//                return response()->json(['status' => 403, 'message' => $array_validation_error]);
            }

        }



        $item = User::where('id', $request->input('item_id'))->first();
        $item->name = $request->input('name');
        $item->email = $request->input('email');
        if ($request->input('password')) {

            $item->password = bcrypt($request->input('password'));
        }
        $item->type = $request->input('type');
        $item->added_by = Auth::user()->id;

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$edit_successfully[$_SESSION['lang']]);
            return redirect(UserController::$base . '/users');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(UserController::$base . '/users');
        }
    }

    public function remove(Request $request, $item_id)
    {


        $item = User::where('id', $item_id)->first();

        $this->check_added_by($request, $item_id);
        if (sizeof(CliceModel::where('added_by', $item_id)->get()) > 0) {

            $request->session()->flash('error', "يجب  حذف الكليشات المرتبطة ");
            return redirect(UserController::$base . '/users');
        }


        if (sizeof(DepartmentModel::where('added_by', $item_id)->get()) > 0) {

            $request->session()->flash('error', "يجب حذف اختصاصات الموظفين المرتبطة ");
            return redirect(UserController::$base . '/users');
        }

        if (sizeof(OrderModel::where('added_by', $item_id)->get()) > 0) {

            $request->session()->flash('error', "يجب حذف الطلبات المرتبطة ");
            return redirect(UserController::$base . '/users');
        }

        if (sizeof(ProjectModel::where('added_by', $item_id)->get()) > 0) {

            $request->session()->flash('error', "يجب حذف المشاريع المرتبطة ");
            return redirect(UserController::$base . '/users');
        }
        if (sizeof(NatureProjectModel::where('added_by', $item_id)->get()) > 0) {

            $request->session()->flash('error', "يجب حذف طبيعة المشاريع المرتبطة ");
            return redirect(UserController::$base . '/users');
        }
        if (sizeof(SpaceModel::where('added_by', $item_id)->get()) > 0) {

            $request->session()->flash('error', "يجب حذف الفراغات المرتبطة ");
            return redirect(UserController::$base . '/users');
        }

        if (sizeof(SpaceLocationModel::where('added_by', $item_id)->get()) > 0) {

            $request->session()->flash('error', "يجب حذف مواقع الفراغات المرتبطة ");
            return redirect(UserController::$base . '/users');
        }
        if (sizeof(SpaceTypeModel::where('added_by', $item_id)->get()) > 0) {

            $request->session()->flash('error', "يجب حذف أنواع الفراغات المرتبطة ");
            return redirect(UserController::$base . '/users');
        }

        if (sizeof(StudyTypeModel::where('added_by', $item_id)->get()) > 0) {

            $request->session()->flash('error', "يجب حذف أنواع الدراسة المرتبطة ");
            return redirect(UserController::$base . '/users');
        }

        if (sizeof(TaskModel::where('added_by', $item_id)->get()) > 0) {

            $request->session()->flash('error', "يجب حذف المهام المرتبطة ");
            return redirect(UserController::$base . '/users');
        }

        if (sizeof(TaskTypeModel::where('added_by', $item_id)->get()) > 0) {

            $request->session()->flash('error', "يجب حذف أنواع المهام المرتبطة ");
            return redirect(UserController::$base . '/users');
        }
        if (sizeof(TaskEditTypeModel::where('added_by', $item_id)->get()) > 0) {

            $request->session()->flash('error', "يجب حذف أنواع  التعديل المهام المرتبطة ");
            return redirect(UserController::$base . '/users');
        }
        if (sizeof(User::where('added_by', $item_id)->get()) > 0) {

            $request->session()->flash('error', "يجب حذف المستخدمين المرتبطة ");
            return redirect(UserController::$base . '/users');
        }
        if ($item->type == "technical_supervisor") {

            if (sizeof(TaskModel::where('technical_supervisor', $item_id)->get()) > 0) {

                $request->session()->flash('error', "يجب حذف المهام المرتبطة ");
                return redirect(UserController::$base . '/users');
            }

        }

        if ($item->type == "user_collect") {

            if (sizeof(TaskModel::where('collected_by', $item_id)->get()) > 0) {

                $request->session()->flash('error', "يجب حذف المهام المرتبطة ");
                return redirect(UserController::$base . '/users');
            }

        }



        if ($item->delete()) {
            $request->session()->flash('success', ConstantController::$deleted_successfully[$_SESSION['lang']]);
            return redirect(UserController::$base . '/users');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(UserController::$base . '/users');
        }


    }


    function check_added_by(Request $request, $item_id)
    {

    }
}
