<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Http\Controllers\dashboard\ConstantController;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserGetWorkController extends Controller
{
    public static $path_view = "dashboard.main_views.admin.";
    public static $base = "admin";

    public function index(Request $request)
    {

        $data = array(
            'view_name' => "المسوقين",
            'view_parent' => null,
            'a_id' => "a_user_get_work",
            'view_url' => "/admin/users_get_works",
            "users" => User::where('type','=','user_get_work')->where('deleted',0)->get(),

        );
        return view($this::$path_view . 'users_get_work', $data);

    }


    public function save(Request $request)
    {
        $pro_find = User::where('name', $request->input('name'))->where('type','user_get_work')->get();
        if (sizeof($pro_find) > 0) {


            $request->session()->flash('error', "الاسم مستخدم من قبل ");
            return redirect(EmployeeController::$base . '/users_get_works');
        }


        $item = new User();

        $item->name = $request->input('name');
        $item->type = 'user_get_work';
        $item->added_by = Auth::user()->id;

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$saved[$_SESSION['lang']]);
            return redirect(UserController::$base . '/users_get_works');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(UserController::$base . '/users_get_works');
        }

    }


    public function edit(Request $request)
    {
        $item = User::where('id', $request->input('item_id'))->first();

        $item->name = $request->input('name');
        $item->type = 'user_get_work';
        $item->added_by = Auth::user()->id;

        if ($item->save()) {

            $request->session()->flash('success', ConstantController::$edit_successfully[$_SESSION['lang']]);
            return redirect(UserController::$base . '/users_get_works');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(UserController::$base . '/users_get_works');
        }
    }

    public function remove(Request $request, $item_id)
    {

        if (sizeof(User::where('user_connect', $item_id)->get()) > 0) {
            $request->session()->flash('error', "الرجاء حذف جميع الزبائن المرتبطين أولاً");
            return redirect(UserController::$base . '/users_get_works');
        } else {
            $item = User::where('id', $item_id)->first();


            if ($item->delete()) {
                $request->session()->flash('success', ConstantController::$deleted_successfully[$_SESSION['lang']]);
                return redirect(UserController::$base . '/users_get_works');
            } else {

                $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
                return redirect(UserController::$base . '/users_get_works');
            }
        }



    }
}
