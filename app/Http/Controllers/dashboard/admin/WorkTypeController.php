<?php

namespace App\Http\Controllers\dashboard\admin;

use App\Models\DepartmentModel;
use App\Models\WorkTypeModel;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class WorkTypeController extends Controller
{


    public static $path_view = "dashboard.main_views.admin.";
    public static $base = "admin";

    public function index(Request $request)
    {

        $data = array(
            'view_name' => "مجال العمل ",
            'view_parent' => null,
            'a_id' => "a_work_type",
            'view_url' => "/admin/work_types",
            "work_types" => WorkTypeModel::where('deleted', 0)->get(),

        );
        return view($this::$path_view . 'work_types', $data);

    }


    public function save(Request $request)
    {
        $item = new WorkTypeModel();

        $item->name = $request->input('name');
        $item->added_by = Auth::user()->id;

        if ($item->save()) {

            $request->session()->flash('success', \App\Http\Controllers\dashboard\ConstantController::$saved[$_SESSION['lang']]);
            return redirect(DepartmentController::$base . '/work_types');
        } else {

            $request->session()->flash('error', \App\Http\Controllers\dashboard\ConstantController::$error[$_SESSION['lang']]);
            return redirect(DepartmentController::$base . '/work_types');
        }

    }


    public function edit(Request $request)
    {
        $item = WorkTypeModel::where('id', $request->input('item_id'))->first();
        $item->name = $request->input('name');

        if ($item->save()) {

            $request->session()->flash('success', \App\Http\Controllers\dashboard\ConstantController::$edit_successfully[$_SESSION['lang']]);
            return redirect(DepartmentController::$base . '/work_types');
        } else {

            $request->session()->flash('error', \App\Http\Controllers\dashboard\ConstantController::$error[$_SESSION['lang']]);
            return redirect(DepartmentController::$base . '/work_types');
        }
    }

    public function remove(Request $request, $item_id)
    {

        if (sizeof(User::where("work", $item_id)->get()) > 0) {
            $request->session()->flash('error', "الرجاء حذف جميع الزبائن المرتبطين أولاً");
            return redirect(DepartmentController::$base . '/work_types');
        } else {
            $item = WorkTypeModel::where('id', $item_id)->first();


            if ($item->delete()) {
                $request->session()->flash('success', \App\Http\Controllers\dashboard\ConstantController::$deleted_successfully[$_SESSION['lang']]);
                return redirect(DepartmentController::$base . '/work_types');
            } else {

                $request->session()->flash('error', \App\Http\Controllers\dashboard\ConstantController::$error[$_SESSION['lang']]);
                return redirect(DepartmentController::$base . '/work_types');
            }

        }


    }

}
