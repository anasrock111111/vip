<?php

namespace App\Http\Controllers\dashboard\employee;

use App\Http\Controllers\dashboard\ConstantController;
use App\Models\CliceModel;
use App\Models\OrderModel;
use App\Models\ProjectModel;
use App\Models\TaskCommentModel;
use App\Models\TaskEditTypeModel;
use App\Models\TaskModel;
use App\Models\TaskTypeModel;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public static $path_view = "dashboard.main_views.employee.";
    public static $base = "employee";

    public function index(Request $request)
    {

        $date_today = date('Y-m-d');
        $user_id = Auth::user()->id;

        $tasks_today = $order_delivery_today_ = DB::select("
            select * from `task`
            where  ((start_date='$date_today' ) or (start_date<'$date_today'   ) )
            and deleted=0 and employee=$user_id and collected=0
            order by created_at desc
            ");

        $date_today = date('Y-m-d');
        $next_date = date('Y-m-d', strtotime($date_today . " + 1 day"));
        $next_date_ = "";
        if (date('D', strtotime($date_today)) === 'Thu') {

            $next_date_ = date('Y-m-d', strtotime($date_today . " + 2 day"));
        }

        $orders_next_day = TaskModel::where('deleted', 0)->whereIn('start_date', [$next_date,$next_date_])->where('deleted', 0)->where('collected', 0)->where('employee', Auth::user()->id)->orderBy('created_at', 'desc')->get();

        $arr_of_ids = array();
        foreach ($tasks_today as $item) {
            array_push($arr_of_ids, $item->id);
        }
        foreach ($orders_next_day as $item) {
            array_push($arr_of_ids, $item->id);
        }



        $other_task = TaskModel::whereNotIn('id', $arr_of_ids)->where('deleted', 0)->where('collected', 0)->where('employee', '=',Auth::user()->id)->get();

        $data = array(
            'view_name' => ConstantController::$home[$_SESSION['lang']],
            'view_parent' => null,
            'view_url' => "/employee/home",
            'a_id' => "a_home",
            'tasks_today' => $tasks_today,
            'task_next_day' => $orders_next_day,
            'other_task' => $other_task

        );

        return view($this::$path_view . 'home', $data);


    }

    public function task_details(Request $request, $id)
    {

        $comment = TaskCommentModel::where('added_type', '<>', 'employee')->where('task', $id)->where('deleted', 0)->where('seen', 0)->get();


        foreach ($comment as $item) {

            if ($item->added_type != "employee") {
                $comm = new TaskCommentModel();

                $comm = $item;
                $comm->seen = 1;
                $comm->save();
            }

        }


        $data = array(
            'view_name' => ConstantController::$home[$_SESSION['lang']],
            'view_parent' => null,
            'view_url' => "/employee/home",
            'a_id' => "a_home",
            'item' => TaskModel::find($id)

        );

        return view($this::$path_view . 'task_details', $data);


    }

    public function add_comment(Request $request)
    {

        $task_id = $request->input('task_id');

        $comment = $request->input('comment');
        $employee = Auth::user()->id;

        $add_comm = new TaskCommentModel();
        $add_comm->task = $task_id;
        $add_comm->user = $employee;
        $add_comm->comment = $comment;
        $add_comm->added_type = Auth::user()->type;

        if ($add_comm->save()) {

            $request->session()->flash('success', ConstantController::$saved[$_SESSION['lang']]);
            return redirect(HomeController::$base . '/task_details/' . $task_id);
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(HomeController::$base . '/task_details/' . $task_id);
        }

    }

    public function finish_task(Request $request, $id)
    {

        $task = TaskModel::find($id);

        $task->finished = 1;
        $task->end_date = date('Y-m-d H:i:s');

        if ($task->save()) {

            $request->session()->flash('success', ConstantController::$saved[$_SESSION['lang']]);
            return redirect(HomeController::$base . '/home/');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(HomeController::$base . '/home/');
        }
    }

    public function set_direction(Request $request)
    {
        $item_id = $request->input('item_id');

        $item = TaskModel::where('id', $item_id)->first();
        $item->task_direction = $request->input('task_direction');


        if ($item->save()) {


            $request->session()->flash('success', ConstantController::$edit_successfully[$_SESSION['lang']]);
//            return redirect(HomeController::$base . '/task_details/' . $item_id);
            return redirect(HomeController::$base . '/home/');

        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
//            return redirect(HomeController::$base . '/task_details/' . $item_id);
            return redirect(HomeController::$base . '/home/');
        }
    }

    public function add_have_inquiry(Request $request, $id)
    {
        $item_id = $id;

        $item = TaskModel::where('id', $item_id)->first();
        if ($item->have_inquiry == 0) {

            $item->have_inquiry = 1;
        } else {

            $item->have_inquiry = 0;
        }


        if ($item->save()) {


            $request->session()->flash('success', ConstantController::$saved[$_SESSION['lang']]);
            return redirect(HomeController::$base . '/home/');
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(HomeController::$base . '/home/');
        }
    }

    public function tasks_history(Request $request)
    {

        $id = $request->input('elem_id');
        $task = TaskModel::find($id);
        $items = TaskModel::where('space', $task->space)->where('deleted', 0)->get();

        foreach ($items as $item) {

            $item->order = OrderModel::find($item->order)->order_number ." | ". OrderModel::find($item->order)->delivery_date;
            $item->type = TaskTypeModel::find($item->type)->name;
            if ($item->employee > 0) {

                $item->employee = User::find($item->employee)->name;
            } else {

                $item->employee = "";
            }
            $item->edit_type = $item->edit_type . "";
            if ($item->edit_type != null) {
                $item->edit_type = TaskEditTypeModel::find($item->edit_type)->name;
            }

        }


        return response()->json(['status' => 200, 'items' => $items]);
    }


    public function project_note(Request $request)
    {

        $order_id = $request->input('elem_id');

        $order = OrderModel::find($order_id);
        $project = ProjectModel::find($order->project);
        $customer = User::find($project->user);


        $data = [
            'project_user_note' => $project->user_note,
            'project_technical_supervisor_note' => $project->technical_supervisor_note,
            'order_note' => $order->note,
            'customer_code' => $customer->code,
            'customer_note' => $customer->note,
            'clice_design' => CliceModel::find($customer->clice_design)->name,
            'clice_plan' => CliceModel::find($customer->clice_plan)->name,
        ];
        return response()->json(['status' => 200, 'data' => $data]);

    }

}
