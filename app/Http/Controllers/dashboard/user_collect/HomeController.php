<?php

namespace App\Http\Controllers\dashboard\user_collect;

use App\Http\Controllers\dashboard\ConstantController;
use App\Models\CliceModel;
use App\Models\OrderModel;
use App\Models\ProjectModel;
use App\Models\TaskEditTypeModel;
use App\Models\TaskModel;
use App\Models\TaskTypeModel;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public static $path_view = "dashboard.main_views.user_collect.";
    public static $base = "user_collect";


    public function order_assigned(Request $request)
    {

        $project = ProjectModel::where('project_ended', 0)->where('deleted', 0)->get();
        $order = array();


        foreach ($project as $item) {


            $orders = OrderModel::where('project', $item->id)->where('assign_to_collector', 1)->where('deleted', 0)->get();

            foreach ($orders as $item_) {

//                array_push($ids_for_other_orders,$item_->id);
                $tasks = TaskModel::where('order', $item_->id)->where('deleted', 0)->get();
                $all_tasks = sizeof($tasks);
                $tasks_ended = 0;
                $task_received_from_technical = 0;
                foreach ($tasks as $task) {

                    if ($task->finished == 1) {
                        $tasks_ended++;
                    }
                    if ($task->technical_supervisor_receive == 1) {
                        $task_received_from_technical++;
                    }

                    $item_->tasks_ended = $tasks_ended;
                    $item_->all_tasks = $all_tasks;
                    $item_->task_received_from_technical = $task_received_from_technical;
                }

                $item_->tasks_not_inserted = sizeof(TaskModel::where('order', $item_->id)->where('deleted', 0)->where('employee', '=', null)->get());
                $item_->tasks_collected = sizeof(TaskModel::where('order', $item_->id)->where('collected', 1)->get());

                array_push($order, $item_);

            }

        }


        $data = array(
            'view_name' => ConstantController::$home[$_SESSION['lang']],
            'view_parent' => null,
            'view_url' => "/admin/home",
            'a_id' => "a_order_assign",
            'orders' => $order,
        );


        return view($this::$path_view . 'order_assign', $data);


    }

    public function index(Request $request)
    {


        $ids_for_other_orders = array();
        $project = ProjectModel::where('project_ended', 0)->where('deleted', 0)->get();
        $order = array();


        foreach ($project as $item) {


            $orders = OrderModel::where('project', $item->id)->where('delivered', 0)->where('deleted', 0)->get();

            foreach ($orders as $item_) {

//                array_push($ids_for_other_orders,$item_->id);
                $tasks = TaskModel::where('order', $item_->id)->where('deleted', 0)->get();
                $all_tasks = sizeof($tasks);
                $tasks_ended = 0;
                $task_received_from_technical = 0;
                foreach ($tasks as $task) {

                    if ($task->finished == 1) {
                        $tasks_ended++;
                    }
                    if ($task->technical_supervisor_receive == 1) {
                        $task_received_from_technical++;
                    }

                    $item_->tasks_ended = $tasks_ended;
                    $item_->all_tasks = $all_tasks;
                    $item_->task_received_from_technical = $task_received_from_technical;
                }

                $item_->tasks_not_inserted = sizeof(TaskModel::where('order', $item_->id)->where('deleted', 0)->where('employee', '=', null)->get());
                $item_->tasks_collected = sizeof(TaskModel::where('order', $item_->id)->where('collected', 1)->get());

                array_push($order, $item_);

            }

        }

        $date_today = date('Y-m-d');
        $next_date = date('Y-m-d', strtotime($date_today . " + 1 day"));

        $next_date_ = "";
        if (date('D', strtotime($date_today)) === 'Thu') {

            $next_date_ = date('Y-m-d', strtotime($date_today . " + 2 day"));
        }

        $order_delivery_today_ = DB::select("
            
            select * from `order`
            where ((delivery_date='$next_date' or delivery_date='$next_date_' ) or (delivery_date<'$date_today' and delivered=0  ))
            and order.delivered=0
            
            ");


        $order_delivery_today__ = array();
        foreach ($order_delivery_today_ as $item_) {

            array_push($ids_for_other_orders, $item_->id);
            $tasks = TaskModel::where('order', $item_->id)->where('deleted', 0)->get();
            $all_tasks = sizeof($tasks);
            $tasks_ended = 0;
            $task_received_from_technical = 0;
            foreach ($tasks as $task) {

                if ($task->finished == 1) {
                    $tasks_ended++;
                }
                if ($task->technical_supervisor_receive == 1) {
                    $task_received_from_technical++;
                }

                $item_->tasks_ended = $tasks_ended;
                $item_->all_tasks = $all_tasks;
                $item_->task_received_from_technical = $task_received_from_technical;
            }
            $item_->tasks_not_inserted = sizeof(TaskModel::where('order', $item_->id)->where('deleted', 0)->where('employee', '=', null)->get());
            $item_->tasks_collected = sizeof(TaskModel::where('order', $item_->id)->where('collected', 1)->get());

            array_push($order_delivery_today__, $item_);

        }


        $orders_next_day = OrderModel::where('deleted', 0)->where('delivered', 0)->whereIn('delivery_date', [$next_date, $next_date_])->get();

        $orders_next_day_f = array();
        foreach ($orders_next_day as $item_) {

            array_push($ids_for_other_orders, $item_->id);
            $tasks = TaskModel::where('order', $item_->id)->where('deleted', 0)->get();
            $all_tasks = sizeof($tasks);
            $tasks_ended = 0;
            $task_received_from_technical = 0;
            foreach ($tasks as $task) {

                if ($task->finished == 1) {
                    $tasks_ended++;
                }
                if ($task->technical_supervisor_receive == 1) {
                    $task_received_from_technical++;
                }

                $item_->tasks_ended = $tasks_ended;
                $item_->all_tasks = $all_tasks;
                $item_->task_received_from_technical = $task_received_from_technical;
            }
            $item_->tasks_not_inserted = sizeof(TaskModel::where('order', $item_->id)->where('deleted', 0)->where('employee', '=', null)->get());
            $item_->tasks_collected = sizeof(TaskModel::where('order', $item_->id)->where('collected', 1)->get());

            array_push($orders_next_day_f, $item_);

        }


        $orders = OrderModel::where('deleted', 0)->where('delivered', 0)->whereNotIn('id', $ids_for_other_orders)->get();

        $order_other = array();
        foreach ($orders as $item_) {

            array_push($ids_for_other_orders, $item_->id);
            $tasks = TaskModel::where('order', $item_->id)->where('deleted', 0)->get();
            $all_tasks = sizeof($tasks);
            $tasks_ended = 0;
            $task_received_from_technical = 0;
            foreach ($tasks as $task) {

                if ($task->finished == 1) {
                    $tasks_ended++;
                }
                if ($task->technical_supervisor_receive == 1) {
                    $task_received_from_technical++;
                }

                $item_->tasks_ended = $tasks_ended;
                $item_->all_tasks = $all_tasks;
                $item_->task_received_from_technical = $task_received_from_technical;
            }
            $item_->tasks_not_inserted = sizeof(TaskModel::where('order', $item_->id)->where('deleted', 0)->where('employee', '=', null)->get());
            $item_->tasks_collected = sizeof(TaskModel::where('order', $item_->id)->where('collected', 1)->get());


            array_push($order_other, $item_);

        }


        $data = array(
            'view_name' => ConstantController::$home[$_SESSION['lang']],
            'view_parent' => null,
            'view_url' => "/admin/home",
            'a_id' => "a_home",
            'orders' => $order,
            'orders_next_day' => $orders_next_day_f,
            'order_delivery_today' => $order_delivery_today__,
            'other' => $order_other,
            'tasks' => TaskModel::where('deleted', 0)->where('start_date', '' . date('Y-m-d'))->where('employee', '<>', null)->get(),
        );


//        $data = array(
//            'view_name' => ConstantController::$tasks[$_SESSION['lang']],
//            'view_parent' => null,
//            'a_id' => "a_task",
//            'view_url' => "/admin/tasks",
//            "tasks" => TaskModel::where('deleted', 0)->orderBy('created_at', 'desc')->get(),
//
//        );
        return view($this::$path_view . 'home', $data);

    }

    public function tasks(Request $request, $order)
    {

        $data = array(
            'view_name' => ConstantController::$tasks[$_SESSION['lang']],
            'view_parent' => null,
            'a_id' => "a_task",
            'view_url' => "/admin/tasks",
            "tasks" => TaskModel::where('deleted', 0)->where('order', $order)->orderBy('collected', 'asc')->get(),

        );
        return view($this::$path_view . 'tasks', $data);

    }


    public function edit_task_to_collected(Request $request)
    {

        $item = TaskModel::where('id', $request->input('item_id'))->first();
        $order_id = $item->order;
        if ($item->collected == 1) {
            $item->collect_direction = $request->input('collect_direction');
        } else {
            $item->collect_direction = $request->input('collect_direction');
            $item->collected_date = date('Y-m-d H:i:s');
            $item->collected_by = Auth::user()->id;
            $item->collected = 1;
        }


        if ($item->save()) {

            $order = OrderModel::find($order_id);
            $tasks = TaskModel::where('order', $order_id)->where('deleted', 0)->get();

            $collected = 1;
            foreach ($tasks as $item) {

                if ($item->collected == 0) {
                    $collected = 0;
                }

            }
            if ($collected == 1) {
                if ($order->collected == 0) {
                    $order->collected = 1;
                    $order->collected_date = date('Y-m-d H:i:s');
                    $order->save();
                }
            }
            $request->session()->flash('success', ConstantController::$edit_successfully[$_SESSION['lang']]);
            return redirect(HomeController::$base . '/tasks/' . $order_id);
        } else {

            $request->session()->flash('error', ConstantController::$error[$_SESSION['lang']]);
            return redirect(HomeController::$base . '/tasks/' . $order_id);
        }
    }


    public function edit_task_to_collected_back(Request $request)
    {

        $item = TaskModel::where('id', $request->input('item_id'))->first();

        $item->collected = 0;
        $item->technical_supervisor_receive_date = null;
        $item->collected_date = null;
        $item->end_date = null;
        $item->technical_supervisor_receive = 0;
        $item->finished = 0;
        $item->save();

        $request->session()->flash('success', ConstantController::$edit_successfully[$_SESSION['lang']]);
        return redirect(HomeController::$base . '/tasks/' . $item->order);

    }


    public function tasks_history(Request $request)
    {

        $id = $request->input('elem_id');
        $task = TaskModel::find($id);
        $items = TaskModel::where('space', $task->space)->where('deleted', 0)->get();

        foreach ($items as $item) {

            $item->order = OrderModel::find($item->order)->order_number . " | " . OrderModel::find($item->order)->delivery_date;
            $item->type = TaskTypeModel::find($item->type)->name;
            if ($item->employee > 0) {

                $item->employee = User::find($item->employee)->name;
            } else {

                $item->employee = "";
            }
            $item->edit_type = $item->edit_type . "";
            if ($item->edit_type != null) {
                $item->edit_type = TaskEditTypeModel::find($item->edit_type)->name;
            }

        }


        return response()->json(['status' => 200, 'items' => $items]);
    }


    public function project_note(Request $request)
    {

        $order_id = $request->input('elem_id');

        $order = OrderModel::find($order_id);
        $project = ProjectModel::find($order->project);
        $customer = User::find($project->user);


        $data = [
            'project_user_note' => $project->user_note,
            'project_technical_supervisor_note' => $project->technical_supervisor_note,
            'order_note' => $order->note,
            'customer_code' => $customer->code,
            'customer_note' => $customer->note,
            'clice_design' => CliceModel::find($customer->clice_design)->name,
            'clice_plan' => CliceModel::find($customer->clice_plan)->name,
        ];
        return response()->json(['status' => 200, 'data' => $data]);

    }

}
