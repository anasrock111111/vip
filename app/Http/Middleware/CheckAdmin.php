<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        if (Auth::user()) {

            if (User::find(Auth::user()->id)->type == "admin" || User::find(Auth::user()->id)->type == "project_manager" || User::find(Auth::user()->id)->type == "technical_supervisor") {

                return $next($request);
            } else {

                return redirect('login');
            }

        } else {
            return redirect('login');
        }
    }
}
