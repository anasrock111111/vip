<?php

namespace App\Http\Middleware;

use Closure;

class CheckLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session_id() == '') {
            session_start();
        }

        if ($request->input('lang')) {
            $_SESSION['lang'] = $request->input('lang');
        } else {
            if (isset($_SESSION['lang'])) {

            } else {
                $_SESSION['lang'] = "ar";
            }
        }


        $request->merge(['lang' => $_SESSION['lang']]);

        if (\Request::get('lang')) {

            $lang = \Request::get('lang');

            $request->merge(['lang' => $lang]);
        }
        return $next($request);

    }
}
