<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckUserCollect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()) {

            if (User::find(Auth::user()->id)->type == "user_collect") {

                return $next($request);
            } else {

                return redirect('login');
            }

        } else {
            return redirect('login');
        }
    }
}
