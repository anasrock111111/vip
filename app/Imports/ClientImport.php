<?php

namespace App\Imports;

use App\User;
use Maatwebsite\Excel\Concerns\ToModel;

class ClientImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {


        if (sizeof($row) > 2) {


            if ($row[0] == "id") {
                return;
            }


            $user = new User();

            $user->base_id = $row[0];
            $user->added_by = 1;
            $user->name = $row[1];
            $user->work = $row[2];
            $user->responsible_person = $row[3];
            $user->country = $row[4];
            $user->city = $row[5];
            $user->customer_ownership = $row[6];
            $user->type = "customer";
            $user->code = $row[7];
            $user->address = $row[8];
            $user->phone = $row[9];
            $user->email = $row[10];
            $user->website = $row[11];
            $user->skype = $row[12];
            $user->other_contact_name = $row[13];
            $user->other_contact_phone = $row[14];
            $user->note = $row[15];

            $user->save();

        }


    }
}
