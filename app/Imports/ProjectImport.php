<?php

namespace App\Imports;

use App\Models\ConstantModel;
use App\Models\ProjectModel;
use App\User;
use Maatwebsite\Excel\Concerns\ToModel;

class ProjectImport implements ToModel
{

    public function model(array $row)
    {

        if ($row[0] == "ProjectID") {
            return;
        }

        if (sizeof($row) > 2) {

//            var_dump($row);
            $project = new ProjectModel();
            $project->id = $row[0];
            $project->added_by = 1;
            $project->name_ar = $row[1];
            $project->user = User::where('base_id',$row[2])->get()->first()->id;
//            try
//            {
//
//
//            }
//            catch(\Exception $e)
//            {
//                var_dump($row[2]);
//            }
            $project->type = $row[3];
            if (strlen($row[4]) > 0) {

                $UNIX_DATE = ($row[4] - 25569) * 86400;
                $project->receive_date = gmdate("Y-m-d", $UNIX_DATE);

            } else {
                $project->receive_date = "2010-01-01";
            }

            $project->user_note = $row[5];
            $project->internal_space = $row[6];
            if (strlen($row[7]) > 0) {

                $project->base_edit_dir = ConstantModel::get_const("edit_dir");
                $project->base_project_dir = ConstantModel::get_const("project_dir");

                $project->name_en = $row[7];
                $project->edit_direction = ConstantModel::get_const("edit_dir") . str_replace(" ", "_", $row[7]);
                $project->project_direction = ConstantModel::get_const("project_dir") . str_replace(" ", "_", $row[7]);

            }
            $project->price = $row[8];
            $project->currency = $row[9];
            $project->project_ended = $row[10] ? 1 : 0;
            $project->save();
        }


    }
}
