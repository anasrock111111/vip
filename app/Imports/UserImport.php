<?php

namespace App\Imports;

use App\User;
use Maatwebsite\Excel\Concerns\ToModel;

class UserImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {


        if (sizeof($row) > 2) {


            if($row[0]=="id"){
                return;
            }

            $user =  new User();

            $user->id= $row[0];
            $user->user_name= $row[0];
            $user->name= $row[1];
            $user->added_by =1;
            $user->email= $row[2];
            $user->type= "employee";
            $user->password= bcrypt('123456');
            $user->mobile= $row[3];
            $user->phone= $row[4];
            $user->address= $row[5];

            $user->save();

        }


    }
}
