<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConstantModel extends Model
{
    protected $table = "constants";

    public static function get_const($item)
    {
        $item_ = ConstantModel::where('item', $item)->get()[0];
        return $item_->value;
    }


}
