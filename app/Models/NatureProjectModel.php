<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NatureProjectModel extends Model
{
    protected $table = "project_nature";
}
