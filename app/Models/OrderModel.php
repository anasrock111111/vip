<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderModel extends Model
{
    protected $table = "order";

    public static function check_if_have_inquiry($order_id)
    {

        $tasks = TaskModel::where('order', $order_id)->where('deleted', 0)->where('employee', '<>', null)->get();

        foreach ($tasks as $item) {
            if ($item->have_inquiry == 1) {
                return true;
            }

        }
        return false;
    }
}
