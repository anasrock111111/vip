<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalaryLogModel extends Model
{
    protected $table = "salary_log";
}
