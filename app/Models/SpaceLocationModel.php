<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpaceLocationModel extends Model
{
    protected $table = "space_location";
}
