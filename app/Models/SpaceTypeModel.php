<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpaceTypeModel extends Model
{

    protected $table = "space_type";
}
