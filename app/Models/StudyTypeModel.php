<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudyTypeModel extends Model
{
    protected $table="study_type";
}
