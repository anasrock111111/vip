<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskCommentModel extends Model
{
    protected $table="task_comment";
}
