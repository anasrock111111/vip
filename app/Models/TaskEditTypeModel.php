<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskEditTypeModel extends Model
{
    protected $table="task_edit_type";
}
