<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskTypeModel extends Model
{
    protected $table = "task_type";
}
