<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkTypeModel extends Model
{
    protected $table="work_type";
}
