
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>VIP-LOGIN</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    @if($_SESSION['lang']=="en")
        <link href="{{URL::asset('assets/dashboard/en/css/icons/icomoon/styles.css')}}" rel="stylesheet"
              type="text/css">
        <link href="{{URL::asset('assets/dashboard/en/css/bootstrap.min.css')}}" rel="stylesheet"
              type="text/css">
        <link href="{{URL::asset('assets/dashboard/en/css/core.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{URL::asset('assets/dashboard/en/css/components.min.css')}}" rel="stylesheet"
              type="text/css">
        <link href="{{URL::asset('assets/dashboard/en/css/colors.min.css')}}" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script src="{{URL::asset('assets/dashboard/plugins/loaders/pace.min.js')}}"></script>
        <script src="{{URL::asset('assets/dashboard/core/libraries/jquery.min.js')}}"></script>
        <script src="{{URL::asset('assets/dashboard/core/libraries/bootstrap.min.js')}}"></script>
        <script src="{{URL::asset('assets/dashboard/plugins/loaders/blockui.min.js')}}"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script src="{{URL::asset('assets/dashboard/en/js/app.js')}}"></script>

    @else

        <link href="{{URL::asset('assets/dashboard/ar/css/icons/icomoon/styles.css')}}" rel="stylesheet"
              type="text/css">
        <link href="{{URL::asset('assets/dashboard/ar/css/bootstrap.min.css')}}" rel="stylesheet"
              type="text/css">
        <link href="{{URL::asset('assets/dashboard/ar/css/core.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{URL::asset('assets/dashboard/ar/css/components.min.css')}}" rel="stylesheet"
              type="text/css">
        <link href="{{URL::asset('assets/dashboard/ar/css/colors.min.css')}}" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script src="{{URL::asset('assets/dashboard/plugins/loaders/pace.min.js')}}"></script>
        <script src="{{URL::asset('assets/dashboard/core/libraries/jquery.min.js')}}"></script>
        <script src="{{URL::asset('assets/dashboard/core/libraries/bootstrap.min.js')}}"></script>
        <script src="{{URL::asset('assets/dashboard/plugins/loaders/blockui.min.js')}}"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script src="{{URL::asset('assets/dashboard/ar/js/app.js')}}"></script>

        <script src="{{URL::asset('assets/dashboard/ar/js/demo_pages/login.js')}}"></script>
    @endif
    <!-- /theme JS files -->
    <style>
        @import url("http://fonts.googleapis.com/earlyaccess/droidarabickufi.css");

        abbr, acronym, address, applet, article, aside, audio, b, big, blockquote,
        body, caption, canvas, center, cite, code,
        dd, del, details, dfn, dialog, div, dl, dt, em, embed, fieldset, figcaption,
        figure, form, footer, header, hgroup, h1, h2, h3, h4, h5, h6, html, iframe, img,
        ins, kbd, label, legend, li, mark, menu, nav, object, ol, output, p, pre, q, ruby, s,
        samp, section, small, strike, strong, sub, summary, sup, tt, table, tbody,
        textarea, tfoot, thead, time, tr, th, td, u, ul, var, video {

            font-family: 'Droid Arabic Kufi' !important;
        }

        span.select2-container {
            z-index: 10050;
        }



    </style>
</head>

<body class="login-container bg-slate-800">

<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

                <!-- Advanced login -->
                <form  method="post" action="{{ route('login') }}">
                    @foreach($errors->all() as $error)
                        {{$error}}
                    @endforeach
                    {{ csrf_field() }}
                    <div class="panel panel-body login-form">
                        <div class="text-center">
                            <div class="icon-object border-warning-400 text-warning-400"><i class="icon-people"></i></div>
                            <h5 class="content-group-lg">{{\App\Http\Controllers\dashboard\ConstantController::$login_to_your_account[$_SESSION['lang']]}}</h5>
                        </div>

                        <div class="form-group has-feedback has-feedback-left">
                            <input name="email" type="email" class="form-control {{ $errors->has('email') ? ' has-error' : '' }}" placeholder="{{\App\Http\Controllers\dashboard\ConstantController::$email[$_SESSION['lang']]}}" >
                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group has-feedback has-feedback-left">
                            <input name="password" type="password" class="form-control {{ $errors->has('password') ? ' has-error' : '' }} " placeholder="{{\App\Http\Controllers\dashboard\ConstantController::$password[$_SESSION['lang']]}}" >
                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                        </div>


                        <div class="form-group">
                            <button type="submit" class="btn bg-slate btn-block content-group" >{{\App\Http\Controllers\dashboard\ConstantController::$login[$_SESSION['lang']]}}  </button>
                        </div>
                        @if( isset($error_login))
                            <h5 class="alert alert-danger no-border"
                                style="    width: 100%; text-align: center">    {{$error_login}}</h5>
                        @endif

                        </div>
                </form>
                <!-- /advanced login -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
