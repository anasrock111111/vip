<!DOCTYPE html>
@if($_SESSION['lang']=="ar")
    <html lang="en" dir="rtl">
    @else
        <html lang="en">
        @endif
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>VIP</title>
            <style>

                @import url("http://fonts.googleapis.com/earlyaccess/droidarabickufi.css");
            </style>
            <!-- Global stylesheets -->
            <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
                  type="text/css">

            @if($_SESSION['lang']=="en")
                <link href="{{URL::asset('assets/dashboard/en/css/icons/icomoon/styles.css')}}" rel="stylesheet"
                      type="text/css">
                <link href="{{URL::asset('assets/dashboard/en/css/bootstrap.min.css')}}" rel="stylesheet"
                      type="text/css">
                <link href="{{URL::asset('assets/dashboard/en/css/core.min.css')}}" rel="stylesheet" type="text/css">
                <link href="{{URL::asset('assets/dashboard/en/css/components.min.css')}}" rel="stylesheet"
                      type="text/css">
                <link href="{{URL::asset('assets/dashboard/en/css/colors.min.css')}}" rel="stylesheet" type="text/css">
                <!-- /global stylesheets -->

                <!-- Core JS files -->
                <script src="{{URL::asset('assets/dashboard/plugins/loaders/pace.min.js')}}"></script>
                <script src="{{URL::asset('assets/dashboard/core/libraries/jquery.min.js')}}"></script>
                <script src="{{URL::asset('assets/dashboard/core/libraries/bootstrap.min.js')}}"></script>
                <script src="{{URL::asset('assets/dashboard/plugins/loaders/blockui.min.js')}}"></script>
                <!-- /core JS files -->

                <script type="text/javascript"
                        src="{{URL::asset('assets/dashboard/plugins/notifications/noty.min.js')}}"></script>
                <script type="text/javascript"
                        src="{{URL::asset('assets/dashboard/plugins/notifications/sweet_alert.min.js')}}"></script>

                <script src="{{URL::asset('assets/dashboard/plugins/tables/datatables/datatables.min.js')}}"></script>
                <script src="{{URL::asset('assets/dashboard/plugins/forms/selects/select2.min.js')}}"></script>
                <script src="{{URL::asset('assets/dashboard/plugins/forms/styling/uniform.min.js')}}"></script>


                <script src="{{URL::asset('assets/dashboard/plugins/editors/ckeditor/ckeditor.js')}}"></script>

                <!-- Theme JS files -->
                <script src="{{URL::asset('assets/dashboard/en/js/app.js')}}"></script>



                <script src="{{URL::asset('assets/dashboard/demo_pages/datatables_basic.js')}}"></script>


            @else

                <link href="{{URL::asset('assets/dashboard/ar/css/icons/icomoon/styles.css')}}" rel="stylesheet"
                      type="text/css">
                <link href="{{URL::asset('assets/dashboard/ar/css/bootstrap.min.css')}}" rel="stylesheet"
                      type="text/css">
                <link href="{{URL::asset('assets/dashboard/ar/css/core.min.css')}}" rel="stylesheet" type="text/css">
                <link href="{{URL::asset('assets/dashboard/ar/css/components.min.css')}}" rel="stylesheet"
                      type="text/css">
                <link href="{{URL::asset('assets/dashboard/ar/css/colors.min.css')}}" rel="stylesheet" type="text/css">
                <!-- /global stylesheets -->

                <!-- Core JS files -->
                <script src="{{URL::asset('assets/dashboard/plugins/loaders/pace.min.js')}}"></script>
                <script src="{{URL::asset('assets/dashboard/core/libraries/jquery.min.js')}}"></script>
                <script src="{{URL::asset('assets/dashboard/core/libraries/bootstrap.min.js')}}"></script>
                <script src="{{URL::asset('assets/dashboard/plugins/loaders/blockui.min.js')}}"></script>
                <!-- /core JS files -->

                <!-- Theme JS files -->
                <script type="text/javascript"
                        src="{{URL::asset('assets/dashboard/plugins/notifications/noty.min.js')}}"></script>
                <script type="text/javascript"
                        src="{{URL::asset('assets/dashboard/plugins/notifications/sweet_alert.min.js')}}"></script>

                <script src="{{URL::asset('assets/dashboard/plugins/tables/datatables/datatables.min.js')}}"></script>
                <script src="{{URL::asset('assets/dashboard/plugins/forms/selects/select2.min.js')}}"></script>
                <script src="{{URL::asset('assets/dashboard/plugins/forms/styling/uniform.min.js')}}"></script>

                <script src="{{URL::asset('assets/dashboard/plugins/editors/ckeditor/ckeditor.js')}}"></script>
                <script src="{{URL::asset('assets/dashboard/plugins/forms/styling/uniform.min.js')}}"></script>
                {{--<script src="{{URL::asset('assets/dashboard/plugins/forms/styling/switchery.min.js')}}"></script>--}}
                {{--<script src="{{URL::asset('assets/dashboard/plugins/forms/styling/switch.min.js')}}"></script>--}}
                <!-- Theme JS files -->
                <script src="{{URL::asset('assets/dashboard/en/js/app.js')}}"></script>



                <script src="{{URL::asset('assets/dashboard/demo_pages/datatables_basic.js')}}"></script>
                {{--<script src="{{URL::asset('assets/dashboard/demo_pages/form_checkboxes_radios.js')}}"></script>--}}


        @endif

        <!-- /theme JS files -->
            <style>
                .noty_layout {
                    z-index: 99999 !important;
                }

                .AnyTime-win {
                    z-index: 9999;
                    padding: 0px !important;
                }

                .AnyTime-hdr {
                    display: none;
                }

                .text-success-600, .text-success-600:hover, .text-success-600:focus {
                    font-size: 15px;
                }

                a, abbr, acronym, address, applet, article, aside, audio, b, big, blockquote,
                body, caption, canvas, center, cite, code,
                dd, del, details, dfn, dialog, div, dl, dt, em, embed, fieldset, figcaption,
                figure, form, footer, header, h1, h2, h3, h4, h5, h6, html, iframe, img,
                ins, kbd, label, legend, li, mark, menu, nav, object, ol, output, p, pre, q, ruby, s,
                samp, section, small, strike, strong, sub, summary, sup, tt, table, tbody,
                textarea, tfoot, thead, time, tr, th, td, u, ul, var, video {

                    font-family: 'Droid Arabic Kufi' !important;
                }

                .text-success-600, .text-success-600:hover, .text-success-600:focus {
                    font-size: 15px;
                }

                .text-warning-600, .text-warning-600:hover, .text-warning-600:focus {
                    font-size: 15px;
                }

                .icon-add:hover {
                    cursor: pointer;
                }

                .bg-pink-400 {
                    background-color: #2196f3 !important;
                    border-color: #b4b4b4 !important;
                }

                [class^="icon-"], [class*=" icon-"] {
                    top: 1px !important;
                }

                .daterangepicker.dropdown-menu {
                    z-index: 99999;
                }

                .datatable-scroll {

                    overflow-x: scroll;
                }

                .table > tbody > tr > td {
                    padding: 7px 13px !important;
                }
            </style>

        </head>

        <?php $home = 'admin/home'?>
        <?php $base = "admin"?>
        <?php $class = ""?>

        @if(\Illuminate\Support\Facades\Auth::user()->type=="employee")

            <?php $class = " sidebar-xs"?>
        @endif
        <body class="{{$class}}">

        <!-- Main navbar -->
        <div class="navbar navbar-inverse">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html"><img src="{{url('assets/dashboard/images/logo_light.png')}}"
                                                               alt=""></a>

                <ul class="nav navbar-nav visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                    <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>
            </div>

            <div class="navbar-collapse collapse" id="navbar-mobile">
                <ul class="nav navbar-nav">
                    <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a>
                    </li>


                </ul>


                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="dropdown dropdown-user">
                            <a class="dropdown-toggle" data-toggle="dropdown">
                                <img
                                    src="{{\Illuminate\Support\Facades\URL::asset('uploads/users/'.\Illuminate\Support\Facades\Auth::user()->image_url)}}"
                                    alt="">
                                @if (\Illuminate\Support\Facades\Auth::check())
                                    <span>{{\Illuminate\Support\Facades\Auth::user()->name}}</span>
                                    <i class="caret"></i>
                                @else

                                @endif


                            </a>

                            <ul class="dropdown-menu dropdown-menu-right">

                                <li><a href="{{url($base.'/profile')}}"><i class="icon-profile"></i>
                                        {{\App\Http\Controllers\dashboard\ConstantController::$profile['ar']}}

                                    </a></li>
                                <li><a href="{{url('/logout')}}"><i class="icon-switch2"></i>
                                        {{\App\Http\Controllers\dashboard\ConstantController::$logout['ar']}}
                                    </a></li>

                            </ul>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
        <!-- /main navbar -->


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->
                        <div class="sidebar-user">
                            <div class="category-content">
                                <div class="media">
                                    <a href="#" class="media-left"><img
                                            src="   {{URL::asset('uploads/users/'.\Illuminate\Support\Facades\Auth::user()->image_url)}}"
                                            class="img-circle img-sm" alt=""></a>

                                    <div class="media-body">
                                <span
                                    class="media-heading text-semibold">{{\Illuminate\Support\Facades\Auth::user()->name}}</span>
                                        <div class="text-size-mini text-muted">
                                            <i class="icon-pin text-size-small"></i> {{\Illuminate\Support\Facades\Auth::user()->type}}
                                        </div>
                                    </div>

                                    <div class="media-right media-middle">
                                        <ul class="icons-list">
                                            <li>
                                                <a href="#"><i class="icon-cog3"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /user menu -->


                        <!-- Main navigation -->
                        <div class="sidebar-category sidebar-category-visible">
                            <div class="category-content no-padding">
                                <ul class="navigation navigation-main navigation-accordion">

                                    <li class="navigation-header"><span>

                                    {{\App\Http\Controllers\dashboard\ConstantController::$main['ar']}}
                                </span> <i class="icon-menu" title="Main pages"></i>
                                    </li>


                                    @if(\Illuminate\Support\Facades\Auth::user()->type=="admin" )



                                        <li><a id="a_home" href="{{url($base.'/constant')}}"><i class="icon-home4"></i>
                                                <span>{{\App\Http\Controllers\dashboard\ConstantController::$constant['ar']}}</span></a>
                                        </li>

                                        <li><a id="a_home" href="{{url($base.'/home')}}"><i class="icon-home4"></i>
                                                <span>{{\App\Http\Controllers\dashboard\ConstantController::$home['ar']}}</span></a>
                                        </li>


                                        @if(\Illuminate\Support\Facades\Auth::user()->type=="admin")

                                            <li><a id="a_user" href="{{url($base.'/users')}}"><i class="icon-home4"></i>
                                                    <span>{{\App\Http\Controllers\dashboard\ConstantController::$users['ar']}}</span></a>
                                            </li>

                                        @endif
                                        <li class="">
                                            <a href="#" class="has-ul"><i class="icon-stack2"></i>
                                                <span>{{\App\Http\Controllers\dashboard\ConstantController::$employees[$_SESSION['lang']]}}</span></a>
                                            <ul class="hidden-ul" style="display: none;">
                                                <li><a id="a_employee" href="{{url($base.'/employees')}}"><i
                                                            class="icon-home4"></i>
                                                        <span>{{\App\Http\Controllers\dashboard\ConstantController::$employees['ar']}}</span></a>
                                                </li>

                                                <li><a id="a_department" href="{{url($base.'/departments')}}"><i
                                                            class="icon-home4"></i>
                                                        <span>{{\App\Http\Controllers\dashboard\ConstantController::$department['ar']}}</span></a>
                                                </li>

                                                <li><a id="a_study_type" href="{{url($base.'/study_types')}}"><i
                                                            class="icon-home4"></i>
                                                        <span>أنواع الدراسة</span></a>
                                                </li>

                                            </ul>
                                        </li>


                                        <li class="">
                                            <a href="#" class="has-ul"><i class="icon-stack2"></i>
                                                <span>{{\App\Http\Controllers\dashboard\ConstantController::$projects[$_SESSION['lang']]}}</span></a>
                                            <ul class="hidden-ul" style="display: none;">
                                                <li><a id="a_project" href="{{url($base.'/projects')}}"><i
                                                            class="icon-home4"></i>
                                                        <span>{{\App\Http\Controllers\dashboard\ConstantController::$projects['ar']}}</span></a>
                                                </li>

                                                <li><a id="a_nature_project" href="{{url($base.'/nature_projects')}}"><i
                                                            class="icon-home4"></i>
                                                        <span>{{\App\Http\Controllers\dashboard\ConstantController::$nature_project['ar']}}</span></a>
                                                </li>
                                            </ul>
                                        </li>


                                        <li class="">
                                            <a href="#" class="has-ul"><i class="icon-stack2"></i>
                                                <span>{{\App\Http\Controllers\dashboard\ConstantController::$spaces[$_SESSION['lang']]}}</span></a>
                                            <ul class="hidden-ul" style="display: none;">

                                                <li><a id="a_space" href="{{url($base.'/spaces')}}"><i
                                                            class="icon-home4"></i>
                                                        <span>{{\App\Http\Controllers\dashboard\ConstantController::$spaces['ar']}}</span></a>
                                                </li>

                                                <li><a id="a_space_type" href="{{url($base.'/space_types')}}"><i
                                                            class="icon-home4"></i>
                                                        <span>{{\App\Http\Controllers\dashboard\ConstantController::$space_type['ar']}}</span></a>
                                                </li>

                                                <li><a id="a_space_location" href="{{url($base.'/space_locations')}}"><i
                                                            class="icon-home4"></i>
                                                        <span>{{\App\Http\Controllers\dashboard\ConstantController::$space_location['ar']}}</span></a>
                                                </li>
                                            </ul>
                                        </li>



                                        <li class="">
                                            <a href="#" class="has-ul"><i class="icon-stack2"></i>
                                                <span>{{\App\Http\Controllers\dashboard\ConstantController::$orders[$_SESSION['lang']]}}</span></a>
                                            <ul class="hidden-ul" style="display: none;">

                                                <li><a id="a_order" href="{{url($base.'/orders')}}"><i
                                                            class="icon-home4"></i>
                                                        <span>{{\App\Http\Controllers\dashboard\ConstantController::$orders['ar']}}</span></a>
                                                </li>


                                            </ul>
                                        </li>




                                        <li class="">
                                            <a href="#" class="has-ul"><i class="icon-stack2"></i>
                                                <span>{{\App\Http\Controllers\dashboard\ConstantController::$tasks[$_SESSION['lang']]}}</span></a>
                                            <ul class="hidden-ul" style="display: none;">

                                                <li><a id="a_task" href="{{url($base.'/tasks')}}"><i
                                                            class="icon-home4"></i>
                                                        <span>{{\App\Http\Controllers\dashboard\ConstantController::$tasks['ar']}}</span></a>
                                                </li>


                                                <li><a id="a_task_type" href="{{url($base.'/task_types')}}"><i
                                                            class="icon-home4"></i>
                                                        <span>{{\App\Http\Controllers\dashboard\ConstantController::$TasksType['ar']}}</span></a>
                                                </li>

                                                <li><a id="a_task_edit_type" href="{{url($base.'/task_edit_types')}}"><i
                                                            class="icon-home4"></i>
                                                        <span>{{\App\Http\Controllers\dashboard\ConstantController::$TasksEditType['ar']}}</span></a>
                                                </li>
                                            </ul>
                                        </li>




                                        <li class="">
                                            <a href="#" class="has-ul"><i
                                                    class="icon-stack2"></i><span>{{\App\Http\Controllers\dashboard\ConstantController::$customers[$_SESSION['lang']]}}</span></a>
                                            <ul class="hidden-ul" style="display: none;">


                                                <li><a id="a_customer" href="{{url($base.'/customers')}}"><i
                                                            class="icon-home4"></i>
                                                        <span>{{\App\Http\Controllers\dashboard\ConstantController::$customers['ar']}}</span></a>
                                                </li>

                                                <li><a id="a_user_get_work" href="{{url($base.'/users_get_works')}}"><i
                                                            class="icon-home4"></i>
                                                        <span>{{\App\Http\Controllers\dashboard\ConstantController::$marketing['ar']}}</span></a>
                                                </li>

                                                <li><a id="a_clice" href="{{url($base.'/clices')}}"><i
                                                            class="icon-home4"></i>
                                                        <span>{{\App\Http\Controllers\dashboard\ConstantController::$clice['ar']}}</span></a>
                                                </li>

                                                <li><a id="a_work_type" href="{{url($base.'/work_types')}}"><i
                                                            class="icon-home4"></i>
                                                        <span>مجال العمل</span></a>
                                                </li>
                                            </ul>
                                        </li>


                                    @endif



                                    @if( \Illuminate\Support\Facades\Auth::user()->type=="project_manager")



                                        <li><a id="a_home" href="{{url($base.'/home')}}"><i class="icon-home4"></i>
                                                <span>{{\App\Http\Controllers\dashboard\ConstantController::$home['ar']}}</span></a>
                                        </li>


                                        <li class="">
                                            <a href="#" class="has-ul"><i class="icon-stack2"></i>
                                                <span>{{\App\Http\Controllers\dashboard\ConstantController::$employees[$_SESSION['lang']]}}</span></a>
                                            <ul class="hidden-ul" style="display: none;">
                                                <li><a id="a_employee" href="{{url($base.'/employees')}}"><i
                                                            class="icon-home4"></i>
                                                        <span>{{\App\Http\Controllers\dashboard\ConstantController::$employees['ar']}}</span></a>
                                                </li>

                                            </ul>
                                        </li>

                                        <li class="">
                                            <a href="#" class="has-ul"><i class="icon-stack2"></i>
                                                <span>{{\App\Http\Controllers\dashboard\ConstantController::$projects[$_SESSION['lang']]}}</span></a>
                                            <ul class="hidden-ul" style="display: none;">
                                                <li><a id="a_project" href="{{url($base.'/projects')}}"><i
                                                            class="icon-home4"></i>
                                                        <span>{{\App\Http\Controllers\dashboard\ConstantController::$projects['ar']}}</span></a>
                                                </li>

                                            </ul>
                                        </li>



                                        <li class="">
                                            <a href="#" class="has-ul"><i class="icon-stack2"></i>
                                                <span>{{\App\Http\Controllers\dashboard\ConstantController::$spaces[$_SESSION['lang']]}}</span></a>
                                            <ul class="hidden-ul" style="display: none;">

                                                <li><a id="a_space" href="{{url($base.'/spaces')}}"><i
                                                            class="icon-home4"></i>
                                                        <span>{{\App\Http\Controllers\dashboard\ConstantController::$spaces['ar']}}</span></a>
                                                </li>

                                            </ul>
                                        </li>



                                        <li class="">
                                            <a href="#" class="has-ul"><i class="icon-stack2"></i>
                                                <span>{{\App\Http\Controllers\dashboard\ConstantController::$orders[$_SESSION['lang']]}}</span></a>
                                            <ul class="hidden-ul" style="display: none;">

                                                <li><a id="a_order" href="{{url($base.'/orders')}}"><i
                                                            class="icon-home4"></i>
                                                        <span>{{\App\Http\Controllers\dashboard\ConstantController::$orders['ar']}}</span></a>
                                                </li>


                                            </ul>
                                        </li>




                                        <li class="">
                                            <a href="#" class="has-ul"><i class="icon-stack2"></i>
                                                <span>{{\App\Http\Controllers\dashboard\ConstantController::$tasks[$_SESSION['lang']]}}</span></a>
                                            <ul class="hidden-ul" style="display: none;">

                                                <li><a id="a_task" href="{{url($base.'/tasks')}}"><i
                                                            class="icon-home4"></i>
                                                        <span>{{\App\Http\Controllers\dashboard\ConstantController::$tasks['ar']}}</span></a>
                                                </li>

                                            </ul>
                                        </li>





                                    @endif

                                    @if(\Illuminate\Support\Facades\Auth::user()->type=="technical_supervisor")
                                        <li><a id="a_home" href="{{url('admin/home')}}"><i
                                                    class="icon-home4"></i>
                                                <span>{{\App\Http\Controllers\dashboard\ConstantController::$home['ar']}}</span></a>
                                        </li>


                                        <li><a id="a_order" href="{{url($base.'/orders')}}"><i
                                                    class="icon-home4"></i>
                                                <span>{{\App\Http\Controllers\dashboard\ConstantController::$orders['ar']}}</span></a>
                                        </li>

                                        <li><a id="a_task" href="{{url($base.'/tasks')}}"><i
                                                    class="icon-home4"></i>
                                                <span>{{\App\Http\Controllers\dashboard\ConstantController::$tasks['ar']}}</span></a>
                                        </li>


                                    @endif

                                    @if(\Illuminate\Support\Facades\Auth::user()->type=="employee")
                                        <li><a id="a_home" href="{{url('employee/home')}}"><i
                                                    class="icon-home4"></i>
                                                <span>{{\App\Http\Controllers\dashboard\ConstantController::$home['ar']}}</span></a>
                                        </li>

                                    @endif
                                    @if(\Illuminate\Support\Facades\Auth::user()->type=="user_collect")
                                        <li><a id="a_home" href="{{url('user_collect/home')}}"><i
                                                    class="icon-home4"></i>
                                                <span>{{\App\Http\Controllers\dashboard\ConstantController::$home['ar']}}</span></a>
                                        </li>

                                        <li><a id="a_order_assign" href="{{url('user_collect/order_assigned')}}"><i
                                                    class="icon-home4"></i>
                                                <span> الطلبات للتجميع</span></a>
                                        </li>


                                    @endif


                                </ul>
                            </div>
                        </div>
                        <!-- /main navigation -->

                    </div>
                </div>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header page-header-default">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">
                                        @if(isset($view_name))
                                            {{$view_name}}
                                        @endif
</span>
                                </h4>
                            </div>

                            <div class="heading-elements">
                                <div class="heading-btn-group">
                                    <a href="#" class="btn btn-link btn-float has-text"><i
                                            class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
                                    <a href="#" class="btn btn-link btn-float has-text"><i
                                            class="icon-calculator text-primary"></i> <span>Invoices</span></a>
                                    <a href="#" class="btn btn-link btn-float has-text"><i
                                            class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
                                </div>
                            </div>
                        </div>

                        <div class="breadcrumb-line">
                            <ul class="breadcrumb">
                                @if(isset($view_parent) && strlen($view_parent))
                                    <li><a href="javascript:;"><i class="icon-home2 position-left"></i> {{$view_parent}}
                                        </a>
                                    </li>
                                    @if(isset($view_name) && strlen($view_name))
                                        <li class="active"><a href="{{url($view_url)}}">{{$view_name}}</a></li>
                                    @endif

                                @else
                                    @if(isset($view_name) && strlen($view_name))
                                        <li><a href="{{url($view_url)}}"><i
                                                    class="icon-home2 position-left"></i>{{$view_name}}
                                            </a>
                                        </li>

                                    @endif
                                @endif

                            </ul>

                        </div>
                    </div>
                    <!-- /page header -->


                    <!-- Content area -->
                    <div class="content">

                        @yield('content')
                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

        </body>
        </html>

        {{csrf_field()}}

        <div id="tasks_history" class="modal fade ">
            <div class="modal-dialog  modal-full ">
                <div class="modal-content">
                    <div class="modal-header">
                        <ul class="icons-list" style="float: left;">
                        </ul>

                        <h5 class="modal-title">مهام سابقة </h5>


                    </div>
                    <div class="modal-body">

                        <div class="row">


                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table class="table ">
                                        <thead>
                                        <tr>
                                            <th>رقم الطلب</th>
                                            <th>الموظف</th>
                                            <th>نوع المهمة</th>
                                            <th>نوع التعديل</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tasks_history_items">

                                        </tbody>
                                    </table>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="modal-footer">

                        <button type="button" class="btn btn-link"
                                data-dismiss="modal">{{\App\Http\Controllers\dashboard\ConstantController::$close[$_SESSION['lang']]}}</button>

                    </div>
                </div>
            </div>
        </div>



        <div id="tasks_time" class="modal fade ">
            <div class="modal-dialog  modal-full ">
                <div class="modal-content">
                    <form action="{{url('admin/update_task_date')}}" method="post">
                        {{csrf_field()}}
                        <input type="hidden" value="" name="order_id" id="order_id_edit_task">
                    <div class="modal-header">
                        <ul class="icons-list" style="float: left;">
                        </ul>

                        <h5 class="modal-title"> موعد التسليم</h5>


                    </div>
                    <div class="modal-body">

                        <div class="row">

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>تاريخ تسليم الطلب</label>
                                    <input type="date" id="delivery_date_" name="delivery_date" class="form-control"
                                           placeholder=""
                                    >
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">

                        <button type="button" class="btn btn-link"
                                data-dismiss="modal">{{\App\Http\Controllers\dashboard\ConstantController::$close[$_SESSION['lang']]}}</button>


                        <button type="submit" class="btn btn-default "
                                 >تعديل موعد كامل المهام </button>

                    </div>
                    </form>
                </div>
            </div>
        </div>



        <div id="order_notes" class="modal fade ">
            <div class="modal-dialog  modal-full ">
                <div class="modal-content">
                    <div class="modal-header">
                        <ul class="icons-list" style="float: left;">
                        </ul>

                        <h5 class="modal-title">ملاحظات المشروع </h5>


                    </div>
                    <div class="modal-body">

                        <div class="row">

                            <div class="col-lg-12">

                                <div class="form-group">
                                    <label>ملاحظة المشروع </label>
                                    <input type="text" id="project_user_note" disabled="disabled" name="name"
                                           class="form-control" placeholder="" required="required">
                                </div>
                            </div>

                            {{--<div class="col-lg-12">--}}

                                {{--<div class="form-group">--}}
                                    {{--<label>ملاحاظات الزبون</label>--}}
                                    {{--<input type="text" id="customer_note" disabled="disabled" name="name"--}}
                                           {{--class="form-control" placeholder="" required="required">--}}
                                {{--</div>--}}
                            {{--</div>--}}


                            <div class="col-lg-12">

                                <div class="form-group">
                                    <label>ملاحاظات الفنية على المشروع</label>
                                    <input type="text" id="project_technical_supervisor_note" disabled="disabled"
                                           name="name" class="form-control" placeholder="" required="required">
                                </div>
                            </div>

                            <div class="col-lg-12">

                                <div class="form-group">
                                    <label>ملاحاظات الطلب</label>
                                    <input type="text" id="order_note" disabled="disabled" name="name"
                                           class="form-control" placeholder="" required="required">
                                </div>
                            </div>
                            <div class="col-lg-4">

                                <div class="form-group">
                                    <label>رمز الزبون</label>
                                    <input type="text" id="customer_code" disabled="disabled" name="name"
                                           class="form-control" placeholder="" required="required">
                                </div>
                            </div>
                            <div class="col-lg-4">

                                <div class="form-group">
                                    <label>كليشة التصميم</label>
                                    <input type="text" id="clice_design" disabled="disabled" name="name"
                                           class="form-control" placeholder="" required="required">
                                </div>
                            </div>
                            <div class="col-lg-4">

                                <div class="form-group">
                                    <label>كليشة التخطيط</label>
                                    <input type="text" id="clice_plan" disabled="disabled" name="name"
                                           class="form-control" placeholder="" required="required">
                                </div>
                            </div>


                        </div>
                    </div>

                    <div class="modal-footer">

                        <button type="button" class="btn btn-link"
                                data-dismiss="modal">{{\App\Http\Controllers\dashboard\ConstantController::$close[$_SESSION['lang']]}}</button>

                    </div>
                </div>
            </div>
        </div>


        <script>

            $(".styled").uniform();




            $(".file-styled").uniform({
                fileButtonClass: 'action btn btn-default'
            });


            // Primary file input
            $(".file-styled-primary").uniform({
                fileButtonClass: 'action btn bg-blue'
            });


            $('.select-search').select2();
            var a_id = null;
            @if(isset($a_id))
                a_id = "#" + "{{$a_id}}";

            @endif

            $(a_id).parent().addClass('active');
            $(a_id).parent().parent().css('display', 'block');
            $(a_id).parent().parent().parent().addClass('active');

            @if(session()->has('success'))
            new Noty({

                text: "{{session('success')}}",
                type: 'success',
                dismissQueue: true,
                timeout: 4000,
                layout: "topRight",
            }).show();
            @endif

            @if(session()->has('error'))
            new Noty({

                text: "{{session('error')}}",
                type: 'error',
                dismissQueue: true,
                timeout: 4000,
                layout: "topRight",
            }).show();
            @endif


            $(document).ready(function () {
                $(document).on('focus', ':input', function () {
                    $(this).attr('autocomplete', 'off');
                });
            });


            function tasks_history(elem) {


                var _token = $('input[name="_token"]').val();
                var elem_id = $(elem).attr('item_id');

                var url = "";
                @if(\Illuminate\Support\Facades\Auth::user()->type=="employee")
                    url = "{{url('employee/tasks_history')}}"
                @elseif(\Illuminate\Support\Facades\Auth::user()->type=="user_collect"  )
                    url = "{{url('user_collect/tasks_history')}}"
                @else
                    url = "{{url('admin/tasks_history')}}"
                @endif
                $.ajax({
                    url: url, // Url to which the request is send
                    type: "POST",             // Type of request to be send, called as method
                    data: {
                        elem_id: elem_id,
                        _token: _token,
                    }, // Data sent to server, a set of key/value pairs (i.e. form fields and values)

                    success: function (data)   // A function to be called if request succeeds
                    {

                        if (data.status == 200) {


                            var items = data.items;

                            $('#tasks_history_items').html('');
                            if (items.length > 0) {
                                $('#tasks_history_items').html('');
                                var html = "";


                                for (var i = 0; i < items.length; i++) {

                                    html += '<tr>';
                                    html += '<td>' + items[i].order + '</td>';
                                    html += '<td>' + items[i].employee + '</td>';
                                    html += '<td>' + items[i].type + '</td>';
                                    html += '<td>' + items[i].edit_type + '</td>';

                                    html += '</tr>';
                                }
                                $('#tasks_history_items').append(html);
                            }

                        } else {

                        }


                    }
                });
            }


            function project_note(elem) {


                var _token = $('input[name="_token"]').val();
                var elem_id = $(elem).attr('item_id');

                var url = "";
                @if(\Illuminate\Support\Facades\Auth::user()->type=="employee")
                    url = "{{url('employee/project_note')}}"
                @elseif(\Illuminate\Support\Facades\Auth::user()->type=="user_collect"  )
                    url = "{{url('user_collect/project_note')}}"
                @else
                    url = "{{url('admin/project_note')}}"
                @endif
                $.ajax({
                    url: url, // Url to which the request is send
                    type: "POST",             // Type of request to be send, called as method
                    data: {
                        elem_id: elem_id,
                        _token: _token,
                    }, // Data sent to server, a set of key/value pairs (i.e. form fields and values)

                    success: function (data)   // A function to be called if request succeeds
                    {

                        if (data.status == 200) {


                            var items = data.data;


                            $('#project_user_note').val(items.project_user_note)
                            $('#project_technical_supervisor_note').val(items.project_technical_supervisor_note)
                            $('#order_note').val(items.order_note)
                            $('#clice_plan').val(items.clice_plan)
                            $('#clice_design').val(items.clice_design)
                            $('#customer_code').val(items.customer_code)
                            // $('#customer_note').val(items.customer_note)


                        } else {

                        }


                    }
                });
            }


            function task_time(elem) {
                console.log($(elem).attr('delivery_date'))

                $('#delivery_date_').val($(elem).attr('delivery_date'))
                $('#order_id_edit_task').val($(elem).attr('item_id'))
            }
        </script>
