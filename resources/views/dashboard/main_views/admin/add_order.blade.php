@extends('dashboard.layout.index')
@section('content')

    <!-- Basic table -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">إضافة طلب جديد</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <form action="{{url('admin/add_order')}}" id="add_form" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="row">


                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>اسم المشروع </label>
                            <select class="select-search" name="project" id="project" required="required"
                                    onchange="get_code_order(this)">
                                <option value="">اختر</option>
                                @foreach(\App\Models\ProjectModel::where('deleted',0)->get() as $item)
                                    <option
                                        value="{{$item->id}}">{{$item->name_ar." - ".\App\User::find($item->user)->code}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>ترقيم الطلب </label>
                            <input type="text" id="order_number" onchange="check_validation(this)" name="order_number"
                                   class="form-control" placeholder=""
                                   required="required">
                        </div>
                    </div>

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>تاريخ استلام الطلب </label>
                            <input type="date" name="date_received" class="form-control" placeholder=""
                                   required="required">
                        </div>
                    </div>

                    {{--<div class="col-lg-6">--}}

                    {{--<div class="form-group">--}}
                    {{--<label>مسار الطلب </label>--}}
                    {{--<input type="text" name="order_direction" class="form-control" placeholder=""--}}
                    {{--required="required">--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>تاريخ تسليم الطلب</label>
                            <input type="date" id="delivery_date" name="delivery_date" class="form-control"
                                   placeholder=""
                            >
                        </div>
                    </div>


                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>الأهمية</label>
                            <select class="select-search" id="add_importance" name="importance" required="required">
                                <option value="normal">عادي</option>
                                <option value="important">عالي</option>

                            </select>
                        </div>

                    </div>

                    <div class="col-lg-12">

                        <div class="form-group">
                            <label>ملاحظات</label>
                            <textarea name="note" class="form-control" placeholder=""
                            ></textarea>
                        </div>
                    </div>

                    <div class="col-lg-6">


                        <div class="form-group" id="edt_tybe_block">
                            <label>إنجاز المشروع</label>
                            <select class="select-search" name="completion" id="completion"
                                    onchange="disable_type(this)"
                            >
                                <option value="">اختر</option>
                                <option value="start">بداية المشروع</option>
                                <option value="delivery_design">تسليم التصميم</option>
                                <option value="delivery_all_project">تسليم كامل المشروع</option>
                                <option value="other">غيرها</option>

                            </select>
                        </div>
                    </div>
                    <div class=" col-lg-6">
                        <div class="form-group" id="edit_other_content">
                            <label>ملاحظة الانجاز</label>


                            <input type="text" name="edit_other" id="edit_other_input" class="form-control"
                                   placeholder="" >
                        </div>
                    </div>


                </div>
                <button type="submit"
                        class="btn btn-primary">{{\App\Http\Controllers\dashboard\ConstantController::$add[$_SESSION['lang']]}}
                    <i style="    float: right; " id="loader" class="icon-spinner spinner position-left"></i>
                </button>

            </form>
        </div>

    </div>

    </div>
    <!-- /basic table -->

    {{csrf_field()}}
    <script>
        var _token = $('input[name="_token"]').val();


        $('#edit_other_content').css('display', 'none');

        function disable_type(elem) {

            if ($(elem).val() == 'other') {

                console.log($(elem).val());
                $('#edit_other_content').css('display', 'block');
            }
            else {

                $('#edit_other_content').css('display', 'none');
            }


        }


            <?php  if (isset($project) && $project > 0): ?>
        var project = '<?php echo $project?>';
        console.log(project);

        $('#project').val(project).change();

        $('.select-search').select2();

        <?php endif ?>


        function get_code_order(elem) {

            var elem_id = $(elem).val();
            $.ajax({
                url: "{{url('admin/get_next_order_code')}}", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: {
                    item_id: elem_id,
                    _token: _token,
                }, // Data sent to server, a set of key/value pairs (i.e. form fields and values)

                success: function (data)   // A function to be called if request succeeds
                {

                    if (data.status == 200) {


                        var salary = data.salary;


                        $('#order_number').val(data.code)


                    } else {
                        swal({
                                title: "هل هذا الطلب الأول للمشروع؟",
                                text: "",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#EF5350",
                                confirmButtonText: "نعم",
                                cancelButtonText: "لا",
                                closeOnConfirm: true,
                                closeOnCancel: true
                            },
                            function (isConfirm) {

                                if (isConfirm) {

                                    $('#order_number').val(1);

                                }
                                else {

                                    $('#order_number').val('');
                                }
                            });
                    }


                }
            });

        }

        function check_validation(elem) {

            {{--var _token = $('input[name="_token"]').val();--}}

            {{--if($('#order_number').val().length>0 )--}}
            {{--{--}}
            {{--$.ajax({--}}
            {{--url: "{{url('admin/check_validation_order')}}", // Url to which the request is send--}}
            {{--type: "POST",             // Type of request to be send, called as method--}}
            {{--data: {--}}
            {{--order_number: $('#order_number').val(),--}}
            {{--_token: _token,--}}
            {{--}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)--}}

            {{--success: function (data)   // A function to be called if request succeeds--}}
            {{--{--}}


            {{--if (data.status == 403) {--}}

            {{--new Noty({--}}
            {{--text: data.message,--}}
            {{--type: 'error',--}}
            {{--dismissQueue: true,--}}
            {{--timeout: 4000,--}}
            {{--layout: "topRight",--}}
            {{--}).show();--}}


            {{--} else {--}}

            {{--}--}}


            {{--}--}}
            {{--});--}}
            {{--}--}}


        }


        $('#delivery_date').change(function () {

            var d = new Date($(this).val());
            var n = d.getDay();
            if (n == 5) {
                new Noty({

                    text: "لقد اخترت يوم الجمعة لموعد التسليم",
                    type: 'warning',
                    dismissQueue: true,
                    timeout: 4000,
                    layout: "topRight",
                }).show();
            }
        })


        $('#loader').css('display', 'none');

        $("#add_form").submit(function (e) {

            e.preventDefault(); // avoid to execute the actual submit of the form.

            $('#loader').css('display', 'block');
            console.log("dfd");
            $.ajax({
                    url: "{{url('admin/add_order_ajax')}}", // Url to which the request is send
                    type: "POST",             // Type of request to be send, called as method
                    data: $(this).serialize(),
                    success: function (data)   // A function to be called if request succeeds
                    {


                        $('#loader').css('display', 'none');
                        if (data.status == 403 || data.status == 400) {

                            for (var i = 0; i < data.message.length; i++) {
                                new Noty({

                                    text: "" + data.message[i].message,
                                    type: 'error',
                                    dismissQueue: true,
                                    timeout: 4000,
                                    layout: "topRight",
                                }).show();
                            }
                        }
                        else if (data.status == 200) {


                            $('#add_form').trigger("reset");

                            $('#add_importance').val();
                            $('#project').val();
                            $('.select-search').select2();


                            new Noty({

                                text: "" + data.message[0].message,
                                type: 'success',
                                dismissQueue: true,
                                timeout: 4000,
                                layout: "topRight",
                            }).show();

                            {{--setTimeout(function(){--}}

                            {{--window.location.href='{{url('admin/orders')}}';--}}
                            {{--},1000);--}}

                        }
                    }
                }
            );

        });


    </script>
@endsection
