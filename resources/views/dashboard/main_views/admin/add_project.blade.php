@extends('dashboard.layout.index')
@section('content')

    <!-- Basic table -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">إضافة مشروع جديد</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <form action="{{url('admin/add_project')}}" id="add_form" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="row">



                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>الاسم بالعربي </label>
                            <input type="text" name="name_ar" id="add_name_ar" onchange="check_validation(this)" class="form-control" placeholder=""
                                   required="required">
                        </div>
                    </div>


                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>الاسم بالانكليزي </label>
                            <input type="text" name="name_en" class="form-control" placeholder=""
                                   required="required">
                        </div>
                    </div>




                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>رمز الزبون </label>
                            <select class="select-search" id="add_name" name="user" required="required">
                                <option value="">اختر</option>
                                @foreach(\App\User::where('type','customer')->where('deleted',0)->get() as $item)
                                    <option value="{{$item->id}}">{{$item->code}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>نوع المشروع</label>
                            <select class="select-search" id="add_type" name="type" required="required">

                                <option value="">اختر</option>
                                <option value="geometric">هندسي</option>
                                <option value="advertising">إعلان</option>
                                <option value="marketing">تسويق</option>

                            </select>
                        </div>

                    </div>

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label> طبيعة المشروع </label>
                            <select class="select-search" id="add_nature" name="nature" required="required">
                                <option value="">اختر</option>
                                @foreach(\App\Models\NatureProjectModel::where('deleted',0)->get() as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>تاريخ استلام المشروع </label>
                            <input type="date" name="receive_date" class="form-control" placeholder=""
                                   required="required">
                        </div>
                    </div>

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>المساحة الداخلية </label>
                            <input type="text" name="internal_space" class="form-control" placeholder=""
                            >
                        </div>
                    </div>

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>السعر </label>
                            <input type="text" name="price" class="form-control" placeholder=""
                            >
                        </div>
                    </div>

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>العملة </label>
                            <select class="select-search" id="add_currency" name="currency">
                                <option value="">اختر</option>
                                <option value="doller">دولار</option>
                                <option value="euro">يورو</option>
                                <option value="ksa">ريال سعودي</option>
                                <option value="uae">درهم إماراتي</option>
                                <option value="sp">ليرة سورية</option>
                            </select>
                        </div>
                    </div>

                    {{--<div class="col-lg-6">--}}

                        {{--<div class="form-group">--}}
                            {{--<label>مسار التعديلات </label>--}}
                            {{--<input type="text" name="edit_direction" class="form-control" placeholder=""--}}
                                   {{--required="required">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-lg-6">--}}

                        {{--<div class="form-group">--}}
                            {{--<label>مسار المشروع </label>--}}
                            {{--<input type="text" name="project_direction" class="form-control" placeholder=""--}}
                                   {{--required="required">--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>ارتفاع السقف </label>
                            <input type="text" name="ceiling_height" class="form-control" placeholder=""
                            >
                        </div>
                    </div>
                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>نوع التكييف </label>
                            <input type="text" name="type_condition" class="form-control" placeholder=""
                            >
                        </div>
                    </div>
                    <div class="col-lg-6">

                        <div class="form-group">
                            <label> نزول التكييف </label>
                            <input type="text" name="space_condition" class="form-control" placeholder=""
                            >
                        </div>
                    </div>
                    <div class="col-lg-6">

                        <div class="form-group">
                            <label> مخطط التكييف </label>
                            <select class="select-search" id="add_planer_condition_file" name="planer_condition_file" >
                                <option value="">اختر</option>
                                <option value="1">يوجد</option>
                                <option value="0">لا يوجد</option>
                            </select>
                        </div>

                    </div>
                    <div class="col-lg-12">

                        <div class="form-group">
                            <label> ملاحظات المشروع </label>
                            <input type="text" name="user_note" class="form-control" placeholder=""
                            >
                        </div>
                    </div>

                    <div class="col-lg-12">

                        <div class="form-group">
                            <label> ملاحظات الفنية </label>
                            <input type="text" name="technical_supervisor_note" class="form-control" placeholder=""
                            >
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>انتهاء المشروع </label>
                            <select class="select-search" id="add_project_ended" name="project_ended" required="required">
                                <option value="0">لم ينتهي</option>
                                <option value="1">انتهى</option>
                            </select>
                        </div>
                    </div>

                </div>

                <button type="submit"
                        class="btn btn-primary">{{\App\Http\Controllers\dashboard\ConstantController::$add[$_SESSION['lang']]}}
                    <i  style="    float: right; " id="loader" class="icon-spinner spinner position-left"></i>
                </button>
            </form>
        </div>

    </div>

    </div>
    <!-- /basic table -->
{{csrf_field()}}
    <script>
        function check_validation(elem)
        {

            {{--var _token = $('input[name="_token"]').val();--}}
            {{--$.ajax({--}}
                {{--url: "{{url('admin/check_validation_project')}}", // Url to which the request is send--}}
                {{--type: "POST",             // Type of request to be send, called as method--}}
                {{--data: {--}}
                    {{--name_ar: $('#add_name_ar').val(),--}}
                    {{--_token: _token,--}}
                {{--}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)--}}

                {{--success: function (data)   // A function to be called if request succeeds--}}
                {{--{--}}


                    {{--if (data.status == 403) {--}}

                        {{--new Noty({--}}
                            {{--text: data.message,--}}
                            {{--type: 'error',--}}
                            {{--dismissQueue: true,--}}
                            {{--timeout: 4000,--}}
                            {{--layout: "topRight",--}}
                        {{--}).show();--}}


                    {{--} else {--}}

                    {{--}--}}


                {{--}--}}
            {{--});--}}
        }


        $('#loader').css('display', 'none');

        $("#add_form").submit(function (e) {

            e.preventDefault(); // avoid to execute the actual submit of the form.

            $('#loader').css('display', 'block');
            console.log("dfd");
            $.ajax({
                    url: "{{url('admin/add_project_ajax')}}", // Url to which the request is send
                    type: "POST",             // Type of request to be send, called as method
                    data: $(this).serialize(),
                    success: function (data)   // A function to be called if request succeeds
                    {


                        $('#loader').css('display', 'none');
                        if (data.status == 403 || data.status == 400) {

                            for (var i = 0; i < data.message.length; i++) {
                                new Noty({

                                    text: "" + data.message[i].message,
                                    type: 'error',
                                    dismissQueue: true,
                                    timeout: 4000,
                                    layout: "topRight",
                                }).show();
                            }
                        }
                        else if (data.status == 200) {


                            $('#add_form').trigger("reset");

                            $('#add_department').val();
                            $('.select-search').select2();


                            new Noty({

                                text: "" + data.message[0].message,
                                type: 'success',
                                dismissQueue: true,
                                timeout: 4000,
                                layout: "topRight",

                            }).show();

                            setTimeout(function(){

                                window.location.href='{{url('admin/projects')}}';
                            },1000);
                        }
                    }
                }
            );

        });

    </script>
@endsection
