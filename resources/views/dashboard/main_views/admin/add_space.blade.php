@extends('dashboard.layout.index')
@section('content')

    <!-- Basic table -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">إضافة فراغ جديد</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <form id="form_add" action="{{url('admin/add_space')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="row">


                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>اسم المشروع </label>
                            <select class="select-search" id="project" name="project" required="required">
                                <option value="">اختر</option>
                                @foreach(\App\Models\ProjectModel::where('deleted',0)->get() as $item)
                                    <option value="{{$item->id}}">{{$item->name_ar}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>نوع الفراغ </label>
                            <select class="select-search" id="type" name="type" required="required"
                                    onchange="set_elem(this)">
                                <option value="">اختر</option>
                                @foreach(\App\Models\SpaceTypeModel::where('deleted',0)->get() as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>موقع الفراغ </label>
                            <select class="select-search" id="location" name="location" required="required">
                                <option value="">اختر</option>
                                @foreach(\App\Models\SpaceLocationModel::where('deleted',0)->get() as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label> اسم الفراغ <span style="color: red" onclick="set_name(this)">تعيين الاسم </span>
                            </label>
                            <input type="text" name="name" id="name" class="form-control" placeholder=""
                                   required="required">
                        </div>
                    </div>

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>العدد </label>
                            <input type="text" name="count" id="count" class="form-control" placeholder=""
                            >
                        </div>
                    </div>

                </div>

                {{--<button type="submit"--}}
                        {{--class="btn btn-primary">{{\App\Http\Controllers\dashboard\ConstantController::$add[$_SESSION['lang']]}}</button>--}}




                <button type="button" onclick="save_one(this)"
                        class="btn btn-primary">حفظ
                    <i  style="    float: right; " id="loader_one" class="icon-spinner spinner position-left"></i>
                </button>

                <button type="button" onclick="save(this)"
                        class="btn btn-primary">إضافة فراغ جديد
                    <i  style="    float: right; " id="loader" class="icon-spinner spinner position-left"></i>
                </button>

                <button type="button" onclick="get_spaces(this)"

                        data-toggle="modal" data-target="#all_spaces"
                        class="btn btn-primary">فراغات المشروع
                </button>


            </form>
        </div>

    </div>

    </div>
    <!-- /basic table -->




    <div id="all_spaces" class="modal fade ">
        <div class="modal-dialog  modal-full ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">الفراغات </h5>
                </div>
                <div class="modal-body">

                    <div class="row">


                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>الموقع</th>
                                        <th>اسم الفراغ</th>

                                    </tr>
                                    </thead>
                                    <tbody id="items">

                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link"
                            data-dismiss="modal">{{\App\Http\Controllers\dashboard\ConstantController::$close[$_SESSION['lang']]}}</button>

                </div>
            </div>
        </div>
    </div>


    {{csrf_field()}}
    <script>


        $('#loader').css('display', 'none');
        $('#loader_one').css('display', 'none');
            <?php  if (isset($project) && $project > 0): ?>
        var project = '<?php echo $project?>';
        console.log(project);

        $('#project').val(project).change();

        $('.select-search').select2();
            <?php endif ?>

        var _token = $('input[name="_token"]').val();


        function set_elem(elem) {
            // $('#name').val($('#type  option:selected').text());

        }

        function set_name(elem) {

            $('#name').val($('#type  option:selected').text());
        }

        function save_one(elem) {

            $('#loader_one').css('display', 'block');
            var pr = $('#project').val();
            $.ajax({
                url: "{{url('admin/add_space_ajax')}}", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: {
                    project: $('#project').val(),
                    type: $('#type').val(),
                    location: $('#location').val(),
                    name: $('#name').val(),
                    count: $('#count').val(),
                    _token: _token,
                }, // Data sent to server, a set of key/value pairs (i.e. form fields and values)

                success: function (data)   // A function to be called if request succeeds
                {


                    $('#loader_one').css('display', 'none');
                    if (data.status == 403 || data.status == 400) {

                        for (var i = 0; i < data.message.length; i++) {
                            new Noty({

                                text: "" + data.message[i].message,
                                type: 'error',
                                dismissQueue: true,
                                timeout: 4000,
                                layout: "topRight",
                            }).show();
                        }
                    }
                    else if (data.status == 200) {



                        $('#form_add').trigger("reset");
                        $('.select-search').select2();

                        $('#project').val(pr).change();

                        new Noty({

                            text: "" + data.message[0].message,
                            type: 'success',
                            dismissQueue: true,
                            timeout: 4000,
                            layout: "topRight",

                        }).show();

                        window.location.href="{{url('admin/spaces')}}"

                    }


                }
            });


        }

        function save(elem) {

            $('#loader').css('display', 'block');
            var pr = $('#project').val();
            $.ajax({
                url: "{{url('admin/add_space_ajax')}}", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: {
                    project: $('#project').val(),
                    type: $('#type').val(),
                    location: $('#location').val(),
                    name: $('#name').val(),
                    count: $('#count').val(),
                    _token: _token,
                }, // Data sent to server, a set of key/value pairs (i.e. form fields and values)

                success: function (data)   // A function to be called if request succeeds
                {


                    $('#loader').css('display', 'none');
                    if (data.status == 403 || data.status == 400) {

                        for (var i = 0; i < data.message.length; i++) {
                            new Noty({

                                text: "" + data.message[i].message,
                                type: 'error',
                                dismissQueue: true,
                                timeout: 4000,
                                layout: "topRight",
                            }).show();
                        }
                    }
                    else if (data.status == 200) {



                        $('#form_add').trigger("reset");
                        $('.select-search').select2();

                        $('#project').val(pr).change();

                        new Noty({

                            text: "" + data.message[0].message,
                            type: 'success',
                            dismissQueue: true,
                            timeout: 4000,
                            layout: "topRight",

                        }).show();


                    }


                }
            });


        }


        var _token = $('input[name="_token"]').val();

        function get_spaces(elem) {

            var elem_id = $(elem).attr('item_id');
            $.ajax({
                url: "{{url('admin/get_spaces_project')}}", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: {
                    elem_id: $('#project').val(),
                    _token: _token,
                }, // Data sent to server, a set of key/value pairs (i.e. form fields and values)

                success: function (data)   // A function to be called if request succeeds
                {

                    if (data.status == 200) {


                        var items = data.data;

                        $('#items').html('');
                        if (items.length > 0) {
                            $('#items').html('');
                            var html = "";


                            for (var i = 0; i < items.length; i++) {

                                html += '<tr>';
                                html += '<td>' + items[i].id + '</td>';
                                html += '<td>' + items[i].location + '</td>';
                                html += '<td>' + items[i].name + '</td>';
                                html += '</tr>';
                            }
                            $('#items').append(html);

                            $('.datatable-basic').DataTable();
                        }


                        // Basic datatable


                    } else {

                    }


                }
            });
        }
    </script>

@endsection
