@extends('dashboard.layout.index')
@section('content')

    <!-- Basic table -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">إضافة مهمة جديد</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <form id="form_add" action="{{url('admin/add_task')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="row">


                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>اسم المشروع </label>
                            <select class="select-search" name="project" id="project" required="required"
                                    onchange="getOrderAndSpace(this)">
                                <option value="">اختر</option>
                                @foreach(\App\Models\ProjectModel::where('deleted',0)->get() as $item)
                                    <option
                                        value="{{$item->id}}">{{$item->name_ar." - ".\App\User::find($item->user)->code}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>الطلب</label>
                            <select class="select-search" name="order" id="order" required="required"
                                    onchange="set_time(this)"
                            >

                            </select>
                        </div>

                    </div>
                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>الفراغ</label>
                            <select class="select-search" name="space" id="space" required="required"
                            >

                            </select>
                        </div>

                    </div>


                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>نوع التاسك </label>
                            <select class="select-search" name="type" required="required" id="type"
                                    onchange="disable_type(this)"
                            >
                                <option value="">اختر</option>
                                @foreach(\App\Models\TaskTypeModel::where('deleted',0)->get() as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>

                    <div class="col-lg-6">

                        <div class="form-group" id="edt_tybe_block">
                            <label>نوع التعديل </label>
                            <select class="select-search" name="edit_type" id="edit_type"
                            >
                                <option value="">اختر</option>
                                @foreach(\App\Models\TaskEditTypeModel::where('deleted',0)->get() as $item)
                                    <option
                                        value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group" id="note_plan_content">
                            <label>ملاحظة المخطط التنفيذي </label>

                            <input type="text" name="note_plan" id="note_plan" class="form-control" placeholder=""
                                   required="required">
                        </div>


                    </div>


                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>المشرف </label>
                            <select class="select-search" name="technical_supervisor" required="required"
                                    id="technical_supervisor"
                            >
                                @foreach(\App\User::where('type','technical_supervisor')->get() as $item)
                                    <option
                                        value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>الموظف </label>
                            <select class="select-search" name="employee" id="employee"
                            >
                                <option value="">اختر</option>
                                @foreach(\App\User::where('type','employee')->where('employee_active',1)->get() as $item)
                                    <option
                                        value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>

                    <div class="col-lg-6">

                        <div class="form-group">

                            <label> تاريخ تسليم المهمة</label> <span><a href="javascript:;" id="dev_date_1"
                                                                        dev_date_1="" onclick="set_date_past(this)">موعد التسليم -1</a></span>
                            <input type="date" name="start_date" id="start_date" class="form-control" placeholder=""
                                   required="required">
                        </div>
                    </div>


                    {{--<div class="col-lg-12">--}}

                    {{--<div class="form-group">--}}
                    {{--<label>مسار التاسك</label>--}}
                    {{--<input type="text" name="task_direction" class="form-control" placeholder=""--}}
                    {{--id="task_direction"--}}
                    {{--required="required">--}}
                    {{--</div>--}}
                    {{--</div>--}}


                </div>

                {{--<button type="submit"--}}
                {{--class="btn btn-primary">{{\App\Http\Controllers\dashboard\ConstantController::$add[$_SESSION['lang']]}}</button>--}}

                <button type="button" onclick="save_new(this)"
                        class="btn btn-primary">حفظ
                    <i style="    float: right; " id="loader_new" class="icon-spinner spinner position-left"></i>
                </button>

                <button type="button" onclick="save(this)"
                        class="btn btn-primary">إضافة مهمة ثانية للطلب
                    <i style="    float: right; " id="loader" class="icon-spinner spinner position-left"></i>
                </button>


                <button type="button" onclick="get_tasks(this)"

                        data-toggle="modal" data-target="#all_task"
                        class="btn btn-primary">مهام الطلب
                </button>

            </form>
        </div>

    </div>

    </div>




    <div id="all_task" class="modal fade ">
        <div class="modal-dialog  modal-full ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">المهام </h5>
                </div>
                <div class="modal-body">

                    <div class="row">


                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>المشروع</th>
                                        <th>الطلب</th>
                                        <th>الفراغ</th>
                                        <th>تاريخ التسليم</th>
                                        <th>الموظف</th>
                                    </tr>
                                    </thead>
                                    <tbody id="items">

                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link"
                            data-dismiss="modal">{{\App\Http\Controllers\dashboard\ConstantController::$close[$_SESSION['lang']]}}</button>

                </div>
            </div>
        </div>
    </div>


    <!-- /basic table -->

    {{csrf_field()}}
    <script>


        $('#edt_tybe_block').css('display', 'none');
        $('#note_plan_content').css('display', 'none');

        function disable_type(elem) {

            if ($(elem).val() == '2') {

                console.log($(elem).val());
                $('#edt_tybe_block').css('display', 'block');
                $('#note_plan_content').css('display', 'none');
            }
            else if ($(elem).val() == '3') {

                $('#edt_tybe_block').css('display', 'none');
                $('#note_plan_content').css('display', 'block');


            }
            else {

                $('#edt_tybe_block').css('display', 'none');
                $('#note_plan_content').css('display', 'none');

            }

        }


        $('#loader').css('display', 'none');
        $('#loader_new').css('display', 'none');

        var _token = $('input[name="_token"]').val();

            <?php  if (isset($order) && $order > 0): ?>
        var order = '<?php echo $order?>';
        var project = '<?php echo $project?>';
        console.log(order);
        console.log(project);

        $('#project').val(project).change();

        $('.select-search').select2();

        <?php endif ?>
        function getOrderAndSpace(elem) {

            $.ajax({
                url: "{{url('admin/get_orders_spaces')}}", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: {
                    project: $(elem).val(),

                    _token: _token,
                }, // Data sent to server, a set of key/value pairs (i.e. form fields and values)

                success: function (data)   // A function to be called if request succeeds
                {


                    $('#order').html('');
                    $('#space').html('');
                    if (data.status == 200) {


                        console.log(data.orders.length);
                        console.log(data.space);

                        var orders = data.orders;
                        var spaces = data.space;

                        if (orders.length > 0) {
                            var html = "";
                            html += '<option value="">اختر</option>';
                            for (var i = 0; i < orders.length; i++) {

                                html += '<option date_received="' + orders[i].date_received + '" delivery_date="' + orders[i].delivery_date + '" value="' + orders[i].id + '">' + orders[i].order_number + ' | ' + orders[i].order_direction + '</option>';
                            }
                            $('#order').append(html);

                            $('#order').val(order).change();
                        }

                        if (spaces.length > 0) {
                            var html = "";
                            html += '<option value="">اختر</option>';
                            for (var i = 0; i < spaces.length; i++) {

                                html += '<option value="' + spaces[i].id + '">' + spaces[i].location + " - " + spaces[i].name + '</option>';
                            }
                            $('#space').append(html);
                        }

                        $('.select-search').select2();
                    } else {

                    }


                }
            });


        }

        function set_time(elem) {


            var delivery_date = $('option:selected', elem).attr('delivery_date');
            var date_received = $('option:selected', elem).attr('date_received');

            if (delivery_date != null) {
                $('#start_date').val(delivery_date);

                $('#dev_date_1').text(date_received);

            }

        }

        function set_date_past(elem) {


            $('#start_date').val($(elem).text());

        }


        function check_order_have_tasks() {

            $('#loader').css('display', 'block');

            $.ajax({
                url: "{{url('admin/check_order_have_tasks')}}", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: {
                    space: $('#space').val(),
                    _token: _token,
                }, // Data sent to server, a set of key/value pairs (i.e. form fields and values)

                success: function (data)   // A function to be called if request succeeds
                {


                    $('#loader').css('display', 'none');


                    if (data.status == 200) {


                        if (data.more_than == 1) {


                            swal({
                                    title: " هناك أكثر من مهمة لهذا الفراغ هل تريد المتابعة؟",
                                    text: "",
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonColor: "#EF5350",
                                    confirmButtonText: "نعم",
                                    cancelButtonText: "لا",
                                    closeOnConfirm: true,
                                    closeOnCancel: true
                                },
                                function (isConfirm) {

                                    if (isConfirm) {

                                        save(this);
                                    }
                                    else {


                                    }
                                });

                            //
                            // new Noty({
                            //
                            //     text: "هناك أكثر من مهمة لهذا الفراغ",
                            //     type: 'warning',
                            //     dismissQueue: true,
                            //     timeout: 4000,
                            //     layout: "topRight",
                            // }).show();
                            //


                        }

                    } else {


                    }


                }
            });


        }

        var if_more_than = 0;

        function save(elem) {


            $('#loader').css('display', 'block');

            var pr = $('#project').val();
            var order = $('#order').val();
            console.log("irder");
            console.log(order);
            $.ajax({
                url: "{{url('admin/add_task_ajax')}}", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: {
                    project: $('#project').val(),
                    order: $('#order').val(),
                    space: $('#space').val(),
                    type: $('#type').val(),
                    edit_type: $('#edit_type').val(),
                    technical_supervisor: $('#technical_supervisor').val(),
                    employee: $('#employee').val(),
                    start_date: $('#start_date').val(),
                    task_direction: $('#task_direction').val(),
                    if_more_than: if_more_than,
                    _token: _token,
                }, // Data sent to server, a set of key/value pairs (i.e. form fields and values)

                success: function (data)   // A function to be called if request succeeds
                {


                    $('#loader').css('display', 'none');


                    if (data.status == 200) {


                        if (data.added == 1) {

                            if_more_than = 0;
                            //   $('#form_add').trigger("reset");
                            // $('#edit_type option:first').attr('selected', true);
                            // $('#space option:first').attr('selected', true);
                            // $('#type option:first').attr('selected', true);
                            // $('#employee option:first').attr('selected', true);

                            $('#edit_type').val('');
                            $('#space').val('');
                            $('#type').val('');
                            $('#employee').val('');
                            $('#note_plan').val('');
                            $('#start_date').val('');


                            $('#edt_tybe_block').css('display', 'none');
                            $('#note_plan_content').css('display', 'none');

                            // $('#start_date').val('');
                            $('#task_direction').val('');


                            $('.select-search').select2();
                            new Noty({

                                text: "تمت الاضافة بنجاح",
                                type: 'success',
                                dismissQueue: true,
                                timeout: 4000,
                                layout: "topRight",
                            }).show();


                        }
                        else {

                            if (data.more_than == 1) {

                                if_more_than = 1;

                                swal({
                                        title: " هناك أكثر من مهمة لهذا الفراغ هل تريد المتابعة؟",
                                        text: "",
                                        type: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: "#EF5350",
                                        confirmButtonText: "نعم",
                                        cancelButtonText: "لا",
                                        closeOnConfirm: true,
                                        closeOnCancel: true
                                    },
                                    function (isConfirm) {

                                        if (isConfirm) {

                                            save(this);
                                        }
                                        else {


                                            if_more_than = 0;
                                        }
                                    });

                            }


                        }


                        // if (data.more_than == 1) {
                        //     new Noty({
                        //
                        //         text: "هناك أكثر من مهمة لهذا الفراغ",
                        //         type: 'warning',
                        //         dismissQueue: true,
                        //         timeout: 4000,
                        //         layout: "topRight",
                        //     }).show();
                        //
                        // }

                    } else {


                        for (var i = 0; i < data.message.length; i++) {
                            new Noty({

                                text: "" + data.message[i].message,
                                type: 'error',
                                dismissQueue: true,
                                timeout: 4000,
                                layout: "topRight",
                            }).show();
                        }

                    }


                }
            });


        }


        function save_new(elem) {


            $('#loader_new').css('display', 'block');

            var pr = $('#project').val();
            var order = $('#order').val();
            console.log("irder");
            console.log(order);
            $.ajax({
                url: "{{url('admin/add_task_ajax')}}", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: {
                    project: $('#project').val(),
                    order: $('#order').val(),
                    space: $('#space').val(),
                    type: $('#type').val(),
                    edit_type: $('#edit_type').val(),
                    technical_supervisor: $('#technical_supervisor').val(),
                    employee: $('#employee').val(),
                    start_date: $('#start_date').val(),
                    task_direction: $('#task_direction').val(),
                    note_plan: $('#note_plan').val(),
                    if_more_than: if_more_than,
                    _token: _token,
                }, // Data sent to server, a set of key/value pairs (i.e. form fields and values)

                success: function (data)   // A function to be called if request succeeds
                {


                    $('#loader_new').css('display', 'none');


                    if (data.status == 200) {


                        if (data.added == 1) {

                            if_more_than = 0;
                            //   $('#form_add').trigger("reset");
                            // $('#edit_type option:first').attr('selected', true);
                            // $('#space option:first').attr('selected', true);
                            // $('#type option:first').attr('selected', true);
                            // $('#employee option:first').attr('selected', true);

                            $('#edit_type').val('');
                            $('#space').val('');
                            $('#type').val('');
                            $('#employee').val('');


                            // $('#start_date').val('');
                            $('#task_direction').val('');


                            $('.select-search').select2();
                            new Noty({

                                text: "تمت الاضافة بنجاح",
                                type: 'success',
                                dismissQueue: true,
                                timeout: 4000,
                                layout: "topRight",
                            }).show();


                            window.location.href = "{{url('admin/tasks')}}"
                        }
                        else {

                            if (data.more_than == 1) {

                                if_more_than = 1;

                                swal({
                                        title: " هناك أكثر من مهمة لهذا الفراغ هل تريد المتابعة؟",
                                        text: "",
                                        type: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: "#EF5350",
                                        confirmButtonText: "نعم",
                                        cancelButtonText: "لا",
                                        closeOnConfirm: true,
                                        closeOnCancel: true
                                    },
                                    function (isConfirm) {

                                        if (isConfirm) {

                                            save_new(this);
                                        }
                                        else {


                                            if_more_than = 0;
                                        }
                                    });

                            }


                        }


                        // if (data.more_than == 1) {
                        //     new Noty({
                        //
                        //         text: "هناك أكثر من مهمة لهذا الفراغ",
                        //         type: 'warning',
                        //         dismissQueue: true,
                        //         timeout: 4000,
                        //         layout: "topRight",
                        //     }).show();
                        //
                        // }

                    } else {


                        for (var i = 0; i < data.message.length; i++) {
                            new Noty({

                                text: "" + data.message[i].message,
                                type: 'error',
                                dismissQueue: true,
                                timeout: 4000,
                                layout: "topRight",
                            }).show();
                        }

                    }


                }
            });


        }


        function get_tasks(elem) {

            var elem_id = $(elem).attr('item_id');
            $.ajax({
                url: "{{url('admin/get_tasks_order')}}", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: {
                    elem_id: $('#order').val(),
                    _token: _token,
                }, // Data sent to server, a set of key/value pairs (i.e. form fields and values)

                success: function (data)   // A function to be called if request succeeds
                {

                    if (data.status == 200) {


                        var items = data.items;

                        $('#items').html('');
                        if (items.length > 0) {
                            $('#items').html('');
                            var html = "";


                            for (var i = 0; i < items.length; i++) {

                                html += '<tr>';
                                html += '<td>' + items[i].id + '</td>';
                                html += '<td>' + items[i].project + '</td>';
                                html += '<td>' + items[i].order + '</td>';
                                html += '<td>' + items[i].space + '</td>';
                                html += '<td>' + items[i].start_date + '</td>';
                                html += '<td>' + items[i].employee + '</td>';
                                html += '</tr>';
                            }
                            $('#items').append(html);

                            $('.datatable-basic').DataTable();
                        }


                        // Basic datatable


                    } else {

                    }


                }
            });
        }

        $('#start_date').change(function () {

            var d = new Date($(this).val());
            var n = d.getDay();
            if (n == 5) {
                new Noty({

                    text: "لقد اخترت يوم الجمعة لموعد التسليم",
                    type: 'warning',
                    dismissQueue: true,
                    timeout: 4000,
                    layout: "topRight",
                }).show();
            }
        })

    </script>
@endsection
