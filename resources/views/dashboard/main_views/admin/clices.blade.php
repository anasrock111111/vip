@extends('dashboard.layout.index')
@section('content')

    <!-- Body classes -->
    <style>

        .image_review {
            width: 50%;
            margin-top: 5%;
        }
    </style>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title"> {{\App\Http\Controllers\dashboard\ConstantController::$content[$_SESSION['lang']]}} {{$view_name}}</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <button data-toggle="modal" data-target="#add" data-popup="tooltip" title="إضافة" type="button"
                            class="btn border-slate text-slate-800 btn-flat"><i class="icon-add "></i>
                    </button>

                </ul>

            </div>

        </div>

        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th>#</th>
                    <th>أضيف بواسطة</th>
                    <th>الاسم</th>
                    <th>النوع</th>
                    <th>افتراضية</th>
                    <th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($clices as $key=>$item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{\App\User::find($item->added_by)->name}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{\App\Http\Controllers\dashboard\ConstantController::clice_type($item->type)}}</td>
                        <td> @if($item->default==0)
                                لا
                            @else
                                نعم
                            @endif
                        </td>
                        <td style="    text-align: center;">


                            <ul class="icons-list">

                                @if($item->default==0)

                                    <li><a href="{{url('admin/defautl_clices/'.$item->id."?type=".$item->type)}}"
                                           data-popup="tooltip"
                                           title="القيمة الافتراضية"
                                        ><i class="glyphicon glyphicon-star"></i></a></li>

                                @endif
                                <li><a href="javascript:;" data-popup="tooltip" title="تعديل"
                                       full_name="{{$item->name}}"
                                       type_clice="{{$item->type}}"
                                       item_id="{{$item->id}}"
                                       onclick="event_edit(this)"
                                       data-toggle="modal" data-target="#edit"
                                    ><i class="icon-pencil7"></i></a></li>


                                <li><a href="javascript:;" data-popup="tooltip" title="حذف"
                                       onclick="remove_item(this)" item_id="{{$item->id}}"
                                    ><i class="icon-trash"></i></a></li>
                            </ul>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
    <!-- /body classes -->





    <div id="add" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">إضافة </h5>
                </div>
                <form action="{{url('admin/add_clice')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">

                        <div class="row">


                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الاسم </label>
                                    <input type="text" name="name" class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>
                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>النوع</label>
                                    <select class="select-search" name="type" required="required">
                                        <option value="design">تصميم 3D</option>
                                        <option value="plan">مخططات تنفيذية</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link"
                                data-dismiss="modal">{{\App\Http\Controllers\dashboard\ConstantController::$close[$_SESSION['lang']]}}</button>
                        <button type="submit"
                                class="btn btn-primary">{{\App\Http\Controllers\dashboard\ConstantController::$add[$_SESSION['lang']]}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div id="edit" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">تعديل </h5>
                </div>
                <form action="{{url('admin/edit_clice')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <input type="hidden" id="item_id" name="item_id" value="true">
                        <div class="row">


                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الاسم </label>
                                    <input id="name" type="text" name="name" class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>
                            <div class="col-lg-6">


                                <div class="form-group">
                                    <label>النوع</label>
                                    <select class="select-search" id="type" name="type" required="required">
                                        <option value="design">تصميم 3D</option>
                                        <option value="plan">مخططات تنفيذية</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">إلغاء</button>
                        <button type="submit" class="btn btn-primary">حفظ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>


        function event_edit(elem) {

            $("#name").val($(elem).attr('full_name'));
            $("#type").val($(elem).attr('type_clice')).change();
            $("#item_id").val($(elem).attr('item_id'));

        }


        function remove_item(elem) {

            swal({
                    title: "{{\App\Http\Controllers\dashboard\ConstantController::$are_you_sure[$_SESSION['lang']]}}",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#EF5350",
                    confirmButtonText: "{{\App\Http\Controllers\dashboard\ConstantController::$on[$_SESSION['lang']]}}",
                    cancelButtonText: "{{\App\Http\Controllers\dashboard\ConstantController::$off[$_SESSION['lang']]}}",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {

                    if (isConfirm) {
                        var item_id = $(elem).attr('item_id');
                        var url = "{{url('admin/remove_clice')}}";
                        $('<form>', {
                            "action": url + "/" + item_id,
                        }).appendTo(document.body).submit();

                        {{--swal({--}}
                            {{--title: "{{\App\Http\Controllers\dashboard\ConstantController::$deleted[$_SESSION['lang']]}} ",--}}
                            {{--text: "{{\App\Http\Controllers\dashboard\ConstantController::$deleted_success[$_SESSION['lang']]}}",--}}
                            {{--confirmButtonColor: "#66BB6A",--}}
                            {{--type: "success"--}}
                        {{--});--}}
                    }
                    else {
                        swal({
                            title: "{{\App\Http\Controllers\dashboard\ConstantController::$close[$_SESSION['lang']]}}",
                            text: "",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });

        }
    </script>

@endsection
