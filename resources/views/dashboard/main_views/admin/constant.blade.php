@extends('dashboard.layout.index')
@section('content')


    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">الثوابت </h5>
            <div class="heading-elements">
            </div>
        </div>
        <div class="panel-body">
            <form action="{{url('admin/constant')}}" method="post">

                {{csrf_field()}}
                <div class="row">


                    <div class="col-md-6">
                        <div class="form-group">
                            <label>مسار التعديلات </label>
                            <input dir="ltr" type="text" name="edit_dir" class="form-control"
                                   value="{{$constants['edit_dir']}}"
                                   placeholder="" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>مسار المشروع</label>
                            <input dir="ltr" type="text" dir="ltr" name="project_dir" class="form-control"
                                   value="{{$constants['project_dir']}}"
                                   placeholder="" required>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>تفعيل تقييم المشرف الفني </label>


                            <select class="select-search"  name="if_rate"
                                    required="required">
                                @if($constants['if_rate']=="yes")

                                    <option value="yes" selected>نعم</option>
                                    <option value="no">لا</option>
                                    @else

                                    <option value="yes">نعم</option>
                                    <option  selected value="no">لا</option>
                                    @endif
                            </select>

                        </div>
                    </div>


                </div>

                <?php
                function get_client_ip() {
                    $ipaddress = '';
                    if (isset($_SERVER['HTTP_CLIENT_IP']))
                        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
                    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
                    else if(isset($_SERVER['HTTP_X_FORWARDED']))
                        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
                    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
                    else if(isset($_SERVER['HTTP_FORWARDED']))
                        $ipaddress = $_SERVER['HTTP_FORWARDED'];
                    else if(isset($_SERVER['REMOTE_ADDR']))
                        $ipaddress = $_SERVER['REMOTE_ADDR'];
                    else
                        $ipaddress = 'UNKNOWN';
                    return $ipaddress;
                }

                echo 'User Real IP - '.get_client_ip();
                ?>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary"> حفظ <i
                            class="icon-arrow-right14 position-right"></i></button>
                </div>
            </form>

        </div>

    </div>
    <script>


    </script>
    <!-- /body classes -->
@endsection
