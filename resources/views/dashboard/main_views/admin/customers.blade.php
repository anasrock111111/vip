@extends('dashboard.layout.index')
@section('content')

    <!-- Body classes -->
    <style>

        .image_review {
            width: 50%;
            margin-top: 5%;
        }

        .datatable-scroll {

            overflow-x: scroll;
        }
    </style>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title"> {{\App\Http\Controllers\dashboard\ConstantController::$content[$_SESSION['lang']]}} {{$view_name}}</h5>
            <div class="heading-elements">
                @if(\Illuminate\Support\Facades\Auth::user()->type=="admin")

                    <ul class="icons-list">
                        <button data-toggle="modal" data-target="#add" data-popup="tooltip" title="إضافة" type="button"
                                class="btn border-slate text-slate-800 btn-flat"><i class="icon-add "></i>
                        </button>

                    </ul>
                @endif
            </div>

        </div>

        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>
                    <th>#</th>
                    <th>أضيف بواسطة</th>
                    <th>الاسم</th>
                    <th>الرمز</th>
                    <th>العمل</th>
                    <th>الملكية</th>
                    <th>الدولة</th>
                    <th>المدينة</th>
                    <th>الشخص المسوول</th>
                    <th>العنوان</th>
                    <th>الجوال</th>
                    <th>الهاتف</th>
                    <th>الايميل</th>
                    <th>معلومات شخص اخر (اسم)</th>
                    <th>معلومات شخص اخر (هاتف)</th>
                    <th>الزبون عن طريق</th>
                    <th>ويب سايت</th>
                    <th>فيسبوك</th>
                    <th>انستغرام</th>
                    <th>تويتر</th>
                    <th>ملاحظات</th>
                    <th>كليشة التصميم</th>
                </tr>
                </thead>
                <tbody>
                @foreach($customers as $key=>$item)
                    <tr>
                        <td style="    text-align: center;">

                            <ul class="icons-list">
                                <li><a href="{{url('admin/files/'.$item->id."?type=user")}}" data-popup="tooltip"
                                       title="الملفات"
                                    ><i class="icon icon-files-empty2"></i></a></li>
                                <li><a href="javascript:;" data-popup="tooltip" title="تعديل"
                                       full_name="{{$item->name}}"
                                       email="{{$item->email}}"
                                       address="{{$item->address}}"
                                       code="{{$item->code}}"
                                       phone="{{$item->phone}}"
                                       work="{{$item->work}}"
                                       customer_ownership="{{$item->customer_ownership}}"
                                       country="{{$item->country}}"
                                       city="{{$item->city}}"
                                       responsible_person="{{$item->responsible_person}}"
                                       mobile="{{$item->mobile}}"
                                       skype="{{$item->skype}}"
                                       other_contact_name="{{$item->other_contact_name}}"
                                       other_contact_phone="{{$item->other_contact_phone}}"
                                       note="{{$item->note}}"
                                       website="{{$item->website}}"
                                       facebook="{{$item->facebook}}"
                                       twitter="{{$item->twitter}}"
                                       instagram="{{$item->instagram}}"
                                       user_connect="{{$item->user_connect}}"
                                       clice_plan="{{$item->clice_plan}}"
                                       clice_design="{{$item->clice_design}}"
                                       item_id="{{$item->id}}"
                                       onclick="event_edit(this)"
                                       data-toggle="modal" data-target="#edit"
                                    ><i class="icon-pencil7"></i></a></li>
                                <li><a href="javascript:;" data-popup="tooltip" title="حذف"
                                       onclick="remove_item(this)" item_id="{{$item->id}}"
                                    ><i class="icon-trash"></i></a></li>
                            </ul>
                        </td>
                        <td>{{$item->id}}</td>

                        <td>{{\App\User::find($item->added_by)->name}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->code}}</td>

                        @if($item->work>0)
                            <td>{{\App\Models\WorkTypeModel::find($item->work)->name}}</td>
                        @else

                            <td></td>
                        @endif
                        <td>{{$item->customer_ownership}}</td>
                        <td>{{$item->country}}</td>
                        <td>{{$item->city}}</td>
                        <td>{{$item->responsible_person}}</td>
                        <td>{{$item->address}}</td>
                        <td>{{$item->mobile}}</td>
                        <td>{{$item->phone}}</td>
                        <td>{{$item->email}}</td>
                        <td>{{$item->other_contact_name}}</td>
                        <td>{{$item->other_contact_phone}}</td>
                        <td>

                            @if($item->user_connect)
                                {{ \App\User::find($item->user_connect)->name}}
                            @endif

                        </td>
                        <td><a target="_blank" href="{{$item->website}}">{{$item->website}}</a></td>
                        <td><a target="_blank" href="{{$item->facebook}}">{{$item->facebook}}</a></td>
                        <td><a target="_blank" href="{{$item->instagram}}">{{$item->instagram}}</a></td>
                        <td><a target="_blank" href="{{$item->twitter}}">{{$item->twitter}}</a></td>
                        {{--<td>{{$item->note}}</td>--}}


                        @if($item->clice_design>0)
                            <td>{{\App\Models\CliceModel::find($item->clice_design)->name}}</td>
                            @else
                            <td></td>
                            @endif

                        @if($item->clice_plan>0)
                            <td>{{\App\Models\CliceModel::find($item->clice_plan)->name}}</td>
                        @else
                            <td></td>
                        @endif

                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>

    </div>
    <!-- /body classes -->





    <div id="add" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content ">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">إضافة </h5>
                </div>
                <form action="{{url('admin/add_customer')}}" id="add_form" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">

                        <div class="row">

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الاسم </label>
                                    <input type="text" name="name" id="add_name" class="form-control" placeholder=""
                                           required="required" onchange="check_validation(this)">
                                </div>
                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الرمز </label>
                                    <input type="text" name="code" class="form-control" placeholder=""
                                           required="required" id="add_code" onchange="check_validation(this)">
                                </div>
                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label> مجال العمل </label>
                                    <select class="select-search" id="work" name="work"
                                            required="required">
                                        <option value="0">اختر</option>
                                        @foreach(\App\Models\WorkTypeModel::where('deleted',0)->get() as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>ملكية الزبون </label>
                                    <select class="select-search" id="add_customer_ownership" name="customer_ownership"
                                            required="required">
                                        <option value="0">اختر</option>
                                        <option value="dubai">dubai</option>
                                        <option value="damascus">damascus</option>
                                        <option value="egypt">egypt</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الدولة </label>
                                    <input type="text" name="country" class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>
                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>المدينة </label>
                                    <input type="text" name="city" class="form-control" placeholder=""
                                    >
                                </div>
                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الشخص المسوول </label>
                                    <input type="text" name="responsible_person" class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>
                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>العنوان </label>
                                    <input type="text" name="address" class="form-control" placeholder=""
                                    >
                                </div>
                            </div>

                            <div class="col-lg-6">


                                <div class="form-group">
                                    <label>الهاتف </label>
                                    <input type="text" name="phone" class="form-control" placeholder=""
                                    >
                                </div>
                            </div>
                            <div class="col-lg-6">


                                <div class="form-group">
                                    <label>الجوال </label>
                                    <input type="text" name="mobile" class="form-control" placeholder=""
                                           required id="add_mobile" onchange="check_validation(this)">
                                </div>
                            </div>
                            <div class="col-lg-6">


                                <div class="form-group">
                                    <label>الايميل </label>
                                    <input type="text" name="email" id="add_email" onchange="check_validation(this)"
                                           class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>

                            <div class="col-lg-6">


                                <div class="form-group">
                                    <label>معلومات شخص اخر (اسم) </label>
                                    <input type="text" name="other_contact_name" class="form-control" placeholder=""
                                    >
                                </div>
                            </div>
                            <div class="col-lg-6">


                                <div class="form-group">
                                    <label>معلومات شخص اخر (هاتف) </label>
                                    <input type="text" name="other_contact_phone" class="form-control" placeholder=""
                                    >
                                </div>
                            </div>
                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الزبون عن طريق</label>
                                    <select class="select-search" id="add_user_connect" name="user_connect"
                                    >
                                        <option value="">اختر</option>
                                        @foreach(\App\User::where('type','user_get_work')->where('deleted',0)->get() as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>


                            <div class="col-lg-6">


                                <div class="form-group">
                                    <label>ويب سايت</label>
                                    <input type="text" name="website" class="form-control" placeholder=""
                                    >
                                </div>
                            </div>
                            <div class="col-lg-6">


                                <div class="form-group">
                                    <label>فيسبوك</label>
                                    <input type="text" name="facebook" class="form-control" placeholder=""
                                    >
                                </div>
                            </div>
                            <div class="col-lg-6">


                                <div class="form-group">
                                    <label>انستغرام</label>
                                    <input type="text" name="instagram" class="form-control" placeholder=""
                                    >
                                </div>
                            </div>
                            <div class="col-lg-6">


                                <div class="form-group">
                                    <label>تويتر</label>
                                    <input type="text" name="twitter" class="form-control" placeholder=""
                                    >
                                </div>
                            </div>

                            <div class="col-lg-6">


                                <div class="form-group">
                                    <label>ملاحظات</label>
                                    <input type="text" name="note" class="form-control" placeholder=""
                                    >
                                </div>
                            </div>

                            {{--<div class="col-lg-6">--}}

                            {{--<div class="form-group">--}}
                            {{--<label>كليشة التصميم</label>--}}
                            {{--<input type="file" name="logo_design" class="file-styled">--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-lg-6">--}}

                            {{--<div class="form-group">--}}
                            {{--<label>كليشة التصميم</label>--}}
                            {{--<input type="file" name="logo_executive_planner" class="file-styled">--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>كليشة التصميم</label>
                                    <select class="select-search" id="add_clice_design" name="clice_design"
                                            required="required">
                                        <option value="">اختر</option>
                                        @foreach(\App\Models\CliceModel::where('type','design')->where('deleted',0)->get() as $item)

                                            @if($item->default==1)
                                                <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                            @else
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                            @endif
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>كليشة المخططات التنفيذية</label>
                                    <select class="select-search" id="add_clice_plan" name="clice_plan"
                                            required="required">
                                        <option value="">اختر</option>
                                        @foreach(\App\Models\CliceModel::where('type','plan')->where('deleted',0)->get() as $item)

                                            @if($item->default==1)
                                                <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                            @else
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                            @endif
                                        @endforeach

                                    </select>
                                </div>
                            </div>


                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link"
                                data-dismiss="modal">{{\App\Http\Controllers\dashboard\ConstantController::$close[$_SESSION['lang']]}}</button>
                        <button type="submit"
                                class="btn btn-primary">{{\App\Http\Controllers\dashboard\ConstantController::$add[$_SESSION['lang']]}}
                            <i style="    float: right; " id="loader" class="icon-spinner spinner position-left"></i>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div id="edit" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">تعديل </h5>
                </div>
                <form action="{{url('admin/edit_customer')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <input type="hidden" id="item_id" name="item_id" value="true">
                        <div class="row">

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الاسم </label>
                                    <input id="name" type="text" name="name" class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الرمز </label>
                                    <input id="code" type="text" name="code" class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>
                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label> مجال العمل </label>
                                    <select class="select-search"  name="work"
                                            required="required">
                                        <option value="0">اختر</option>
                                        @foreach(\App\Models\WorkTypeModel::where('deleted',0)->get() as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>ملكية الزبون </label>
                                    <select id="customer_ownership" class="select-search" name="customer_ownership"
                                            required="required">
                                        <option value="dubai">dubai</option>
                                        <option value="damascus">damascus</option>
                                        <option value="egypt">egypt</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الدولة </label>
                                    <input id="country" type="text" name="country" class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>
                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>المدينة </label>
                                    <input id="city" type="text" name="city" class="form-control" placeholder=""
                                    >
                                </div>
                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الشخص المسوول </label>
                                    <input id="responsible_person" type="text" name="responsible_person"
                                           class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>
                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>العنوان </label>
                                    <input id="address" type="text" name="address" class="form-control" placeholder=""
                                    >
                                </div>
                            </div>

                            <div class="col-lg-6">


                                <div class="form-group">
                                    <label>الهاتف </label>
                                    <input id="phone" type="text" name="phone" class="form-control" placeholder=""
                                    >
                                </div>
                            </div>
                            <div class="col-lg-6">


                                <div class="form-group">
                                    <label>الجوال </label>
                                    <input id="mobile" type="text" name="mobile" class="form-control" placeholder=""
                                           required>
                                </div>
                            </div>
                            <div class="col-lg-6">


                                <div class="form-group">
                                    <label>الايميل </label>
                                    <input id="email" type="text" name="email" class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>
                            <div class="col-lg-6">


                                <div class="form-group">
                                    <label>معلومات شخص اخر (اسم) </label>
                                    <input type="text" id="other_contact_name" name="other_contact_name"
                                           class="form-control" placeholder=""
                                    >
                                </div>
                            </div>
                            <div class="col-lg-6">


                                <div class="form-group">
                                    <label>معلومات شخص اخر (هاتف) </label>
                                    <input type="text" id="other_contact_phone" name="other_contact_phone"
                                           class="form-control" placeholder=""
                                    >
                                </div>
                            </div>
                            <div class="col-lg-6">


                                <div class="form-group">
                                    <label>الزبون عن طريق</label>
                                    <select class="select-search" id="user_connect" name="user_connect"
                                    >
                                        <option value="">اختر</option>
                                        @foreach(\App\User::where('type','user_get_work')->where('deleted',0)->get() as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach

                                    </select>

                                </div>
                            </div>

                            <div class="col-lg-6">


                                <div class="form-group">
                                    <label>ويب سايت</label>
                                    <input type="text" id="website" name="website" class="form-control" placeholder=""
                                    >
                                </div>
                            </div>
                            <div class="col-lg-6">


                                <div class="form-group">
                                    <label>فيسبوك</label>
                                    <input type="text" id="facebook" name="facebook" class="form-control" placeholder=""
                                    >
                                </div>
                            </div>
                            <div class="col-lg-6">


                                <div class="form-group">
                                    <label>انستغرام</label>
                                    <input type="text" id="instagram" name="instagram" class="form-control"
                                           placeholder=""
                                    >
                                </div>
                            </div>

                            <div class="col-lg-6">


                                <div class="form-group">
                                    <label>تويتر</label>
                                    <input type="text" id="twitter" name="twitter" class="form-control" placeholder=""
                                    >
                                </div>
                            </div>


                            <div class="col-lg-6">


                                <div class="form-group">
                                    <label>ملاحظات</label>
                                    <input id="note" type="text" name="note" class="form-control" placeholder=""
                                    >
                                </div>
                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>كليشة التصميم</label>
                                    <select class="select-search" name="clice_design" id="clice_design"
                                            required="required">
                                        <option value="">اختر</option>
                                        @foreach(\App\Models\CliceModel::where('type','design')->where('deleted',0)->get() as $item)

                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>كليشة المخططات التنفيذية</label>
                                    <select class="select-search" name="clice_plan" id="clice_plan" required="required">
                                        <option value="">اختر</option>
                                        @foreach(\App\Models\CliceModel::where('type','plan')->where('deleted',0)->get() as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>


                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">إلغاء</button>
                        <button type="submit" class="btn btn-primary">حفظ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    {{csrf_field()}}
    <script>


        function event_edit(elem) {

            $("#name").val($(elem).attr('full_name'));
            $("#email").val($(elem).attr('email'));
            $("#address").val($(elem).attr('address'));
            $("#phone").val($(elem).attr('phone'));
            $("#code").val($(elem).attr('code'));
            $("#work").val($(elem).attr('work')).change();
            $("#country").val($(elem).attr('country'));
            $("#city").val($(elem).attr('city'));
            $("#responsible_person").val($(elem).attr('responsible_person'));
            $("#mobile").val($(elem).attr('mobile'));
            $("#other_contact_phone").val($(elem).attr('other_contact_phone'));
            $("#other_contact_name").val($(elem).attr('other_contact_name'));
            $("#note").val($(elem).attr('note'));
            $("#website").val($(elem).attr('website'));
            $("#facebook").val($(elem).attr('facebook'));
            $("#instagram").val($(elem).attr('instagram'));
            $("#twitter").val($(elem).attr('twitter'));
            $("#user_connect").val($(elem).attr('user_connect')).change();
            $("#clice_design").val($(elem).attr('clice_design')).change();
            $("#clice_plan").val($(elem).attr('clice_plan')).change();
            $("#customer_ownership").val($(elem).attr('customer_ownership')).change();

            $("#department").val($(elem).attr('department')).change();
            $("#item_id").val($(elem).attr('item_id'));

        }


        function remove_item(elem) {

            swal({
                    title: "{{\App\Http\Controllers\dashboard\ConstantController::$are_you_sure[$_SESSION['lang']]}}",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#EF5350",
                    confirmButtonText: "{{\App\Http\Controllers\dashboard\ConstantController::$on[$_SESSION['lang']]}}",
                    cancelButtonText: "{{\App\Http\Controllers\dashboard\ConstantController::$off[$_SESSION['lang']]}}",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {

                    if (isConfirm) {
                        var item_id = $(elem).attr('item_id');
                        var url = "{{url('admin/remove_customer')}}";
                        $('<form>', {
                            "action": url + "/" + item_id,
                        }).appendTo(document.body).submit();

                        {{--swal({--}}
                        {{--title: "{{\App\Http\Controllers\dashboard\ConstantController::$deleted[$_SESSION['lang']]}} ",--}}
                        {{--text: "{{\App\Http\Controllers\dashboard\ConstantController::$deleted_success[$_SESSION['lang']]}}",--}}
                        {{--confirmButtonColor: "#66BB6A",--}}
                        {{--type: "success"--}}
                        {{--});--}}
                    }
                    else {
                        swal({
                            title: "{{\App\Http\Controllers\dashboard\ConstantController::$close[$_SESSION['lang']]}}",
                            text: "",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });

        }

        function check_validation(elem) {

            {{--var _token = $('input[name="_token"]').val();--}}

            {{--if($('#add_code').val().length>0 && $('#add_name').val().length>0 && $('#add_mobile').val().length>0&& $('#add_email').val().length>0)--}}
            {{--{--}}
            {{--$.ajax({--}}
            {{--url: "{{url('admin/check_validation')}}", // Url to which the request is send--}}
            {{--type: "POST",             // Type of request to be send, called as method--}}
            {{--data: {--}}
            {{--code: $('#add_code').val(),--}}
            {{--name: $('#add_name').val(),--}}
            {{--mobile: $('#add_mobile').val(),--}}
            {{--email: $('#add_email').val(),--}}
            {{--_token: _token,--}}
            {{--}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)--}}

            {{--success: function (data)   // A function to be called if request succeeds--}}
            {{--{--}}


            {{--if (data.status == 403) {--}}

            {{--new Noty({--}}
            {{--text: data.message,--}}
            {{--type: 'error',--}}
            {{--dismissQueue: true,--}}
            {{--timeout: 4000,--}}
            {{--layout: "topRight",--}}
            {{--}).show();--}}


            {{--} else {--}}

            {{--}--}}


            {{--}--}}
            {{--});--}}
            {{--}--}}


        }


        $('#loader').css('display', 'none');

        $("#add_form").submit(function (e) {

            e.preventDefault(); // avoid to execute the actual submit of the form.

            $('#loader').css('display', 'block');
            console.log("dfd");
            $.ajax({
                    url: "{{url('admin/add_customer_ajax')}}", // Url to which the request is send
                    type: "POST",             // Type of request to be send, called as method
                    data: $(this).serialize(),
                    success: function (data)   // A function to be called if request succeeds
                    {


                        $('#loader').css('display', 'none');
                        if (data.status == 403 || data.status == 400) {

                            for (var i = 0; i < data.message.length; i++) {
                                new Noty({

                                    text: "" + data.message[i].message,
                                    type: 'error',
                                    dismissQueue: true,
                                    timeout: 4000,
                                    layout: "topRight",
                                }).show();
                            }
                        }
                        else if (data.status == 200) {


                            $('#add_form').trigger("reset");

                            $('#add_customer_ownership').val();
                            $('#add_user_connect').val();
                            $('#add_clice_design').val();
                            $('#add_clice_plan').val();
                            $('.select-search').select2();


                            new Noty({

                                text: "" + data.message[0].message,
                                type: 'success',
                                dismissQueue: true,
                                timeout: 4000,
                                layout: "topRight",
                            }).show();

                            setTimeout(function () {


                                window.location.href = "{{url('admin/customers')}}"
                            }, 1000)

                        }
                    }
                }
            );

        });


    </script>

@endsection
