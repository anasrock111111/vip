@extends('dashboard.layout.index')
@section('content')

    @if(\Illuminate\Support\Facades\Auth::user()->type=="admin" || \Illuminate\Support\Facades\Auth::user()->type=="project_manager")
        <?php  $disables = "";?>
    @else
        <?php  $disables = "disabled";?>

    @endif

    <!-- Basic table -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">تعديل الطلب</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <form action="{{url('admin/edit_order')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="row">


                    <input type="hidden" name="item_id" value="{{$order->id}}">
                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>اسم المشروع </label>
                            <select class="select-search" {{$disables}} name="project" required="required">
                                @foreach(\App\Models\ProjectModel::where('deleted',0)->get() as $item)
                                    @if($item->id==$order->project)
                                        <option
                                            value="{{$item->id}}"
                                            selected>{{$item->name_ar." - ".\App\User::find($item->user)->name}}</option>
                                    @else
                                        <option
                                            value="{{$item->id}}">{{$item->name_ar." - ".\App\User::find($item->user)->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                    </div>

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>ترقيم الطلب </label>
                            <input type="text" {{$disables}} value="{{$order->order_number}}" name="order_number"
                                   class="form-control"
                                   placeholder=""
                                   required="required">
                        </div>
                    </div>

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>تاريخ استلام الطلب </label>
                            <input type="date" {{$disables}} value="{{$order->date_received}}" name="date_received"
                                   class="form-control" placeholder=""
                                   required="required">
                        </div>
                    </div>

                    {{--<div class="col-lg-6">--}}

                    {{--<div class="form-group">--}}
                    {{--<label>مسار الطلب </label>--}}
                    {{--<input type="text" value="{{$order->order_direction}}" name="order_direction"--}}
                    {{--class="form-control" placeholder=""--}}
                    {{--required="required">--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>تاريخ تسليم الطلب</label>
                            <input type="date" {{$disables}} value="{{$order->delivery_date}}" name="delivery_date"
                                   class="form-control" placeholder=""
                                   required="required">
                        </div>
                    </div>


                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>الأهمية</label>
                            <select class="select-search" {{$disables}} name="importance" required="required">
                                @if($order->importance=="normal")
                                    <option value="normal" selected>عادي</option>
                                @else

                                    <option value="normal">عادي</option>
                                @endif

                                @if($order->importance=="important")
                                    <option value="important" selected>عالي</option>
                                @else

                                    <option value="important">عالي</option>
                                @endif


                            </select>
                        </div>

                    </div>

                    <div class="col-lg-12">

                        <div class="form-group">
                            <label>ملاحظات</label>
                            <textarea {{$disables}} name="note" class="form-control" placeholder=""
                                      rows="8"><?php echo $order->note?></textarea>
                        </div>
                    </div>


                    <div class="col-lg-6">


                        <div class="form-group" id="edt_tybe_block">
                            <label>إنجاز المشروع</label>
                            <select class="select-search" name="completion" id="completion"
                                    onchange="disable_type(this)"
                            >
                                <?php

                                $start = "";
                                $delivery_design = "";
                                $delivery_all_project = "";
                                $other = "";

                                if ($order->completion == "start") {
                                    $start = "selected";
                                } elseif ($order->completion == "delivery_design") {
                                    $delivery_design = "selected";
                                } elseif ($order->completion == "delivery_all_project") {
                                    $delivery_all_project = "selected";
                                } elseif ($order->completion == "other") {
                                    $other = "selected";
                                }
                                ?>
                                <option value="">اختر</option>
                                <option value="start" <?php echo $start?> >بداية المشروع</option>
                                <option value="delivery_design" <?php echo $delivery_design?> >تسليم التصميم</option>
                                <option value="delivery_all_project" <?php echo $delivery_all_project?>>تسليم كامل
                                    المشروع
                                </option>
                                <option value="other" <?php echo $other?>>غيرها</option>

                            </select>
                        </div>
                    </div>
                    <div class=" col-lg-6">
                        <div class="form-group" id="edit_other_content">
                            <label>ملاحظة الانجاز</label>


                            <input type="text" name="edit_other" id="edit_other_input" class="form-control"
                                   value="<?php echo $order->edit_other ?>"
                                   placeholder="">
                        </div>
                    </div>


                </div>

                @if($disables=="")
                    <button type="submit"
                            class="btn btn-primary">حفظ
                    </button>
                @endif

            </form>
        </div>

    </div>

    </div>
    <!-- /basic table -->

    <script>

        $('#edit_other_content').css('display', 'none');

        function disable_type(elem) {

            if ($(elem).val() == 'other') {

                console.log($(elem).val());
                $('#edit_other_content').css('display', 'block');
            }
            else {

                $('#edit_other_content').css('display', 'none');
            }


        }


    </script>
@endsection
