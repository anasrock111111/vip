@extends('dashboard.layout.index')
@section('content')

    <!-- Basic table -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">تعديل المشروع</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                </ul>
            </div>
        </div>


        <div class="panel-body">
            <form action="{{url('admin/edit_project')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="row">

                    <input type="hidden" name="item_id" value="{{$project->id}}">

                    @if(\Illuminate\Support\Facades\Auth::user()->type=="admin")

                        <div class="col-lg-6">

                            <div class="form-group">
                                <label>الاسم بالعربي </label>
                                <input type="text" value="{{$project->name_ar}}" name="name_ar" class="form-control"
                                       placeholder=""
                                       required="required">
                            </div>
                        </div>

                    @else

                        <div class="col-lg-6">

                            <div class="form-group">
                                <label>الاسم بالعربي </label>
                                <input readonly type="text" value="{{$project->name_ar}}" name="name_ar"
                                       class="form-control"
                                       placeholder=""
                                       required="required">
                            </div>
                        </div>


                    @endif


                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>الاسم بالانكليزي </label>
                            <input type="text" value="{{$project->name_en}}" name="name_en" class="form-control"
                                   placeholder=""
                                   required="required">
                        </div>
                    </div>


                    @if(\Illuminate\Support\Facades\Auth::user()->type=="admin")

                        <div class="col-lg-6">

                            <div class="form-group">
                                <label>رمز الزبون </label>
                                <select class="select-search" name="user" required="required">
                                    @foreach(\App\User::where('type','customer')->where('deleted',0)->get() as $item)
                                        @if($item->id==$project->user)
                                            <option value="{{$item->id}}" selected="selected">{{$item->code}}</option>
                                        @else
                                            <option value="{{$item->id}}">{{$item->code}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                        </div>
                    @else

                        <div class="col-lg-6">

                            <div class="form-group">
                                <label>رمز الزبون </label>
                                <select disabled="disabled" class="select-search" name="user" required="required">
                                    @foreach(\App\User::where('type','customer')->where('deleted',0)->get() as $item)
                                        @if($item->id==$project->user)
                                            <option value="{{$item->id}}" selected="selected">{{$item->code}}</option>
                                        @else
                                            <option value="{{$item->id}}">{{$item->code}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                        </div>
                    @endif


                    @if(\Illuminate\Support\Facades\Auth::user()->type=="admin")
                        <div class="col-lg-6">

                            <div class="form-group">
                                <label>نوع المشروع</label>
                                <select class="select-search" name="type" required="required">
                                    @if($project->type=="geometric")
                                        <option value="geometric" selected="selected">هندسي</option>
                                    @else
                                        <option value="geometric">هندسي</option>
                                    @endif


                                    @if($project->type=="advertising")
                                        <option value="advertising" selected="selected">إعلان</option>
                                    @else
                                        <option value="advertising">إعلان</option>
                                    @endif


                                    @if($project->type=="marketing")
                                        <option value="marketing" selected="selected">إعلان</option>
                                    @else
                                        <option value="marketing">تسويق</option>
                                    @endif

                                </select>
                            </div>

                        </div>
                    @else

                        <div class="col-lg-6">

                            <div class="form-group">
                                <label>نوع المشروع</label>
                                <select disabled="disabled" class="select-search" name="type" required="required">
                                    @if($project->type=="geometric")
                                        <option value="geometric" selected="selected">هندسي</option>
                                    @else
                                        <option value="geometric">هندسي</option>
                                    @endif


                                    @if($project->type=="advertising")
                                        <option value="advertising" selected="selected">إعلان</option>
                                    @else
                                        <option value="advertising">إعلان</option>
                                    @endif


                                    @if($project->type=="marketing")
                                        <option value="marketing" selected="selected">إعلان</option>
                                    @else
                                        <option value="marketing">تسويق</option>
                                    @endif

                                </select>
                            </div>

                        </div>
                    @endif


                    @if(\Illuminate\Support\Facades\Auth::user()->type=="admin")
                        <div class="col-lg-6">

                            <div class="form-group">
                                <label> طبيعة المشروع </label>
                                <select class="select-search" name="nature" required="required">
                                    @foreach(\App\Models\NatureProjectModel::where('deleted',0)->get() as $item)

                                        @if($item->id==$project->nature)
                                            <option value="{{$item->id}}" selected="selected">{{$item->name}}</option>
                                        @else

                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endif

                                    @endforeach
                                </select>
                            </div>

                        </div>

                    @else

                        <div class="col-lg-6">

                            <div class="form-group">
                                <label> طبيعة المشروع </label>
                                <select disabled="disabled" class="select-search" name="nature" required="required">
                                    @foreach(\App\Models\NatureProjectModel::where('deleted',0)->get() as $item)

                                        @if($item->id==$project->nature)
                                            <option value="{{$item->id}}" selected="selected">{{$item->name}}</option>
                                        @else

                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endif

                                    @endforeach
                                </select>
                            </div>

                        </div>

                    @endif

                    @if(\Illuminate\Support\Facades\Auth::user()->type=="admin")
                        <div class="col-lg-6">

                            <div class="form-group">
                                <label>تاريخ استلام المشروع </label>
                                <input type="date" value="{{$project->receive_date}}" name="receive_date"
                                       class="form-control" placeholder=""
                                       required="required">
                            </div>
                        </div>
                    @else
                        <div class="col-lg-6">

                            <div class="form-group">
                                <label>تاريخ استلام المشروع </label>
                                <input readonly type="date" value="{{$project->receive_date}}" name="receive_date"
                                       class="form-control" placeholder=""
                                       required="required">
                            </div>
                        </div>
                    @endif

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>المساحة الداخلية </label>
                            <input type="text" value="{{$project->internal_space}}" name="internal_space"
                                   class="form-control" placeholder=""
                            >
                        </div>
                    </div>

                    @if(\Illuminate\Support\Facades\Auth::user()->type=="admin")

                        <div class="col-lg-6">

                            <div class="form-group">
                                <label>السعر </label>
                                <input type="text" value="{{$project->price}}" name="price" class="form-control"
                                       placeholder="">
                            </div>
                        </div>
                    @else
                        <div class="col-lg-6" style="display: none">

                            <div class="form-group">
                                <label>السعر </label>
                                <input type="text" value="{{$project->price}}" name="price" class="form-control"
                                       placeholder="">
                            </div>
                        </div>
                    @endif

                    @if(\Illuminate\Support\Facades\Auth::user()->type=="admin")
                        <div class="col-lg-6">

                            <div class="form-group">
                                <label>العملة </label>
                                <select class="select-search" name="currency">


                                    @if($project->currency=="doller")
                                        <option value="doller" selected="selected">دولار</option>
                                    @else
                                        <option value="doller">دولار</option>
                                    @endif

                                    @if($project->currency=="euro")
                                        <option value="euro" selected="selected">يورو</option>
                                    @else
                                        <option value="euro">يورو</option>
                                    @endif

                                    @if($project->currency=="ksa")
                                        <option value="ksa" selected="selected">ريال سعودي</option>
                                    @else
                                        <option value="ksa">ريال سعودي</option>
                                    @endif

                                    @if($project->currency=="uae")
                                        <option value="uae" selected="selected">درهم إماراتي</option>
                                    @else
                                        <option value="uae">درهم إماراتي</option>
                                    @endif


                                    @if($project->currency=="sp")
                                        <option value="sp" selected="selected">ليرة سورية</option>
                                    @else
                                        <option value="sp">ليرة سورية</option>
                                    @endif

                                </select>
                            </div>
                        </div>
                    @else

                        <div class="col-lg-6" style="display: none;">

                            <div class="form-group">
                                <label>العملة </label>
                                <select class="select-search" name="currency">


                                    @if($project->currency=="doller")
                                        <option value="doller" selected="selected">دولار</option>
                                    @else
                                        <option value="doller">دولار</option>
                                    @endif

                                    @if($project->currency=="euro")
                                        <option value="euro" selected="selected">يورو</option>
                                    @else
                                        <option value="euro">يورو</option>
                                    @endif

                                    @if($project->currency=="ksa")
                                        <option value="ksa" selected="selected">ريال سعودي</option>
                                    @else
                                        <option value="ksa">ريال سعودي</option>
                                    @endif

                                    @if($project->currency=="uae")
                                        <option value="uae" selected="selected">درهم إماراتي</option>
                                    @else
                                        <option value="uae">درهم إماراتي</option>
                                    @endif


                                    @if($project->currency=="sp")
                                        <option value="sp" selected="selected">ليرة سورية</option>
                                    @else
                                        <option value="sp">ليرة سورية</option>
                                    @endif

                                </select>
                            </div>
                        </div>
                    @endif

                    {{--<div class="col-lg-6">--}}

                    {{--<div class="form-group">--}}
                    {{--<label>مسار التعديلات </label>--}}
                    {{--<input style="    direction: ltr;" type="text" value="{{$project->edit_direction}}" name="edit_direction"--}}
                    {{--class="form-control" placeholder=""--}}
                    {{--required="required">--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-lg-6">--}}

                    {{--<div class="form-group">--}}
                    {{--<label>مسار المشروع </label>--}}
                    {{--<input  style="    direction: ltr;" type="text" value="{{$project->project_direction}}" name="project_direction"--}}
                    {{--class="form-control" placeholder=""--}}
                    {{--required="required">--}}
                    {{--</div>--}}
                    {{--</div>--}}

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>ارتفاع السقف </label>
                            <input type="text" value="{{$project->ceiling_height}}" name="ceiling_height"
                                   class="form-control" placeholder=""
                            >
                        </div>
                    </div>
                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>نوع التكييف </label>
                            <input type="text" value="{{$project->type_condition}}" name="type_condition"
                                   class="form-control" placeholder=""
                            >
                        </div>
                    </div>
                    <div class="col-lg-6">

                        <div class="form-group">
                            <label> نزول التكييف </label>
                            <input type="text" value="{{$project->space_condition}}" name="space_condition"
                                   class="form-control" placeholder=""
                            >
                        </div>
                    </div>

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label> مخطط التكييف </label>
                            <select class="select-search" name="planer_condition_file" required="required">


                                <option value="">اختر</option>
                                @if($project->planer_condition_file==1)
                                    <option value="">اختر</option>
                                    <option value="1" selected="selected">يوجد</option>
                                    <option value="0">لا يوجد</option>
                                @elseif($project->planer_condition_file==0)

                                    <option value="1">يوجد</option>
                                    <option value="0" selected="selected">لا يوجد</option>

                                @endif


                            </select>
                        </div>

                    </div>

                    {{--<div class="col-lg-6">--}}

                    {{--<div class="form-group">--}}
                    {{--<label>مخطط التكييف</label>--}}
                    {{--<input type="file" name="planer_condition_file" class="file-styled">--}}

                    {{--@if(strlen($project->planer_condition_file)>0)--}}

                    {{--<ul class="media-list">--}}

                    {{--<li class="media">--}}
                    {{--<div class="media-left media-middle">--}}
                    {{--</div>--}}

                    {{--<div class="media-body">--}}
                    {{--<a href="{{url('uploads/planer_condition_file/'.$project->planer_condition_file)}}" download class="media-heading text-semibold"> تحميل الملف {{$project->planer_condition_file}}</a>--}}

                    {{--</div>--}}


                    {{--</li>--}}

                    {{--</ul>--}}
                    {{--@endif--}}
                    {{--</div>--}}
                    {{--</div>--}}


                    @if(\Illuminate\Support\Facades\Auth::user()->type=="admin")
                        <div class="col-lg-6">

                            <div class="form-group">
                                <label> ملاحظات المشرف الفني </label>
                                <input type="text" value="{{$project->technical_supervisor_note}}"
                                       name="technical_supervisor_note" class="form-control" placeholder=""
                                >
                            </div>
                        </div>

                    @else
                        <div class="col-lg-6">

                            <div class="form-group">
                                <label> ملاحظات المشرف الفني </label>
                                <input readonly type="text" value="{{$project->technical_supervisor_note}}"
                                       name="technical_supervisor_note" class="form-control" placeholder=""
                                >
                            </div>
                        </div>
                    @endif
                    <div class="col-lg-12">

                        <div class="form-group">
                            <label> ملاحظات الزبون </label>
                            <input type="text" value="{{$project->user_note}}" name="user_note" class="form-control"
                                   placeholder="">
                        </div>
                    </div>


                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>انتهاء المشروع </label>
                            <select class="select-search" name="project_ended" required="required">


                                @if($project->project_ended==1)
                                    <option value="1" selected="selected">انتهى</option>
                                    <option value="0">لم ينتهي</option>
                                @else

                                    <option value="1">انتهى</option>
                                    <option value="0" selected="selected">لم ينتهي</option>
                                @endif


                            </select>
                        </div>

                    </div>


                </div>


                <button type="submit"
                        class="btn btn-primary">حفظ
                </button>

                <a href="{{url('admin/add_order?project='.$item->id."")}}"
                   data-popup="tooltip"
                   target="_blank"
                >
                    <button type="button"
                            class="btn btn-primary">إضافة طلب
                    </button>
                </a>


                <a href="{{url('admin/add_space?project='.$item->id."")}}"
                   data-popup="tooltip"
                   target="_blank"
                >
                    <button type="button"
                            class="btn btn-primary">إضافة فراغ
                    </button>
                </a>


            </form>
        </div>

    </div>

    </div>
    <!-- /basic table -->

@endsection
