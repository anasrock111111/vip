@extends('dashboard.layout.index')
@section('content')

    <!-- Basic table -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">تعديل الفراغ</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <form action="{{url('admin/edit_space')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="row">

                    <input type="hidden" name="item_id" value="{{$space->id}}">

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>اسم المشروع </label>
                            <select class="select-search" name="project" required="required">
                                @foreach(\App\Models\ProjectModel::where('deleted',0)->get() as $item)
                                    @if($item->id==$space->project)
                                        <option value="{{$item->id}}" selected>{{$item->name_ar}}</option>
                                    @else
                                        <option value="{{$item->id}}">{{$item->name_ar}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                    </div>

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>نوع الفراغ </label>
                            <select class="select-search" id="type" name="type" required="required" onchange="set_elem(this)">
                                @foreach(\App\Models\SpaceTypeModel::where('deleted',0)->get() as $item)

                                    @if($item->id==$space->type)
                                        <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                    @else
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endif

                                @endforeach
                            </select>
                        </div>

                    </div>

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>موقع الفراغ </label>
                            <select class="select-search" name="location" required="required">
                                @foreach(\App\Models\SpaceLocationModel::where('deleted',0)->get() as $item)

                                    @if($item->id==$space->location)
                                        <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                    @else
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endif


                                @endforeach
                            </select>
                        </div>

                    </div>

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>اسم الفراغ </label>
                            <input type="text"  value="{{$space->name}}" name="name" id="name" class="form-control" placeholder=""
                                   required="required">
                        </div>
                    </div>

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>العدد </label>
                            <input type="number" value="{{$space->count}}"  name="count" class="form-control" placeholder=""
                                  >
                        </div>
                    </div>


                </div>

                <button type="submit"
                        class="btn btn-primary">حفظ</button>

            </form>
        </div>

    </div>

    </div>
    <!-- /basic table -->

    <script>

        function set_elem(elem)
        {
            $('#name').val($('#type  option:selected').text());

        }
    </script>
@endsection
