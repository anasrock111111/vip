@extends('dashboard.layout.index')
@section('content')



    @if(\Illuminate\Support\Facades\Auth::user()->type=="admin")
        <?php  $disables = "";?>
    @else
        <?php  $disables = "disabled";?>

    @endif


    <!-- Basic table -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">تعديل مهمة</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <form action="{{url('admin/edit_task')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="row">

                    <input type="hidden" name="item_id" value="{{$task->id}}">

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>اسم المشروع </label>
                            <select class="select-search" {{$disables}} name="project" required="required"
                                    onchange="getOrderAndSpace(this)">
                                <option value="0">اختر</option>
                                @foreach(\App\Models\ProjectModel::where('deleted',0)->get() as $item)

                                    @if($item->id==$task->project)

                                        <option
                                            value="{{$item->id}}"
                                            selected="selected">{{$item->name_ar." - ".\App\User::find($item->user)->code}}</option>
                                    @else

                                        <option
                                            value="{{$item->id}}">{{$item->name_ar." - ".\App\User::find($item->user)->code}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                    </div>

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>الطلب</label>
                            <select class="select-search" {{$disables}} name="order" id="order" required="required"
                            >

                            </select>
                        </div>

                    </div>
                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>الفراغ</label>
                            <select class="select-search" {{$disables}} name="space" id="space" required="required"
                            >

                            </select>
                        </div>

                    </div>


                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>نوع التاسك </label>
                            <select class="select-search" {{$disables}} name="type" required="required" onchange="disable_type(this)"
                            >
                                @foreach(\App\Models\TaskTypeModel::where('deleted',0)->get() as $item)

                                    @if($item->id==$task->type)

                                        <option selected="selected"
                                                value="{{$item->id}}">{{$item->name}}</option>
                                    @else

                                        <option
                                            value="{{$item->id}}">{{$item->name}}</option>
                                    @endif

                                @endforeach
                            </select>
                        </div>

                    </div>

                    <div class="col-lg-6">

                        <div class="form-group" id="edt_tybe_block" >
                            <label>نوع التعديل </label>
                            <select class="select-search" {{$disables}} name="edit_type"
                            >
                                <option value="">اختر</option>
                                @foreach(\App\Models\TaskEditTypeModel::where('deleted',0)->get() as $item)

                                    @if($item->id==$task->edit_type)

                                        <option selected="selected"
                                                value="{{$item->id}}">{{$item->name}}</option>
                                    @else

                                        <option
                                            value="{{$item->id}}">{{$item->name}}</option>
                                    @endif


                                @endforeach
                            </select>
                        </div>

                    </div>


                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>المشرف </label>
                            <select class="select-search" {{$disables}} name="technical_supervisor" required="required"
                            >
                                @foreach(\App\User::where('type','technical_supervisor')->get() as $item)

                                    @if($item->id==$task->technical_supervisor)

                                        <option selected="selected"
                                                value="{{$item->id}}">{{$item->name}}</option>
                                    @else

                                        <option
                                            value="{{$item->id}}">{{$item->name}}</option>
                                    @endif

                                @endforeach
                            </select>
                        </div>

                    </div>
                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>الموظف </label>
                            <select class="select-search" {{$disables}}  name="employee"
                            >

                                <option value="">اختر</option>
                                @foreach(\App\User::where('type','employee')->get() as $item)

                                    @if($item->id==$task->employee)

                                        <option
                                            value="{{$item->id}}" selected="selected">{{$item->name}}</option>
                                    @else

                                        <option
                                            value="{{$item->id}}">{{$item->name}}</option>
                                    @endif

                                @endforeach
                            </select>
                        </div>

                    </div>

                    <div class="col-lg-6">

                        <div class="form-group">
                            <label>تاريخ التسليم</label>
                            <input type="date" {{$disables}} name="start_date" class="form-control" placeholder=""
                                   value="{{$task->start_date}}"
                                  >
                        </div>
                    </div>


                    <div class="col-lg-12">

                        <div class="form-group">
                            <label>مسار التاسك</label>
                            <input type="text" {{$disables}} name="task_direction" class="form-control" placeholder=""
                                     value="{{$task->task_direction}}">
                        </div>
                    </div>


                </div>

                @if($disables=="")
                    <button type="submit"
                            class="btn btn-primary">حفظ
                    </button>
                @endif

            </form>
        </div>

    </div>

    </div>
    <!-- /basic table -->

    {{csrf_field()}}
    <script>


        var _token = $('input[name="_token"]').val();

        var project = '{{$task->project}}';
        var order = '{{$task->order}}';
        var space = '{{$task->space}}';

        console.log(project);
        console.log(order);
        console.log(space);

        $.ajax({
            url: "{{url('admin/get_orders_spaces')}}", // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            data: {
                project: project,

                _token: _token,
            }, // Data sent to server, a set of key/value pairs (i.e. form fields and values)

            success: function (data)   // A function to be called if request succeeds
            {


                if (data.status == 200) {


                    console.log(data.orders.length);
                    console.log(data.space);

                    var orders = data.orders;
                    var spaces = data.space;

                    if (orders.length > 0) {
                        $('#order').html('');
                        var html = "";
                        for (var i = 0; i < orders.length; i++) {

                            if (orders[i].id == order) {

                                html += '<option selected="selected" value="' + orders[i].id + '">' + orders[i].order_number + '</option>';
                            }
                            else {

                                html += '<option value="' + orders[i].id + '">' + orders[i].order_number + '</option>';
                            }
                        }
                        $('#order').append(html);
                    }

                    if (spaces.length > 0) {
                        $('#space').html('');
                        var html = "";
                        for (var i = 0; i < spaces.length; i++) {
                            if (spaces[i].id == space) {

                                html += '<option selected="selected" value="' + spaces[i].id + '">' + spaces[i].name + '</option>';
                            }
                            else {

                                html += '<option value="' + spaces[i].id + '">' + spaces[i].name + '</option>';
                            }
                        }
                        $('#space').append(html);
                    }

                    $('.select-search').select2();
                } else {

                }


            }
        });


        function getOrderAndSpace(elem) {


            if (!$(elem).val() > 0) {
                return;
            }
            $.ajax({
                url: "{{url('admin/get_orders_spaces')}}", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: {
                    project: $(elem).val(),

                    _token: _token,
                }, // Data sent to server, a set of key/value pairs (i.e. form fields and values)

                success: function (data)   // A function to be called if request succeeds
                {


                    if (data.status == 200) {


                        console.log(data.orders.length);
                        console.log(data.space);

                        var orders = data.orders;
                        var spaces = data.space;

                        if (orders.length > 0) {
                            $('#order').html('');
                            var html = "";
                            for (var i = 0; i < orders.length; i++) {

                                html += '<option value="' + orders[i].id + '">' + orders[i].order_number + '</option>';
                            }
                            $('#order').append(html);
                        }

                        if (spaces.length > 0) {
                            $('#space').html('');
                            var html = "";
                            for (var i = 0; i < spaces.length; i++) {

                                html += '<option value="' + spaces[i].id + '">' + spaces[i].name + '</option>';
                            }
                            $('#space').append(html);
                        }

                        $('.select-search').select2();
                    } else {

                    }


                }
            });


        }



        $('#edt_tybe_block').css('display','none');

        function disable_type(elem)
        {

            if($(elem).val()=='2'){

                console.log($(elem).val());
                $('#edt_tybe_block').css('display','block');
            }
            else
            {
                $('#edt_tybe_block').css('display','none');

            }

        }

    </script>
@endsection


