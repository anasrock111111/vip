@extends('dashboard.layout.index')
@section('content')

    <!-- Body classes -->
    <style>

        .image_review {
            width: 50%;
            margin-top: 5%;
        }
    </style>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title"> {{\App\Http\Controllers\dashboard\ConstantController::$content[$_SESSION['lang']]}} {{$view_name}}</h5>
            <div class="heading-elements">
                <ul class="icons-list">


                    @if(\Illuminate\Support\Facades\Auth::user()->type=="admin")

                        <button data-toggle="modal" data-target="#add" data-popup="tooltip" title="إضافة" type="button"
                                class="btn border-slate text-slate-800 btn-flat"><i class="icon-add "></i>
                        </button>

                    @endif

                </ul>

            </div>

        </div>

        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>

                    <th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>
                    <th>#</th>
                    <th>الاسم</th>
                    <th>الاختصاص</th>
                    <th>الدراسة</th>
                    <th>تفاصيل الدراسة</th>
                    <th>الهاتف</th>
                    <th>الجوال</th>
                    <th>العنوان</th>
                    <th>البريد الالكتروني</th>
                    <th>تاريخ البدء</th>
                    <th>فعال</th>
                    @if(\Illuminate\Support\Facades\Auth::user()->type=="admin")
                        <th>الراتب</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                @foreach($employees as $key=>$item)
                    <tr>
                        <td style="    text-align: center;">

                            <ul class="icons-list">


                                @if(\Illuminate\Support\Facades\Auth::user()->type=="admin")

                                    <li><a href="#" data-toggle="modal" data-target="#salary_modal"
                                           data-popup="tooltip"
                                           item_id="{{$item->id}}"
                                           onclick="get_salary(this)"
                                           title="سجل الرواتب"
                                        ><i class="icon icon-coins"></i></a></li>

                                    <li><a href="{{url('admin/files/'.$item->id."?type=employee")}}" data-popup="tooltip"
                                           title="الملفات"
                                        ><i class="icon icon-files-empty2"></i></a></li>

                                    <li><a href="javascript:;" data-popup="tooltip" title="تعديل"
                                           full_name="{{$item->name}}"
                                           email="{{$item->email}}"
                                           department="{{$item->department}}"
                                           study_type="{{$item->study_type}}"
                                           address="{{$item->address}}"
                                           phone="{{$item->phone}}"
                                           mobile="{{$item->mobile}}"
                                           study_note="{{$item->study_note}}"
                                           study_type="{{$item->study_type}}"
                                           employee_start="{{$item->employee_start}}"
                                           employee_active="{{$item->employee_active}}"
                                           salary="{{$item->salary}}"
                                           item_id="{{$item->id}}"
                                           currency="{{$item->currency}}"
                                           onclick="event_edit(this)"
                                           data-toggle="modal" data-target="#edit"
                                        ><i class="icon-pencil7"></i></a></li>
                                    <li><a href="javascript:;" data-popup="tooltip" title="حذف"
                                           onclick="remove_item(this)" item_id="{{$item->id}}"
                                        ><i class="icon-trash"></i></a></li>

                                @endif


                            </ul>
                        </td>
                        <td>{{$key+1}}</td>
                        <td>{{$item->name}}</td>
                        @if($item->department>0)
                            <td>{{\App\Models\DepartmentModel::find($item->department)->name}}</td>
                            @else
                            <td></td>
                            @endif

                        @if($item->study_type>0)
                            <td>{{\App\Models\StudyTypeModel::find($item->study_type)->name}}</td>
                        @else
                            <td></td>
                        @endif
                        <td>{{$item->study_note}}</td>
                        <td>{{$item->phone}}</td>
                        <td>{{$item->mobile}}</td>
                        <td>{{$item->address}}</td>
                        <td>{{$item->email}}</td>
                        <td>{{$item->employee_start}}</td>
                        <td>
                            @if($item->employee_active==1)
                                نعم
                            @else
                                لا
                            @endif
                        </td>
                        @if(\Illuminate\Support\Facades\Auth::user()->type=="admin")

                            <td>{{$item->salary." ".\App\Http\Controllers\dashboard\ConstantController::currency($item->currency ). " " }}</td>
                        @endif

                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>

    </div>
    <!-- /body classes -->





    <div id="add" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content modal-lg">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">إضافة </h5>
                </div>
                <form action="{{url('admin/add_employee')}}" id="add_form" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">

                        <div class="row">

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الاسم </label>
                                    <input type="text" id="add_name" name="name" class="form-control" placeholder=""
                                           required="required" onchange="check_validation(this)">
                                </div>
                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الاختصاص </label>
                                    <select class="select-search" id="add_department" name="department"
                                            required="required">
                                        @foreach(\App\Models\DepartmentModel::where('deleted',0)->get() as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>نوع الدراسة </label>
                                    <select class="select-search" id="add_study_type" name="study_type"
                                            required="required">
                                        @foreach(\App\Models\StudyTypeModel::where('deleted',0)->get() as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>


                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>تفاصيل الدراسة </label>
                                    <input type="text" name="study_note" class="form-control" placeholder=""
                                    >
                                </div>
                            </div>


                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>موبايل </label>
                                    <input type="text" name="mobile" class="form-control" placeholder=""
                                           required="required" id="add_mobile" onchange="check_validation(this)">
                                </div>
                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الهاتف </label>
                                    <input type="text" name="phone" class="form-control" placeholder=""
                                           id="add_phone" onchange="check_validation(this)">
                                </div>
                            </div>


                            <div class="col-lg-12">

                                <div class="form-group">
                                    <label>العنوان </label>
                                    <input type="text" name="address" class="form-control" placeholder=""
                                    >
                                </div>
                            </div>


                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>البريد الالكتروني</label>
                                    <input type="email" name="email" class="form-control" placeholder="" value=""
                                           required="required" id="add_email" onchange="check_validation(this)"
                                           autocomplete="off">
                                </div>
                            </div>


                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>تاريخ بدء العمل</label>
                                    <input type="date" name="employee_start" class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>


                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الموظف فعال</label>
                                    <select class="select-search" id="add_employee_active" name="employee_active"
                                            required="required">
                                        <option value="1">فعال</option>
                                        <option value="0">غير فعال</option>
                                    </select>
                                </div>

                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>العملة </label>
                                    <select class="select-search" id="add_currency" name="currency" required="required">
                                        <option value="">اختر</option>
                                        <option value="doller">دولار</option>
                                        <option value="euro">يورو</option>
                                        <option value="ksa">ريال سعودي</option>
                                        <option value="uae">درهم إماراتي</option>
                                        <option value="sp">ليرة سورية</option>
                                    </select>
                                </div>
                            </div>


                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الراتب</label>
                                    <input type="text" name="salary" class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>


                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>كلمة المرور</label>
                                    <input type="password" name="password" class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>


                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link"
                                data-dismiss="modal">{{\App\Http\Controllers\dashboard\ConstantController::$close[$_SESSION['lang']]}}</button>
                        <button type="submit"
                                class="btn btn-primary">{{\App\Http\Controllers\dashboard\ConstantController::$add[$_SESSION['lang']]}}
                            <i style="    float: right; " id="loader" class="icon-spinner spinner position-left"></i>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div id="edit" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">تعديل </h5>
                </div>
                <form action="{{url('admin/edit_employee')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <input type="hidden" id="item_id" name="item_id" value="true">
                        <div class="row">

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الاسم </label>
                                    <input id="name" type="text" name="name" class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>القسم </label>
                                    <select id="department" class="select-search" name="department" required="required">
                                        @foreach(\App\Models\DepartmentModel::where('deleted',0)->get() as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>
                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>نوع الدراسة </label>
                                    <select id="study_type" class="select-search" name="study_type" required="required">
                                        @foreach(\App\Models\StudyTypeModel::where('deleted',0)->get() as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>تفاصيل الدراسة </label>
                                    <input type="text" id="study_note" name="study_note" class="form-control"
                                           placeholder="">
                                </div>
                            </div>


                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>موبايل </label>
                                    <input type="text" id="mobile" name="mobile" class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الهاتف </label>
                                    <input type="text" id="phone" name="phone" class="form-control" placeholder=""
                                    >
                                </div>
                            </div>


                            <div class="col-lg-12">

                                <div class="form-group">
                                    <label>العنوان </label>
                                    <input type="text" id="address" name="address" class="form-control" placeholder=""
                                    >
                                </div>
                            </div>


                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>البريد الالكتروني</label>
                                    <input type="email" id="email" name="email" class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>


                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>تاريخ بدء العمل</label>
                                    <input type="date" id="employee_start" name="employee_start" class="form-control"
                                           placeholder=""
                                           required="required">
                                </div>
                            </div>


                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الموظف فعال</label>
                                    <select class="select-search" id="employee_active" name="employee_active"
                                            required="required">
                                        <option value="1">فعال</option>
                                        <option value="0">غير فعال</option>
                                    </select>
                                </div>

                            </div>
                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>العملة </label>
                                    <select class="select-search" name="currency" id="currency" required="required">
                                        <option value="">اختر</option>
                                        <option value="doller">دولار</option>
                                        <option value="euro">يورو</option>
                                        <option value="ksa">ريال سعودي</option>
                                        <option value="uae">درهم إماراتي</option>
                                        <option value="sp">ليرة سورية</option>
                                    </select>
                                </div>
                            </div>


                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الراتب</label>
                                    <input type="text" id="salary" name="salary" class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>كلمة المرور</label>
                                    <input id="password" type="password" name="password" class="form-control"
                                           placeholder="">
                                </div>
                            </div>


                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">إلغاء</button>
                        <button type="submit" class="btn btn-primary">حفظ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div id="salary_modal" class="modal fade ">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">سجل الرواتب </h5>
                </div>

                <div class="modal-body">

                    <div class="row">


                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>

                                        <th>الراتب القديم</th>
                                        <th>الراتب الجديد</th>
                                        <th>تاريخ التغيير</th>
                                        <th>العملة</th>
                                    </tr>
                                    </thead>
                                    <tbody id="salary_bod">

                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>
                </div>


            </div>
        </div>
    </div>


    {{csrf_field()}}


    <script>


        function event_edit(elem) {

            $("#name").val($(elem).attr('full_name'));
            $("#email").val($(elem).attr('email'));
            // $("#job_title").val($(elem).attr('job_title'));
            $("#phone").val($(elem).attr('phone'));
            $("#mobile").val($(elem).attr('mobile'));
            $("#address").val($(elem).attr('address'));
            $("#study_note").val($(elem).attr('study_note'));
            $("#employee_start").val($(elem).attr('employee_start'));
            $("#salary").val($(elem).attr('salary'));
            $("#employee_active").val($(elem).attr('employee_active')).change();
            $("#department").val($(elem).attr('department')).change();
            $("#study_type").val($(elem).attr('study_type')).change();
            $("#currency").val($(elem).attr('currency')).change();
            $("#item_id").val($(elem).attr('item_id'));

        }


        function remove_item(elem) {

            swal({
                    title: "{{\App\Http\Controllers\dashboard\ConstantController::$are_you_sure[$_SESSION['lang']]}}",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#EF5350",
                    confirmButtonText: "{{\App\Http\Controllers\dashboard\ConstantController::$on[$_SESSION['lang']]}}",
                    cancelButtonText: "{{\App\Http\Controllers\dashboard\ConstantController::$off[$_SESSION['lang']]}}",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {

                    if (isConfirm) {
                        var item_id = $(elem).attr('item_id');
                        var url = "{{url('admin/remove_employee')}}";
                        $('<form>', {
                            "action": url + "/" + item_id,
                        }).appendTo(document.body).submit();

                        {{--swal({--}}
                        {{--title: "{{\App\Http\Controllers\dashboard\ConstantController::$deleted[$_SESSION['lang']]}} ",--}}
                        {{--text: "{{\App\Http\Controllers\dashboard\ConstantController::$deleted_success[$_SESSION['lang']]}}",--}}
                        {{--confirmButtonColor: "#66BB6A",--}}
                        {{--type: "success"--}}
                        {{--});--}}
                    }
                    else {
                        swal({
                            title: "{{\App\Http\Controllers\dashboard\ConstantController::$close[$_SESSION['lang']]}}",
                            text: "",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });

        }

        var _token = $('input[name="_token"]').val();

        function get_salary(elem) {

            var elem_id = $(elem).attr('item_id');
            $('#task_id').val(elem_id);
            $.ajax({
                url: "{{url('admin/employee_salary')}}", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: {
                    elem_id: elem_id,
                    _token: _token,
                }, // Data sent to server, a set of key/value pairs (i.e. form fields and values)

                success: function (data)   // A function to be called if request succeeds
                {

                    if (data.status == 200) {


                        var salary = data.salary;

                        $('#salary_bod').html('');
                        if (salary.length > 0) {
                            $('#salary_bod').html('');
                            var html = "";
                            console.log("d")

                            for (var i = 0; i < salary.length; i++) {

                                html += '<tr><td>' + salary[i].old + '</td><td>' + salary[i].new + '</td><td>' + salary[i].created_at + '</td><td>' + salary[i].currency + '</td></tr>';
                            }
                            $('#salary_bod').append(html);
                        }

                    } else {

                    }


                }
            });

        }


        function check_validation(elem) {

            {{--var _token = $('input[name="_token"]').val();--}}
            {{--console.log($(elem).val().length);--}}
            {{--if($('#add_name').val().length>0 && $('#add_mobile').val().length>0 && $('#add_phone').val().length>0 && $('#add_email').val().length>0)--}}
            {{--{--}}
            {{--$.ajax({--}}
            {{--url: "{{url('admin/check_validation')}}", // Url to which the request is send--}}
            {{--type: "POST",             // Type of request to be send, called as method--}}
            {{--data: {--}}
            {{--name: $('#add_name').val(),--}}
            {{--mobile: $('#add_mobile').val(),--}}
            {{--phone: $('#add_phone').val(),--}}
            {{--email: $('#add_email').val(),--}}
            {{--_token: _token,--}}
            {{--}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)--}}

            {{--success: function (data)   // A function to be called if request succeeds--}}
            {{--{--}}


            {{--if (data.status == 403) {--}}

            {{--new Noty({--}}
            {{--text: data.message,--}}
            {{--type: 'error',--}}
            {{--dismissQueue: true,--}}
            {{--timeout: 4000,--}}
            {{--layout: "topRight",--}}
            {{--}).show();--}}


            {{--} else {--}}

            {{--}--}}


            {{--}--}}
            {{--});--}}
            {{--}--}}


        }


        $('#loader').css('display', 'none');

        $("#add_form").submit(function (e) {

            e.preventDefault(); // avoid to execute the actual submit of the form.

            $('#loader').css('display', 'block');
            console.log("dfd");
            $.ajax({
                    url: "{{url('admin/add_employee_ajax')}}", // Url to which the request is send
                    type: "POST",             // Type of request to be send, called as method
                    data: $(this).serialize(),
                    success: function (data)   // A function to be called if request succeeds
                    {


                        $('#loader').css('display', 'none');
                        if (data.status == 403 || data.status == 400) {

                            for (var i = 0; i < data.message.length; i++) {
                                new Noty({

                                    text: "" + data.message[i].message,
                                    type: 'error',
                                    dismissQueue: true,
                                    timeout: 4000,
                                    layout: "topRight",
                                }).show();
                            }
                        }
                        else if (data.status == 200) {


                            $('#add_form').trigger("reset");

                            $('#add_department').val();
                            $('#add_study_type').val();
                            $('#add_employee_active').val();
                            $('#add_currency').val();
                            $('.select-search').select2();


                            new Noty({

                                text: "" + data.message[0].message,
                                type: 'success',
                                dismissQueue: true,
                                timeout: 4000,
                                layout: "topRight",
                            }).show();

                            setTimeout(function () {

                                window.location.href="{{url('admin/employees')}}"
                            },1000)

                        }
                    }
                }
            );

        });


    </script>

@endsection
