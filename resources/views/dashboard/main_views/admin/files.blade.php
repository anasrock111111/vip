@extends('dashboard.layout.index')
@section('content')

    <!-- Body classes -->
    <style>

        .image_review {
            width: 50%;
            margin-top: 5%;
        }
    </style>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title"> {{\App\Http\Controllers\dashboard\ConstantController::$content[$_SESSION['lang']]}} {{$view_name}}


                @if($type=="project")

                    المشروع    {{\App\Models\ProjectModel::find($item_id)->name_ar}}
                @elseif($type=="space")

                    الفراغ     {{\App\Models\SpaceModel::find($item_id)->name}}
                @elseif($type=="order")


                    الطلب  {{\App\Models\OrderModel::find($item_id)->order_number}} في
                    المشروع {{\App\Models\ProjectModel::find(\App\Models\OrderModel::find($item_id)->project)->name_ar}}

                @endif

            </h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    @if(\Illuminate\Support\Facades\Auth::user()->type=="admin" )
                    <button data-toggle="modal" data-target="#add" data-popup="tooltip" title="إضافة" type="button"
                            class="btn border-slate text-slate-800 btn-flat"><i class="icon-add "></i>
                    </button>
                @endif
                </ul>

            </div>

        </div>

        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th>#</th>
                    <th>الاسم</th>
                    <th>ملاحظة</th>
                    <th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($files as $key=>$item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->file}}</td>
                        <td>{{$item->note}}</td>
                        <td style="    text-align: center;">

                            <ul class="icons-list">


                                <li><a download href="{{url('uploads/files/'.$type.'/'.$item->file)}}"
                                       data-popup="tooltip" title="تحميل"
                                    ><i class="glyphicon glyphicon-cloud-download"></i></a></li>

                                @if(\Illuminate\Support\Facades\Auth::user()->type=="admin" )
                                    <li><a href="javascript:;" data-popup="tooltip" title="حذف"
                                           onclick="remove_item(this)" item_id="{{$item->id}}"
                                           type_item="{{$item->type}}"
                                           type_item_id="{{$item->item_id}}"
                                        ><i class="icon-trash"></i></a></li>
                                @endif
                            </ul>
                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>

    </div>
    <!-- /body classes -->





    <div id="add" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">إضافة </h5>
                </div>
                <form action="{{url('admin/add_file')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">

                        <div class="row">

                            <input type="hidden" value="{{$item_id}}" name="item_id">
                            <input type="hidden" value="{{$type}}" name="type">


                            <div class="col-lg-12">

                                <div class="form-group">
                                    <label>ملاحظة </label>
                                    <input type="text" name="note" class="form-control" placeholder=""
                                    >
                                </div>
                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الملفات</label>
                                    <input type="file" name="filesType[]" multiple class="file-styled">
                                </div>
                            </div>


                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link"
                                data-dismiss="modal">{{\App\Http\Controllers\dashboard\ConstantController::$close[$_SESSION['lang']]}}</button>
                        <button type="submit"
                                class="btn btn-primary">{{\App\Http\Controllers\dashboard\ConstantController::$add[$_SESSION['lang']]}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>


        function remove_item(elem) {

            swal({
                    title: "{{\App\Http\Controllers\dashboard\ConstantController::$are_you_sure[$_SESSION['lang']]}}",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#EF5350",
                    confirmButtonText: "{{\App\Http\Controllers\dashboard\ConstantController::$on[$_SESSION['lang']]}}",
                    cancelButtonText: "{{\App\Http\Controllers\dashboard\ConstantController::$off[$_SESSION['lang']]}}",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {

                    if (isConfirm) {
                        var item_id = $(elem).attr('item_id');
                        var type_item = $(elem).attr('type_item');
                        var type_item_id = $(elem).attr('type_item_id');
                        var url = "{{url('admin/remove_file')}}";

                        var url_f = url + "/" + item_id + "?type=" + type_item + "&type_item_id=" + type_item_id;
                        console.log(item_id);
                        console.log(type_item);
                        console.log(type_item_id);
                        console.log(url_f);
                        window.location.href = url_f;
                        // $('<form method="get">', {
                        //     "action": url_f,
                        // }).appendTo(document.body).submit();

                        swal({
                            title: "{{\App\Http\Controllers\dashboard\ConstantController::$deleted[$_SESSION['lang']]}} ",
                            text: "{{\App\Http\Controllers\dashboard\ConstantController::$deleted_success[$_SESSION['lang']]}}",
                            confirmButtonColor: "#66BB6A",
                            type: "success"
                        });
                    }
                    else {
                        swal({
                            title: "{{\App\Http\Controllers\dashboard\ConstantController::$close[$_SESSION['lang']]}}",
                            text: "",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });

        }
    </script>

@endsection
