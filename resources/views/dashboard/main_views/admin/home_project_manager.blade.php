@extends('dashboard.layout.index')
@section('content')

    <!-- Basic table -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">طلبات المشريع قيد العمل</h5>
            <div class="heading-elements">
            </div>
        </div>


        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th>أحداث</th>
                    <th>#</th>
                    <th>اسم المشروع</th>
                    <th>ترقيم الطلب</th>
                    <th>الزبون</th>
                    <th>تاريخ التسليم</th>
                    <th>الأهمية</th>
                    <th>المهام المنجزة</th>
                    <th>المهام غير موزعة</th>
                    <th>المهام المستلمة من المشرف الفني</th>
                    <th>استفسار</th>
                    {{--<th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>--}}
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $key=>$item)
                    @if((strtotime($item->delivery_date) < strtotime(date('Y-m-d') ) ) && $item->delivered==0)

                        <?php  $class = "alert-warning"?>
                    @else

                        <?php  $class = ""?>
                    @endif
                    <tr class="{{$class}}">
                        <td style="    text-align: center;">

                            <ul class="icons-list">

                                <li>
                                    <a href="javascript:;" data-popup="tooltip" item_id="{{$item->id}}"
                                       onclick="project_note(this)" data-toggle="modal" data-target="#order_notes"
                                       title="" data-original-title="ملاحظات المشروع"><i class="icon  icon-quill4"></i></a>
                                </li>


                                <li><a href="javascript:;" data-popup="tooltip"
                                       item_id="{{$item->id}}"
                                       onclick="tasks(this)"
                                       data-toggle="modal" data-target="#items_"
                                       title="مهام الطلب"
                                    ><i class="icon icon-design"></i></a></li>

                            </ul>
                        </td>
                        <td>{{$item->id}}</td>
                        <td>{{\App\Models\ProjectModel::find($item->project)->name_ar}}</td>
                        <td>{{$item->order_number}}</td>
                        <td>{{\App\User::find(\App\Models\ProjectModel::find($item->project)->user)->code}}</td>
                        <td>{{$item->delivery_date}}</td>

                        <td>

                            {{\App\Http\Controllers\dashboard\ConstantController::importance($item->importance)}}

                        </td>


                        @if(isset($item->tasks_ended ) && isset($item->all_tasks) && isset($item->task_received_from_technical))

                            <td>{{$item->tasks_ended ."  \ ". $item->all_tasks }}</td>
                            <td>{{$item->tasks_not_inserted ."  \ ". $item->all_tasks }}</td>
                            <td>{{$item->task_received_from_technical ."  \ ". $item->all_tasks }}</td>
                        @else

                            <td>{{0 ."  \ ". 0 }}</td>
                            <td>{{0 ."  \ ". 0 }}</td>
                            <td>{{0 ."  \ ". 0 }}</td>
                        @endif


                        <td>

                            @if(\App\Models\OrderModel::check_if_have_inquiry($item->id))


                                <a href="javascript:;">
                                    <span class="label label-danger">يوجد</span></a>

                            @else

                            @endif
                        </td>

                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>

        <div class="table-responsive">

        </div>
    </div>
    <!-- /basic table -->


    <!-- Basic table -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">طلبات تسليمها اليوم </h5>
            <div class="heading-elements">
            </div>
        </div>


        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>
                    <th>#</th>
                    <th>اسم المشروع</th>
                    <th>ترقيم الطلب</th>
                    <th>الزبون</th>
                    <th>تاريخ التسليم</th>
                    <th>الأهمية</th>
                    <th>المهام المنجزة</th>
                    <th>المهام غير موزعة</th>
                    <th>المهام المستلمة من المشرف الفني</th>
                    <th>استفسار</th>
                </tr>
                </thead>
                <tbody>


                @foreach($order_delivery_today as $key=>$item)

                    @if((strtotime($item->delivery_date) < strtotime(date('Y-m-d') ) ) && $item->delivered==0)

                        <?php  $class = "alert-warning"?>
                    @else

                        <?php  $class = ""?>
                    @endif
                    <tr class="{{$class}}">
                        <td style="    text-align: center;">

                            <ul class="icons-list">

                                <li>
                                    <a href="javascript:;" data-popup="tooltip" item_id="{{$item->id}}"
                                       onclick="project_note(this)" data-toggle="modal" data-target="#order_notes"
                                       title="" data-original-title="ملاحظات المشروع"><i class="icon  icon-quill4"></i></a>
                                </li>


                                <li><a href="javascript:;" data-popup="tooltip"
                                       item_id="{{$item->id}}"
                                       onclick="tasks(this)"
                                       data-toggle="modal" data-target="#items_"
                                       title="مهام الطلب"
                                    ><i class="icon icon-design"></i></a></li>

                            </ul>
                        </td>
                        <td>{{$item->id}}</td>
                        <td>{{\App\Models\ProjectModel::find($item->project)->name_ar}}</td>
                        <td>{{$item->order_number}}</td>
                        <td>{{\App\User::find(\App\Models\ProjectModel::find($item->project)->user)->code}}</td>
                        <td>{{$item->delivery_date}}</td>
                        <td>
                            {{\App\Http\Controllers\dashboard\ConstantController::importance($item->importance)}}
                        </td>

                        @if(isset($item->tasks_ended ) && isset($item->all_tasks) && isset($item->task_received_from_technical))

                            <td>{{$item->tasks_ended ."  \ ". $item->all_tasks }}</td>
                            <td>{{$item->tasks_not_inserted ."  \ ". $item->all_tasks }}</td>
                            <td>{{$item->task_received_from_technical ."  \ ". $item->all_tasks }}</td>
                        @else

                            <td>{{0 ."  \ ". 0 }}</td>
                            <td>{{0 ."  \ ". 0 }}</td>
                            <td>{{0 ."  \ ". 0 }}</td>
                        @endif
                        <td>

                            @if(\App\Models\OrderModel::check_if_have_inquiry($item->id))


                                <a href="javascript:;">
                                    <span class="label label-danger">يوجد</span></a>

                            @else


                            @endif
                        </td>

                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>

        <div class="table-responsive">

        </div>
    </div>
    <!-- /basic table -->

    <!-- Basic table -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">طلبات تسليمها اليوم التالي </h5>
            <div class="heading-elements">
            </div>
        </div>


        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>
                    <th>#</th>
                    <th>اسم المشروع</th>
                    <th>ترقيم الطلب</th>
                    <th>الزبون</th>
                    <th>تاريخ التسليم</th>
                    <th>الأهمية</th>
                    <th>المهام المنجزة</th>
                    <th>المهام غير موزعة</th>
                    <th>المهام المستلمة من المشرف الفني</th>
                    <th>استفسار</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders_next_day as $key=>$item)
                    <tr>
                        <td style="    text-align: center;">

                            <ul class="icons-list">
                                <li>
                                    <a href="javascript:;" data-popup="tooltip" item_id="{{$item->id}}"
                                       onclick="project_note(this)" data-toggle="modal" data-target="#order_notes"
                                       title="" data-original-title="ملاحظات المشروع"><i class="icon  icon-quill4"></i></a>
                                </li>

                                <li><a href="javascript:;" data-popup="tooltip"
                                       item_id="{{$item->id}}"
                                       onclick="tasks(this)"
                                       data-toggle="modal" data-target="#items_"
                                       title="مهام الطلب"
                                    ><i class="icon icon-design"></i></a></li>

                            </ul>
                        </td>
                        <td>{{$item->id}}</td>
                        <td>{{\App\Models\ProjectModel::find($item->project)->name_ar}}</td>
                        <td>{{$item->order_number}}</td>
                        <td>{{\App\User::find(\App\Models\ProjectModel::find($item->project)->user)->code}}</td>
                        <td>{{$item->delivery_date}}</td>
                        <td>{{\App\Http\Controllers\dashboard\ConstantController::importance($item->Importance)}}</td>

                        @if(isset($item->tasks_ended ) && isset($item->all_tasks) && isset($item->task_received_from_technical))

                            <td>{{$item->tasks_ended ."  \ ". $item->all_tasks }}</td>
                            <td>{{$item->tasks_not_inserted ."  \ ". $item->all_tasks }}</td>
                            <td>{{$item->task_received_from_technical ."  \ ". $item->all_tasks }}</td>
                        @else

                            <td>{{0 ."  \ ". 0 }}</td>
                            <td>{{0 ."  \ ". 0 }}</td>
                            <td>{{0 ."  \ ". 0 }}</td>
                        @endif

                        {{--<td>{{$item->tasks_ended ."  \ ". $item->all_tasks }}</td>--}}
                        {{--<td>{{$item->task_received_from_technical ."  \ ". $item->all_tasks }}</td>--}}
                        {{----}}
                        <td>

                            @if(\App\Models\OrderModel::check_if_have_inquiry($item->id))


                                <a href="javascript:;">
                                    <span class="label label-danger">يوجد</span></a>

                            @else

                                <a href="javascript:;">
                                    <span class="label label-default ">لا يوجد</span></a>
                            @endif
                        </td>

                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>

        <div class="table-responsive">

        </div>
    </div>
    <!-- /basic table -->



    <div id="items_" class="modal fade ">
        <div class="modal-dialog  modal-full ">
            <div class="modal-content">
                <div class="modal-header">

                    <ul class="icons-list" style="float: left;">
                        <li id="loader_refresh_tasks"><a data-action="reload"></a></li>
                    </ul>

                    <h5 class="modal-title">المهام </h5>
                </div>
                <div class="modal-body">

                    <input name="task_id" id="task_id" value="" type="hidden">
                    <div class="row">


                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table ">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>أضيف بواسطة</th>
                                        <th>المشروع</th>
                                        <th>الموقع - اسم الفراغ </th>
                                        <th>نوع المهمة</th>
                                        <th>نوع التعديل</th>
                                        <th>تاريخ التسليم</th>
                                        <th>انتهاء الموظف</th>
                                        <th>الموظف</th>
                                        <th>المشرف</th>
                                        <th>تاريخ استلام المشرف</th>
                                        <th>تم التجميع</th>
                                        <th>تاريخ التجميع</th>
                                        <th>تقييم المشرف</th>
                                        {{--<th>أحداث</th>--}}

                                        @if(\Illuminate\Support\Facades\Auth::user()->type!='technical_supervisor')
                                            <th>أحداث</th>
                                        @endif

                                        @if(\Illuminate\Support\Facades\Auth::user()->type=='technical_supervisor')
                                            <th>أحداث</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody id="items">

                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="modal-footer">

                    @if(\Illuminate\Support\Facades\Auth::user()->type!='technical_supervisor')
                        <a href="" id="link_add_task" target="_blank">

                            <button type="button" class="btn btn-primary">إضافة مهمة جديدة</button>
                        </a>
                    @endif

                    <button type="button" class="btn btn-link"
                            data-dismiss="modal">{{\App\Http\Controllers\dashboard\ConstantController::$close[$_SESSION['lang']]}}</button>

                </div>
            </div>
        </div>
    </div>

    {{csrf_field()}}

    <script>


        var elem_clicked = null;

        $('#loader_refresh_tasks').on('click', function () {

            console.log("ddd");
            tasks(elem_clicked)

        })


        var _token = $('input[name="_token"]').val();

        function tasks(elem) {
            elem_clicked=elem;
            var elem_id = $(elem).attr('item_id');
            $.ajax({
                url: "{{url('admin/tasks_order')}}", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: {
                    elem_id: elem_id,
                    _token: _token,
                }, // Data sent to server, a set of key/value pairs (i.e. form fields and values)

                success: function (data)   // A function to be called if request succeeds
                {

                    if (data.status == 200) {


                        var items = data.items;

                        $('#items').html('');
                        if (items.length > 0) {
                            $('#items').html('');
                            var html = "";


                            for (var i = 0; i < items.length; i++) {

                                html += '<tr>';
                                html += '<td>' + items[i].id + '</td>';
                                html += '<td>' + items[i].added_by + '</td>';
                                html += '<td>' + items[i].project + '</td>';
                                html += '<td>' + items[i].space + '</td>';
                                html += '<td>' + items[i].type + '</td>';
                                html += '<td>' + items[i].edit_type + '</td>';
                                html += '<td>' + items[i].start_date + '</td>';
                                html += '<td>' + items[i].end_date + '</td>';
                                html += '<td>' + items[i].employee + '</td>';
                                html += '<td>' + items[i].technical_supervisor + '</td>';
                                html += '<td>' + items[i].technical_supervisor_receive_date + '</td>';
                                if (items[i].collected == 0) {

                                    html += '<td>لا</td>';
                                }
                                else {

                                    html += '<td>نعم</td>';
                                }
                                html += '<td>' + items[i].collected_date + '</td>';
                                html += '<td>' + items[i].rate + '</td>';
                                // html += '<td>' + items[i].rate + '</td>';
                                    @if(\Illuminate\Support\Facades\Auth::user()->type!='technical_supervisor')
                                var link = "{{url('admin/edit_task/')}}/" + items[i].id;
                                html += '<td> <ul class="icons-list"><li><a target="_blank" href="' + link + '" data-popup="tooltip"  title="تعديل"  ><i class="icon-pencil7"></i></a></li></ul></td>';
                                @endif

                                    @if(\Illuminate\Support\Facades\Auth::user()->type=='technical_supervisor')

                                if (items[i].technical_supervisor_receive === 0) {
                                    var link = "{{url('admin/technical_supervisor_receive_task/')}}/" + items[i].id;
                                    html += '<td> <ul class="icons-list"><li><a   href="' + link + '" data-popup="tooltip"  title="تم الاستلام"  ><i class="icon-checkmark4"></i></a></li></ul></td>';

                                }
                                @endif

                                html += '</tr>';
                            }
                            $('#items').append(html);
                        }

                    } else {

                    }


                }
            });
        }

        function rec_task(elem) {

            $('#task_id').val($(elem).attr('item_id'));
        }
    </script>
@endsection
