@extends('dashboard.layout.index')
@section('content')

    <!-- Basic table -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">طلبات بانتظار الاستلام</h5>
            <div class="heading-elements">
            </div>
        </div>


        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>
                    <th>#</th>
                    <th>اسم المشروع</th>
                    <th>ترقيم الطلب</th>
                    <th>تاريخ التسليم</th>
                    <th>الأهمية</th>
                    <th>المهام المنجزة</th>
                    <th>المهام غير موزعة</th>
                    <th>المهام المستلمة من المشرف الفني</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $key=>$item)
                    <tr>
                        <td style="    text-align: center;">

                            <ul class="icons-list">

                                <li>
                                    <a href="javascript:;" data-popup="tooltip" item_id="{{$item->id}}"
                                       onclick="project_note(this)" data-toggle="modal" data-target="#order_notes"
                                       title="" data-original-title="ملاحظات المشروع"><i class="icon  icon-quill4"></i></a>
                                </li>


                                <li><a href="{{url('admin/tasks/?order_id='.$item->id)}}" data-popup="tooltip"
                                       title="المهام"
                                    ><i class="icon icon-design"></i></a></li>


                                {{--<li><a href="{{url('admin/edit_order/'.$item->id)}}" data-popup="tooltip"--}}
                                {{--title="التفاصيل"--}}
                                {{--><i class="icon-eye4"></i></a></li>--}}

                                {{--<li><a href="{{url('admin/seen_by_technical/'.$item->id)}}" data-popup="tooltip"--}}
                                {{--title="تم الاستلام"--}}
                                {{--><i class="icon-checkmark4"></i></a></li>--}}


                            </ul>
                        </td>
                        <td>{{$item->id}}</td>
                        <td>{{\App\Models\ProjectModel::find($item->project)->name_ar}}</td>
                        <td>{{$item->order_number." | ".$item->date_received}}</td>
                        <td>{{$item->delivery_date}}</td>
                        <td>{{\App\Http\Controllers\dashboard\ConstantController::importance($item->Importance)}}</td>
                        @if(isset($item->tasks_ended ) && isset($item->all_tasks) && isset($item->task_received_from_technical))

                            <td>{{$item->tasks_ended ."  \ ". $item->all_tasks }}</td>
                            <td>{{$item->tasks_not_inserted ."  \ ". $item->all_tasks }}</td>
                            <td>{{$item->task_received_from_technical ."  \ ". $item->all_tasks }}</td>
                        @else

                            <td>{{0 ."  \ ". 0 }}</td>
                            <td>{{0 ."  \ ". 0 }}</td>
                            <td>{{0 ."  \ ". 0 }}</td>
                        @endif


                        {{--<td>{{$item->tasks_ended ."  \ ". $item->all_tasks }}</td>--}}
                        {{--<td>{{$item->task_received_from_technical ."  \ ". $item->all_tasks }}</td>--}}
                        {{----}}

                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>

        <div class="table-responsive">

        </div>
    </div>
    <!-- /basic table -->



    <!-- Basic table -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">مهام جاهزة للاستلام </h5>
            <div class="heading-elements">
            </div>
        </div>


        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>
                    <th>#</th>
                    <th>المشروع</th>
                    <th>الطلب</th>
                    <th>الموقع - اسم الفراغ</th>
                    <th>نوع المهمة</th>
                    <th>نوع التعديل</th>
                    <th>تاريخ التسليم</th>
                    <th>انتهى الموظف عند</th>
                    <th>الموظف</th>
                    {{--<th>المشرف</th>--}}
                    <th>تاريخ استلام المشرف</th>
                    <th>تقييم المشرف</th>
                    <th>استفسار</th>
                </tr>
                </thead>
                <tbody>
                @foreach($task_ready_to_recive as $key=>$item)
                    @if((strtotime($item->start_date) < strtotime(date('Y-m-d') ) ) && $item->finished==0)

                        <?php  $class = "alert-warning"?>
                    @else

                        <?php  $class = ""?>
                    @endif

                    @if($item->technical_supervisor_receive==1)
                        <tr style="background-color: #6bb479">
                    @else

                        <tr class="{{$class}}">
                            @endif
                            <td style="    text-align: center;">

                                <ul class="icons-list">


                                    <li>
                                        <a href="javascript:;" data-popup="tooltip" item_id="{{$item->order}}"
                                           onclick="project_note(this)" data-toggle="modal" data-target="#order_notes"
                                           title="" data-original-title="ملاحظات المشروع"><i class="icon  icon-quill4"></i></a>
                                    </li>


                                    <li><a href="javascript:;" data-popup="tooltip"
                                           item_id="{{$item->id}}"
                                           onclick="tasks_history(this)"
                                           data-toggle="modal" data-target="#tasks_history"
                                           title="مهام سابقة"
                                        ><i class="icon icon-history"></i></a></li>


                                    {{--<li><a href="{{url('admin/files/'.$item->id."?type=task")}}" data-popup="tooltip"--}}
                                    {{--title="الملفات"--}}
                                    {{--><i class="icon icon-files-empty2"></i></a></li>--}}

                                    {{--<li><a href="{{url('admin/edit_task/'.$item->id)}}" data-popup="tooltip"--}}
                                    {{--title="التفاصيل"--}}
                                    {{--><i class="icon-eye4"></i></a></li>--}}

                                    @if($item->technical_supervisor_receive==0 && $item->finished==1)
                                        {{--<li><a href="{{url('admin/technical_supervisor_receive_task/'.$item->id)}}"--}}
                                        {{--data-popup="tooltip"--}}
                                        {{--title="تم الاستلام"--}}
                                        {{--><i class="icon-checkmark4"></i></a></li>--}}

                                        <li><a href="#" data-toggle="modal" data-target="#rec_task"
                                               data-popup="tooltip"
                                               item_id="{{$item->id}}"
                                               onclick="rec_task(this)"
                                               title="تم الاستلام"
                                            ><i class="icon-checkmark4"></i></a></li>
                                    @endif
                                </ul>
                            </td>
                            <td>{{$item->id}}</td>
                            <td>{{\App\Models\ProjectModel::find($item->project)->name_ar}}</td>
                            <td>{{\App\Models\OrderModel::find($item->order)->order_number}}</td>

                            @if($item->space)
                                <td>


                                    {{\App\Models\SpaceLocationModel::find(\App\Models\SpaceModel::find($item->space)->location)->name." | ".\App\Models\SpaceModel::find($item->space)->name}}

                                </td>

                            @else
                                <td></td>
                                @endif

                            <td>{{\App\Models\TaskTypeModel::find($item->type)->name}}</td>
                            <td>
                                @if($item->edit_type>0)
                                    {{\App\Models\TaskEditTypeModel::find($item->type)->name}}
                                @endif
                            </td>
                            <td>{{$item->start_date}}</td>
                            <td>
                                @if($item->end_date)

                                    {{date('Y-m-d H:i',strtotime($item->end_date))}}
                                @endif
                            </td>
                            <td>{{\App\User::find($item->employee)->name}}</td>
                            {{--<td>{{\App\User::find($item->technical_supervisor)->name}}</td>--}}
                            <td>{{$item->technical_supervisor_receive_date}}</td>
                            <td>{{$item->rate}}</td>
                            <td>

                                @if($item->have_inquiry==0)

                                @else



                                    <span class="label label-danger">يوجد</span>

                                @endif

                            </td>

                        </tr>

                        @endforeach
                </tbody>
            </table>
        </div>

        <div class="table-responsive">

        </div>
    </div>
    <!-- /basic table -->





    <!-- Basic table -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">مهام تسليمها اليوم </h5>
            <div class="heading-elements">
            </div>
        </div>


        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>
                    <th>#</th>
                    <th>المشروع</th>
                    <th>الطلب</th>
                    <th>الموقع - اسم الفراغ</th>
                    <th>نوع المهمة</th>
                    <th>نوع التعديل</th>
                    <th>تاريخ التسليم</th>
                    <th>انتهى الموظف عند</th>
                    <th>الموظف</th>
                    {{--<th>المشرف</th>--}}
                    <th>تاريخ استلام المشرف</th>
                    <th>تقييم المشرف</th>
                    <th>استفسار</th>
                </tr>
                </thead>
                <tbody>
                @foreach($tasks as $key=>$item)
                    @if((strtotime($item->start_date) < strtotime(date('Y-m-d') ) ) && $item->finished==0)

                        <?php  $class = "alert-warning"?>
                    @else

                        <?php  $class = ""?>
                    @endif

                    @if($item->technical_supervisor_receive==1)
                        <tr style="background-color: #6bb479">
                    @else

                        <tr class="{{$class}}">
                            @endif
                            <td style="    text-align: center;">

                                <ul class="icons-list">

                                    <li>
                                        <a href="javascript:;" data-popup="tooltip" item_id="{{$item->order}}"
                                           onclick="project_note(this)" data-toggle="modal" data-target="#order_notes"
                                           title="" data-original-title="ملاحظات المشروع"><i class="icon  icon-quill4"></i></a>
                                    </li>

                                    <li><a href="javascript:;" data-popup="tooltip"
                                           item_id="{{$item->id}}"
                                           onclick="tasks_history(this)"
                                           data-toggle="modal" data-target="#tasks_history"
                                           title="مهام سابقة"
                                        ><i class="icon icon-history"></i></a></li>

                                    {{--<li><a href="{{url('admin/files/'.$item->id."?type=task")}}" data-popup="tooltip"--}}
                                    {{--title="الملفات"--}}
                                    {{--><i class="icon icon-files-empty2"></i></a></li>--}}

                                    {{--<li><a href="{{url('admin/edit_task/'.$item->id)}}" data-popup="tooltip"--}}
                                    {{--title="التفاصيل"--}}
                                    {{--><i class="icon-eye4"></i></a></li>--}}

                                    @if($item->technical_supervisor_receive==0 && $item->finished==1)
                                        {{--<li><a href="{{url('admin/technical_supervisor_receive_task/'.$item->id)}}"--}}
                                        {{--data-popup="tooltip"--}}
                                        {{--title="تم الاستلام"--}}
                                        {{--><i class="icon-checkmark4"></i></a></li>--}}

                                        <li><a href="#" data-toggle="modal" data-target="#rec_task"
                                               data-popup="tooltip"
                                               item_id="{{$item->id}}"
                                               onclick="rec_task(this)"
                                               title="تم الاستلام"
                                            ><i class="icon-checkmark4"></i></a></li>
                                    @endif
                                </ul>
                            </td>
                            <td>{{$item->id}}</td>
                            <td>{{\App\Models\ProjectModel::find($item->project)->name_ar}}</td>
                            <td>{{\App\Models\OrderModel::find($item->order)->order_number}}</td>
                            @if($item->space)
                            <td>


                                {{\App\Models\SpaceLocationModel::find(\App\Models\SpaceModel::find($item->space)->location)->name." | ".\App\Models\SpaceModel::find($item->space)->name}}

                            </td>
                            @else
                                <td></td>
                                @endif

                            <td>{{\App\Models\TaskTypeModel::find($item->type)->name}}</td>
                            <td>
                                @if($item->edit_type>0)
                                    {{\App\Models\TaskEditTypeModel::find($item->type)->name}}
                                @endif
                            </td>
                            <td>{{$item->start_date}}</td>
                            <td>
                                @if($item->end_date)

                                    {{date('Y-m-d H:i',strtotime($item->end_date))}}
                                @endif
                            </td>
                            <td>{{\App\User::find($item->employee)->name}}</td>
                            {{--<td>{{\App\User::find($item->technical_supervisor)->name}}</td>--}}
                            <td>{{$item->technical_supervisor_receive_date}}</td>
                            <td>{{$item->rate}}</td>
                            <td>

                                @if($item->have_inquiry==0)

                                @else



                                    <span class="label label-danger">يوجد</span>

                                @endif

                            </td>

                        </tr>

                        @endforeach
                </tbody>
            </table>
        </div>

        <div class="table-responsive">

        </div>
    </div>
    <!-- /basic table -->


    <!-- Basic table -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">مهام يجب استلامها </h5>
            <div class="heading-elements">
            </div>
        </div>


        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>
                    <th>#</th>
                    <th>المشروع</th>
                    <th>الطلب</th>
                    <th>الموقع - اسم الفراغ</th>
                    <th>نوع المهمة</th>
                    <th>نوع التعديل</th>
                    <th>تاريخ التسليم</th>
                    <th>انتهى الموظف عند</th>
                    <th>الموظف</th>
                    {{--<th>المشرف</th>--}}
                    <th>تاريخ استلام المشرف</th>
                    <th>تقييم المشرف</th>
                    <th>استفسار</th>
                </tr>
                </thead>
                <tbody>
                @foreach($tasks_should_recived as $key=>$item)



                    @if($item->technical_supervisor_receive==1)
                        <tr style="background-color: #6bb479">
                    @else
                        <tr>
                            @endif
                            <td style="    text-align: center;">

                                <ul class="icons-list">

                                    <li>
                                        <a href="javascript:;" data-popup="tooltip" item_id="{{$item->order}}"
                                           onclick="project_note(this)" data-toggle="modal" data-target="#order_notes"
                                           title="" data-original-title="ملاحظات المشروع"><i class="icon  icon-quill4"></i></a>
                                    </li>



                                    <li><a href="javascript:;" data-popup="tooltip"
                                           item_id="{{$item->id}}"
                                           onclick="tasks_history(this)"
                                           data-toggle="modal" data-target="#tasks_history"
                                           title="مهام سابقة"
                                        ><i class="icon icon-history"></i></a></li>

                                    {{--<li><a href="{{url('admin/files/'.$item->id."?type=task")}}" data-popup="tooltip"--}}
                                    {{--title="الملفات"--}}
                                    {{--><i class="icon icon-files-empty2"></i></a></li>--}}

                                    {{--<li><a href="{{url('admin/edit_task/'.$item->id)}}" data-popup="tooltip"--}}
                                    {{--title="التفاصيل"--}}
                                    {{--><i class="icon-eye4"></i></a></li>--}}

                                    @if($item->technical_supervisor_receive==0 && $item->finished==1)
                                        {{--<li><a href="{{url('admin/technical_supervisor_receive_task/'.$item->id)}}"--}}
                                        {{--data-popup="tooltip"--}}
                                        {{--title="تم الاستلام"--}}
                                        {{--><i class="icon-checkmark4"></i></a></li>--}}

                                        <li><a href="#" data-toggle="modal" data-target="#rec_task"
                                               data-popup="tooltip"
                                               item_id="{{$item->id}}"
                                               onclick="rec_task(this)"
                                               title="تم الاستلام"
                                            ><i class="icon-checkmark4"></i></a></li>
                                    @endif
                                </ul>
                            </td>
                            <td>{{$item->id}}</td>
                            <td>{{\App\Models\ProjectModel::find($item->project)->name_ar}}</td>
                            <td>{{\App\Models\OrderModel::find($item->order)->order_number." | ".\App\Models\OrderModel::find($item->order)->date_received}}</td>
                            @if($item->space)
                            <td>{{\App\Models\SpaceLocationModel::find(\App\Models\SpaceModel::find($item->space)->location)->name." | ".\App\Models\SpaceModel::find($item->space)->name}}</td>
                           @else
                                <td></td>
                                @endif
                            <td>{{\App\Models\TaskTypeModel::find($item->type)->name}}</td>
                            <td>
                                @if($item->edit_type>0)
                                    {{\App\Models\TaskEditTypeModel::find($item->type)->name}}
                                @endif
                            </td>
                            <td>{{$item->start_date}}</td>
                            <td>
                                @if($item->end_date)

                                    {{date('Y-m-d H:i',strtotime($item->end_date))}}
                                @endif
                            </td>
                            <td>{{\App\User::find($item->employee)->name}}</td>
                            {{--<td>{{\App\User::find($item->technical_supervisor)->name}}</td>--}}
                            <td>{{$item->technical_supervisor_receive_date}}</td>
                            <td>{{$item->rate}}</td>
                            <td>

                                @if($item->have_inquiry==0)


                                @else



                                    <span class="label label-danger">يوجد</span>

                                @endif

                            </td>

                        </tr>

                        @endforeach
                </tbody>
            </table>
        </div>

        <div class="table-responsive">

        </div>
    </div>
    <!-- /basic table -->


    <div id="rec_task" class="modal fade ">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                    @if(\App\Models\ConstantModel::get_const("if_rate")=="yes")
                    <h5 class="modal-title">التقييم </h5>
                        @else
                        <h5 class="modal-title">تأكيد الاستلام </h5>
                    @endif
                </div>
                <form action="{{url('admin/technical_supervisor_receive_task')}}" method="post"
                      enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">

                        <input name="task_id" id="task_id" value="" type="hidden">
                        <div class="row">


                            @if(\App\Models\ConstantModel::get_const("if_rate")=="yes")

                                <div class="col-lg-12">

                                    <div class="form-group">
                                        <label>التققيم </label>
                                        <input type="number" max="5" min="1" name="rate" class="form-control"
                                               placeholder=""
                                        >
                                    </div>
                                </div>
                            @endif


                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link"
                                data-dismiss="modal">{{\App\Http\Controllers\dashboard\ConstantController::$close[$_SESSION['lang']]}}</button>
                        <button type="submit"
                                class="btn btn-primary">تم الاستلام
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>

        function rec_task(elem) {

            $('#task_id').val($(elem).attr('item_id'));
        }
    </script>
@endsection
