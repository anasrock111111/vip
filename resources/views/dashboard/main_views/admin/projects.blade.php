@extends('dashboard.layout.index')
@section('content')

    <!-- Body classes -->
    <style>

        .image_review {
            width: 50%;
            margin-top: 5%;
        }
    </style>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title"> {{\App\Http\Controllers\dashboard\ConstantController::$content[$_SESSION['lang']]}} {{$view_name}}</h5>
            <div class="heading-elements">
                <ul class="icons-list">

                    @if(\Illuminate\Support\Facades\Auth::user()->type=="admin")

                        <a href="{{url('admin/add_project')}}">
                            <button data-popup="tooltip" title="إضافة" type="button"
                                    class="btn border-slate text-slate-800 btn-flat"><i class="icon-add "></i>
                            </button>
                        </a>
                    @endif
                </ul>

            </div>

        </div>

        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>
                    <th>#</th>
                    <th>اضيف بواسطة</th>
                    <th>الاسم</th>
                    <th>الزبون</th>
                    <th>النوع</th>
                    <th>طبيعته</th>
                    <th>الاستلام</th>
                    <th>المساحة</th>
                    @if(\Illuminate\Support\Facades\Auth::user()->type=="admin")
                        <th>السعر</th>
                    @endif
                    <th>مسار التعديلات</th>
                    <th>مسار المشروع</th>
                    <th>ارتفاع السقف</th>
                    <th>نوع التكييف</th>
                    <th>نزول التكييف</th>
                    <th>مخطط التكييف</th>
                    <th>ملاحظات الزبون</th>
                    <th>ملاحظات المشرف الفني</th>
                    <th>حالة المشروع</th>
                </tr>
                </thead>
                <tbody>
                @foreach($projects as $key=>$item)
                    <tr>

                        <td style="    text-align: center;">

                            <ul class="icons-list">
                                @if(\Illuminate\Support\Facades\Auth::user()->type=="admin")
                                    <li>
                                        <a href="{{url('admin/add_space?project='.$item->id."")}}"
                                           data-popup="tooltip"
                                           title="إضافة فراغ"
                                        ><i class="icon icon-folder-plus2"></i></a></li>
                                    <li>
                                        <a href="{{url('admin/add_order?project='.$item->id."")}}"
                                           data-popup="tooltip"
                                           title="إضافة طلب"
                                        ><i class="icon icon-bell-plus"></i></a></li>
                                    <li><a href="javascript:;" data-popup="tooltip" title="حذف"
                                           onclick="remove_item(this)" item_id="{{$item->id}}"
                                        ><i class="icon-trash"></i></a></li>

                                    <li><a href="{{url('admin/files/'.$item->id."?type=project")}}" data-popup="tooltip"
                                           title="الملفات"
                                        ><i class="icon icon-files-empty2"></i></a></li>
                                @endif


                                <li><a href="{{url('admin/edit_project/'.$item->id)}}" data-popup="tooltip"
                                       title="تعديل"
                                    ><i class="icon-pencil7"></i></a></li>

                            </ul>
                        </td>
                        <td>{{$key+1}}</td>
                        <td>{{\App\User::find($item->added_by)->name}}</td>
                        <td>{{$item->name_ar." - ".$item->name_en}}</td>
                        <td>{{\App\User::find($item->user)->code}}</td>
                        <td>{{\App\Http\Controllers\dashboard\ConstantController::project_type($item->type)}}</td>
                        @if($item->nature>0)
                            <td>{{\App\Models\NatureProjectModel::find($item->nature)->name}}</td>
                            @else
                            <td></td>
                            @endif
                        <td>{{$item->receive_date}}</td>
                        <td>{{$item->internal_space}}</td>
                        @if(\Illuminate\Support\Facades\Auth::user()->type=="admin")
                            <td>{{$item->price." ".\App\Http\Controllers\dashboard\ConstantController::currency($item->currency)}}</td>
                        @endif
                        <td style="direction: ltr;"><a href="{{$item->edit_direction}}"
                                                       target="_blank">{{$item->edit_direction}}</a></td>
                        <td style="direction: ltr;"><a href="{{$item->project_direction}}"
                                                       target="_blank">{{$item->project_direction}}</a></td>
                        <td>{{$item->ceiling_height}}</td>
                        <td>{{$item->type_condition}}</td>
                        <td>{{$item->space_condition}}</td>
                        <td>
                            @if($item->planer_condition_file==1)
                                نعم
                            @else
                                لا
                            @endif
                        </td>
                        <td>{{$item->user_note}}</td>
                        <td>{{$item->technical_supervisor_note}}</td>
                        <td>
                            @if($item->project_ended==1)
                                انتهى
                            @else
                                لم ينتهي
                            @endif
                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>

    </div>
    <!-- /body classes -->





    <script>


        function remove_item(elem) {

            swal({
                    title: "{{\App\Http\Controllers\dashboard\ConstantController::$are_you_sure[$_SESSION['lang']]}}",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#EF5350",
                    confirmButtonText: "{{\App\Http\Controllers\dashboard\ConstantController::$on[$_SESSION['lang']]}}",
                    cancelButtonText: "{{\App\Http\Controllers\dashboard\ConstantController::$off[$_SESSION['lang']]}}",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {

                    if (isConfirm) {
                        var item_id = $(elem).attr('item_id');
                        var url = "{{url('admin/remove_project')}}";
                        $('<form>', {
                            "action": url + "/" + item_id,
                        }).appendTo(document.body).submit();

                        {{--swal({--}}
                        {{--title: "{{\App\Http\Controllers\dashboard\ConstantController::$deleted[$_SESSION['lang']]}} ",--}}
                        {{--text: "{{\App\Http\Controllers\dashboard\ConstantController::$deleted_success[$_SESSION['lang']]}}",--}}
                        {{--confirmButtonColor: "#66BB6A",--}}
                        {{--type: "success"--}}
                        {{--});--}}
                    }
                    else {
                        swal({
                            title: "{{\App\Http\Controllers\dashboard\ConstantController::$close[$_SESSION['lang']]}}",
                            text: "",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });

        }
    </script>

@endsection
