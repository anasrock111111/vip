@extends('dashboard.layout.index')
@section('content')

    <!-- Body classes -->
    <style>

        .image_review {
            width: 50%;
            margin-top: 5%;
        }
    </style>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title"> {{\App\Http\Controllers\dashboard\ConstantController::$content[$_SESSION['lang']]}} {{$view_name}}</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <a href="{{url('admin/add_space')}}">
                        <button data-popup="tooltip" title="إضافة" type="button"
                                class="btn border-slate text-slate-800 btn-flat"><i class="icon-add "></i>
                        </button>
                    </a>
                </ul>

            </div>

        </div>

        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>

                    <th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>
                    <th>#</th>
                    <th>اضيف بواسطة</th>
                    <th>اسم المشروع</th>
                    <th>نوع الفراغ</th>
                    <th>الموقع - اسم الفراغ </th>
                    <th>العدد</th>
                </tr>
                </thead>
                <tbody>
                @foreach($spaces as $key=>$item)
                    <tr>
                        <td style="    text-align: center;">

                            <ul class="icons-list">

                                {{--<li><a href="{{url('admin/files/'.$item->id."?type=space")}}" data-popup="tooltip"--}}
                                {{--title="الملفات"--}}
                                {{--><i class="icon icon-files-empty2"></i></a></li>--}}

                                <li><a href="{{url('admin/edit_space/'.$item->id)}}" data-popup="tooltip"
                                       title="تعديل"
                                    ><i class="icon-pencil7"></i></a></li>
                                <li><a href="javascript:;" data-popup="tooltip" title="حذف"
                                       onclick="remove_item(this)" item_id="{{$item->id}}"
                                    ><i class="icon-trash"></i></a></li>
                            </ul>
                        </td>
                        <td>{{$item->id}}</td>
                        <td>{{\App\User::find($item->added_by)->name}}</td>
                        <td>{{\App\Models\ProjectModel::find($item->project)->name_ar}}</td>
                        <td>{{\App\Models\SpaceTypeModel::find($item->type)->name  }}</td>
                        <td>{{$item->name ." - ".\App\Models\SpaceLocationModel::find($item->location)->name}}</td>
                        <td>{{$item->count}}</td>

                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>

    </div>
    <!-- /body classes -->





    <script>


        function remove_item(elem) {

            swal({
                    title: "{{\App\Http\Controllers\dashboard\ConstantController::$are_you_sure[$_SESSION['lang']]}}",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#EF5350",
                    confirmButtonText: "{{\App\Http\Controllers\dashboard\ConstantController::$on[$_SESSION['lang']]}}",
                    cancelButtonText: "{{\App\Http\Controllers\dashboard\ConstantController::$off[$_SESSION['lang']]}}",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {

                    if (isConfirm) {
                        var item_id = $(elem).attr('item_id');
                        var url = "{{url('admin/remove_space')}}";
                        $('<form>', {
                            "action": url + "/" + item_id,
                        }).appendTo(document.body).submit();

                        {{--swal({--}}
                            {{--title: "{{\App\Http\Controllers\dashboard\ConstantController::$deleted[$_SESSION['lang']]}} ",--}}
                            {{--text: "{{\App\Http\Controllers\dashboard\ConstantController::$deleted_success[$_SESSION['lang']]}}",--}}
                            {{--confirmButtonColor: "#66BB6A",--}}
                            {{--type: "success"--}}
                        {{--});--}}
                    }
                    else {
                        swal({
                            title: "{{\App\Http\Controllers\dashboard\ConstantController::$close[$_SESSION['lang']]}}",
                            text: "",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });

        }
    </script>

@endsection
