@extends('dashboard.layout.index')
@section('content')

    <!-- Body classes -->
    <style>

        .image_review {
            width: 50%;
            margin-top: 5%;
        }

    </style>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title"> {{\App\Http\Controllers\dashboard\ConstantController::$content[$_SESSION['lang']]}} {{$view_name}}</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    @if(\Illuminate\Support\Facades\Auth::user()->type=="admin" || \Illuminate\Support\Facades\Auth::user()->type=="project_manager")

                        <a href="{{url('admin/add_task')}}">
                            <button data-popup="tooltip" title="إضافة" type="button"
                                    class="btn border-slate text-slate-800 btn-flat"><i class="icon-add "></i>
                            </button>
                        </a>
                    @endif
                </ul>

            </div>

        </div>

        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>
                    <th>#</th>
                    <th>اضيف بواسطة</th>
                    <th>المشروع</th>
                    <th>الطلب</th>
                    <th>الفراغ</th>
                    <th>نوع المهمة</th>
                    <th>نوع التعديل</th>
                    <th>تاريخ التسليم</th>
                    <th>انتهاء الموظف</th>
                    <th>الموظف</th>
                    <th>المشرف</th>
                    <th>تاريخ استلام المشرف</th>
                    <th>تم التجميع</th>
                    <th>تاريخ التجميع</th>
                    @if(\Illuminate\Support\Facades\Auth::user()->type!="employee")

                        <th>تقييم المشرف</th>
                    @endif
                    <th>استفسار</th>
                </tr>
                </thead>
                <tbody>
                @foreach($tasks as $key=>$item)
                    <tr>
                        <td style="    text-align: center;">

                            <ul class="icons-list">


                                <li>
                                    <a href="javascript:;" data-popup="tooltip" item_id="{{$item->order}}"
                                       onclick="project_note(this)" data-toggle="modal" data-target="#order_notes"
                                       title="" data-original-title="ملاحظات المشروع"><i class="icon  icon-quill4"></i></a>
                                </li>


                                <li><a href="javascript:;" data-popup="tooltip"
                                       item_id="{{$item->id}}"
                                       onclick="tasks_history(this)"
                                       data-toggle="modal" data-target="#tasks_history"
                                       title="مهام سابقة"
                                    ><i class="icon icon-history"></i></a></li>

                                @if(\Illuminate\Support\Facades\Auth::user()->type=="admin" || \Illuminate\Support\Facades\Auth::user()->type=="project_manager" )



                                    {{--<li><a href="{{url('admin/files/'.$item->id."?type=task")}}" data-popup="tooltip"--}}
                                    {{--title="الملفات"--}}
                                    {{--><i class="icon icon-files-empty2"></i></a></li>--}}


                                    {{--<li>{{sizeof(\App\Models\TaskCommentModel::where('added_type', 'employee')->where('task', $item->id)->where('deleted', 0)->where('seen', 0)->get())}}--}}
                                    {{--<a href="#" data-toggle="modal" data-target="#comments_"--}}
                                    {{--data-popup="tooltip"--}}
                                    {{--item_id="{{$item->id}}"--}}
                                    {{--onclick="get_comments(this)"--}}
                                    {{--title="الاستفسارات"--}}
                                    {{--><i class="glyphicon glyphicon-comment"></i></a></li>--}}

                                    <li><a href="{{url('admin/edit_task/'.$item->id)}}" data-popup="tooltip"
                                           title="تعديل"
                                        ><i class="icon-pencil7"></i></a></li>

                                    <li><a href="javascript:;" data-popup="tooltip" title="حذف"
                                           onclick="remove_item(this)" item_id="{{$item->id}}"
                                        ><i class="icon-trash"></i></a></li>

                                    {{--@elseif(\Illuminate\Support\Facades\Auth::user()->type=="project_manager")--}}

                                    {{--<li><a href="{{url('admin/files/'.$item->id."?type=task")}}" data-popup="tooltip"--}}
                                    {{--title="الملفات"--}}
                                    {{--><i class="icon icon-files-empty2"></i></a></li>--}}


                                    {{--<li>{{sizeof(\App\Models\TaskCommentModel::where('added_type', 'employee')->where('task', $item->id)->where('deleted', 0)->where('seen', 0)->get())}}--}}
                                    {{--<a href="#" data-toggle="modal" data-target="#comments_"--}}
                                    {{--data-popup="tooltip"--}}
                                    {{--item_id="{{$item->id}}"--}}
                                    {{--onclick="get_comments(this)"--}}
                                    {{--title="الاستفسارات"--}}
                                    {{--><i class="glyphicon glyphicon-comment"></i></a></li>--}}

                                @elseif(\Illuminate\Support\Facades\Auth::user()->type=="technical_supervisor")
                                    {{--<li><a href="{{url('admin/files/'.$item->id."?type=task")}}" data-popup="tooltip"--}}
                                    {{--title="الملفات"--}}
                                    {{--><i class="icon icon-files-empty2"></i></a></li>--}}

                                    {{--<li><a href="{{url('admin/edit_task/'.$item->id)}}" data-popup="tooltip"--}}
                                    {{--title="التفاصيل"--}}
                                    {{--><i class="icon-eye4"></i></a></li>--}}

                                    @if($item->technical_supervisor_receive==0 && $item->finished==1 )
                                        <li><a href="{{url('admin/technical_supervisor_receive_task/'.$item->id)}}"
                                               data-popup="tooltip"
                                               title="تم الاستلام"
                                            ><i class="icon-checkmark4"></i></a></li>
                                    @endif
                                @endif


                            </ul>
                        </td>
                        <td>{{$item->id}}</td>
                        <td>{{\App\User::find($item->added_by)->name}}</td>
                        <td>{{\App\Models\ProjectModel::find($item->project)->name_ar}}</td>
                        <td>{{\App\Models\OrderModel::find($item->order)->order_number . " | ".\App\Models\OrderModel::find($item->order)->date_received}}</td>

                        @if($item->space>0)
                            <td>{{ \App\Models\SpaceLocationModel::find(\App\Models\SpaceModel::find($item->space)->location)->name ." - ".\App\Models\SpaceModel::find($item->space)->name}}</td>

                        @else
                            <td>{{$item->note_plan}}</td>
                        @endif
                        <td>{{\App\Models\TaskTypeModel::find($item->type)->name}}</td>
                        <td>
                            @if($item->edit_type>0)
                                {{\App\Models\TaskEditTypeModel::find($item->type)->name}}
                            @endif
                        </td>
                        <td>{{$item->start_date}}</td>
                        <td>{{$item->end_date}}</td>
                        <td>


                            @if($item->employee>0)

                                {{\App\User::find($item->employee)->name}}

                            @endif

                        </td>
                        <td>{{\App\User::find($item->technical_supervisor)->name}}</td>
                        <td>{{$item->technical_supervisor_receive_date}}</td>
                        <td>

                            @if($item->collected==0)
                                لا
                            @else
                                نعم
                            @endif
                        </td>

                        <td>{{$item->collected_date}}</td>
                        @if(\Illuminate\Support\Facades\Auth::user()->type!="employee")

                            <td>{{$item->rate}}</td>
                        @endif
                        <td>
                            @if($item->have_inquiry==0)



                            @else

                                يوجد

                                <a href="{{url('admin/add_have_inquiry/'.$item->id)}}">
                                    <span class="label label-danger">تم الرد الاستفسار</span></a>


                            @endif
                        </td>

                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>

    </div>
    <!-- /body classes -->




    @if(\Illuminate\Support\Facades\Auth::user()->type=="admin" || \Illuminate\Support\Facades\Auth::user()->type=="project_manager")

        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">مهام بدون موظفين</h5>
                <div class="heading-elements">
                    <ul class="icons-list">

                    </ul>

                </div>

            </div>

            <div class="panel-body">
                <table class="table datatable-basic">
                    <thead>
                    <tr>

                        <th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>
                        <th>#</th>
                        <th>اضيف بواسطة</th>
                        <th>المشروع</th>
                        <th>الطلب</th>
                        <th>الفراغ</th>
                        <th>نوع المهمة</th>
                        <th>نوع التعديل</th>
                        <th>تاريخ التسليم</th>
                        <th>المشرف</th>
                        <th>تاريخ استلام المشرف</th>
                        <th>تم التجميع</th>
                        <th>تاريخ التجميع</th>
                        @if(\Illuminate\Support\Facades\Auth::user()->type!="employee")

                            <th>تقييم المشرف</th>
                        @endif
                        <th>استفسار</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tasks_with_out_employee as $key=>$item)
                        <tr>
                            <td style="    text-align: center;">

                                <ul class="icons-list">

                                    <li>
                                        <a href="javascript:;" data-popup="tooltip" item_id="{{$item->order}}"
                                           onclick="project_note(this)" data-toggle="modal" data-target="#order_notes"
                                           title="" data-original-title="ملاحظات المشروع"><i
                                                class="icon  icon-quill4"></i></a>
                                    </li>


                                    <li><a href="javascript:;" data-popup="tooltip"
                                           item_id="{{$item->id}}"
                                           onclick="tasks_history(this)"
                                           data-toggle="modal" data-target="#tasks_history"
                                           title="مهام سابقة"
                                        ><i class="icon icon-history"></i></a></li>


                                    @if(\Illuminate\Support\Facades\Auth::user()->type=="admin" || \Illuminate\Support\Facades\Auth::user()->type=="project_manager" )




                                        {{--<li><a href="{{url('admin/files/'.$item->id."?type=task")}}"--}}
                                        {{--data-popup="tooltip"--}}
                                        {{--title="الملفات"--}}
                                        {{--><i class="icon icon-files-empty2"></i></a></li>--}}


                                        {{--<li>{{sizeof(\App\Models\TaskCommentModel::where('added_type', 'employee')->where('task', $item->id)->where('deleted', 0)->where('seen', 0)->get())}}--}}
                                        {{--<a href="#" data-toggle="modal" data-target="#comments_"--}}
                                        {{--data-popup="tooltip"--}}
                                        {{--item_id="{{$item->id}}"--}}
                                        {{--onclick="get_comments(this)"--}}
                                        {{--title="الاستفسارات"--}}
                                        {{--><i class="glyphicon glyphicon-comment"></i></a></li>--}}

                                        <li><a href="{{url('admin/edit_task/'.$item->id)}}" data-popup="tooltip"
                                               title="تعديل"
                                            ><i class="icon-pencil7"></i></a></li>

                                        <li><a href="javascript:;" data-popup="tooltip" title="حذف"
                                               onclick="remove_item(this)" item_id="{{$item->id}}"
                                            ><i class="icon-trash"></i></a></li>

                                        {{--@elseif(\Illuminate\Support\Facades\Auth::user()->type=="project_manager")--}}

                                        {{--<li><a href="{{url('admin/files/'.$item->id."?type=task")}}" data-popup="tooltip"--}}
                                        {{--title="الملفات"--}}
                                        {{--><i class="icon icon-files-empty2"></i></a></li>--}}


                                        {{--<li>{{sizeof(\App\Models\TaskCommentModel::where('added_type', 'employee')->where('task', $item->id)->where('deleted', 0)->where('seen', 0)->get())}}--}}
                                        {{--<a href="#" data-toggle="modal" data-target="#comments_"--}}
                                        {{--data-popup="tooltip"--}}
                                        {{--item_id="{{$item->id}}"--}}
                                        {{--onclick="get_comments(this)"--}}
                                        {{--title="الاستفسارات"--}}
                                        {{--><i class="glyphicon glyphicon-comment"></i></a></li>--}}

                                    @elseif(\Illuminate\Support\Facades\Auth::user()->type=="technical_supervisor")
                                        {{--<li><a href="{{url('admin/files/'.$item->id."?type=task")}}"--}}
                                        {{--data-popup="tooltip"--}}
                                        {{--title="الملفات"--}}
                                        {{--><i class="icon icon-files-empty2"></i></a></li>--}}

                                        <li><a href="{{url('admin/edit_task/'.$item->id)}}" data-popup="tooltip"
                                               title="التفاصيل"
                                            ><i class="icon-eye4"></i></a></li>

                                        @if($item->technical_supervisor_receive==0 && $item->finished==1)
                                            <li><a href="{{url('admin/technical_supervisor_receive_task/'.$item->id)}}"
                                                   data-popup="tooltip"
                                                   title="تم الاستلام"
                                                ><i class="icon-checkmark4"></i></a></li>
                                        @endif
                                    @endif


                                </ul>
                            </td>
                            <td>{{$item->id}}</td>
                            <td>{{\App\User::find($item->added_by)->name}}</td>
                            <td>{{\App\Models\ProjectModel::find($item->project)->name_ar}}</td>
                            <td>{{\App\Models\OrderModel::find($item->order)->order_number . " | ".\App\Models\OrderModel::find($item->order)->date_received}}</td>
                            @if($item->space>0)

                                <td>{{ \App\Models\SpaceLocationModel::find(\App\Models\SpaceModel::find($item->space)->location)->name ." - ".\App\Models\SpaceModel::find($item->space)->name}}</td>
                            @else
                                <td>{{$item->note_plan}}</td>
                            @endif
                            <td>{{\App\Models\TaskTypeModel::find($item->type)->name}}</td>
                            <td>
                                @if($item->edit_type>0)
                                    {{\App\Models\TaskEditTypeModel::find($item->type)->name}}
                                @endif
                            </td>
                            <td>{{$item->start_date}}</td>
                            <td>{{\App\User::find($item->technical_supervisor)->name}}</td>
                            <td>{{$item->technical_supervisor_receive_date}}</td>
                            <td>

                                @if($item->collected==0)
                                    لا
                                @else
                                    نعم
                                @endif
                            </td>

                            <td>{{$item->collected_date}}</td>
                            @if(\Illuminate\Support\Facades\Auth::user()->type!="employee")

                                <td>{{$item->rate}}</td>
                            @endif
                            <td>
                                @if($item->have_inquiry==0)



                                @else

                                    يوجد

                                    <a href="{{url('admin/add_have_inquiry/'.$item->id)}}">
                                        <span class="label label-danger">تم الرد الاستفسار</span></a>


                                @endif
                            </td>

                        </tr>

                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>

    @endif



    @if(\Illuminate\Support\Facades\Auth::user()->type=="technical_supervisor")


        <!-- Basic table -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">مهام جاهزة للاستلام </h5>
                <div class="heading-elements">
                </div>
            </div>


            <div class="panel-body">
                <table class="table datatable-basic">
                    <thead>
                    <tr>
                        <th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>
                        <th>#</th>
                        <th>المشروع</th>
                        <th>الطلب</th>
                        <th>الموقع - اسم الفراغ</th>
                        <th>نوع المهمة</th>
                        <th>نوع التعديل</th>
                        <th>تاريخ التسليم</th>
                        <th>انتهى الموظف عند</th>
                        <th>الموظف</th>
                        {{--<th>المشرف</th>--}}
                        <th>تاريخ استلام المشرف</th>
                        <th>تقييم المشرف</th>
                        <th>استفسار</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($task_ready_to_recive as $key=>$item)
                        @if((strtotime($item->start_date) < strtotime(date('Y-m-d') ) ) && $item->finished==0)

                            <?php  $class = "alert-warning"?>
                        @else

                            <?php  $class = ""?>
                        @endif

                        @if($item->technical_supervisor_receive==1)
                            <tr style="background-color: #6bb479">
                        @else

                            <tr class="{{$class}}">
                                @endif

                                <td style="    text-align: center;">


                                    <ul class="icons-list">

                                        <li>
                                            <a href="javascript:;" data-popup="tooltip" item_id="{{$item->order}}"
                                               onclick="project_note(this)" data-toggle="modal"
                                               data-target="#order_notes"
                                               title="" data-original-title="ملاحظات المشروع"><i
                                                    class="icon  icon-quill4"></i></a>
                                        </li>


                                        {{--<li><a href="{{url('admin/files/'.$item->id."?type=task")}}" data-popup="tooltip"--}}
                                        {{--title="الملفات"--}}
                                        {{--><i class="icon icon-files-empty2"></i></a></li>--}}

                                        {{--<li><a href="{{url('admin/edit_task/'.$item->id)}}" data-popup="tooltip"--}}
                                        {{--title="التفاصيل"--}}
                                        {{--><i class="icon-eye4"></i></a></li>--}}


                                        <li><a href="javascript:;" data-popup="tooltip"
                                               item_id="{{$item->id}}"
                                               onclick="tasks_history(this)"
                                               data-toggle="modal" data-target="#tasks_history"
                                               title="مهام سابقة"
                                            ><i class="icon icon-history"></i></a></li>
                                        @if($item->technical_supervisor_receive==0 && $item->finished==1)
                                            {{--<li><a href="{{url('admin/technical_supervisor_receive_task/'.$item->id)}}"--}}
                                            {{--data-popup="tooltip"--}}
                                            {{--title="تم الاستلام"--}}
                                            {{--><i class="icon-checkmark4"></i></a></li>--}}

                                            <li><a href="#" data-toggle="modal" data-target="#rec_task"
                                                   data-popup="tooltip"
                                                   item_id="{{$item->id}}"
                                                   onclick="rec_task(this)"
                                                   title="تم الاستلام"
                                                ><i class="icon-checkmark4"></i></a></li>
                                        @endif
                                    </ul>
                                </td>
                                <td>{{$item->id}}</td>
                                <td>{{\App\Models\ProjectModel::find($item->project)->name_ar}}</td>
                                <td>{{\App\Models\OrderModel::find($item->order)->order_number}}</td>

                                @if($item->space>0)
                                    <td>


                                        {{\App\Models\SpaceLocationModel::find(\App\Models\SpaceModel::find($item->space)->location)->name." | ".\App\Models\SpaceModel::find($item->space)->name}}

                                    </td>
                                @else
                                    <td>{{$item->note_plan}}</td>
                                @endif

                                <td>{{\App\Models\TaskTypeModel::find($item->type)->name}}</td>
                                <td>
                                    @if($item->edit_type>0)
                                        {{\App\Models\TaskEditTypeModel::find($item->type)->name}}
                                    @endif
                                </td>
                                <td>{{$item->start_date}}</td>
                                <td>
                                    @if($item->end_date)

                                        {{date('Y-m-d H:i',strtotime($item->end_date))}}
                                    @endif
                                </td>
                                <td>{{\App\User::find($item->employee)->name}}</td>
                                {{--<td>{{\App\User::find($item->technical_supervisor)->name}}</td>--}}
                                <td>{{$item->technical_supervisor_receive_date}}</td>
                                <td>{{$item->rate}}</td>
                                <td>

                                    @if($item->have_inquiry==0)

                                    @else



                                        <span class="label label-danger">يوجد</span>

                                    @endif

                                </td>

                            </tr>

                            @endforeach
                    </tbody>
                </table>
            </div>

            <div class="table-responsive">

            </div>
        </div>
        <!-- /basic table -->



        <!-- Basic table -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">مهام تسليمها اليوم</h5>
                <div class="heading-elements">
                </div>
            </div>


            <div class="panel-body">
                <table class="table datatable-basic">
                    <thead>
                    <tr>
                        <th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>
                        <th>#</th>
                        <th>اضيف بواسطة</th>
                        <th>المشروع</th>
                        <th>الطلب</th>
                        <th>الفراغ</th>
                        <th>نوع المهمة</th>
                        <th>نوع التعديل</th>
                        <th>تاريخ التسليم</th>
                        <th>انتهاء الموظف</th>
                        <th>الموظف</th>
                        <th>المشرف</th>
                        <th>تاريخ استلام المشرف</th>
                        <th>تم التجميع</th>
                        <th>تاريخ التجميع</th>
                        @if(\Illuminate\Support\Facades\Auth::user()->type!="employee")

                            <th>تقييم المشرف</th>
                        @endif
                        <th>استفسار</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tasks_today as $key=>$item)
                        @if((strtotime($item->start_date) < strtotime(date('Y-m-d') ) ) && $item->finished==0)

                            <?php  $class = "alert-warning"?>
                        @else

                            <?php  $class = ""?>
                        @endif
                        <tr class="{{$class}}">

                            <td style="    text-align: center;">

                                <ul class="icons-list">

                                    <li>
                                        <a href="javascript:;" data-popup="tooltip" item_id="{{$item->order}}"
                                           onclick="project_note(this)" data-toggle="modal" data-target="#order_notes"
                                           title="" data-original-title="ملاحظات المشروع"><i
                                                class="icon  icon-quill4"></i></a>
                                    </li>


                                    <li><a href="javascript:;" data-popup="tooltip"
                                           item_id="{{$item->id}}"
                                           onclick="tasks_history(this)"
                                           data-toggle="modal" data-target="#tasks_history"
                                           title="مهام سابقة"
                                        ><i class="icon icon-history"></i></a></li>


                                    @if(\Illuminate\Support\Facades\Auth::user()->type=="admin" || \Illuminate\Support\Facades\Auth::user()->type=="project_manager" )





                                        {{--<li><a href="{{url('admin/files/'.$item->id."?type=task")}}"--}}
                                        {{--data-popup="tooltip"--}}
                                        {{--title="الملفات"--}}
                                        {{--><i class="icon icon-files-empty2"></i></a></li>--}}


                                        {{--<li>{{sizeof(\App\Models\TaskCommentModel::where('added_type', 'employee')->where('task', $item->id)->where('deleted', 0)->where('seen', 0)->get())}}--}}
                                        {{--<a href="#" data-toggle="modal" data-target="#comments_"--}}
                                        {{--data-popup="tooltip"--}}
                                        {{--item_id="{{$item->id}}"--}}
                                        {{--onclick="get_comments(this)"--}}
                                        {{--title="الاستفسارات"--}}
                                        {{--><i class="glyphicon glyphicon-comment"></i></a></li>--}}

                                        <li><a href="{{url('admin/edit_task/'.$item->id)}}" data-popup="tooltip"
                                               title="تعديل"
                                            ><i class="icon-pencil7"></i></a></li>

                                        <li><a href="javascript:;" data-popup="tooltip" title="حذف"
                                               onclick="remove_item(this)" item_id="{{$item->id}}"
                                            ><i class="icon-trash"></i></a></li>

                                        {{--@elseif(\Illuminate\Support\Facades\Auth::user()->type=="project_manager")--}}

                                        {{--<li><a href="{{url('admin/files/'.$item->id."?type=task")}}" data-popup="tooltip"--}}
                                        {{--title="الملفات"--}}
                                        {{--><i class="icon icon-files-empty2"></i></a></li>--}}


                                        {{--<li>{{sizeof(\App\Models\TaskCommentModel::where('added_type', 'employee')->where('task', $item->id)->where('deleted', 0)->where('seen', 0)->get())}}--}}
                                        {{--<a href="#" data-toggle="modal" data-target="#comments_"--}}
                                        {{--data-popup="tooltip"--}}
                                        {{--item_id="{{$item->id}}"--}}
                                        {{--onclick="get_comments(this)"--}}
                                        {{--title="الاستفسارات"--}}
                                        {{--><i class="glyphicon glyphicon-comment"></i></a></li>--}}

                                    @elseif(\Illuminate\Support\Facades\Auth::user()->type=="technical_supervisor")
                                        {{--<li><a href="{{url('admin/files/'.$item->id."?type=task")}}"--}}
                                        {{--data-popup="tooltip"--}}
                                        {{--title="الملفات"--}}
                                        {{--><i class="icon icon-files-empty2"></i></a></li>--}}

                                        <li><a href="{{url('admin/edit_task/'.$item->id)}}" data-popup="tooltip"
                                               title="التفاصيل"
                                            ><i class="icon-eye4"></i></a></li>

                                        @if($item->technical_supervisor_receive==0 && $item->finished==1)
                                            <li><a href="{{url('admin/technical_supervisor_receive_task/'.$item->id)}}"
                                                   data-popup="tooltip"
                                                   title="تم الاستلام"
                                                ><i class="icon-checkmark4"></i></a></li>
                                        @endif
                                    @endif


                                </ul>
                            </td>
                            <td>{{$item->id}}</td>
                            <td>{{\App\User::find($item->added_by)->name}}</td>
                            <td>{{\App\Models\ProjectModel::find($item->project)->name_ar}}</td>
                            <td>{{\App\Models\OrderModel::find($item->order)->order_number . " | ".\App\Models\OrderModel::find($item->order)->date_received}}</td>
                            @if($item->space>0)
                                <td>{{ \App\Models\SpaceLocationModel::find(\App\Models\SpaceModel::find($item->space)->location)->name ." - ".\App\Models\SpaceModel::find($item->space)->name}}</td>
                            @else
                                <td>{{$item->note_plan}}</td>
                            @endif
                            <td>{{\App\Models\TaskTypeModel::find($item->type)->name}}</td>
                            <td>
                                @if($item->edit_type>0)
                                    {{\App\Models\TaskEditTypeModel::find($item->type)->name}}
                                @endif
                            </td>
                            <td>{{$item->start_date}}</td>
                            <td>{{$item->end_date}}</td>
                            <td>
                                @if($item->employee>0)

                                    {{\App\User::find($item->employee)->name}}

                                @endif


                            </td>
                            <td>{{\App\User::find($item->technical_supervisor)->name}}</td>
                            <td>{{$item->technical_supervisor_receive_date}}</td>
                            <td>

                                @if($item->collected==0)
                                    لا
                                @else
                                    نعم
                                @endif
                            </td>

                            <td>{{$item->collected_date}}</td>
                            @if(\Illuminate\Support\Facades\Auth::user()->type!="employee")

                                <td>{{$item->rate}}</td>
                            @endif
                            <td>
                                @if($item->have_inquiry==0)
                                    لا يوجد


                                @else

                                    يوجد

                                    <a href="{{url('admin/add_have_inquiry/'.$item->id)}}">
                                        <span class="label label-danger">تم الرد الاستفسار</span></a>


                                @endif
                            </td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="table-responsive">

            </div>
        </div>
        <!-- /basic table -->



        <!-- Basic table -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">مهام تسليمها اليوم التالي</h5>
                <div class="heading-elements">
                </div>
            </div>


            <div class="panel-body">
                <table class="table datatable-basic">
                    <thead>
                    <tr>
                        <th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>
                        <th>#</th>
                        <th>اضيف بواسطة</th>
                        <th>المشروع</th>
                        <th>الطلب</th>
                        <th>الفراغ</th>
                        <th>نوع المهمة</th>
                        <th>نوع التعديل</th>
                        <th>تاريخ التسليم</th>
                        <th>انتهاء الموظف</th>
                        <th>الموظف</th>
                        <th>المشرف</th>
                        <th>تاريخ استلام المشرف</th>
                        <th>تم التجميع</th>
                        <th>تاريخ التجميع</th>
                        @if(\Illuminate\Support\Facades\Auth::user()->type!="employee")

                            <th>تقييم المشرف</th>
                        @endif
                        <th>استفسار</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($task_next_day as $key=>$item)
                        <tr>
                            <td style="    text-align: center;">

                                <ul class="icons-list">

                                    <li>
                                        <a href="javascript:;" data-popup="tooltip" item_id="{{$item->order}}"
                                           onclick="project_note(this)" data-toggle="modal" data-target="#order_notes"
                                           title="" data-original-title="ملاحظات المشروع"><i
                                                class="icon  icon-quill4"></i></a>
                                    </li>


                                    <li><a href="javascript:;" data-popup="tooltip"
                                           item_id="{{$item->id}}"
                                           onclick="tasks_history(this)"
                                           data-toggle="modal" data-target="#tasks_history"
                                           title="مهام سابقة"
                                        ><i class="icon icon-history"></i></a></li>

                                    @if(\Illuminate\Support\Facades\Auth::user()->type=="admin" || \Illuminate\Support\Facades\Auth::user()->type=="project_manager" )






                                        {{--<li><a href="{{url('admin/files/'.$item->id."?type=task")}}"--}}
                                        {{--data-popup="tooltip"--}}
                                        {{--title="الملفات"--}}
                                        {{--><i class="icon icon-files-empty2"></i></a></li>--}}


                                        {{--<li>{{sizeof(\App\Models\TaskCommentModel::where('added_type', 'employee')->where('task', $item->id)->where('deleted', 0)->where('seen', 0)->get())}}--}}
                                        {{--<a href="#" data-toggle="modal" data-target="#comments_"--}}
                                        {{--data-popup="tooltip"--}}
                                        {{--item_id="{{$item->id}}"--}}
                                        {{--onclick="get_comments(this)"--}}
                                        {{--title="الاستفسارات"--}}
                                        {{--><i class="glyphicon glyphicon-comment"></i></a></li>--}}

                                        <li><a href="{{url('admin/edit_task/'.$item->id)}}" data-popup="tooltip"
                                               title="تعديل"
                                            ><i class="icon-pencil7"></i></a></li>

                                        <li><a href="javascript:;" data-popup="tooltip" title="حذف"
                                               onclick="remove_item(this)" item_id="{{$item->id}}"
                                            ><i class="icon-trash"></i></a></li>

                                        {{--@elseif(\Illuminate\Support\Facades\Auth::user()->type=="project_manager")--}}

                                        {{--<li><a href="{{url('admin/files/'.$item->id."?type=task")}}" data-popup="tooltip"--}}
                                        {{--title="الملفات"--}}
                                        {{--><i class="icon icon-files-empty2"></i></a></li>--}}


                                        {{--<li>{{sizeof(\App\Models\TaskCommentModel::where('added_type', 'employee')->where('task', $item->id)->where('deleted', 0)->where('seen', 0)->get())}}--}}
                                        {{--<a href="#" data-toggle="modal" data-target="#comments_"--}}
                                        {{--data-popup="tooltip"--}}
                                        {{--item_id="{{$item->id}}"--}}
                                        {{--onclick="get_comments(this)"--}}
                                        {{--title="الاستفسارات"--}}
                                        {{--><i class="glyphicon glyphicon-comment"></i></a></li>--}}

                                    @elseif(\Illuminate\Support\Facades\Auth::user()->type=="technical_supervisor")
                                        {{--<li><a href="{{url('admin/files/'.$item->id."?type=task")}}"--}}
                                        {{--data-popup="tooltip"--}}
                                        {{--title="الملفات"--}}
                                        {{--><i class="icon icon-files-empty2"></i></a></li>--}}

                                        <li><a href="{{url('admin/edit_task/'.$item->id)}}" data-popup="tooltip"
                                               title="التفاصيل"
                                            ><i class="icon-eye4"></i></a></li>

                                        @if($item->technical_supervisor_receive==0 && $item->finished==1)
                                            <li><a href="{{url('admin/technical_supervisor_receive_task/'.$item->id)}}"
                                                   data-popup="tooltip"
                                                   title="تم الاستلام"
                                                ><i class="icon-checkmark4"></i></a></li>
                                        @endif
                                    @endif


                                </ul>
                            </td>
                            <td>{{$item->id}}</td>
                            <td>{{\App\User::find($item->added_by)->name}}</td>
                            <td>{{\App\Models\ProjectModel::find($item->project)->name_ar}}</td>
                            <td>{{\App\Models\OrderModel::find($item->order)->order_number . " | ".\App\Models\OrderModel::find($item->order)->date_received}}</td>
                            @if($item->space>0)
                                <td>{{ \App\Models\SpaceLocationModel::find(\App\Models\SpaceModel::find($item->space)->location)->name ." - ".\App\Models\SpaceModel::find($item->space)->name}}</td>
                            @else
                                <td>{{$item->note_plan}}</td>
                            @endif
                            <td>{{\App\Models\TaskTypeModel::find($item->type)->name}}</td>
                            <td>
                                @if($item->edit_type>0)
                                    {{\App\Models\TaskEditTypeModel::find($item->type)->name}}
                                @endif
                            </td>
                            <td>{{$item->start_date}}</td>
                            <td>{{$item->end_date}}</td>
                            <td>    @if($item->employee>0)

                                    {{\App\User::find($item->employee)->name}}

                                @endif

                            </td>
                            <td>{{\App\User::find($item->technical_supervisor)->name}}</td>
                            <td>{{$item->technical_supervisor_receive_date}}</td>
                            <td>

                                @if($item->collected==0)
                                    لا
                                @else
                                    نعم
                                @endif
                            </td>

                            <td>{{$item->collected_date}}</td>
                            @if(\Illuminate\Support\Facades\Auth::user()->type!="employee")

                                <td>{{$item->rate}}</td>
                            @endif
                            <td>
                                @if($item->have_inquiry==0)
                                    لا يوجد


                                @else

                                    يوجد

                                    <a href="{{url('admin/add_have_inquiry/'.$item->id)}}">
                                        <span class="label label-danger">تم الرد الاستفسار</span></a>


                                @endif
                            </td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="table-responsive">

            </div>
        </div>
        <!-- /basic table -->


    @endif


    <div id="comments_" class="modal fade ">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">الاستفسارات </h5>
                </div>
                <form action="{{url('admin/add_comment')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">

                        <input name="task_id" id="task_id" value="" type="hidden">
                        <div class="row">


                            <div class="col-lg-12">

                                <div class="form-group">
                                    <label>الرد </label>
                                    <input type="text" name="comment" class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>

                            <div class="col-lg-12">

                                <div class="form-group">
                                    <label>إلغاء وضع المهمة تحت حالة الرد على الاستفسار </label>
                                    <select class="select-search" name="have_inquiry" required="required">
                                        <option value="0">نعم</option>
                                        <option value="1">لا</option>

                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>صاحب الاستفسار</th>
                                            <th>الاستفسار</th>
                                        </tr>
                                        </thead>
                                        <tbody id="comments">

                                        </tbody>
                                    </table>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link"
                                data-dismiss="modal">{{\App\Http\Controllers\dashboard\ConstantController::$close[$_SESSION['lang']]}}</button>
                        <button type="submit"
                                class="btn btn-primary">{{\App\Http\Controllers\dashboard\ConstantController::$add[$_SESSION['lang']]}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    {{csrf_field()}}

    <script>


        function remove_item(elem) {

            swal({
                    title: "{{\App\Http\Controllers\dashboard\ConstantController::$are_you_sure[$_SESSION['lang']]}}",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#EF5350",
                    confirmButtonText: "{{\App\Http\Controllers\dashboard\ConstantController::$on[$_SESSION['lang']]}}",
                    cancelButtonText: "{{\App\Http\Controllers\dashboard\ConstantController::$off[$_SESSION['lang']]}}",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {

                    if (isConfirm) {
                        var item_id = $(elem).attr('item_id');
                        var url = "{{url('admin/remove_task')}}";
                        $('<form>', {
                            "action": url + "/" + item_id,
                        }).appendTo(document.body).submit();

                        {{--swal({--}}
                        {{--title: "{{\App\Http\Controllers\dashboard\ConstantController::$deleted[$_SESSION['lang']]}} ",--}}
                        {{--text: "{{\App\Http\Controllers\dashboard\ConstantController::$deleted_success[$_SESSION['lang']]}}",--}}
                        {{--confirmButtonColor: "#66BB6A",--}}
                        {{--type: "success"--}}
                        {{--});--}}
                    }
                    else {
                        swal({
                            title: "{{\App\Http\Controllers\dashboard\ConstantController::$close[$_SESSION['lang']]}}",
                            text: "",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });

        }

        var _token = $('input[name="_token"]').val();

        function get_comments(elem) {

            var elem_id = $(elem).attr('item_id');
            $('#task_id').val(elem_id);
            $.ajax({
                url: "{{url('admin/task_comments')}}", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: {
                    elem_id: elem_id,
                    _token: _token,
                }, // Data sent to server, a set of key/value pairs (i.e. form fields and values)

                success: function (data)   // A function to be called if request succeeds
                {

                    if (data.status == 200) {


                        var comments = data.comment;

                        $('#comments').html('');
                        if (comments.length > 0) {
                            $('#comments').html('');
                            var html = "";


                            for (var i = 0; i < comments.length; i++) {

                                html += '<tr><td>' + comments[i].created_at + '</td><td>' + comments[i].user + '</td><td>' + comments[i].comment + '</td></tr>';
                            }
                            $('#comments').append(html);
                        }

                    } else {

                    }


                }
            });

        }
    </script>

@endsection
