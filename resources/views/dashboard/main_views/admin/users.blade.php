@extends('dashboard.layout.index')
@section('content')

    <!-- Body classes -->
    <style>

        .image_review {
            width: 50%;
            margin-top: 5%;
        }
    </style>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title"> {{\App\Http\Controllers\dashboard\ConstantController::$content[$_SESSION['lang']]}} {{$view_name}}</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <button data-toggle="modal" data-target="#add" data-popup="tooltip" title="إضافة" type="button"
                            class="btn border-slate text-slate-800 btn-flat"><i class="icon-add "></i>
                    </button>

                </ul>

            </div>

        </div>

        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th>#</th>
                    <th>الاسم</th>
                    <th>البريد الالكتروني</th>
                    <th>الدور</th>
                    <th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $key=>$item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->email}}</td>
                        <td>{{\App\Http\Controllers\dashboard\ConstantController::role($item->type)}}</td>
                        <td style="    text-align: center;">


                            <ul class="icons-list">
                                <li><a href="javascript:;" data-popup="tooltip" title="تعديل"
                                       full_name="{{$item->name}}"
                                       email="{{$item->email}}"
                                       type="{{$item->type}}"
                                       item_id="{{$item->id}}"
                                       onclick="event_edit(this)"
                                       data-toggle="modal" data-target="#edit"
                                    ><i class="icon-pencil7"></i></a></li>
                                <li><a href="javascript:;" data-popup="tooltip" title="حذف"
                                       onclick="remove_item(this)" item_id="{{$item->id}}"
                                    ><i class="icon-trash"></i></a></li>
                            </ul>
                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>

    </div>
    <!-- /body classes -->





    <div id="add" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">إضافة </h5>
                </div>
                <form action="{{url('admin/add_user')}}" id="add_form" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">

                        <div class="row">


                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الدور </label>
                                    <select class="select-search" name="type" id="add_type" required="required">
                                        <option value="admin">أدمن</option>
                                        <option value="project_manager">مدير المشريع</option>
                                        <option value="technical_supervisor">المشرف الفني</option>
                                        <option value="user_collect">مسؤول التجميع</option>
                                    </select>
                                </div>

                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الاسم </label>
                                    <input type="text" name="name" class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>
                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>البريد الالكتروني</label>
                                    <input type="email" name="email" class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>كلمة المرور</label>
                                    <input type="password" name="password" class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>


                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link"
                                data-dismiss="modal">{{\App\Http\Controllers\dashboard\ConstantController::$close[$_SESSION['lang']]}}</button>
                        <button type="submit"
                                class="btn btn-primary">{{\App\Http\Controllers\dashboard\ConstantController::$add[$_SESSION['lang']]}}
                            <i  style="    float: right; " id="loader" class="icon-spinner spinner position-left"></i>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div id="edit" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">تعديل </h5>
                </div>
                <form action="{{url('admin/edit_user')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <input type="hidden" id="item_id" name="item_id" value="true">
                        <div class="row">

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الدور </label>
                                    <select id="type" class="select-search" name="type" required="required">
                                        <option value="admin">أدمن</option>
                                        <option value="project_manager">مدير المشريع</option>
                                        <option value="technical_supervisor">المشرف الفني</option>
                                        <option value="user_collect">مسؤول التجميع</option>
                                    </select>
                                </div>

                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الاسم </label>
                                    <input id="name" type="text" name="name" class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>
                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>البريد الالكتروني</label>
                                    <input id="email" type="email" name="email" class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>

                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>كلمة المرور</label>
                                    <input id="password" type="password" name="password" class="form-control"
                                           placeholder="">
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">إلغاء</button>
                        <button type="submit" class="btn btn-primary">حفظ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>


        function event_edit(elem) {

            $("#name").val($(elem).attr('full_name'));
            $("#email").val($(elem).attr('email'));
            $("#type").val($(elem).attr('type')).change();
            $("#item_id").val($(elem).attr('item_id'));

        }


        function remove_item(elem) {

            swal({
                    title: "{{\App\Http\Controllers\dashboard\ConstantController::$are_you_sure[$_SESSION['lang']]}}",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#EF5350",
                    confirmButtonText: "{{\App\Http\Controllers\dashboard\ConstantController::$on[$_SESSION['lang']]}}",
                    cancelButtonText: "{{\App\Http\Controllers\dashboard\ConstantController::$off[$_SESSION['lang']]}}",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {

                    if (isConfirm) {
                        var item_id = $(elem).attr('item_id');
                        var url = "{{url('admin/remove_user')}}";
                        $('<form>', {
                            "action": url + "/" + item_id,
                        }).appendTo(document.body).submit();

                        {{--swal({--}}
                            {{--title: "{{\App\Http\Controllers\dashboard\ConstantController::$deleted[$_SESSION['lang']]}} ",--}}
                            {{--text: "{{\App\Http\Controllers\dashboard\ConstantController::$deleted_success[$_SESSION['lang']]}}",--}}
                            {{--confirmButtonColor: "#66BB6A",--}}
                            {{--type: "success"--}}
                        {{--});--}}
                    }
                    else {
                        swal({
                            title: "{{\App\Http\Controllers\dashboard\ConstantController::$close[$_SESSION['lang']]}}",
                            text: "",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });

        }
        $('#loader').css('display','none');

        $("#add_form").submit(function (e) {

            e.preventDefault(); // avoid to execute the actual submit of the form.

            $('#loader').css('display','block');
            console.log("dfd");
            $.ajax({
                    url: "{{url('admin/add_user_ajax')}}", // Url to which the request is send
                    type: "POST",             // Type of request to be send, called as method
                    data: $(this).serialize(),
                    success: function (data)   // A function to be called if request succeeds
                    {


                       $('#loader').css('display','none');
                        if (data.status == 403 || data.status == 400  ) {

                            for (var i = 0; i < data.message.length; i++) {
                                new Noty({

                                    text: "" + data.message[i].message,
                                    type: 'error',
                                    dismissQueue: true,
                                    timeout: 4000,
                                    layout: "topRight",
                                }).show();
                            }
                        }
                        else if (data.status == 200) {


                            $('#add_form').trigger("reset");

                            $('#add_type').val();
                            $('.select-search').select2();


                            new Noty({

                                text: "" + data.message[0].message,
                                type: 'success',
                                dismissQueue: true,
                                timeout: 4000,
                                layout: "topRight",
                            }).show();

                        }
                    }
                }
            );

        });

    </script>

@endsection
