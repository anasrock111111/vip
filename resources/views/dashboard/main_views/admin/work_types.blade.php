@extends('dashboard.layout.index')
@section('content')

    <!-- Body classes -->
    <style>

        .image_review {
            width: 50%;
            margin-top: 5%;
        }
    </style>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title"> {{\App\Http\Controllers\dashboard\ConstantController::$content[$_SESSION['lang']]}} {{$view_name}}</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <button data-toggle="modal" data-target="#add" data-popup="tooltip" title="إضافة" type="button"
                            class="btn border-slate text-slate-800 btn-flat"><i class="icon-add "></i>
                    </button>

                </ul>

            </div>

        </div>

        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th>#</th>
                    <th>الاسم</th>
                    <th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($work_types as $key=>$item)
                    <tr>
                        <td>{{$item->id}}</td>
                        <td>{{$item->name}}</td>
                        <td style="    text-align: center;">


                            <ul class="icons-list">
                                <li><a href="javascript:;" data-popup="tooltip" title="تعديل"
                                       full_name="{{$item->name}}"
                                       item_id="{{$item->id}}"
                                       onclick="event_edit(this)"
                                       data-toggle="modal" data-target="#edit"
                                    ><i class="icon-pencil7"></i></a></li>
                                <li><a href="javascript:;" data-popup="tooltip" title="حذف"
                                       onclick="remove_item(this)" item_id="{{$item->id}}"
                                    ><i class="icon-trash"></i></a></li>
                            </ul>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
    <!-- /body classes -->





    <div id="add" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">إضافة </h5>
                </div>
                <form action="{{url('admin/add_work_type')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">

                        <div class="row">




                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الاسم </label>
                                    <input type="text" name="name" class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link"
                                data-dismiss="modal">{{\App\Http\Controllers\dashboard\ConstantController::$close[$_SESSION['lang']]}}</button>
                        <button type="submit"
                                class="btn btn-primary">{{\App\Http\Controllers\dashboard\ConstantController::$add[$_SESSION['lang']]}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div id="edit" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">تعديل </h5>
                </div>
                <form action="{{url('admin/edit_work_type')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <input type="hidden" id="item_id" name="item_id" value="true">
                        <div class="row">



                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>الاسم </label>
                                    <input id="name" type="text" name="name" class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">إلغاء</button>
                        <button type="submit" class="btn btn-primary">حفظ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>


        function event_edit(elem) {

            $("#name").val($(elem).attr('full_name'));
            $("#item_id").val($(elem).attr('item_id'));

        }


        function remove_item(elem) {

            swal({
                    title: "{{\App\Http\Controllers\dashboard\ConstantController::$are_you_sure[$_SESSION['lang']]}}",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#EF5350",
                    confirmButtonText: "{{\App\Http\Controllers\dashboard\ConstantController::$on[$_SESSION['lang']]}}",
                    cancelButtonText: "{{\App\Http\Controllers\dashboard\ConstantController::$off[$_SESSION['lang']]}}",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {

                    if (isConfirm) {
                        var item_id = $(elem).attr('item_id');
                        var url = "{{url('admin/remove_work_type')}}";
                        $('<form>', {
                            "action": url + "/" + item_id,
                        }).appendTo(document.body).submit();


                    }
                    else {
                        swal({
                            title: "{{\App\Http\Controllers\dashboard\ConstantController::$close[$_SESSION['lang']]}}",
                            text: "",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });

        }
    </script>

@endsection
