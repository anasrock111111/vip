@extends('dashboard.layout.index')
@section('content')


    <style>

        .icons-list {
            float: right;
            margin-left: 2%;
        }

        .li_cus {
            padding: 26%;
            background: #208d83;
            color: white;
            border-radius: 15%;
            margin: 0%;
        }
    </style>

    <!-- Basic table -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">مهام تسليمها اليوم</h5>
            <div class="heading-elements">
            </div>
        </div>


        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th>أحداث</th>
                    <th>#</th>
                    <th>اسم المشروع</th>
                    {{--<th>الفراغ</th>--}}
                    {{--<th>نوع الفراغ</th>--}}
                    {{--<th>الموقع</th>--}}
                    {{--<th>اسم الفراغ</th>--}}
                    <th style="">الفراغ</th>
                    <th>رقم الطلب</th>
                    <th>مسار الطلب</th>
                    <th>مسار المهمة</th>
                    <th>تاريخ الانشاء</th>
                    <th>تاريخ التسليم</th>
                    <th>نوع المهمة</th>
                    {{--<th>الردود</th>--}}
                    <th>استفسار</th>
                    {{--<th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>--}}
                </tr>
                </thead>
                <tbody>
                @foreach($tasks_today as $key=>$item)
                    @if((strtotime($item->start_date) < strtotime(date('Y-m-d') ) ) && $item->finished==0)

                        <?php  $class = "alert-warning"?>
                    @else

                        <?php  $class = ""?>
                    @endif
                    <tr class="{{$class}}">
                        <td>


                            <ul class="icons-list">
                                <li class="li_cus"><a href="javascript:;" data-popup="tooltip"
                                                      item_id="{{$item->id}}"
                                                      onclick="tasks_history(this)"
                                                      data-toggle="modal" data-target="#tasks_history"
                                                      title="مهام سابقة"
                                    ><i class="icon icon-history"></i></a></li>


                            </ul>
                            <div>
                                <ul class="icons-list">
                                    <li class="li_cus"><a href="javascript:;" data-popup="tooltip"
                                                          item_id="{{$item->order}}"
                                                          onclick="project_note(this)"
                                                          data-toggle="modal" data-target="#order_notes"
                                                          title="ملاحظات المشروع"
                                        ><i class="icon  icon-quill4"></i></a></li>

                                </ul>
                            </div>
                            @if($item->finished==0)
                                <a href="javascript:;"
                                   onclick="close_task(this)" item_id="{{$item->id}}"
                                   class="btn bg-teal-400 btn-sm btn-labeled btn-labeled-right heading-btn"> انهاء
                                    التاسك
                                    <b><i class="icon-alarm-check"></i></b></a>
                            @else

                                <a class="btn bg-teal-400 btn-sm btn-labeled btn-labeled-right heading-btn">
                                    تم انهاء التاسك عند {{$item->end_date}}
                                    <b><i class="icon-alarm-check"></i></b></a>
                            @endif


                            <a href="javascript:;" data-popup="tooltip"
                               item_id="{{$item->id}}"
                               task_direction="{{$item->task_direction}}"
                               onclick="event_edit(this)"
                               data-toggle="modal" data-target="#dire"
                            ><span class="label label-default">مسار المهمة</span></a>


                        </td>
                        <td>{{$item->id}}</td>
                        <td>{{\App\Models\ProjectModel::find($item->project)->name_ar}}</td>

                        @if($item->space>0)
                            <td style=" ">{{\App\Models\SpaceLocationModel::find(\App\Models\SpaceModel::find($item->space)->location)->name ." - ".\App\Models\SpaceModel::find($item->space)->name}}</td>
                        @else
                            <td>{{$item->note_plan}}</td>
                        @endif

                        <td>{{\App\Models\OrderModel::find($item->order)->order_number." / ".\App\Models\OrderModel::find($item->order)->date_received}}</td>
                        <td style="direction: ltr"><a target="_blank"
                                                      href="{{\App\Models\OrderModel::find($item->order)->order_direction}}">{{\App\Models\OrderModel::find($item->order)->order_direction}}</a>
                        </td>
                        <td style="direction: ltr"><a target="_blank"
                                                      href="{{$item->task_direction}}">{{$item->task_direction}}</a>
                        </td>
                        <td>{{date('Y-m-d',strtotime($item->created_at))}}</td>
                        <td>{{$item->start_date}}</td>
                        <td> {{\App\Models\TaskTypeModel::find($item->type)->name}}</td>
                        {{--<td>--}}

                        {{--@if($item->edit_type>0)--}}
                        {{--{{\App\Models\TaskEditTypeModel::find($item->edit_type)->name}}--}}
                        {{--@endif--}}
                        {{--</td>--}}
                        {{--<td>--}}


                        {{--<a href="{{url('employee/task_details/'.$item->id)}}">  {{sizeof(\App\Models\TaskCommentModel::where('added_type','<>', 'employee')->where('task', $item->id)->where('deleted', 0)->where('seen', 0)->get())}}--}}
                        {{--من الردرو </a>--}}
                        {{--</td>--}}
                        <td>

                            @if($item->have_inquiry==0)


                                <a href="{{url('employee/add_have_inquiry/'.$item->id)}}">
                                    <span class="label label-success">إضافة استفسار</span></a>

                            @else

                                يوجد

                                <a href="{{url('employee/add_have_inquiry/'.$item->id)}}">
                                    <span class="label label-danger">إلغاء الاستفسار</span></a>


                            @endif

                        </td>


                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>

        <div class="table-responsive">

        </div>
    </div>
    <!-- /basic table -->



    <!-- Basic table -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">مهام تسليمها اليوم التالي</h5>
            <div class="heading-elements">
            </div>
        </div>


        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th>أحداث</th>
                    <th>#</th>
                    <th>اسم المشروع</th>
                    <th>الفراغ</th>
                    <th>رقم الطلب</th>
                    <th>مسار الطلب</th>
                    <th>مسار المهمة</th>
                    <th>تاريخ الانشاء</th>
                    <th>تاريخ التسليم</th>
                    <th>نوع المهمة</th>
                    {{--<th>نوع التعديل</th>--}}
                    {{--<th>الردود</th>--}}
                    <th>استفسار</th>
                    {{--<th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>--}}
                </tr>
                </thead>
                <tbody>
                @foreach($task_next_day as $key=>$item)
                    @if((strtotime($item->start_date) < strtotime(date('Y-m-d') ) ) && $item->finished==0)

                        <?php  $class = "alert-warning"?>
                    @else

                        <?php  $class = ""?>
                    @endif
                    <tr class="{{$class}}">

                        <td>


                            <ul class="icons-list">
                                <li class="li_cus"><a href="javascript:;" data-popup="tooltip"
                                                      item_id="{{$item->id}}"
                                                      onclick="tasks_history(this)"
                                                      data-toggle="modal" data-target="#tasks_history"
                                                      title="مهام سابقة"
                                    ><i class="icon icon-history"></i></a></li>
                            </ul>
                            <div>
                                <ul class="icons-list">
                                    <li class="li_cus"><a href="javascript:;" data-popup="tooltip"
                                                          item_id="{{$item->order}}"
                                                          onclick="project_note(this)"
                                                          data-toggle="modal" data-target="#order_notes"
                                                          title="ملاحظات المشروع"
                                        ><i class="icon  icon-quill4"></i></a></li>

                                </ul>
                            </div>


                            @if($item->finished==0)
                                <a href="javascript:;"
                                   onclick="close_task(this)" item_id="{{$item->id}}"
                                   class="btn bg-teal-400 btn-sm btn-labeled btn-labeled-right heading-btn"> انهاء
                                    التاسك
                                    <b><i class="icon-alarm-check"></i></b></a>
                            @else

                                <a class="btn bg-teal-400 btn-sm btn-labeled btn-labeled-right heading-btn">
                                    تم انهاء التاسك عند {{$item->end_date}}
                                    <b><i class="icon-alarm-check"></i></b></a>
                            @endif
                            <a href="javascript:;" data-popup="tooltip"
                               item_id="{{$item->id}}"
                               task_direction="{{$item->task_direction}}"
                               onclick="event_edit(this)"
                               data-toggle="modal" data-target="#dire"
                            ><span class="label label-default">مسار المهمة</span></a>


                        </td>

                        <td>{{$item->id}}</td>
                        <td>{{\App\Models\ProjectModel::find($item->project)->name_ar}}</td>

                        @if($item->space>0)
                        <td style=" ">{{\App\Models\SpaceLocationModel::find(\App\Models\SpaceModel::find($item->space)->location)->name ." - ".\App\Models\SpaceModel::find($item->space)->name}}</td>
                      @else
                            <td>{{$item->note_plan}}</td>
                            @endif
                       <td>{{\App\Models\OrderModel::find($item->order)->order_number." / ".\App\Models\OrderModel::find($item->order)->date_received}}</td>
                        <td style="direction: ltr">{{\App\Models\OrderModel::find($item->order)->order_direction}}</td>
                        <td style="direction: ltr">
                            <a href="{{$item->task_direction}}">{{$item->task_direction}}</a>
                        </td>
                        <td>{{date('Y-m-d',strtotime($item->created_at))}}</td>
                        <td>{{$item->start_date}}</td>
                        <td> {{\App\Models\TaskTypeModel::find($item->type)->name}}</td>
                        {{--<td>--}}

                        {{--@if($item->edit_type>0)--}}
                        {{--{{\App\Models\TaskEditTypeModel::find($item->edit_type)->name}}--}}
                        {{--@endif--}}
                        {{--</td>--}}
                        {{--<td>--}}


                        {{--<a href="{{url('employee/task_details/'.$item->id)}}">  {{sizeof(\App\Models\TaskCommentModel::where('added_type','<>', 'employee')->where('task', $item->id)->where('deleted', 0)->where('seen', 0)->get())}}--}}
                        {{--من الردرو </a>--}}
                        {{--</td>--}}
                        <td>

                            @if($item->have_inquiry==0)


                                <a href="{{url('employee/add_have_inquiry/'.$item->id)}}">
                                    <span class="label label-success">إضافة استفسار</span></a>

                            @else

                                يوجد

                                <a href="{{url('employee/add_have_inquiry/'.$item->id)}}">
                                    <span class="label label-danger">إلغاء الاستفسار</span></a>


                            @endif

                        </td>


                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>

        <div class="table-responsive">

        </div>
    </div>
    <!-- /basic table -->



    <!-- Basic table -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">المهام المتبقية</h5>
            <div class="heading-elements">
            </div>
        </div>


        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th>أحداث</th>
                    <th>#</th>
                    <th>اسم المشروع</th>
                    <th>الفراغ</th>
                    <th>رقم الطلب</th>
                    <th>مسار الطلب</th>
                    <th>مسار المهمة</th>
                    <th>تاريخ الانشاء</th>
                    <th>تاريخ التسليم</th>
                    <th>نوع المهمة</th>
                    {{--<th>نوع التعديل</th>--}}
                    {{--<th>الردود</th>--}}

                    <th>استفسار</th>
                    {{--<th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>--}}
                </tr>
                </thead>
                <tbody>
                @foreach($other_task as $key=>$item)
                    @if((strtotime($item->start_date) < strtotime(date('Y-m-d') ) ) && $item->finished==0)

                        <?php  $class = "alert-warning"?>
                    @else

                        <?php  $class = ""?>
                    @endif
                    <tr class="{{$class}}">
                        <td>


                            <ul class="icons-list">
                                <li class="li_cus"><a href="javascript:;" data-popup="tooltip"
                                                      item_id="{{$item->id}}"
                                                      onclick="tasks_history(this)"
                                                      data-toggle="modal" data-target="#tasks_history"
                                                      title="مهام سابقة"
                                    ><i class="icon icon-history"></i></a></li>
                            </ul>

                            <div>
                                <ul class="icons-list">
                                    <li class="li_cus"><a href="javascript:;" data-popup="tooltip"
                                                          item_id="{{$item->order}}"
                                                          onclick="project_note(this)"
                                                          data-toggle="modal" data-target="#order_notes"
                                                          title="ملاحظات المشروع"
                                        ><i class="icon  icon-quill4"></i></a></li>

                                </ul>
                            </div>
                            @if($item->finished==0)
                                <a href="javascript:;"
                                   onclick="close_task(this)" item_id="{{$item->id}}"
                                   class="btn bg-teal-400 btn-sm btn-labeled btn-labeled-right heading-btn"> انهاء
                                    التاسك
                                    <b><i class="icon-alarm-check"></i></b></a>
                            @else

                                <a class="btn bg-teal-400 btn-sm btn-labeled btn-labeled-right heading-btn">
                                    تم انهاء التاسك عند {{$item->end_date}}
                                    <b><i class="icon-alarm-check"></i></b></a>
                            @endif

                            <a href="javascript:;" data-popup="tooltip"
                               item_id="{{$item->id}}"
                               task_direction="{{$item->task_direction}}"
                               onclick="event_edit(this)"
                               data-toggle="modal" data-target="#dire"
                            ><span class="label label-default">مسار المهمة</span></a>


                        </td>

                        <td>{{$item->id}}</td>
                        <td>{{\App\Models\ProjectModel::find($item->project)->name_ar}}</td>

                        @if($item->space>0)
                        <td style=" ">{{\App\Models\SpaceLocationModel::find(\App\Models\SpaceModel::find($item->space)->location)->name ." - ".\App\Models\SpaceModel::find($item->space)->name}}</td>
                        @else
                            <td>{{$item->note_plan}}</td>
                            @endif
                            <td>{{\App\Models\OrderModel::find($item->order)->order_number." / ".\App\Models\OrderModel::find($item->order)->date_received}}</td>
                        <td style="direction: ltr">{{\App\Models\OrderModel::find($item->order)->order_direction}}</td>
                        <td style="direction: ltr">{{$item->task_direction}}</td>

                        <td>{{date('Y-m-d',strtotime($item->created_at))}}</td>
                        <td>{{$item->start_date}}</td>
                        <td> {{\App\Models\TaskTypeModel::find($item->type)->name}}</td>
                        {{--<td>--}}

                        {{--@if($item->edit_type>0)--}}
                        {{--{{\App\Models\TaskEditTypeModel::find($item->edit_type)->name}}--}}
                        {{--@endif--}}
                        {{--</td>--}}
                        {{--<td>--}}


                        {{--<a href="{{url('employee/task_details/'.$item->id)}}">  {{sizeof(\App\Models\TaskCommentModel::where('added_type','<>', 'employee')->where('task', $item->id)->where('deleted', 0)->where('seen', 0)->get())}}--}}
                        {{--من الردرو </a>--}}
                        {{--</td>--}}
                        <td>

                            @if($item->have_inquiry==0)


                                <a href="{{url('employee/add_have_inquiry/'.$item->id)}}">
                                    <span class="label label-success">إضافة استفسار</span></a>

                            @else

                                يوجد

                                <a href="{{url('employee/add_have_inquiry/'.$item->id)}}">
                                    <span class="label label-danger">إلغاء الاستفسار</span></a>


                            @endif

                        </td>

                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>

        <div class="table-responsive">

        </div>
    </div>
    <!-- /basic table -->



    <div id="dire" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">تعديل</h5>
                </div>
                <form action="{{url('employee/set_direction')}}" method="post"
                      enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <input type="hidden" id="item_id" name="item_id" value="true">
                        <div class="row">

                            <div class="col-lg-12">

                                <div class="form-group">
                                    <label>مسار المهمة </label>
                                    <input id="task_direction" type="text" name="task_direction"
                                           class="form-control" placeholder=""
                                           required="required">
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">إلغاء</button>
                        <button type="submit" class="btn btn-primary">حفظ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>


        function event_edit(elem) {

            $("#item_id").val($(elem).attr('item_id'));
            $("#task_direction").val($(elem).attr('task_direction'));
        }


        function close_task(elem) {

            swal({
                    title: "{{\App\Http\Controllers\dashboard\ConstantController::$are_you_sure[$_SESSION['lang']]}}",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#EF5350",
                    confirmButtonText: "{{\App\Http\Controllers\dashboard\ConstantController::$on[$_SESSION['lang']]}}",
                    cancelButtonText: "{{\App\Http\Controllers\dashboard\ConstantController::$off[$_SESSION['lang']]}}",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {

                    if (isConfirm) {
                        var item_id = $(elem).attr('item_id');
                        var url = "{{url('employee/finish_task')}}";
                        $('<form>', {
                            "action": url + "/" + item_id,
                        }).appendTo(document.body).submit();


                    }
                    else {
                        swal({
                            title: "{{\App\Http\Controllers\dashboard\ConstantController::$close[$_SESSION['lang']]}}",
                            text: "",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });
        }
    </script>
@endsection
