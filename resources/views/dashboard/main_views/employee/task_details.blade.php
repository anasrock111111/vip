@extends('dashboard.layout.index')
@section('content')

    <div class="row">
        <div class="col-lg-9">

            <!-- Task overview -->
            <div class="panel panel-flat">
                <div class="panel-heading mt-5">
                    <h5 class="panel-title"># {{$item->id}}
                        | {{\App\Models\ProjectModel::find($item->project)->name_ar}}</h5>
                    <div class="heading-elements">


                        @if($item->finished==0)
                            <a href="javascript:;"
                               onclick="close_task(this)" item_id="{{$item->id}}"
                               class="btn bg-teal-400 btn-sm btn-labeled btn-labeled-right heading-btn"> انهاء التاسك
                                <b><i class="icon-alarm-check"></i></b></a>
                        @else

                            <a class="btn bg-teal-400 btn-sm btn-labeled btn-labeled-right heading-btn">
                                تم انهاء التاسك عند {{$item->end_date}}
                                <b><i class="icon-alarm-check"></i></b></a>
                        @endif

                    </div>
                </div>

                <div class="panel-body">

                    <div class="row container-fluid">
                        <div class="col-sm-6">
                            <div class="content-group">
                                <dl>
                                    <dt class="text-size-small text-primary text-uppercase"> الفراغ</dt>
                                    <dd>{{\App\Models\SpaceModel::find($item->space)->name}}</dd>

                                    <dt class="text-size-small text-primary text-uppercase">الموقع</dt>
                                    <dd>{{\App\Models\SpaceLocationModel::find(\App\Models\SpaceModel::find($item->space)->location)->name}}</dd>

                                    <dt class="text-size-small text-primary text-uppercase"> نوع المهمة</dt>
                                    <dd><a href="#" class="label label-success dropdown-toggle"
                                        > {{\App\Models\TaskTypeModel::find($item->type)->name}}
                                        </a></dd>
                                </dl>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="content-group">
                                <dl>
                                    <dt class="text-size-small text-primary text-uppercase">نوع الفراغ</dt>
                                    <dd> {{\App\Models\SpaceTypeModel::find(\App\Models\SpaceModel::find($item->space)->type)->name}}</dd>

                                    <dt class="text-size-small text-primary text-uppercase"> مسار الطلب</dt>
                                    <dd>{{\App\Models\OrderModel::find($item->order)->order_direction}}</dd>
                                    @if($item->edit_type>0)
                                        <dt class="text-size-small text-primary text-uppercase"> نوع التعديل
                                        </dt>
                                        <dd>{{\App\Models\TaskEditTypeModel::find($item->edit_type)->name}}
                                        </dd>
                                    @endif
                                </dl>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="panel-footer">
                    <div class="heading-elements">
                        {{--<ul class="list-inline list-inline-condensed heading-text">--}}
                        {{--<li><span class="status-mark border-blue position-left"></span> الحالة :</li>--}}
                        {{--<li class="dropdown">--}}
                        {{--<a href="#" class="text-default text-semibold dropdown-toggle" data-toggle="dropdown">Open--}}
                        {{--<span class="caret"></span></a>--}}
                        {{--<ul class="dropdown-menu">--}}
                        {{--<li class="active"><a href="#">Open</a></li>--}}
                        {{--<li><a href="#">On hold</a></li>--}}
                        {{--<li><a href="#">Resolved</a></li>--}}
                        {{--<li><a href="#">Closed</a></li>--}}
                        {{--<li class="divider"></li>--}}
                        {{--<li><a href="#">Dublicate</a></li>--}}
                        {{--<li><a href="#">Invalid</a></li>--}}
                        {{--<li><a href="#">Wontfix</a></li>--}}
                        {{--</ul>--}}
                        {{--</li>--}}
                        {{--</ul>--}}
                        <form action="{{url('employee/set_direction')}}" method="post">

                            <input type="hidden" value="{{$item->id}}" name="item_id">
                            {{csrf_field()}}
                            <div class="row" style="padding: 1%;">
                                <div class="col-lg-8">

                                    <div class="form-group">
                                        <label>مسار المهمة</label>
                                        <input type="text" name="task_direction" value="{{$item->task_direction}}" class="form-control" placeholder=""
                                        >
                                    </div>
                                </div>
                                <div class="col-lg-4">

                                    <button type="submit" style="margin-top: 7%;"
                                            class="btn btn-primary">تثبيت
                                    </button>
                                </div>
                            </div>
                        </form>
                        <ul class="list-inline list-inline-condensed heading-text pull-right">

                        </ul>
                    </div>
                </div>
            </div>
            <!-- /task overview -->


            <!-- Comments -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title text-semiold"><i class="icon-bubbles4 position-left"></i> الاستفسارات</h5>
                    <div class="heading-elements">

                    </div>
                </div>

                <div class="panel-body">
                    <ul class="media-list content-group-lg stack-media-on-mobile">

                        @foreach(\App\Models\TaskCommentModel::where('task',$item->id)->get()  as $item_comment)
                            <li class="media" style="    padding: 1%;
    border: 1px solid #e9e9e9;
    border-radius: 8px;
    box-shadow: 0px 0px 10px #e9e9e9;">
                                <div class="media-left">
                                    <a href="#"><img
                                            src="{{url('assets/dashboard/images/placeholders/placeholder.jpg')}}"
                                            class="img-circle img-sm" alt=""></a>
                                </div>

                                <div class="media-body">
                                    <div class="media-heading">
                                        <a href="#"
                                           class="text-semibold">{{\App\User::find($item_comment->user)->name}}</a>
                                        <span class="media-annotation dotted">{{ $item_comment->created_at }}</span>
                                    </div>

                                    <p>{{ $item_comment->comment }}</p>


                                </div>
                            </li>
                        @endforeach

                    </ul>

                    <h6 class="text-semibold"><i class="icon-pencil7 position-left"></i> اكتب استفسارك هنا </h6>
                    <form action="{{url('employee/add_comment')}}" method="post">
                        <input type="hidden" name="task_id" value="{{$item->id}}">
                        {{csrf_field()}}
                        <div class="content-group">

                            <textarea name="comment" rows="6" class="form-control"></textarea>
                        </div>

                        <div class="text-right">
                            <button type="submit" class="btn bg-blue"><i class="icon-plus22"></i> إضافة استفسار</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /comments -->

        </div>

        <div class="col-lg-3">

            <!-- Attached files -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title"><i class="icon-link position-left"></i> الملفات الملحقة </h6>
                    <div class="heading-elements">
                        <ul class="icons-list">

                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <ul class="media-list">

                        @foreach(\App\Models\FileModel::where('type','task')->where('item_id',$item->id)->get() as $file)
                            <li class="media">
                                <div class="media-left media-middle">
                                    <i class="icon-file-stats icon-2x text-muted"></i>
                                </div>

                                <div class="media-body">
                                    <a download href="{{url('uploads/files/task/'.$file->file)}}"
                                       class="media-heading text-semibold">{{$file->file}} {{$file->note}}</a>
                                    <ul class="list-inline list-inline-separate list-inline-condensed text-size-mini text-muted">

                                        <li>أضيف بواسطة: <a href="#"> {{\App\User::find($file->added_by)->name}}</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="media-right media-middle">
                                    <ul class="icons-list">
                                        <li><a download href="{{url('uploads/files/task/'.$file->file)}}"><i
                                                    class="icon-download"></i></a></li>
                                    </ul>
                                </div>
                            </li>


                        @endforeach

                    </ul>
                </div>
            </div>
            <!-- /attached files -->

        </div>
    </div>
    <!-- /detailed task -->


    <script>

        function close_task(elem) {

            swal({
                    title: "{{\App\Http\Controllers\dashboard\ConstantController::$are_you_sure[$_SESSION['lang']]}}",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#EF5350",
                    confirmButtonText: "{{\App\Http\Controllers\dashboard\ConstantController::$on[$_SESSION['lang']]}}",
                    cancelButtonText: "{{\App\Http\Controllers\dashboard\ConstantController::$off[$_SESSION['lang']]}}",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {

                    if (isConfirm) {
                        var item_id = $(elem).attr('item_id');
                        var url = "{{url('employee/finish_task')}}";
                        $('<form>', {
                            "action": url + "/" + item_id,
                        }).appendTo(document.body).submit();


                    }
                    else {
                        swal({
                            title: "{{\App\Http\Controllers\dashboard\ConstantController::$close[$_SESSION['lang']]}}",
                            text: "",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });
        }
    </script>
@endsection
