@extends('dashboard.layout.index')
@section('content')

    <!-- Basic table -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">طلبات المشريع قيد العمل</h5>
            <div class="heading-elements">
            </div>
        </div>


        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>
                    <th>#</th>
                    <th>اسم المشروع</th>
                    <th>ترقيم الطلب</th>
                    <th>الزبون</th>
                    <th>تاريخ التسليم</th>
                    <th>الأهمية</th>
                    <th>المهام المنجزة</th>
                    <th>المهام المستلمة من المشرف الفني</th>
                    <th>المهام المجمَعة</th>
                    <th>استفسار</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $key=>$item)
                    <tr>
                        <td style="    text-align: center;">

                            <ul class="icons-list">

                                <li>
                                    <a href="javascript:;" data-popup="tooltip" item_id="{{$item->id}}"
                                       onclick="project_note(this)" data-toggle="modal" data-target="#order_notes"
                                       title="" data-original-title="ملاحظات المشروع"><i class="icon  icon-quill4"></i></a>
                                </li>


                                <li><a href="{{url('user_collect/tasks/'.$item->id."")}}" data-popup="tooltip"
                                       title="المهام"
                                    ><i class="icon icon-design"></i></a></li>
                            </ul>
                        </td>
                        <td>{{$item->id}}</td>
                        <td>{{\App\Models\ProjectModel::find($item->project)->name_ar}}</td>
                        <td>{{$item->order_number . " | ".$item->date_received}}</td>
                        <td>{{\App\User::find(\App\Models\ProjectModel::find($item->project)->user)->code}}</td>
                        <td>{{$item->delivery_date}}</td>
                        <td>{{\App\Http\Controllers\dashboard\ConstantController::importance($item->Importance)}}</td>
                        <td>{{$item->tasks_ended ."  \ ". $item->all_tasks }}</td>
                        <td>{{$item->task_received_from_technical ."  \ ". $item->all_tasks }}</td>
                        <td>{{$item->tasks_collected ."  \ ". $item->all_tasks }}</td>
                        <td>

                            @if(\App\Models\OrderModel::check_if_have_inquiry($item->id))


                                <a href="javascript:;">
                                    <span class="label label-danger">يوجد</span></a>

                            @else

                                <a href="javascript:;">
                                    <span class="label label-default ">لا يوجد</span></a>
                            @endif
                        </td>

                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>

        <div class="table-responsive">

        </div>
    </div>
    <!-- /basic table -->


    <!-- Basic table -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">طلبات تسليمها اليوم </h5>
            <div class="heading-elements">
            </div>
        </div>


        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th>أحداث</th>
                    <th>#</th>
                    <th>اسم المشروع</th>
                    <th>ترقيم الطلب</th>
                    <th>الزبون</th>
                    <th>كليشة التصميم</th>
                    <th>تاريخ التسليم</th>
                    <th>الأهمية</th>
                    <th>المهام المنجزة</th>
                    <th>المهام المستلمة من المشرف الفني</th>
                    <th>المهام المجمَعة</th>
                    {{--<th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>--}}
                </tr>
                </thead>
                <tbody>
                @foreach($order_delivery_today as $key=>$item)

                    @if((strtotime($item->delivery_date) < strtotime(date('Y-m-d') ) ) && $item->delivered==0)

                        <?php  $class = "alert-warning"?>
                    @else

                        <?php  $class = ""?>
                    @endif
                    <tr class="{{$class}}">
                        <td>

                            <ul class="icons-list">
                                <li>
                                    <a href="javascript:;" data-popup="tooltip" item_id="{{$item->id}}"
                                       onclick="project_note(this)" data-toggle="modal" data-target="#order_notes"
                                       title="" data-original-title="ملاحظات المشروع"><i class="icon  icon-quill4"></i></a>
                                </li>

                                <li><a href="{{url('user_collect/tasks/'.$item->id."")}}" data-popup="tooltip"
                                       title="المهام"
                                    ><i class="icon icon-design"></i></a></li>
                            </ul>
                        </td>
                        <td>{{$item->id}}</td>
                        <td>{{\App\Models\ProjectModel::find($item->project)->name_ar}}</td>
                        <td>{{$item->order_number. " | ".$item->date_received}}</td>
                        <?php $user = \App\User::find(\App\Models\ProjectModel::find($item->project)->user);?>
                        <td>{{$user->code}}</td>
                        <td>{{\App\Models\CliceModel::find($user->clice_design)->name}}</td>

                        <td>{{$item->delivery_date}}</td>
                        <td>
                            {{\App\Http\Controllers\dashboard\ConstantController::importance($item->importance)}}
                        </td>

                        @if(isset($item->tasks_ended ) && isset($item->all_tasks) && isset($item->task_received_from_technical))

                            <td>{{$item->tasks_ended ."  \ ". $item->all_tasks }}</td>
                            <td>{{$item->task_received_from_technical ."  \ ". $item->all_tasks }}</td>
                            <td>{{$item->tasks_collected ."  \ ". $item->all_tasks }}</td>
                        @else

                            <td>{{0 ."  \ ". 0 }}</td>
                            <td>{{0 ."  \ ". 0 }}</td>
                            <td>{{0 ."  \ ". 0 }}</td>
                        @endif


                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>

        <div class="table-responsive">

        </div>
    </div>
    <!-- /basic table -->

    <!-- Basic table -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">طلبات تسليمها اليوم التالي </h5>
            <div class="heading-elements">
            </div>
        </div>


        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th>#</th>
                    <th>اسم المشروع</th>
                    <th>ترقيم الطلب</th>
                    <th>الزبون</th>
                    <th>كليشة التصميم</th>
                    <th>تاريخ التسليم</th>
                    <th>الأهمية</th>
                    <th>المهام المنجزة</th>
                    <th>المهام المستلمة من المشرف الفني</th>
                    <th>المهام المجمَعة</th>
                    {{--<th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>--}}
                </tr>
                </thead>
                <tbody>
                @foreach($orders_next_day as $key=>$item)
                    <tr>
                        <td>{{$item->id}}</td>
                        <td>{{\App\Models\ProjectModel::find($item->project)->name_ar}}</td>
                        <td>{{$item->order_number  . " | ".$item->date_received}}</td>
                        <?php $user = \App\User::find(\App\Models\ProjectModel::find($item->project)->user);?>
                        <td>{{$user->code}}</td>
                        <td>{{\App\Models\CliceModel::find($user->clice_design)->name}}</td>
                        <td>{{$item->delivery_date}}</td>
                        <td>{{\App\Http\Controllers\dashboard\ConstantController::importance($item->Importance)}}</td>

                        <td>{{$item->tasks_ended ."  \ ". $item->all_tasks }}</td>
                        <td>{{$item->task_received_from_technical ."  \ ". $item->all_tasks }}</td>
                        <td>{{$item->tasks_collected ."  \ ". $item->all_tasks }}</td>
                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>

        <div class="table-responsive">

        </div>
    </div>
    <!-- /basic table -->



    {{--<div class="panel panel-flat">--}}
        {{--<div class="panel-heading">--}}
            {{--<h5 class="panel-title">طلبات أٌخرى </h5>--}}
            {{--<div class="heading-elements">--}}
            {{--</div>--}}
        {{--</div>--}}


        {{--<div class="panel-body">--}}
            {{--<table class="table datatable-basic">--}}
                {{--<thead>--}}
                {{--<tr>--}}
                    {{--<th>#</th>--}}
                    {{--<th>اسم المشروع</th>--}}
                    {{--<th>ترقيم الطلب</th>--}}
                    {{--<th>الزبون</th>--}}
                    {{--<th>كليشة التصميم</th>--}}
                    {{--<th>تاريخ التسليم</th>--}}
                    {{--<th>الأهمية</th>--}}
                    {{--<th>المهام المنجزة</th>--}}
                    {{--<th>المهام المستلمة من المشرف الفني</th>--}}
                    {{--<th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>--}}
                {{--</tr>--}}
                {{--</thead>--}}
                {{--<tbody>--}}
                {{--@foreach($other as $key=>$item)--}}
                    {{--<tr>--}}
                        {{--<td>{{$item->id}}</td>--}}
                        {{--<td>{{\App\Models\ProjectModel::find($item->project)->name_ar}}</td>--}}
                        {{--<td>{{$item->order_number ." | ". $item->date_received}}</td>--}}
                        {{--<?php $user = \App\User::find(\App\Models\ProjectModel::find($item->project)->user);?>--}}
                        {{--<td>{{$user->code}}</td>--}}
                        {{--<td>{{\App\Models\CliceModel::find($user->clice_design)->name}}</td>--}}
                        {{--<td>{{$item->delivery_date}}</td>--}}
                        {{--<td>{{\App\Http\Controllers\dashboard\ConstantController::importance($item->Importance)}}</td>--}}

                        {{--<td>{{$item->tasks_ended ."  \ ". $item->all_tasks }}</td>--}}
                        {{--<td>{{$item->task_received_from_technical ."  \ ". $item->all_tasks }}</td>--}}

                    {{--</tr>--}}

                {{--@endforeach--}}
                {{--</tbody>--}}
            {{--</table>--}}
        {{--</div>--}}

        {{--<div class="table-responsive">--}}

        {{--</div>--}}
    {{--</div>--}}


    <script>

        function rec_task(elem) {

            $('#task_id').val($(elem).attr('item_id'));
        }
    </script>
@endsection
