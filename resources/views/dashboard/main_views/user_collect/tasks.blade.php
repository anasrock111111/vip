@extends('dashboard.layout.index')
@section('content')

    <!-- Body classes -->
    <style>

        .image_review {
            width: 50%;
            margin-top: 5%;
        }
    </style>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title"> {{\App\Http\Controllers\dashboard\ConstantController::$content[$_SESSION['lang']]}} {{$view_name}}</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    @if(\Illuminate\Support\Facades\Auth::user()->type=="admin" || \Illuminate\Support\Facades\Auth::user()->type=="project_manager")

                        <a href="{{url('admin/add_task')}}">
                            <button data-popup="tooltip" title="إضافة" type="button"
                                    class="btn border-slate text-slate-800 btn-flat"><i class="icon-add "></i>
                            </button>
                        </a>
                    @endif
                </ul>

            </div>

        </div>

        <div class="panel-body">
            <table class="table datatable-basic">
                <thead>
                <tr>
                    <th class="text-center">{{\App\Http\Controllers\dashboard\ConstantController::$actions[$_SESSION['lang']]}}</th>
                    <th>#</th>
                    <th>المشروع</th>
                    <th>الطلب</th>
                    <th>الفراغ</th>
                    <th>نوع المهمة</th>
                    <th>نوع التعديل</th>
                    <th>تاريخ التسليم</th>
                    <th>انتهاء الموظف</th>
                    <th>الموظف</th>
                    <th>تاريخ استلام المشرف</th>
                    <th>تم التجميع</th>
                    <th>تاريخ التجميع</th>
                    <th>المسار</th>
                </tr>
                </thead>
                <tbody>
                @foreach($tasks as $key=>$item)

                    @if($item->collected==1)
                        <tr style="background-color: #49c973">
                    @else
                        <tr>
                            @endif

                            <td style="    text-align: center;">

                                <ul class="icons-list">
                                    <li>
                                        <a href="javascript:;" data-popup="tooltip" item_id="{{$item->order}}"
                                           onclick="project_note(this)" data-toggle="modal" data-target="#order_notes"
                                           title="" data-original-title="ملاحظات المشروع"><i class="icon  icon-quill4"></i></a>
                                    </li>


                                    <li><a href="javascript:;" data-popup="tooltip"
                                           item_id="{{$item->id}}"
                                           onclick="tasks_history(this)"
                                           data-toggle="modal" data-target="#tasks_history"
                                           title="مهام سابقة"
                                        ><i class="icon icon-history"></i></a></li>

                                    @if($item->collected==0 && $item->technical_supervisor_receive==1  && $item->finished==1)

                                        <li><a href="javascript:;" data-popup="tooltip" title="تم التجميع"
                                               collect_direction="{{$item->collect_direction}}"
                                               item_id="{{$item->id}}"
                                               onclick="event_edit(this)"
                                               data-toggle="modal" data-target="#edit"
                                            ><i class="icon-pencil7"></i></a></li>
                                    @else
                                        {{--<li><a href="javascript:;" data-popup="tooltip" title="مسار التجميع"--}}
                                        {{--collect_direction="{{$item->collect_direction}}"--}}
                                        {{--item_id="{{$item->id}}"--}}
                                        {{--onclick="event_edit(this)"--}}
                                        {{--data-toggle="modal" data-target="#edit"--}}
                                        {{--><i class="icon-pencil7"></i></a></li>--}}
                                    @endif



                                    @if($item->collected==1)

                                        <li><a href="javascript:;" data-popup="tooltip" title="إلفاء التجميع"
                                               item_id="{{$item->id}}"
                                               onclick="event_edit_back(this)"
                                               data-toggle="modal" data-target="#edit_back"
                                            ><i class="icon-pencil7"></i></a></li>
                                    @else
                                        {{--<li><a href="javascript:;" data-popup="tooltip" title="مسار التجميع"--}}
                                        {{--collect_direction="{{$item->collect_direction}}"--}}
                                        {{--item_id="{{$item->id}}"--}}
                                        {{--onclick="event_edit(this)"--}}
                                        {{--data-toggle="modal" data-target="#edit"--}}
                                        {{--><i class="icon-pencil7"></i></a></li>--}}
                                    @endif


                                </ul>
                            </td>
                            <td>{{$item->id}}</td>
                            <td>{{\App\Models\ProjectModel::find($item->project)->name_ar}}</td>
                            <td>{{\App\Models\OrderModel::find($item->order)->order_number}}</td>

                            @if($item->space>0)
                            <td>{{\App\Models\SpaceLocationModel::find(\App\Models\SpaceModel::find($item->space)->location)->name." - ".\App\Models\SpaceModel::find($item->space)->name}}</td>
                           @else
                                <td> {{$item->note_plan}}</td>
                                @endif
                            <td>{{\App\Models\TaskTypeModel::find($item->type)->name}}</td>
                            <td>
                                @if($item->edit_type>0)
                                    {{\App\Models\TaskEditTypeModel::find($item->type)->name}}
                                @endif
                            </td>
                            <td>{{$item->start_date}}</td>
                            <td>{{$item->end_date}}</td>
                            <td>

                                @if($item->employee)
                                    {{\App\User::find($item->employee)->name}}
                                @endif
                            </td>
                            <td>{{$item->technical_supervisor_receive_date}}</td>
                            <td>

                                @if($item->collected==0)
                                    لا
                                @else
                                    نعم
                                @endif
                            </td>

                            <td>{{$item->collected_date}}</td>
                            <td style="direction: ltr"><a target="_blank"
                                                          href="{{$item->task_direction}}">{{$item->task_direction}} </a>
                            </td>

                        </tr>

                        @endforeach
                </tbody>
            </table>
        </div>

    </div>
    <!-- /body classes -->



    <div id="edit" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">تأكيد التجميع </h5>
                </div>
                <form action="{{url('user_collect/edit_task_to_collected')}}" method="post"
                      enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <input type="hidden" id="item_id" name="item_id" value="true">
                        <div class="row">

                            <p> هل تريد التجميع لهذه المهمة بالفعل </p>

                            {{--<div class="col-lg-12">--}}

                            {{--<div class="form-group">--}}
                            {{--<label>مسار التجميع </label>--}}
                            {{--<input id="collect_direction" type="text" name="collect_direction"--}}
                            {{--class="form-control" placeholder=""--}}
                            {{--required="required">--}}
                            {{--</div>--}}
                            {{--</div>--}}

                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">إلغاء</button>
                        <button type="submit" class="btn btn-primary">حفظ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <div id="edit_back" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">تأكيد إلغاء التجميع </h5>
                </div>
                <form action="{{url('user_collect/edit_task_to_collected_back')}}" method="post"
                      enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <input type="hidden" id="item_id_back" name="item_id" value="true">
                        <div class="row">

                            <p> هل تريد إلغاء التجميع لهذه المهمة بالفعل </p>


                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">إلغاء</button>
                        <button type="submit" class="btn btn-primary">حفظ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>


        function event_edit(elem) {

            $("#item_id").val($(elem).attr('item_id'));
            $("#collect_direction").val($(elem).attr('collect_direction'));

        }


        function event_edit_back(elem) {

            $("#item_id_back").val($(elem).attr('item_id'));

        }


    </script>

@endsection
