<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::group(['middleware' => ['language']], function () {


    Route::get('/login', ['as' => 'login', 'uses' => 'dashboard\AuthController@show']);
    Route::post('/login', 'dashboard\AuthController@do_login');
    Route::get('/logout', ['as' => 'logout', 'uses' => 'dashboard\AuthController@do_logout']);

});


Route::prefix('admin')->group(function () {

    Route::group(['middleware' => ['admin']], function () {


        Route::get('import', 'dashboard\admin\HomeController@importUser');
        Route::get('constant', 'dashboard\admin\ConstantController@index');
        Route::post('constant', 'dashboard\admin\ConstantController@save');


        Route::get('home', 'dashboard\admin\HomeController@index');


        Route::get('users', 'dashboard\admin\UserController@index');
        Route::post('add_user', 'dashboard\admin\UserController@save');
        Route::post('add_user_ajax', 'dashboard\admin\UserController@save_ajax');
        Route::post('edit_user', 'dashboard\admin\UserController@edit');
        Route::get('remove_user/{id}', 'dashboard\admin\UserController@remove');


        Route::get('users_get_works', 'dashboard\admin\UserGetWorkController@index');
        Route::post('add_user_work', 'dashboard\admin\UserGetWorkController@save');
        Route::post('edit_user_work', 'dashboard\admin\UserGetWorkController@edit');
        Route::get('remove_user_work/{id}', 'dashboard\admin\UserGetWorkController@remove');


        Route::get('employees', 'dashboard\admin\EmployeeController@index');
        Route::post('employee_salary', 'dashboard\admin\EmployeeController@employee_salary');
        Route::post('add_employee', 'dashboard\admin\EmployeeController@save');
        Route::post('add_employee_ajax', 'dashboard\admin\EmployeeController@save_ajax');
        Route::post('edit_employee', 'dashboard\admin\EmployeeController@edit');
        Route::get('remove_employee/{id}', 'dashboard\admin\EmployeeController@remove');

        Route::get('departments', 'dashboard\admin\DepartmentController@index');
        Route::post('add_department', 'dashboard\admin\DepartmentController@save');
        Route::post('edit_department', 'dashboard\admin\DepartmentController@edit');
        Route::get('remove_department/{id}', 'dashboard\admin\DepartmentController@remove');

        Route::get('work_types', 'dashboard\admin\WorkTypeController@index');
        Route::post('add_work_type', 'dashboard\admin\WorkTypeController@save');
        Route::post('edit_work_type', 'dashboard\admin\WorkTypeController@edit');
        Route::get('remove_work_type/{id}', 'dashboard\admin\WorkTypeController@remove');

        Route::get('clices', 'dashboard\admin\ClicController@index');
        Route::get('defautl_clices/{id}', 'dashboard\admin\ClicController@defautl_clices');
        Route::post('add_clice', 'dashboard\admin\ClicController@save');
        Route::post('edit_clice', 'dashboard\admin\ClicController@edit');
        Route::get('remove_clice/{id}', 'dashboard\admin\ClicController@remove');


        Route::get('task_types', 'dashboard\admin\TaskTypeController@index');
        Route::post('add_task_type', 'dashboard\admin\TaskTypeController@save');
        Route::post('edit_task_type', 'dashboard\admin\TaskTypeController@edit');
        Route::get('remove_task_type/{id}', 'dashboard\admin\TaskTypeController@remove');


        Route::get('task_edit_types', 'dashboard\admin\TaskEditTypeController@index');
        Route::post('add_task_edit_type', 'dashboard\admin\TaskEditTypeController@save');
        Route::post('edit_task_edit_type', 'dashboard\admin\TaskEditTypeController@edit');
        Route::get('remove_task_edit_type/{id}', 'dashboard\admin\TaskEditTypeController@remove');


        Route::get('nature_projects', 'dashboard\admin\NatureProjectController@index');
        Route::post('add_nature_project', 'dashboard\admin\NatureProjectController@save');
        Route::post('edit_nature_project', 'dashboard\admin\NatureProjectController@edit');
        Route::get('remove_nature_project/{id}', 'dashboard\admin\NatureProjectController@remove');


        Route::get('space_types', 'dashboard\admin\SpaceTypeController@index');
        Route::post('add_space_type', 'dashboard\admin\SpaceTypeController@save');
        Route::post('edit_space_type', 'dashboard\admin\SpaceTypeController@edit');
        Route::get('remove_space_type/{id}', 'dashboard\admin\SpaceTypeController@remove');


        Route::get('study_types', 'dashboard\admin\StudyTypeConroller@index');
        Route::post('add_study_type', 'dashboard\admin\StudyTypeConroller@save');
        Route::post('edit_study_type', 'dashboard\admin\StudyTypeConroller@edit');
        Route::get('remove_study_type/{id}', 'dashboard\admin\StudyTypeConroller@remove');


        Route::get('space_locations', 'dashboard\admin\SpaceLocationController@index');
        Route::post('add_space_location', 'dashboard\admin\SpaceLocationController@save');
        Route::post('edit_space_location', 'dashboard\admin\SpaceLocationController@edit');
        Route::get('remove_space_location/{id}', 'dashboard\admin\SpaceLocationController@remove');


        Route::get('projects', 'dashboard\admin\ProjectController@index');
        Route::get('add_project', 'dashboard\admin\ProjectController@save_view');
        Route::post('add_project', 'dashboard\admin\ProjectController@save');
        Route::post('add_project_ajax', 'dashboard\admin\ProjectController@save_ajax');
        Route::get('edit_project/{id}', 'dashboard\admin\ProjectController@edit_view');
        Route::post('edit_project', 'dashboard\admin\ProjectController@edit');
        Route::post('get_spaces_project', 'dashboard\admin\ProjectController@get_spaces_project');
        Route::get('remove_project/{id}', 'dashboard\admin\ProjectController@remove');

        Route::get('spaces', 'dashboard\admin\SpaceController@index');
        Route::get('add_space', 'dashboard\admin\SpaceController@save_view');
        Route::post('add_space', 'dashboard\admin\SpaceController@save');
        Route::post('add_space_ajax', 'dashboard\admin\SpaceController@save_ajax');
        Route::get('edit_space/{id}', 'dashboard\admin\SpaceController@edit_view');
        Route::post('edit_space', 'dashboard\admin\SpaceController@edit');
        Route::get('remove_space/{id}', 'dashboard\admin\SpaceController@remove');


        Route::get('orders', 'dashboard\admin\OrderController@index');
        Route::get('assign_to_collector', 'dashboard\admin\OrderController@assign_to_collector');
        Route::post('check_validation_order', 'dashboard\admin\OrderController@check_validation_order');
        Route::get('delivered_order', 'dashboard\admin\OrderController@delivered_order');
        Route::post('update_task_date', 'dashboard\admin\OrderController@update_task_date');
        Route::post('get_next_order_code', 'dashboard\admin\OrderController@get_next_order_code');
        Route::post('project_note', 'dashboard\admin\OrderController@project_note');
        Route::get('order_task/{id}', 'dashboard\admin\OrderController@order_task');
        Route::get('add_order', 'dashboard\admin\OrderController@save_view');
        Route::post('add_order', 'dashboard\admin\OrderController@save');
        Route::post('add_order_ajax', 'dashboard\admin\OrderController@save_ajax');
        Route::get('edit_order/{id}', 'dashboard\admin\OrderController@edit_view');
        Route::get('seen_by_technical/{id}', 'dashboard\admin\OrderController@seen_by_technical');
        Route::post('edit_order', 'dashboard\admin\OrderController@edit');
        Route::get('remove_order/{id}', 'dashboard\admin\OrderController@remove');


        Route::get('customers', 'dashboard\admin\CustomerController@index');
        Route::post('add_customer', 'dashboard\admin\CustomerController@save');
        Route::post('add_customer_ajax', 'dashboard\admin\CustomerController@save_ajax');
        Route::post('edit_customer', 'dashboard\admin\CustomerController@edit');
        Route::get('remove_customer/{id}', 'dashboard\admin\CustomerController@remove');

        Route::get('files/{id}', 'dashboard\admin\FileController@index');
        Route::post('add_file', 'dashboard\admin\FileController@save');
        Route::get('remove_file/{id}', 'dashboard\admin\FileController@remove');


        Route::post('get_orders_spaces', 'dashboard\admin\TaskController@get_orders_spaces');
        Route::post('get_tasks_order', 'dashboard\admin\TaskController@get_tasks_order');
        Route::get('tasks', 'dashboard\admin\TaskController@index');
        Route::post('check_order_have_tasks', 'dashboard\admin\TaskController@check_order_have_tasks');
        Route::post('task_comments', 'dashboard\admin\TaskController@task_comments');
        Route::post('tasks_order', 'dashboard\admin\TaskController@tasks_order');
        Route::post('tasks_history', 'dashboard\admin\TaskController@tasks_history');
        Route::post('add_comment', 'dashboard\admin\TaskController@add_comment');
        Route::get('add_have_inquiry/{id}', 'dashboard\admin\TaskController@add_have_inquiry');
        Route::get('add_task', 'dashboard\admin\TaskController@save_view');
        Route::post('add_task', 'dashboard\admin\TaskController@save');
        Route::post('add_task_ajax', 'dashboard\admin\TaskController@add_task_ajax');
        Route::get('edit_task/{id}', 'dashboard\admin\TaskController@edit_view');
        Route::get('technical_supervisor_receive_task/{id}', 'dashboard\admin\TaskController@technical_supervisor_receive_task');
        Route::post('technical_supervisor_receive_task', 'dashboard\admin\TaskController@technical_supervisor_receive_task_post');
        Route::post('edit_task', 'dashboard\admin\TaskController@edit');
        Route::get('remove_task/{id}', 'dashboard\admin\TaskController@remove');


        Route::post('check_validation', 'dashboard\admin\CustomerController@check_validation');
        Route::post('check_validation_project', 'dashboard\admin\ProjectController@check_validation_project');


    });

});


Route::prefix('employee')->group(function () {
    Route::group(['middleware' => ['employee']], function () {

        Route::get('home', 'dashboard\employee\HomeController@index');
        Route::post('add_comment', 'dashboard\employee\HomeController@add_comment');
        Route::get('task_details/{id}', 'dashboard\employee\HomeController@task_details');
        Route::get('finish_task/{id}', 'dashboard\employee\HomeController@finish_task');
        Route::post('set_direction', 'dashboard\employee\HomeController@set_direction');
        Route::get('add_have_inquiry/{id}', 'dashboard\employee\HomeController@add_have_inquiry');

        Route::post('tasks_history', 'dashboard\employee\HomeController@tasks_history');

        Route::post('project_note', 'dashboard\employee\HomeController@project_note');

    });
});

Route::prefix('user_collect')->group(function () {
    Route::group(['middleware' => ['user_collect']], function () {


        Route::post('tasks_history', 'dashboard\user_collect\HomeController@tasks_history');

        Route::get('home', 'dashboard\user_collect\HomeController@index');
        Route::get('order_assigned', 'dashboard\user_collect\HomeController@order_assigned');
        Route::get('tasks/{id}', 'dashboard\user_collect\HomeController@tasks');
        Route::post('edit_task_to_collected', 'dashboard\user_collect\HomeController@edit_task_to_collected');
        Route::post('edit_task_to_collected_back', 'dashboard\user_collect\HomeController@edit_task_to_collected_back');

        Route::post('project_note', 'dashboard\user_collect\HomeController@project_note');
    });
});


